import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './admin-module/admin-layout.component';
import { CoreModule } from './core/core.module';

import { FooterModule } from './shared/components/footer/footer.module';
import { SidebarModule } from './shared/components/sidebar/sidebar.module';
import { NavbarModule } from './shared/components/navbar/navbar.module';
import { AuthGuard } from './shared/guards/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxMaskModule, IConfig } from 'ngx-mask';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    FooterModule,
    SidebarModule,
    NavbarModule,
   
    ToastrModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    LoadingBarHttpClientModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
  ],
  providers: [
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

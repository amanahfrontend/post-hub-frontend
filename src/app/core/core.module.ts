import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FacadeService } from '../services/facade.service';
import { CountryService } from 'app/services/shared/country.service';
import { App } from './app';

const COUNTRIES = 'COUNTRIES';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      defaultLanguage: 'ar',
      isolate: true
    }),
  ],
  providers: [
    CountryService,
    {
      provide: APP_INITIALIZER, useFactory: initCountries,
      deps: [CountryService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
})

export class CoreModule {
  constructor(
    private facadeService: FacadeService) {
    const lng: string = this.facadeService.sharedFacadeService.languageService.currentLanguage || 'ar';
    this.facadeService.sharedFacadeService.languageService.changeLanguage(lng);

    if (this.facadeService.accountService.userInfo) {
      const userLogo: string = `${App.backEndUrl}/${this.facadeService.accountService.userInfo['profilePhotoURL']}`;
      this.facadeService.accountService.changeUserLogo(userLogo);
    }
  }
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function initCountries(countryService: CountryService) {
  return () => {
    countryService.list().subscribe(countries => {
      localStorage.setItem(COUNTRIES, JSON.stringify(countries));
    });
  };
}
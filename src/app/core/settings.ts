/**
 * Setting interface 
 * 
 * @interface Settings
 */

export interface Settings {
  backendUrl: string;
  identityServiceUrl: string;
  orderServiceUrl: string;
  environmentType: EnvironmentType;
  requestTimeout?: number;
  cookieHeader?: boolean;
  allowOffline?: boolean;
}

export enum EnvironmentType {
  Production,
  Development
}
export enum services {
  deliveryService = 0,
    orderService = 1,
    identityService=2

}
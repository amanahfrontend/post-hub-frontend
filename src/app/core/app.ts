import { environment } from '../../environments/environment';

export class App {
    static get backEndUrl(): string {
        return environment.settings.backendUrl;
    }

    static get orderServiceUrl(): string {
        return environment.settings.orderServiceUrl;
    }


    static get identityServiceUrl(): string {
        return environment.settings.identityServiceUrl;
    }
}



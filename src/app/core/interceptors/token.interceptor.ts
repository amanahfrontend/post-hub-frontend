import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { AuthConstants } from '../../shared/constants/auth';
import { TokenResponse } from '../../shared/models/auth/account-result';
import { Settings } from 'app/shared/constants/settings';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private toastr: ToastrService,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = (JSON.parse(localStorage.getItem(AuthConstants.TokenKey)) as TokenResponse);
    const languageCode: string = localStorage.getItem(Settings.languageCode);

    let newHeaders = request.headers;
    if (languageCode) {
      newHeaders = newHeaders.set('Accept-Language', languageCode);
    }

    if (token) {
      newHeaders = newHeaders.set('Authorization', `Bearer ${token.accessToken}`);
    }
    request = request.clone({ headers: newHeaders });

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        // display server errors
        if (err.status == 400) {
          if (typeof err['error'] == 'string') {
            this.toastr.error(`${err['error']}`);
          } else if (typeof err['error']['error'] == 'string') {
            this.toastr.error(`${err['error']['error']}`);
          } else {
            const errorsObject = err['error']['errors'];
            for (let key in errorsObject) {
              if (errorsObject.hasOwnProperty(key)) {
                errorsObject[key].forEach((error: string) => {
                  this.toastr.error(`${error}`);
                });
              }
            }
          }
        }

        if (err.status !== 401) {
          return;
        }

        if (err.status == 401) {
          localStorage.clear();
          this.router.navigate(['auth/login']);
        }
      }
    });
  }
}
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat/Observable';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  static readonly endpoint = 'ServiceSector';

  constructor(private http: HttpClientService) { }

  /**
   * list all sectors
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${SectorService.endpoint}/GetAll`);
  }
}

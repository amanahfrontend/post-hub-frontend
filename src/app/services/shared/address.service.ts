import { Injectable } from '@angular/core';
import { Address, AddressStreet, Option } from '../../shared/models/components/Address';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClientService) { }

  /**
   * get countries
   *
   *
   * @param id
   */
  get countries(): Observable<Option[]> {
    return this.http.get(`Address/governorates`);
  }

  /**
   * get governorates
   *
   *
   * @param id
   */
  get governorates(): Observable<Option[]> {
    return this.http.get(`Address/governorates`);
  }

  /**
   * get areas
   *
   *
   * @param id
   */
  get areas(): Observable<Option[]> {
    return this.http.get(`Address/GetAllAreas`);
  }

  areasByGovernorate(governorate: string): Observable<Option[]> {
    return this.http.get(`Address/governorates/${governorate}/areas`);
  }

  /**
   * get blocks via area id
   *
   *
   * @param area
   */
  getBlocksByArea(area: string): Observable<Option[]> {
    return this.http.get(`Address/get-blocks-from-paci?areaId=${area}`);
  }

  /**
   * get streets via governates / area / block name
   *
   *
   * @param id
   */
  getStreetsByBlock(body: AddressStreet): Observable<Option[]> {
    return this.http.get(`Address/get-streets-from-paci`, body);
  }

  /**
   * address using google or paci
   *
   *
   * @param id
   */
  getAddress(body: Address) {
    return this.http.get(`Address/search-by-components`, body);
  }

  /**
   * address using paci
   *
   *
   * @param id
   */
  getAddressViaPACI(paciNumber: number) {
    return this.http.get(`Address/paci`, { paciNumber: paciNumber });
  }
}

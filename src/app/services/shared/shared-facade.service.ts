import { Injectable, Injector } from '@angular/core';
import { AccountStatusService } from './account-status.service';
import { BusinessTypesService } from './business-types.service';
import { CollectionMethodsService } from './collection-methods.service';
import { ContactFunctionsService } from './contact-functions.service';
import { CountryService } from './country.service';
import { CourtesyService } from './courtesy.service';
import { GenderService } from './gender.service';
import { LanguageService } from './language.service';
import { SectorService } from './sector.service';
import { SidebarService } from './sidebar.service';

@Injectable({
  providedIn: 'root'
})
export class SharedFacadeService {

  /**
   * 
   * @param inject 
   */
  constructor(private inject: Injector) { }

  get courtesyService(): CourtesyService {
    return this.inject.get(CourtesyService);
  }

  get languageService(): LanguageService {
    return this.inject.get(LanguageService);
  }

  get accountStatusService(): AccountStatusService {
    return this.inject.get(AccountStatusService);
  }

  get sidebarService(): SidebarService {
    return this.inject.get(SidebarService);
  }

  get sectorService(): SectorService {
    return this.inject.get(SectorService);
  }

  get genderService(): GenderService {
    return this.inject.get(GenderService);
  }

  get countryService(): CountryService {
    return this.inject.get(CountryService);
  }

  get contactFunctionsService(): ContactFunctionsService {
    return this.inject.get(ContactFunctionsService);
  }

  get collectionMethodsService(): CollectionMethodsService {
    return this.inject.get(CollectionMethodsService);
  }

  get businessTypesService(): BusinessTypesService {
    return this.inject.get(BusinessTypesService);
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat/Observable';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  static readonly endpoint = 'Department';
  constructor(private http: HttpClientService) { }

  /**
   * list all departments
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${DepartmentService.endpoint}/GetAll`);
  }

}

import { Injectable } from '@angular/core';
import { Country } from '../../shared/models/components/country';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  static readonly endpoint = 'Country';

  constructor(private http: HttpClientService) { }


  /**
   * get country details
   *
   *
   * @param id
   */
  getcountry(id: number) {
    return this.http.get(`${CountryService.endpoint}/Details/${id}`);
  }

  /**
   * list all countries
   *
   *
   */
  list(): Observable<Country[]> {
    return this.http.get(`${CountryService.endpoint}/GetAll`);
  }

  /**
   * create new country
   *
   *
   * @param body
   */
  create(body: Object) {
 
    return this.http.post(body, `${CountryService.endpoint}/Create`);
  }

  /**
   * update country
   *
   *
   * @param countryId
   */
  update(body: Object) {
    return this.http.put(body, `${CountryService.endpoint}`);
  }

  /**
   * delete country
   *
   *
   * @param countryId
   */
  delete(countryId: number) {
    return this.http.delete(`${CountryService.endpoint}/Delete/${countryId}`);
  }
  
  listActive() {
 
    let res = this.http.get(`${CountryService.endpoint}/GetAllActive`)
    return res;
  }
  listPaging(body: any): Observable<Country[]> {
    return this.http.post<Country[]>(body, `${CountryService.endpoint}/GetAllByPagination`);
  }
  
  ActiveInActiveGovernorate(model: Object): Observable<Country> {
    return this.http.post(model, `${CountryService.endpoint}/ActiveInActive`);
  }
}

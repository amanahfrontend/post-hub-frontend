import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class AccountStatusService {

  static readonly endpoint = 'AccountStatus';

  constructor(private http: HttpClientService) { }

  /**
   * list all status
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${AccountStatusService.endpoint}/GetAll`);
  }
}

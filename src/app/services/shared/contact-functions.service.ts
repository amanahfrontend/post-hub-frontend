import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactFunctionsService {

    static readonly endpoint = 'ContactFunction';
  constructor(private http: HttpClientService) { }

  /**
   * list all contact functions
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${ContactFunctionsService.endpoint}/GetAll`);
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class CollectionMethodsService {

  static readonly endpoint = 'CollectionMethod';
  constructor(private http: HttpClientService) { }

  /**
   * list all collection types
   *
   *
   */
  list(): Observable<{ id: number, name_en?: string, name_ar?: string }[]> {
    return this.http.get(`${CollectionMethodsService.endpoint}/GetAll`);
  }
}

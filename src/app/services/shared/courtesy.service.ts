import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourtesyService {

  static readonly endpoint = 'Courtesy';
  constructor(private http: HttpClientService) { }

  /**
   * list all Courtesies
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${CourtesyService.endpoint}/GetAll`);
  }
}

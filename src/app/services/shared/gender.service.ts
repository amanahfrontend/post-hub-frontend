import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenderService {


  static readonly endpoint = 'Gender';

  constructor(private http: HttpClientService) { }

  /**
   * list all genders
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${GenderService.endpoint}/GetAll`);
  }
}

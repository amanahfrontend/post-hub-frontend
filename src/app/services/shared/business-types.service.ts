import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BusinessTypesService {

  static readonly endpoint = 'BusinessType';
  constructor(private http: HttpClientService) { }

  /**
   * list all Business Types
   *
   *
   */
  list(): Observable<{ id: number, name?: string, nameAr?: string }[]> {
    return this.http.get(`${BusinessTypesService.endpoint}/GetAll`);
  }
  GetAllByBusinessCustomerId(businessCustomerId:number): Observable<{ id: number, name_en?: string, name_ar?: string }[]> {
    return this.http.get(`${BusinessTypesService.endpoint}/GetAllByBusinessCustomerId/${businessCustomerId}`);
  }
}

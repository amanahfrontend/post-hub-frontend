import { TestBed } from '@angular/core/testing';

import { CollectionMethodsService } from './collection-methods.service';

describe('CollectionMethodsService', () => {
  let service: CollectionMethodsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollectionMethodsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

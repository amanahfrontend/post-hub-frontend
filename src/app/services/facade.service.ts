import { Injectable, Injector } from '@angular/core';
import { SidebarService } from './shared/sidebar.service';
import { AddressService } from './shared/address.service';
import { AccountService } from './auth/account.service';
import { AuthenticatedService } from './auth/authenticated.service';
import { DepartmentService } from './shared/department.service';
import { SharedFacadeService } from './shared/shared-facade.service';
import { AdminFacadeService } from './admin/admin-facade.service';

@Injectable({
  providedIn: 'root'
})

export class FacadeService {

  constructor(
    private inject: Injector,
  ) { }

  get sharedFacadeService(): SharedFacadeService {
    return this.inject.get(SharedFacadeService);
  }

  get adminFacadeService(): AdminFacadeService {
    return this.inject.get(AdminFacadeService);
  }

  get sidebarService(): SidebarService {
    return this.inject.get(SidebarService);
  }

  get addressService(): AddressService {
    return this.inject.get(AddressService);
  }

  get accountService(): AccountService {
    return this.inject.get(AccountService);
  }

  get authenticatedService(): AuthenticatedService {
    return this.inject.get(AuthenticatedService);
  }

  get departmentService(): DepartmentService {
    return this.inject.get(DepartmentService);
  }
}

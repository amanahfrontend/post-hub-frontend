import { TestBed } from '@angular/core/testing';

import { GeoFencesService } from './geo-fences.service';

describe('GeoFencesService', () => {
  let service: GeoFencesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeoFencesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Manager } from '../../shared/models/admin/manager';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';
import { services } from 'app/core/settings';
import { RFQ } from 'app/shared/models/admin/PriceList/RFQ';
import { RFQList } from '@app/shared/models/admin/rfq-list';
import { RFQItemsByRFQ } from '@app/shared/models/admin/rfq-items-by-rfq';
import { PL } from '@app/shared/models/admin/PriceList/PL';
import { RFQItems } from '@app/shared/models/admin/RFQItems';

@Injectable({
  providedIn: 'root'
})
export class ManagePriceListService {

  private static readonly endpoint = 'RFQ';
  private static readonly endpointCustomer = 'Customers';
  private static readonly endpointCountry = 'Country';
  private static readonly endpointCurrency = 'Currency';
  private static readonly endpointPriceQuotation = 'PriceQuotationStatus';
  private static readonly endpointPriceList = 'PriceList';
  private static readonly endpointPriceListSt = 'PriceListStatus';
  private static readonly endpointServiceSector = 'ServiceSector';


  constructor(private http: HttpClientService) { }

  /**
   * PQ details
   *
   *
   * @param id
   */
  get(id: number) {
    return this.http.get<any>(`${ManagePriceListService.endpoint}/GetById/${id}`, undefined, services.orderService);
  }
  getInternationalPostItemsByPQId(id: number) {
    return this.http.get<any>(`${ManagePriceListService.endpoint}/GetInternationalPostItemsByPQId/${id}`, undefined, services.orderService);

  }

  /**
   * all managers
   * 
   * 
   * @returns 
   */
  list() {
    return this.http.get(`${ManagePriceListService.endpoint}/GetAll`);
  }

  /**
   * Paginationed Managers list
   *
   *
   * @param body
   */

  listByPagination(body: any): Observable<any[]> {
    return this.http.post<RFQ[]>(body, `${ManagePriceListService.endpoint}/GetAllPagginated`, undefined, services.orderService);
  }

  /**
   * Add new RFQ
   *
   *
   * @param model
   */
  create(model: Object): Observable<RFQ> {
    return this.http.post(model, `${ManagePriceListService.endpoint}/Create`, undefined, services.orderService);
  }

  update(model: object): Observable<any> {
    return this.http.put(model, `${ManagePriceListService.endpoint}/Update`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${ManagePriceListService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
  GetAllCustomersAsync() {
    return this.http.get(`${ManagePriceListService.endpointCustomer}/GetAll`)

  }
  GetAllCustomerNamesAsync() {
    return this.http.get(`${ManagePriceListService.endpointCustomer}/GetAllCustomerNamesAsync`)
  }
  GetAllCountiesAsync() {
    return this.http.get(`${ManagePriceListService.endpointCountry}/GetAll`)
  }
  GetAllCurrenciesAsync() {
    return this.http.get(`${ManagePriceListService.endpointCurrency}/GetAll`, undefined, services.orderService)
  }
  GetAllPQStatuesAsync() {
    return this.http.get(`${ManagePriceListService.endpointPriceQuotation}/GetAll`, undefined, services.orderService)
  }

  /**
   * PriceList
   * @param body 
   * @returns 
   */
  listPLByPagination(body: any): Observable<any[]> {
    return this.http.post<PL[]>(body, `${ManagePriceListService.endpointPriceList}/GetAllPagginated`, undefined, services.orderService);
  }
  GetAllPLStatuesAsync() {
    return this.http.get(`${ManagePriceListService.endpointPriceListSt}/GetAll`, undefined, services.orderService)
  }
  GetAllServiceSectorsAsync() {
    return this.http.get(`${ManagePriceListService.endpointServiceSector}/GetAll`, undefined, services.orderService)
  }
  createPL(model: Object): Observable<PL> {
    return this.http.post(model, `${ManagePriceListService.endpointPriceList}/Create`, undefined, services.orderService);
  }

  updatePL(model: object): Observable<any> {
    return this.http.put(model, `${ManagePriceListService.endpointPriceList}/Update`, services.orderService);
  }

  deletePL(id: number) {
    return this.http.delete<boolean>(`${ManagePriceListService.endpointPriceList}/Delete/${id}`, undefined, services.orderService);
  }
  getPLDetails(id: number) {
    return this.http.get<any>(`${ManagePriceListService.endpointPriceList}/GetById/${id}`, undefined, services.orderService);
  }
  GetFilteredPLPagginated(body: any): Observable<PL[]> {
    return this.http.post<PL[]>(body, `${ManagePriceListService.endpointPriceList}/GetFilteredPLPagginated`, undefined, services.orderService);
  }
  /**
   * to excel
   * 
   * 
   * @param body 
   * @returns 
   */
  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${ManagePriceListService.endpointPriceList}/ExportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
  }
  getActiveCurrencies() {

    return this.http.get(`${ManagePriceListService.endpointCurrency}/GetAllActive`, undefined, services.orderService)

  }
  rfqList() {
    return this.http.get<RFQList[]>(`${ManagePriceListService.endpoint}/GetRFQList`, undefined, services.orderService);
  }
  getRFQItemsByRFQId(id: number) {
    return this.http.get<RFQItemsByRFQ[]>(`${ManagePriceListService.endpoint}/GetRFQItemsByRFQId/${id}`, undefined, services.orderService);

  }
  getRFQItemsByBusinessCustomerId(businessCustomerId: number) {
    return this.http.get<RFQItems[]>(`${ManagePriceListService.endpoint}/GetRFQItemsByBusinessCustomerId/${businessCustomerId}`, undefined, services.orderService);

  }
}


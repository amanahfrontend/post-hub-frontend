import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { Observable } from 'rxjs';
import { BusinessCustomerContact } from '../../shared/models/admin/business-customer';

@Injectable({
  providedIn: 'root'
})
export class BusinessCustomerContactService {

  private static readonly endpoint = "BusinessCustomerContact";
  constructor(private http: HttpClientService) { }

  create(model: any): Observable<BusinessCustomerContact> {
    return this.http.postFormData(model, `${BusinessCustomerContactService.endpoint}/Create`);
  }

  update(model: BusinessCustomerContact): Observable<BusinessCustomerContact> {
    return this.http.putFormData(model, `${BusinessCustomerContactService.endpoint}/Update`);
  }

  list(businessCustomerId: number): Observable<BusinessCustomerContact[]> {
    return this.http.get<BusinessCustomerContact[]>(`${BusinessCustomerContactService.endpoint}/GetAllByBusinessCustomer?businessCustomerId=${businessCustomerId}`);
  }


  listByBusinssCustomerPagination(body: object): Observable<BusinessCustomerContact[]> {
    return this.http.post<BusinessCustomerContact[]>(body, `${BusinessCustomerContactService.endpoint}/GetAllByBusinssCustomerPagination`);
  }

  /**
   * delete contact 
   * 
   *  
   * @param contactId 
   * @returns 
   */
  delete(contactId: number) {
    return this.http.delete<boolean>(`${BusinessCustomerContactService.endpoint}/Delete/${contactId}`);
  }
}

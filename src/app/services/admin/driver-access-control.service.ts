import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { AccessControl, AccessControlGroup } from '../../shared/models/admin/access-control';

@Injectable({
  providedIn: 'root'
})
export class DriverAccessControlService {
  private static readonly endpoint = 'AgentAccessControl';

  role: AccessControl;

  constructor(private http: HttpClientService) { }

  /**
   * get agent access control details
   *
   *
   * @param roleName
   */
  getRole(RoleName: string) {
    return this.http.get(`${DriverAccessControlService.endpoint}/Get`, { 'roleName': RoleName });
  }

  /**
   * list all agent access control permissions
   *
   *
   * @param body
   * */
  listAllPermissions(): Observable<AccessControlGroup[]> {
    return this.http.get(`${DriverAccessControlService.endpoint}/GetAllPermissions`);
  }
  /**
    * list all agent access control
    *
    *
    */
  list() {
    return this.http.get(`${DriverAccessControlService.endpoint}/GetAll`);
  }

  /**
   * create new agent access control
   *
   *
   */
  create(role) {
    return this.http.post(role, `${DriverAccessControlService.endpoint}/Create`);
  }

  /**
   * update agent access control
   *
   *
   */
  update(role) {
    return this.http.put(role, `${DriverAccessControlService.endpoint}/Update`);
  }

  /**
   * delete agent access control
   *
   *
   * @param roleName
   */
  delete(RoleName: string) {
    return this.http.delete(`${DriverAccessControlService.endpoint}/Delete/${RoleName}`);
  }
}

import { TestBed } from '@angular/core/testing';

import { MailItemTypeService } from './mail-item-type.service';

describe('MailItemTypeService', () => {
  let service: MailItemTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MailItemTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Manager } from '../../shared/models/admin/manager';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageStaffService {

  private static readonly endpoint = 'Manager';
  constructor(private http: HttpClientService) { }

  /**
   * Manager details
   *
   *
   * @param id
   */
  get(id: number) {
    return this.http.get<Manager>(`${ManageStaffService.endpoint}/Details/${id}`);
  }

  /**
   * all managers
   * 
   * 
   * @returns 
   */
  list() {
    return this.http.get(`${ManageStaffService.endpoint}/GetAll`);
  }

  /**
   * Paginationed Managers list
   *
   *
   * @param body
   */
  listByPagination(body: any): Observable<Manager[]> {
    return this.http.post<Manager[]>(body, `${ManageStaffService.endpoint}/GetPaginationBy`);
  }

  /**
   * Add new manager
   *
   *
   * @param model
   */
  create(model: Object): Observable<Manager> {
    return this.http.postFormData(model, `${ManageStaffService.endpoint}/Create/`);
  }

  /**
   * Update current manager
   *
   *
   * @param managerId
   * @returns {Observable}
   */
  update(model: Object): Observable<Manager> {
    return this.http.postFormData<Manager>(model, `${ManageStaffService.endpoint}/Update`);
  }

  /**
   * Delete manager
   *
   *
   * @param managerId
   */
  delete(managerId: number) {
    return this.http.delete<boolean>(`${ManageStaffService.endpoint}/Delete/${managerId}`);
  }

  /**
   * to excel
   * 
   * 
   * @param body 
   * @returns 
   */
  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${ManageStaffService.endpoint}/ExportToExcel`, { responseType: 'blob' }, body);
  }
}

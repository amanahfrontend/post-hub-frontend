import { TestBed } from '@angular/core/testing';

import { ManagerRolesService } from './manager-roles.service';

describe('ManagerRolesService', () => {
  let service: ManagerRolesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagerRolesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

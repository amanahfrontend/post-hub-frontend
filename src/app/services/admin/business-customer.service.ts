import { Injectable } from '@angular/core';
import { services } from 'app/core/settings';
import { BusinessOrderLoad } from 'app/shared/models/admin/business-order-load';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { BusinessInfo } from '../../shared/models/admin/business-customer';

@Injectable({
  providedIn: 'root'
})
export class BusinessCustomerService {

  private static readonly endpoint = "BusinessCustomer";
  constructor(private http: HttpClientService) { }

  /**
   * update current business customer
   * 
   * 
   * @param model 
   * @returns 
   */
  update(model: BusinessInfo): Observable<BusinessInfo> {
    return this.http.putFormData(model, `${BusinessCustomerService.endpoint}/Update`);
  }

  /**
 * create business customer
 * 
 * 
 * @param model 
 * @returns 
 */
  create(model: any): Observable<BusinessInfo> {
    return this.http.postFormData(model, `${BusinessCustomerService.endpoint}/Create`);
  }

  /**
   * Paginationed Business list
   *
   *
   * @param body
   */
  listByPagination(body: any): Observable<BusinessInfo[]> {
    return this.http.post<BusinessInfo[]>(body, `${BusinessCustomerService.endpoint}/GetAllByPagination`);
  }


  list() {

    return this.http.get<BusinessInfo[]>(`${BusinessCustomerService.endpoint}/GetAll`)

  }
  listNew() {
    return this.http.get(`${BusinessCustomerService.endpoint}/GetAll`)
  }

  getById(id: number) {

    return this.http.get<BusinessInfo>(`${BusinessCustomerService.endpoint}/Details/${id}`);

  }
  getAllByBussinesTypeId(id: number) {

    return this.http.get<BusinessInfo[]>(`${BusinessCustomerService.endpoint}/GetAllByBussinesTypeId/${id}`);

  }


  /**
   * business customer details
   * 
   * 
   * @param id 
   * @returns 
   */
  getBusinessCustomerDetails(id: number): Observable<any> {
    return this.http.get(`${BusinessCustomerService.endpoint}/Details/${id}`);
  }

  /**
   * to excel
   * 
   * 
   * @param body 
   * @returns 
   */
  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${BusinessCustomerService.endpoint}/ExportToExcel`, { responseType: 'blob' }, body);
  }

  getBussinessCutomerCount() {

    return this.http.get(`${BusinessCustomerService.endpoint}/GetBussinessCutomerCount`);

  }
}

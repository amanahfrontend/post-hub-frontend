import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { Language } from 'app/shared/models/admin/language';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageLaguageService {

  private static readonly endpoint = 'Language';
  constructor(private http: HttpClientService) { }


  get(id: number) {
    return this.http.get<Language>(`${ManageLaguageService.endpoint}/GetById/${id}`, undefined, services.orderService);
  }

  list() {
    return this.http.get(`${ManageLaguageService.endpoint}/GetAll`, undefined, services.orderService)
  }

  listActive(body: any): Observable<Language[]> {
    return this.http.post<Language[]>(body, `${ManageLaguageService.endpoint}/GetAllActive`, undefined, services.orderService);
  }

  create(model: Object): Observable<Language> {
    return this.http.post(model, `${ManageLaguageService.endpoint}/Create`, undefined, services.orderService);
  }

  ActiveInActiveLanguage(model: Object): Observable<Language> {
    return this.http.post(model, `${ManageLaguageService.endpoint}/ActiveInActiveLanguage/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${ManageLaguageService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(LanguageId: number) {
    return this.http.delete<boolean>(`${ManageLaguageService.endpoint}/Delete/${LanguageId}`, undefined, services.orderService);
  }
}

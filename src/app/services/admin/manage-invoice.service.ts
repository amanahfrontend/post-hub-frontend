import { Injectable } from '@angular/core';
import { Manager } from '../../shared/models/admin/manager';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';
import { services } from 'app/core/settings';
import { invoice } from '@app/shared/models/admin/invoice';


@Injectable({
  providedIn: 'root'
})
export class ManageInvoicesService {

  private static readonly endpoint = 'Invoice';
  private static readonly contractEndpoint = 'Contract';
  private static readonly reportEndpoint = 'Reports';  
  
  constructor(private http: HttpClientService) { }

  /**
   * invoice details
   *
   *
   * @param id
   */
   get(id: number) {
    return this.http.get<any>(`${ManageInvoicesService.endpoint}/GetById/${id}`, undefined, services.orderService);
  }
  
  /**
   * all invoices
   * 
   * 
   * @returns 
   */
  list() {
    return this.http.get(`${ManageInvoicesService.endpoint}/GetAll`);
  }

  /**
   * Paginationed Managers list
   *
   *
   * @param body
   */
 
  listByPagination(body: any): Observable<any[]> {
    return this.http.post<invoice[]>(body, `${ManageInvoicesService.endpoint}/GetAllPagginated`, undefined, services.orderService);
  }
   /**
  * @param id
   */
   getContractsByCustId(id: number) {
    return this.http.get<any>(`${ManageInvoicesService.contractEndpoint}/GetContractsByBusinessCustomerId/${id}`, undefined, services.orderService);
  }

  /**
   * Add new invoice
   *
   *
   * @param model
   */
   create(model: Object): Observable<invoice> {
    return this.http.post(model, `${ManageInvoicesService.endpoint}/Create`, undefined, services.orderService);
  }

  update(model: object): Observable<any> {
    return this.http.put(model, `${ManageInvoicesService.endpoint}/Update`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${ManageInvoicesService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
 
    GetSearchedInvoicesPagginated(body: any): Observable<invoice[]> {
      return this.http.post<invoice[]>(body, `${ManageInvoicesService.endpoint}/GetSearchedInvoicesPagginated`, undefined, services.orderService);
    }

    GetBusinessCustomerDistributionCountReport(body: any): Observable<any[]> {
      return this.http.post<invoice[]>(body, `${ManageInvoicesService.reportEndpoint}/GetBusinessCustomerDistributionCountReport`, undefined, services.orderService);
    }
    GetUnitPriceByBusinessCustomerIdAndContractCode(body: any): Observable<any[]> {
      return this.http.post<number[]>(body, `${ManageInvoicesService.contractEndpoint}/GetUnitPriceByBusinessCustomerIdAndContractCode`, undefined, services.orderService);
    }
    
  /**
   * to excel
   * 
   * 
   * @param body 
   * @returns 
   */
   exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${ManageInvoicesService.endpoint}/ExportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
  }

  getInvoicesCount() {
    return this.http.get(`${ManageInvoicesService.endpoint}/GetInvoicesCount`, undefined, services.orderService);
  }

}


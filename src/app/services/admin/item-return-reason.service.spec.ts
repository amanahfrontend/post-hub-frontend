import { TestBed } from '@angular/core/testing';

import { ItemReturnReasonService } from './item-return-reason.service';

describe('ItemReturnReasonService', () => {
  let service: ItemReturnReasonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemReturnReasonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

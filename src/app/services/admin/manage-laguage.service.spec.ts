import { TestBed } from '@angular/core/testing';

import { ManageLaguageService } from './manage-laguage.service';

describe('LaguageServiceService', () => {
  let service: ManageLaguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageLaguageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

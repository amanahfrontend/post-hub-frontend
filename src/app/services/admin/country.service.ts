
import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { Country } from 'app/shared/models/admin/country';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryAddService {

  private static readonly endpoint = 'Country';
  constructor(private http: HttpClientService) { }
  
  get(id: number) {
    
   return this.http.get<Country>(`${CountryAddService.endpoint}/Details/${id}`);
   
  }
 
  list() {
    
    return  this.http.get(`${CountryAddService.endpoint}/GetAll`)
  
  }
  listActive() {
    
    return this.http.get(`${CountryAddService.endpoint}/GetAllActive`)
   
  }
  listPaging(body: any): Observable<Country[]> {
    return this.http.post<Country[]>(body, `${CountryAddService.endpoint}/GetAllByPagination`);
  }
  
  create(model: Object): Observable<Country> {
    return this.http.post(model, `${CountryAddService.endpoint}/Create`);
  }
  
  ActiveInActiveGovernorate(model: Object): Observable<Country> {
    return this.http.post(model, `${CountryAddService.endpoint}/ActiveInActive`);
  }

  update( model: object): Observable<any> {
    return this.http.put(model, `${CountryAddService.endpoint}/Update`);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${CountryAddService.endpoint}/Delete/${id}`);
  }
}

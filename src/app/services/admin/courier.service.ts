

import { Injectable } from '@angular/core';
import { Courier } from '@app/shared/models/admin/courier';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourierService {

  private static readonly endpoint = 'Courier';
  constructor(private http: HttpClientService) { }


  get(id: number) {
    return this.http.get<Courier>(`${CourierService.endpoint}/GetById/${id}`, undefined, services.orderService);
  }

  list() {
    return this.http.get(`${CourierService.endpoint}/GetAll`, undefined, services.orderService)
  }

  listActive(): Observable<Courier[]> {
    
    return this.http.get<Courier[]>(`${CourierService.endpoint}/GetAllActive`, undefined, services.orderService)

  }

  create(model: Object): Observable<Courier> {
    return this.http.post(model, `${CourierService.endpoint}/Create`, undefined, services.orderService);
  }

  ActiveInActiveCourier(model: Object): Observable<Courier> {
    return this.http.post(model, `${CourierService.endpoint}/ActiveInActiveCourier/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${CourierService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${CourierService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
}

import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class TransportTypesService {

  private static readonly endpoint = 'TransportType';

  constructor(private http: HttpClientService) { }


  /**
   * get Transport Type details
   *
   *
   * @param id
   */
  getTransportype(id: number) {
    return this.http.get(`${TransportTypesService.endpoint}/Details/${id}`);
  }

  /**
   * list all Transport Types
   *
   *
   */
  list() {
    return this.http.get(`${TransportTypesService.endpoint}/GetAll`);
  }
}

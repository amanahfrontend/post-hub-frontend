import { TestBed } from '@angular/core/testing';
import { ManagePriceListService } from './manage-price-list.service';


describe('ManagePriceListService', () => {
  let service: ManagePriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagePriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

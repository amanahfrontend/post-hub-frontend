import { TestBed } from '@angular/core/testing';

import { BusinessCustomerBranchService } from './business-customer-branch.service';

describe('BusinessCustomerBranchService', () => {
  let service: BusinessCustomerBranchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessCustomerBranchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

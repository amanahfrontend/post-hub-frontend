import { TestBed } from '@angular/core/testing';

import { CourierZoneService } from './courier-zone.service';

describe('CourierZoneService', () => {
  let service: CourierZoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourierZoneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

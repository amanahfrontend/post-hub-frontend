import { TestBed } from '@angular/core/testing';

import { DriverAccessControlService } from './driver-access-control.service';

describe('DriverAccessControlService', () => {
  let service: DriverAccessControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DriverAccessControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';
import { services } from '../../core/settings';
import { AreaForDispatch, BlockForDispatch } from '../../shared/models/admin/Order';
import { Area } from '@app/shared/models/admin/area';
import { AreaBlock } from '@app/shared/models/admin/area-block';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private static readonly endpoint = 'Area';

  /**
  * 
  * @param http 
  */
  constructor(private http: HttpClientService) { }

  areasForDispatch(body: { pageNumber: number, pageSize: number }): Observable<AreaForDispatch[]> {
    return this.http.post(body, `${AreaService.endpoint}/GetAreasForDispatch`, undefined, services.orderService)
  }

  blocksByAreaId(areaId: number): Observable<BlockForDispatch[]> {
    return this.http.get(`${AreaService.endpoint}/GetBlocksByAreaIdForDispatch/${areaId}`, undefined, services.orderService)
  }
 
   listAreas(body:object) {
     
     return this.http.post<Area[]>(body,`${AreaService.endpoint}/GetAllAreasPaging`, undefined, services.orderService)
    
   }
   listActiveAreas() {
     
    return this.http.get<Area[]>(`${AreaService.endpoint}/GetAllActive`, undefined, services.orderService)
    
   }
 
   createArea(model: Object): Observable<Area> {
     return this.http.post(model, `${AreaService.endpoint}/CreateArea`, undefined, services.orderService);
   }
  
   updateArea( model: object): Observable<any> {
     return this.http.put(model, `${AreaService.endpoint}/UpdateArea`, services.orderService);
   }
  
   deleteArea(model: object) {
     return this.http.post(model,`${AreaService.endpoint}/DeleteArea`, undefined, services.orderService);
   }
   getAllBlocksByAreaId(areaId:number){
    return this.http.get<AreaBlock[]>(`${AreaService.endpoint}/GetAllBlocksByAreaId/${areaId}`, undefined, services.orderService)

  }
}

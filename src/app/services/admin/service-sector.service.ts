import { Injectable } from '@angular/core';
import { services } from '@app/core/settings';
import { ServiceSector } from '@app/shared/models/admin/service-sector';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceSectorService {
  private static readonly endpoint = 'ServiceSector';

  constructor(private http: HttpClientService) { }

  /**
   * list teams 
   *
   *
   */
   getAll() {
    return this.http.get<ServiceSector[]>(`${ServiceSectorService.endpoint}/GetAll`, undefined, services.orderService);
  }
  getAllActive() {
    return this.http.get<ServiceSector[]>(`${ServiceSectorService.endpoint}/GetAllActive`, undefined, services.orderService);
  }
}
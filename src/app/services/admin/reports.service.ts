

import { Injectable } from '@angular/core';
import { LocalMessageDistributionReport } from '@app/shared/models/admin/local-message-distribution-report';
import { BusinessCustomerAndBranches } from '@app/shared/models/business-customer-and-branches';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private static readonly endpoint = 'Reports';
  constructor(private http: HttpClientService) { }

 
  getLocalMessageDistributionReport(body: any): Observable<LocalMessageDistributionReport[]> {
    return this.http.post<LocalMessageDistributionReport[]>(body, `${ReportsService.endpoint}/GetLocalMessageDistributionReport`, undefined, services.orderService);
  }
  exportLocalMessageToExcel() {
    return this.http.getForDownloadFile<Blob>(`${ReportsService.endpoint}/ExportLocalMessageDistributionReportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
  }
  getBusinessCustomerAndItsBranches(body: any){
    return this.http.post<BusinessCustomerAndBranches[]>(body, `${ReportsService.endpoint}/GetBusinessCustomerAndItsBranches`, undefined, services.orderService);

  }
  getBusinessCustomerDistributionReport(body: any){
    return this.http.post<BusinessCustomerAndBranches[]>(body, `${ReportsService.endpoint}/GetBusinessCustomerDistributionReport`, undefined, services.orderService);

  }
  getBusinessCustomerDistributionForExportReport(body: any)
  {
   // return this.http.getForDownloadFile<Blob>(`${ReportsService.endpoint}/GetBusinessCustomerDistributionForExportReport`, { responseType: 'blob' }, undefined, services.orderService);
    return this.http.getForDownloadFile<Blob>(`${ReportsService.endpoint}/GetBusinessCustomerDistributionForExportReport`, { responseType: 'blob' }, body, services.orderService);
  }
}

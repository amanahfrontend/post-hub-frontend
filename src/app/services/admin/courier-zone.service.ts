import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { CourierZone } from 'app/shared/models/admin/CourierZone';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourierZoneService {

  private static readonly endpoint = 'CourierZone';
  constructor(private http: HttpClientService) { }

  get(id: number) {
    
   return this.http.get<CourierZone>(`${CourierZoneService.endpoint}/GetById/${id}`, undefined, services.orderService);
   
  }

  list() {
    
    return this.http.get(`${CourierZoneService.endpoint}/GetAll`, undefined, services.orderService)
   
  }
  listActive() {
    
   return this.http.get(`${CourierZoneService.endpoint}/GetAllActive`, undefined, services.orderService)
   
  }

  create(model: Object): Observable<CourierZone> {
    return this.http.post(model, `${CourierZoneService.endpoint}/Create`, undefined, services.orderService);
  }
 
  ActiveInActiveLanguage(model: Object): Observable<CourierZone> {
    return this.http.post(model, `${CourierZoneService.endpoint}/ActiveInActiveCourierZone`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${CourierZoneService.endpoint}/Update?id=${id}`, services.orderService);
  }
 
  delete(LanguageId: number) {
    return this.http.delete<boolean>(`${CourierZoneService.endpoint}/Delete/${LanguageId}`, undefined, services.orderService);
  }
}

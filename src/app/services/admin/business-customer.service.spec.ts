import { TestBed } from '@angular/core/testing';

import { BusinessCustomerService } from './business-customer.service';

describe('BusinessCustomerService', () => {
  let service: BusinessCustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessCustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

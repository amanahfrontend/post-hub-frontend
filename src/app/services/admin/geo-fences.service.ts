import { Injectable } from '@angular/core';
import { GeoFence } from '../../shared/models/admin/geo-fence';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class GeoFencesService {

  private static readonly endpoint = 'GeoFence';
  constructor(private http: HttpClientService) { }

  /**
   * Geo Fence details
   * 
   * 
   * @param id 
   */
  get(id: number) {
    return this.http.get<GeoFence>(`${GeoFencesService.endpoint}/Details/${id}`);
  }

  /**
   * list all Geo Fences
   * 
   * 
   */
  list() {
    return this.http.get<GeoFence[]>(`${GeoFencesService.endpoint}/GetAll`);
  }
}

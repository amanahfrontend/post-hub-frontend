
import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { LengthUOM } from 'app/shared/models/admin/length-uom';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LengthUOMService {

  private static readonly endpoint = 'LengthUOM';
  constructor(private http: HttpClientService) { }

  get(id: number) {

    return this.http.get<LengthUOM>(`${LengthUOMService.endpoint}/GetById/${id}`, undefined, services.orderService);

  }

  list() {

    return this.http.get(`${LengthUOMService.endpoint}/GetAll`, undefined, services.orderService)
  }

  listActive(body: any): Observable<LengthUOM[]> {
    return this.http.post<LengthUOM[]>(body, `${LengthUOMService.endpoint}/GetAllActive`, undefined, services.orderService);
  }

  create(model: Object): Observable<LengthUOM> {
    return this.http.post(model, `${LengthUOMService.endpoint}/Create`, undefined, services.orderService);
  }

  ActiveInActiveLengthUOM(model: Object): Observable<LengthUOM> {
    return this.http.post(model, `${LengthUOMService.endpoint}/ActiveInActiveLengthUOM/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${LengthUOMService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${LengthUOMService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
}

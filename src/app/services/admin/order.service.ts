import { Injectable } from '@angular/core';
import { OrderForDispatch } from 'app/shared/models/admin/Order';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { services } from '../../core/settings';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private static readonly endpoint = 'Order';
  private static readonly OrderStatusLogendpoint ='OrderStatusLog';

  /**
   * 
   * @param http 
   */
  constructor(private http: HttpClientService) { }

  orderForDispatch(body: { pageNumber: number, pageSize: number }): Observable<OrderForDispatch[]> {
    return this.http.post(body, `${OrderService.endpoint}/GetOrderForDispatch`, undefined, services.orderService)
  }
   //OrderStatusLogs
  
   geOrderStatusLogsByOrderCode(orderCode: string) {
    return this.http.get<any>(`${OrderService.OrderStatusLogendpoint}/GeOrderStatusLogsByOrderCode/${orderCode}`, undefined, services.orderService)
  }
}

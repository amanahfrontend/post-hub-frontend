import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { services } from '../../core/settings';
import { OrderListModel } from '../../shared/models/admin/Orders/order-list';

@Injectable({
    providedIn: 'root'
})
export class ManageOrderService {
    private static readonly endpoint = 'Order';
    private static readonly endpointWorkorder = 'Workorder';

    constructor(private http: HttpClientService) { }

    /**
     * Paginationed Managers list
     *
     *
     * @param body
     */
    listByPagination(body: any): Observable<OrderListModel[]> {
        return this.http.post<OrderListModel[]>(body, `${ManageOrderService.endpoint}/GetAllPagginated`, undefined, services.orderService);
    }

    delete(id: number) {
        return this.http.delete<boolean>(`${ManageOrderService.endpoint}/Delete/${id}`, undefined, services.orderService);
    }

    listWorkordersByPagination(body: any): Observable<any[]> {
        return this.http.post<any[]>(body, `${ManageOrderService.endpointWorkorder}/WorkorderForDispatch`, undefined, services.orderService);
    }
    exportToExcel(body: object) {
        return this.http.getForDownloadFile<Blob>(`${ManageOrderService.endpoint}/ExportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
      }
}

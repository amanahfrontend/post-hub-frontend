
import { Injectable } from '@angular/core';
import { ContractForEdit } from '@app/shared/models/admin/contract-for-edit';
import { ContractStatus } from '@app/shared/models/admin/contract-status';
import { ContractType } from '@app/shared/models/admin/contract-type';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { Contract } from 'app/shared/models/admin/contract';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  private static readonly endpoint = 'Contract';
  constructor(private http: HttpClientService) { }

  get(contractId: number) {
    
   return this.http.get<Contract>(`${ContractService.endpoint}/GetContractStatusById/${contractId}`, undefined, services.orderService);
    
  }

  list() {
    
    return this.http.get<Contract[]>(`${ContractService.endpoint}/GetAll`, undefined, services.orderService)
    
  }

    listByPagination(body: any): Observable<any[]> {
        return this.http.post<any[]>(body, `${ContractService.endpoint}/GetAllPagginated`, undefined, services.orderService);
    }


    exportToExcel(body: object) {
        return this.http.getForDownloadFile<Blob>(`${ContractService.endpoint}/ExportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
    }

    getContractTypes(){
      return this.http.get<ContractType[]>( `${ContractService.endpoint}/GetContractTypes`, undefined, services.orderService);

    }

    getContractStatuss(){
      return this.http.get<ContractStatus[]>(`${ContractService.endpoint}/GetContractStatuss`, undefined, services.orderService);
    }
   
    delete(model: object) {
      return this.http.post(model,`${ContractService.endpoint}/Delete`, undefined, services.orderService);
    }
    update(model: object){
      return this.http.post(model,`${ContractService.endpoint}/Update`, undefined, services.orderService);

    }
    getContractById(id:number){
      return this.http.get<ContractForEdit>( `${ContractService.endpoint}/GetContractById/${id}`, undefined, services.orderService);

    }
    create(model: object){
      return this.http.post(model,`${ContractService.endpoint}/Create`, undefined, services.orderService);

    }
    changeContractStatus(model: object){
      return this.http.post(model,`${ContractService.endpoint}/ChangeContractStatus`, undefined, services.orderService);
    }

    PrintContract(contractId: number) {

        return this.http.getForDownloadFile<Blob>(`${ContractService.endpoint}/PrintContract/${contractId}`, { responseType: 'blob' }, undefined, services.orderService);

    }

    SendToCustomer(contractId: number) {

        return this.http.getForDownloadFile<Blob>(`${ContractService.endpoint}/SendContractToCustomer/${contractId}`, { responseType: 'blob' }, undefined, services.orderService);

    }

}

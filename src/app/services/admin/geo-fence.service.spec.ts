import { TestBed } from '@angular/core/testing';

import { GeoFenceService } from './geo-fence.service';

describe('GeoFenceService', () => {
  let service: GeoFenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeoFenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

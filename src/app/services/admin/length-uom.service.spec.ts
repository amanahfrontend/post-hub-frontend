import { TestBed } from '@angular/core/testing';

import { LengthUOMService } from './length-uom.service';

describe('LengthUOMService', () => {
  let service: LengthUOMService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LengthUOMService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { BusinessCustomerContactService } from './business-customer-contact.service';

describe('BusinessCustomerContactService', () => {
  let service: BusinessCustomerContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessCustomerContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});



import { Injectable } from '@angular/core';
import { MailItem } from '@app/shared/models/admin/MailITem';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MailItemService {

  private static readonly endpoint = 'MailItem';
  private static readonly endpointStatusLog = 'MailItemStatusLog';
  constructor(private http: HttpClientService) { }

  get(id: number) {
    return this.http.get<any>(`${MailItemService.endpoint}/GetById/${id}`, undefined, services.orderService);

  }
  getDeliveredMailItemById(id: number) {
    return this.http.get<any>(`${MailItemService.endpoint}/GetDeliveredMailItemById/${id}`, undefined, services.orderService);

  }


  list() {
    let res = this.http.get(`${MailItemService.endpoint}/GetAll`, undefined, services.orderService)
    return res;
  }
  GetDeliveredMailItemsPagginated(body: any): Observable<MailItem[]> {
    return this.http.post<MailItem[]>(body, `${MailItemService.endpoint}/GetDeliveredMailItemsPagginated`, undefined, services.orderService);
  }
  getReturnedMailItemsPagginated(body: any): Observable<MailItem[]> {
    return this.http.post<MailItem[]>(body, `${MailItemService.endpoint}/GetReturnedMailItemsPagginated`, undefined, services.orderService);
  }

  create(model: Object): Observable<any> {
    return this.http.post(model, `${MailItemService.endpoint}/Create`, undefined, services.orderService);
  }

  getByOrderId(orderId: number) {
    let res = this.http.get<any>(`${MailItemService.endpoint}/GetByOrderId/${orderId}`, undefined, services.orderService)
    return res;
  }

  getByOrderIdForDispatch(orderId: number) {
    return this.http.post<any>({}, `${MailItemService.endpoint}/GetOrderForDispatch/${orderId}`, undefined, services.orderService)
  }

  getByAreaId(areaId: number) {
    return this.http.get<any>(`${MailItemService.endpoint}/GetAreasForDispatch/${areaId}`, undefined, services.orderService)
  }

  getByItemsOrderId(orderId: number) {
    return this.http.get<any>(`${MailItemService.endpoint}/GetMailItemsByOrderIdForDispatch/${orderId}`, undefined, services.orderService)
  }

  itemsByBlockId(body: { areaId: number, blockId: number }): Observable<any> {
    return this.http.post(body, `${MailItemService.endpoint}/GetMailItemsByBlockForDispatchQuery`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${MailItemService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${MailItemService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }

  // getDrivesrForMailItemDispatch() {
  //   return this.http.get<any>(`${MailItemService.endpoint}/GetDrivesrForMailItemDispatch`, undefined, services.orderService)
  // }

  getMailItemsByAssignedDriverForDispatch(body: { driverId: number }): Observable<any> {
    return this.http.post(body, `${MailItemService.endpoint}/GetMailItemsByAssignedDriverForDispatch`, undefined, services.orderService);
  }

  getDrivesrForMailItemDispatch(body: { pageNumber: number, pageSize: number }): Observable<any> {
    return this.http.post(body, `${MailItemService.endpoint}/GetDrivesrForMailItemDispatch`, undefined, services.orderService);
  }
  //MailItemStatusLogs

  geMailItemStatusLogByItemCode(itemCode: string) {
    return this.http.get<any>(`${MailItemService.endpointStatusLog}/GeMailItemStatusLogByItemCode/${itemCode}`, undefined, services.orderService)
  }

  getMailItemCountByItemStatus() {
    return this.http.get<any>(`${MailItemService.endpoint}/GetMailItemCountByItemStatus`, undefined, services.orderService)

  }
}


import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { workOrderStatus } from 'app/shared/models/admin/work-order-status';
import { workOrder } from 'app/shared/models/admin/workOrder';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageWorkOrderService {

  private static readonly endpoint = 'Workorder';
  constructor(private http: HttpClientService) { }


  get(id: number) {
    return this.http.get<any>(`${ManageWorkOrderService.endpoint}/GetById/${id}`, undefined, services.orderService);
  }

  listByPagination(body: any): Observable<any[]> {
    return this.http.post<workOrder[]>(body, `${ManageWorkOrderService.endpoint}/GetAllPagginated`, undefined, services.orderService);
  }

  GetAllWorkOrderStatuss() {
    return this.http.get<workOrderStatus[]>(`${ManageWorkOrderService.endpoint}/GetAllWorkOrderStatuss`, undefined, services.orderService);

  }

  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${ManageWorkOrderService.endpoint}/ExportToExcel`, { responseType: 'blob' }, undefined, services.orderService);
  }

  create(model: Object): Observable<workOrder> {
    return this.http.post(model, `${ManageWorkOrderService.endpoint}/Create`, undefined, services.orderService);
  }

  update(model: object): Observable<any> {
    return this.http.put(model, `${ManageWorkOrderService.endpoint}/Update`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${ManageWorkOrderService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }

  workorderFormOrders(body: { driverId: number, ordersId: number[], mailItemsId: number[] }): Observable<any[]> {
    return this.http.post(body, `${ManageWorkOrderService.endpoint}/WoFromOrders`, undefined, services.orderService);
  }

  workorderFormAreaBlock(body: { driverId: number, areasId: number[], blocksId: number[], mailItemsId: number[] }): Observable<any[]> {
    return this.http.post(body, `${ManageWorkOrderService.endpoint}/WoFromAreaBlock`, undefined, services.orderService);
  }

  assignDelgateToWorkOrder(body: any): Observable<any[]> {
    return this.http.post<workOrder[]>(body, `${ManageWorkOrderService.endpoint}/AssignDriverToWorkOrder`, undefined, services.orderService);
  }

  dispatchDelgateToWorkOrder(body: any): Observable<any[]> {
    return this.http.post<workOrder[]>(body, `${ManageWorkOrderService.endpoint}/DispatchDriverToWorkOrder`, undefined, services.orderService);
  }

  changeWorkOrderStatus(body: any): Observable<any[]> {
    return this.http.post<workOrder[]>(body, `${ManageWorkOrderService.endpoint}/ChangeWorkOrderStatus`, undefined, services.orderService);
  }

  autoAssignWorkOrders(body: any): Observable<any[]> {
    return this.http.post<workOrder[]>(body, `${ManageWorkOrderService.endpoint}/AutoAssignWorkOrders`, undefined, services.orderService);
  }
}

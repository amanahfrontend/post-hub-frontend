import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  private static readonly endpoint = 'Teams';

  constructor(private http: HttpClientService) { }

  /**
   * list teams 
   *
   *
   */
  teams() {
    return this.http.get(`${TeamsService.endpoint}/GetAll`);
  }
}

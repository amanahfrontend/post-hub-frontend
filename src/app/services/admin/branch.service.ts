import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  private static readonly endpoint = 'Branch';

  /**
   * 
   * @param http 
   */
  constructor(private http: HttpClientService) { }

  /**
   * list all system branches
   * 
   * 
   * @returns 
   */
  list(): Observable<any> {
    return this.http.get(`${BranchService.endpoint}/GetAll`);
  }

  /**
   * company branches
   * 
   * 
   * @param body 
   * @returns 
   */
  listByPagination(body: object): Observable<any> {
    return this.http.post(body, `${BranchService.endpoint}/GetAllByCompanyPagination`);
  }

  /**
   * add new company branch
   * 
   * 
   * @param branch 
   * @returns 
   */
  create(branch: object): Observable<any> {
    return this.http.post(branch, `${BranchService.endpoint}/Create`);
  }

  /**
   * update current company branch
   * 
   * 
   * @param branch 
   * @returns 
   */
  update(branch: object): Observable<any> {
    return this.http.put(branch, `${BranchService.endpoint}/Update`);
  }

  /**
   * delete selected branch
   * 
   * 
   * @param branchId 
   * @returns 
   */
  delete(branchId: number): Observable<any> {
    return this.http.delete(`${BranchService.endpoint}/Delete/${branchId}`);
  }

  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${BranchService.endpoint}/ExportToExcel`, { responseType: 'blob' }, body);
  }
}

import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { GeoFence } from '../../shared/models/admin/geo-fence';

@Injectable({
  providedIn: 'root'
})
export class GeoFenceService {

  private static readonly endpoint = 'GeoFence';
  constructor(private http: HttpClientService) { }

  /**
   * get geoFence details
   * 
   * 
   * @param id 
   */
  get(id: number) {
    return this.http.get<GeoFence>(`${GeoFenceService.endpoint}/Details/${id}`);
  }

  /**
   * list all geoFences
   * 
   * 
   */
  list() {
    return this.http.get<GeoFence[]>(`${GeoFenceService.endpoint}/GetAll`);
  }

  /**
   * create new geoFence
   * 
   * 
   * @param model 
   */
  create(model: GeoFence) {
    return this.http.post<GeoFence>(model, `${GeoFenceService.endpoint}/Create`);
  }

  /**
   * update geoFence
   * 
   * 
   * @param geoFenceId 
   */
  update(model: GeoFence) {
    return this.http.put(model, `${GeoFenceService.endpoint}/Update`);
  }

  /**
   * delete geoFence
   * 
   * 
   * @param geoFenceId 
   */
  delete(geoFenceId: number) {
    return this.http.delete(`${GeoFenceService.endpoint}/Delete/${geoFenceId}`);
  }

  /**
   * Export To Excel 
   * 
   */
  exportToExcel() {
    return this.http.getForDownloadFile<Blob>(`${GeoFenceService.endpoint}/ExportToExcel`, { responseType: 'blob' });
  }
}

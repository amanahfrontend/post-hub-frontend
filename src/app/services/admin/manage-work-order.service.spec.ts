import { TestBed } from '@angular/core/testing';

import { ManageWorkOrderService } from './manage-work-order.service';

describe('ManageWorkOrderService', () => {
  let service: ManageWorkOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageWorkOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

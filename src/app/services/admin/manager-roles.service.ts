import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ManagerRolesService {

  private static readonly endpoint = 'ManagerAccessControl';

  constructor(private http: HttpClientService) { }

  /**
   * Role details
   *
   *
   * @param roleName
   */
  getRole(RoleName: string) {
    return this.http.get(`${ManagerRolesService.endpoint}/Get`, { 'roleName': RoleName });
  }

  list() {
    return this.http.get(`${ManagerRolesService.endpoint}/GetAll`);
  }

  /**
   * Roles permissions
   *
   *
   * @param body
   * */
  listAllPermissions() {
    return this.http.get(`${ManagerRolesService.endpoint}/GetAllPermissions`);
  }

  /**
    * Roles
    *
    *
    */
  roles() {
    return this.http.get(`${ManagerRolesService.endpoint}/GetAll`);
  }

  /**
   * Add 
   *
   *
   */
  create(role) {
    return this.http.post(role, `${ManagerRolesService.endpoint}/Create`);
  }

  /**
   * Update  Role
   *
   *
   */
  update(role) {
    return this.http.put(role, `${ManagerRolesService.endpoint}/Update`);
  }

  /**
   * Delete  role
   *
   *
   * @param roleName
   */
  delete(RoleName: string) {
    return this.http.delete(`${ManagerRolesService.endpoint}/Delete/${RoleName}`);
  }
}

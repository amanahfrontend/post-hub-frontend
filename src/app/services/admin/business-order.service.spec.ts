import { TestBed } from '@angular/core/testing';

import { BusinessOrderService } from './business-order.service';

describe('BusinessOrderService', () => {
  let service: BusinessOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

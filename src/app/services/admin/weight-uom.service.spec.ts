import { TestBed } from '@angular/core/testing';

import { WeightUOMService } from './weight-uom.service';

describe('WeightUOMService', () => {
  let service: WeightUOMService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeightUOMService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

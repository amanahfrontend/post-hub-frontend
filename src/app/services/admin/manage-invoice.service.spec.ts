import { TestBed } from '@angular/core/testing';
import { ManageInvoicesService } from './manage-invoice.service';



describe('ManageInvoicesService', () => {
  let service: ManageInvoicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManageInvoicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

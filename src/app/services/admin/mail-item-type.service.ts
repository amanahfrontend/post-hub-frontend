import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { MailItemType } from 'app/shared/models/admin/MailItemType';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MailItemTypeService {


  private static readonly endpoint = 'MailItemType';
  constructor(private http: HttpClientService) { }


  get(id: number) {

    return this.http.get<MailItemType>(`${MailItemTypeService.endpoint}/GetById/${id}`, undefined, services.orderService);

  }


  list() {

    return this.http.get(`${MailItemTypeService.endpoint}/GetAll`, undefined, services.orderService)

  }


  listActive(): Observable<MailItemType[]> {

    return this.http.get<MailItemType[]>(`${MailItemTypeService.endpoint}/GetAllActive`, undefined, services.orderService)

  }


  create(model: Object): Observable<MailItemType> {
    return this.http.post(model, `${MailItemTypeService.endpoint}/Create`, undefined, services.orderService);
  }

  ActiveInActiveLanguage(model: Object): Observable<MailItemType> {
    return this.http.post(model, `${MailItemTypeService.endpoint}/ActiveInActiveMailItemType/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${MailItemTypeService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(MailItemTypeId: number) {
    return this.http.delete<boolean>(`${MailItemTypeService.endpoint}/Delete/${MailItemTypeId}`, undefined, services.orderService);
  }
}

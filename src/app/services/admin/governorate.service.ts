
import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { CreateGovernorate } from 'app/shared/models/admin/createGovernorate';
import { Governorate } from 'app/shared/models/admin/governorate';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GovernorateService {

  private static readonly endpoint = 'Governorate';
  constructor(private http: HttpClientService) { }


  get(id: number) {
    return this.http.get<Governorate>(`${GovernorateService.endpoint}/Details/${id}`);
  }


  list() {

    return this.http.get(`${GovernorateService.endpoint}/GetAll`)

  }

  
  listPaging(body: any): Observable<Governorate[]> {
    return this.http.post<Governorate[]>(body, `${GovernorateService.endpoint}/GetAllByPagination`);
  }


  create(model: Object): Observable<CreateGovernorate> {
    return this.http.post(model, `${GovernorateService.endpoint}/Create`);
  }

  ActiveInActiveGovernorate(model: Object): Observable<Governorate> {
    return this.http.post(model, `${GovernorateService.endpoint}/ActiveInActive`);
  }


  update(id, model: object): Observable<any> {
    return this.http.put(model, `${GovernorateService.endpoint}/Update`);
  }

  delete(id: string) {
    return this.http.delete<boolean>(`${GovernorateService.endpoint}/Delete/${id}`);
  }
}

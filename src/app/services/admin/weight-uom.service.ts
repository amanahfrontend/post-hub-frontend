
import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { WeightUOM } from 'app/shared/models/admin/weight-uom';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WeightUOMService {
 

  private static readonly endpoint = 'WeightUOM';
  constructor(private http: HttpClientService) { }

 
  get(id: number) {
    
    return  this.http.get<WeightUOM>(`${WeightUOMService.endpoint}/GetById/${id}`, undefined, services.orderService);
  
  }


  list() {
    
    return this.http.get(`${WeightUOMService.endpoint}/GetAll`, undefined, services.orderService)
   
  }


  listActive(): Observable<WeightUOM[]> {
    return this.http.get<WeightUOM[]>(`${WeightUOMService.endpoint}/GetAllActive`, undefined, services.orderService)
  }

  
  create(model: Object): Observable<WeightUOM> {
    return this.http.post(model, `${WeightUOMService.endpoint}/Create`, undefined, services.orderService);
  }
  
  ActiveInActiveWeightUOM(model: Object): Observable<WeightUOM> {
    return this.http.post(model, `${WeightUOMService.endpoint}/ActiveInActiveWeightUOM/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${WeightUOMService.endpoint}/Update?id=${id}`, services.orderService);
  }
 
  delete(id: number) {
    return this.http.delete<boolean>(`${WeightUOMService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
}

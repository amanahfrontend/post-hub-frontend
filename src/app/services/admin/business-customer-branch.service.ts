import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { BusinessCustomerBranch } from '../../shared/models/admin/business-customer';

@Injectable({
  providedIn: 'root'
})
export class BusinessCustomerBranchService {

  private static readonly endpoint = "BusinessCustomerBranch";
  constructor(private http: HttpClientService) { }

  /**
   * create branch 
   * 
   * 
   * @param model 
   * @returns 
   */
  create(model: any): Observable<BusinessCustomerBranch> {
    return this.http.post(model, `${BusinessCustomerBranchService.endpoint}/Create`);
  }

  /**
   * update branch 
   * 
   * 
   * @param model 
   * @returns 
   */
  update(model: BusinessCustomerBranch): Observable<BusinessCustomerBranch> {
    return this.http.put(model, `${BusinessCustomerBranchService.endpoint}/Update`);
  }

  /**
   * get all branches
   * 
   * 
   * @returns 
   */
  list(businessCustomerId: number): Observable<BusinessCustomerBranch[]> {
    return this.http.get<BusinessCustomerBranch[]>(`${BusinessCustomerBranchService.endpoint}/GetAllByBusinessCustomer?businessCustomerId=${businessCustomerId}`);
  }

  /**
   * delete branch 
   * 
   *  
   * @param branchId 
   * @returns 
   */
  delete(branchId: number) {
    return this.http.delete<boolean>(`${BusinessCustomerBranchService.endpoint}/Delete/${branchId}`);
  }
}

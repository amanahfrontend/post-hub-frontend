

import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { ItemReturnReason } from 'app/shared/models/admin/itemReturnReason';
import { LengthUOM } from 'app/shared/models/admin/length-uom';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ItemReturnReasonService {

  private static readonly endpoint = 'ItemReturnReason';
  constructor(private http: HttpClientService) { }

  get(id: number) {

    return this.http.get<ItemReturnReason>(`${ItemReturnReasonService.endpoint}/GetById/${id}`, undefined, services.orderService);

  }

  list() {

    return this.http.get(`${ItemReturnReasonService.endpoint}/GetAll`, undefined, services.orderService)

  }

  listActive(body: any): Observable<ItemReturnReason[]> {
    return this.http.post<ItemReturnReason[]>(body, `${ItemReturnReasonService.endpoint}/ActiveInActiveItemReturnReason`, undefined, services.orderService);
  }

  create(model: Object): Observable<ItemReturnReason> {
    return this.http.post(model, `${ItemReturnReasonService.endpoint}/Create`, undefined, services.orderService);
  }

  ActiveInActiveLengthUOM(model: Object): Observable<ItemReturnReason> {
    return this.http.post(model, `${ItemReturnReasonService.endpoint}/ActiveInActiveLengthUOM/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.put(model, `${ItemReturnReasonService.endpoint}/Update?id=${id}`, services.orderService);
  }

  delete(id: number) {
    return this.http.delete<boolean>(`${ItemReturnReasonService.endpoint}/Delete/${id}`, undefined, services.orderService);
  }
}

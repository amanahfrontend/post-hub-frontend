import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../core/http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private static readonly endpoint = 'Company';
  constructor(private http: HttpClientService) { }

  /**
   * current company details
   * 
   * 
   */
  get currentCompanyProfile(): Observable<any> {
    return this.http.get(`${CompanyService.endpoint}/getCurrentCompanyProfile`);
  }

  /**
   * company details
   * 
   * 
   */
  getCompanyProfile(id: number): Observable<any> {
    return this.http.get(`${CompanyService.endpoint}/Details​/${id}`);
  }

  /**
   * update company data
   * 
   * 
   * @param branch 
   * @returns 
   */
  update(company: object): Observable<any> {
    return this.http.putFormData(company, `${CompanyService.endpoint}`);
  }
}

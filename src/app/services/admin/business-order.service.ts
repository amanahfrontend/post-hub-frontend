import { Injectable } from '@angular/core';
import { HttpClientService } from 'app/core/http-client/http-client.service';
import { services } from 'app/core/settings';
import { BusinessOrderLoad } from 'app/shared/models/admin/business-order-load';
import { BusinessOrder } from 'app/shared/models/admin/businessOrder';
import { CustomerName } from 'app/shared/models/admin/customerName';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BusinessOrderService {

  private static readonly endpoint = 'BusinessOrder';
  private static readonly endpointCustomer = 'Customers';

  constructor(private http: HttpClientService) { }


  get(id: number) {

    return this.http.get<BusinessOrder>(`${BusinessOrderService.endpoint}/GetById/${id}`, undefined, services.orderService);

  }


  listActive(body: any): Observable<BusinessOrder[]> {
    return this.http.post<BusinessOrder[]>(body, `${BusinessOrderService.endpoint}/GetAllActive`, undefined, services.orderService);
  }

  create(model: Object): Observable<BusinessOrder> {
    return this.http.postFormData(model, `${BusinessOrderService.endpoint}/Create`, undefined, services.orderService);
  }
  createBusinessOrderDraft(model: Object): Observable<BusinessOrder> {
    return this.http.postFormData(model, `${BusinessOrderService.endpoint}/CreateBusinessOrderDraft`, undefined, services.orderService);
  }
  getAllDraftBusinessOrder(){
    return this.http.get<BusinessOrderLoad[]>(`${BusinessOrderService.endpoint}/GetAllDraftBusinessOrder`,undefined,services.orderService)
   
   }
  ActiveInActiveLanguage(model: Object): Observable<BusinessOrder> {
    return this.http.post(model, `${BusinessOrderService.endpoint}/ActiveInActiveLanguage/`, undefined, services.orderService);
  }

  update(id, model: object): Observable<any> {
    return this.http.postFormData(model, `${BusinessOrderService.endpoint}/Update`, undefined, services.orderService);
  }

  delete(LanguageId: number) {
    return this.http.delete<boolean>(`${BusinessOrderService.endpoint}/Delete/${LanguageId}`, undefined, services.orderService);
  }

  GetOrderByCustomerId(customerId: number) {
    return this.http.get<BusinessOrder>(`${BusinessOrderService.endpoint}/GetOrderByCustomerId/${customerId}`, undefined, services.orderService);
  }

  GetAllCustomerNamesAsync(customerId: number) {
  return this.http.get(`${BusinessOrderService.endpointCustomer}/GetAllCustomerNamesAsync`)
   
  }

  getBusinessOrderCount() {
    return this.http.get(`${BusinessOrderService.endpoint}/GetBusinessOrderCount`, undefined, services.orderService);
  }

  getDailyOrders() {
    return this.http.get(`${BusinessOrderService.endpoint}/GetDailyOrders`, undefined, services.orderService);
  }
  
  getClosedOrders() {
    return this.http.get(`${BusinessOrderService.endpoint}/GetClosedOrders`, undefined, services.orderService);
  }
}

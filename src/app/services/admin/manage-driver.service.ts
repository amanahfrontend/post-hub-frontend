import { Injectable } from '@angular/core';
import { Driver } from '../../shared/models/admin/driver';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageDriverService {

  private static readonly endpoint = 'Driver';
  constructor(private http: HttpClientService) { }

  /**
   * Driver details
   *
   *
   * @param id
   */
  get(id: number) {
    return this.http.get<Driver>(`${ManageDriverService.endpoint}/Details/${id}`);
  }

  /**
   * All Drivers
   * 
   * 
   * @returns 
   */
  list() {
    return this.http.get<Driver[]>(`${ManageDriverService.endpoint}/GetAll`);
  }

  /**
   * Paginationed drivers list
   *
   *
   * @param body
   */
  listByPagination(body: any): Observable<Driver[]> {
    return this.http.post<Driver[]>(body, `${ManageDriverService.endpoint}/GetAllByPagination`);
  }

  /**
   * Add new manager
   *
   *
   * @param model
   */
  create(model: Object): Observable<Driver> {
    return this.http.postFormData(model, `${ManageDriverService.endpoint}/Create/`);
  }

  /**
   * Update current driver
   *
   *
   * @param driverId
   * @returns {Observable}
   */
  update(model: Object): Observable<Driver> {
    return this.http.postFormData<Driver>(model, `${ManageDriverService.endpoint}/Update`);
  }

  /**
   * Delete Driver
   *
   *
   * @param driverId
   */
  delete(driverId: number) {
    return this.http.delete<boolean>(`${ManageDriverService.endpoint}/Delete/${driverId}`);
  }

  /**
   * to excel
   * 
   * 
   * @param body 
   * @returns 
   */
  exportToExcel(body: object) {
    return this.http.getForDownloadFile<Blob>(`${ManageDriverService.endpoint}/ExportToExcel`, { responseType: 'blob' }, body);
  }
  getDriversCount() {
    return this.http.get(`${ManageDriverService.endpoint}/GetDriversCount`);
  }
}

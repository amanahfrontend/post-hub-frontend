import { Injectable, Injector } from '@angular/core';
import { UserService } from '../auth/user.service';
import { CountryService } from '../shared/country.service';
import { AreaService } from './area.service';
import { BranchService } from './branch.service';
import { BusinessCustomerBranchService } from './business-customer-branch.service';
import { BusinessCustomerContactService } from './business-customer-contact.service';
import { BusinessCustomerService } from './business-customer.service';
import { BusinessOrderService } from './business-order.service';
import { CompanyService } from './company.service';
import { ContractService } from './contract.service';
import { CountryAddService } from './country.service';
import { CourierZoneService } from './courier-zone.service';
import { CourierService } from './courier.service';
import { DriverAccessControlService } from './driver-access-control.service';
import { GeoFenceService } from './geo-fence.service';
import { GeoFencesService } from './geo-fences.service';
import { GovernorateService } from './governorate.service';
import { ItemReturnReasonService } from './item-return-reason.service';
import { LengthUOMService } from './length-uom.service';
import { MailItemTypeService } from './mail-item-type.service';
import { MailItemService } from './mail-item.service';
import { ManageDriverService } from './manage-driver.service';
import { ManageInvoicesService } from './manage-invoice.service';
import { ManageLaguageService } from './manage-laguage.service';
import { ManageOrderService } from './manage-order.service';
import { ManagePriceListService } from './manage-price-list.service';
import { ManageStaffService } from './manage-staff.service';
import { ManageWorkOrderService } from './manage-work-order.service';
import { ManagerRolesService } from './manager-roles.service';
import { OrderService } from './order.service';
import { ProfileService } from './Profile/profile.service';
import { ReportsService } from './reports.service';
import { ServiceSectorService } from './service-sector.service';
import { TeamsService } from './teams.service';
import { TransportTypesService } from './transport-types.service';
import { WeightUOMService } from './weight-uom.service';

@Injectable({
  providedIn: 'root'
})
export class AdminFacadeService {

  /**
   * 
   * @param inject 
   */
  constructor(private inject: Injector) { }

  get countryService(): CountryService {
    return this.inject.get(CountryService);
  }

  get companyService(): CompanyService {
    return this.inject.get(CompanyService);
  }

  get branchService(): BranchService {
    return this.inject.get(BranchService);
  }

  get geoFencesService(): GeoFencesService {
    return this.inject.get(GeoFencesService);
  }

  get manageDriverService(): ManageDriverService {
    return this.inject.get(ManageDriverService);
  }
 
  get manageStaffService(): ManageStaffService {    
    return this.inject.get(ManageStaffService);
  }
  get managePriceListService(): ManagePriceListService {    
    return this.inject.get(ManagePriceListService);
  }
  get ManageInvoicesService(): ManageInvoicesService {    
    return this.inject.get(ManageInvoicesService);
  }
  

  get managerRolesService(): ManagerRolesService {
    return this.inject.get(ManagerRolesService);
  }

  get teamsService(): TeamsService {
    return this.inject.get(TeamsService);
  }

  get transportTypesService(): TransportTypesService {
    return this.inject.get(TransportTypesService);
  }

  get businessCustomerService(): BusinessCustomerService {
    return this.inject.get(BusinessCustomerService);
  }

  get businessCustomerBranchService(): BusinessCustomerBranchService {
    return this.inject.get(BusinessCustomerBranchService);
  }

  get businessCustomerContactService(): BusinessCustomerContactService {
    return this.inject.get(BusinessCustomerContactService);
  }

  get manageLaguageService(): ManageLaguageService {
    return this.inject.get(ManageLaguageService);
  }

  get geoFenceService(): GeoFenceService {
    return this.inject.get(GeoFenceService);
  }

  get mailItemTypeService(): MailItemTypeService {
    return this.inject.get(MailItemTypeService);
  }

  get weightUOMService(): WeightUOMService {
    return this.inject.get(WeightUOMService);
  }

  get lengthUOMService(): LengthUOMService {
    return this.inject.get(LengthUOMService);
  }
  get businessOrderService(): BusinessOrderService {
    return this.inject.get(BusinessOrderService);
  }
  get contractService(): ContractService {
    return this.inject.get(ContractService);
  }

  get governorateService(): GovernorateService {
    return this.inject.get(GovernorateService);
  }
  get courierZoneService(): CourierZoneService {
    return this.inject.get(CourierZoneService);
  }
  get CountryAddService(): CountryAddService {
    return this.inject.get(CountryAddService);
  }


  get profileService(): ProfileService {
    return this.inject.get(ProfileService);
  }

  get userService(): UserService {
    return this.inject.get(UserService);
  }

  get driverAccessControlService(): DriverAccessControlService {
    return this.inject.get(DriverAccessControlService);
  }

  get itemReturnReasonService(): ItemReturnReasonService {
    return this.inject.get(ItemReturnReasonService);
  }

  get manageOrderService(): ManageOrderService {
    return this.inject.get(ManageOrderService);
  }

  get manageWorkOrderService(): ManageWorkOrderService {
    return this.inject.get(ManageWorkOrderService);
  }

  get mailItemService(): MailItemService {
    return this.inject.get(MailItemService);
  }

  get orderService(): OrderService {
    return this.inject.get(OrderService);
  }

  get areaService(): AreaService {
    return this.inject.get(AreaService);
  }
  get reportsService():ReportsService{
    return this.inject.get(ReportsService);
  }
  get serviceSectorService():ServiceSectorService{
    return this.inject.get(ServiceSectorService);
  }

  get courierService():CourierService{
    return this.inject.get(CourierService);
  }
  
}

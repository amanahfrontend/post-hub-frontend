import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../../core/http-client/http-client.service';
import { services } from '../../../core/settings';
import { ChangeLanguage } from '../../../shared/models/admin/profile/ChangeLanguage';
import { changeLanguageResult } from '../../../shared/models/admin/profile/changeLanguageResult';
import { changePassword } from '../../../shared/models/admin/profile/changePassword';
import { changePasswordResult } from '../../../shared/models/admin/profile/changePasswordResult';
import { DeactivateAccount } from '../../../shared/models/admin/profile/DeactivateAccount';
import { DeativationResult } from '../../../shared/models/admin/profile/DeativationResult';
import { Profile } from '../../../shared/models/admin/profile/Profile';
import { ProfileResult } from '../../../shared/models/admin/profile/ProfileResult';

const endpoint = 'User'

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

    constructor(private http: HttpClientService) { }


    /**
     * get LoggedIn Admin  details
     *
     */
    GetCurrentAdmin(): Observable<Profile> {
        return this.http.get<Profile>(`${endpoint}/GetUserAdminInfo/`, null, services.identityService);
    }

    
    /**
     * Update User info
     * 
     * 
     * @param data 
     */
    UpdateUserInfo(data: Profile): Observable<ProfileResult> {
        return this.http.postFormData(data, `${endpoint}/UpdateProfile/`);
    }

    /**
     * Change Passsword of User
     * 
     * 
     */
    changePassword(data: changePassword): Observable<changePasswordResult> {
        return this.http.post(data, `${endpoint}/ChangePassword/`,null, services.identityService);
    }

    /**
     * Change the User Language
     * 
     * 
     * @param data 
     */
    changeLanguage(data: ChangeLanguage): Observable<changeLanguageResult> {
        return this.http.post(data, `${endpoint}/UpdateLanguage/`);
    }


    changeDefaultBranch(data: any): Observable<changeLanguageResult> {
        return this.http.post(data, `${endpoint}/UpdateDefaultBranch/`);
    }

    /**
     * Deactivate Account
     * 
     * 
     * @param data 
     */
    deleteAccount(data: DeactivateAccount): Observable<DeativationResult> {
        return this.http.post(data, `${endpoint}/DeleteAccount/o`);
    }

}

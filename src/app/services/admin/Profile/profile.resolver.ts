import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Profile } from '../../../shared/models/admin/profile/Profile';
import { AdminFacadeService } from '../admin-facade.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileResolver implements Resolve<Profile> {

    constructor(private adminFacadeService: AdminFacadeService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Profile> {
        return this.adminFacadeService.profileService.GetCurrentAdmin();
  }
}

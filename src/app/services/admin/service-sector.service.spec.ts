import { TestBed } from '@angular/core/testing';

import { ServiceSectorService } from './service-sector.service';

describe('ServiceSectorService', () => {
  let service: ServiceSectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceSectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

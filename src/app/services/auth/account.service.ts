import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticatedService } from './authenticated.service';
import { AuthConstants } from '../../shared/constants/auth';
import { LoginResult } from '../../shared/models/auth/account-result';
import { Router } from '@angular/router';
import { services } from '@app/core/settings';

@Injectable({
  providedIn: 'root'
})

export class AccountService {
  private readonly endPoint: string = 'Account';

  private logo = new BehaviorSubject<string>('./assets/img/faces/avtar.jpeg');
  currentUserLogo = this.logo.asObservable();

  constructor(
    private http: HttpClientService,
    private authenticatedService: AuthenticatedService,
    private router: Router) {
  }

  /**
   * login
   *
   *
   * @param body
   */
  login(body: { email: string, password: string }): Observable<LoginResult> {
    return this.http.post<LoginResult>(body, `${this.endPoint}/Login`, null, services.identityService)
      .pipe(tap(result => this.setUser(result))
        , tap(() => this.authenticatedService.changeStatus(true)));
  }

  private setUser(res: LoginResult): boolean {
    if (res && res.user && res.tokenResponse) {
      localStorage.setItem(AuthConstants.UserKey, JSON.stringify(res.user));
      localStorage.setItem(AuthConstants.TokenKey, JSON.stringify(res.tokenResponse));
      return true;
    }
    return false;
  }

  /**
   * get user data
   * 
   * 
   */
  get user() {
    if (localStorage.getItem(AuthConstants.UserKey) !== null) {
      return JSON.parse(localStorage.getItem(AuthConstants.UserKey));
    }
  }

  get userInfo() {
    if (localStorage.getItem(AuthConstants.UserInfoKey) !== null) {
      return JSON.parse(localStorage.getItem(AuthConstants.UserInfoKey));
    }
  }


  /**
   * detecte if user logged in
   * 
   * 
   */
  get isLoggedIn(): boolean {
    if (localStorage.getItem(AuthConstants.UserKey) !== null) {
      return true;
    }
  }


  /**
   * update displayed name
   * 
   *  
   */
  updateSomeUserData(name: string, logo: string) {
    let user = this.user;
    user.fullName = name;

    localStorage.setItem(AuthConstants.UserKey, JSON.stringify(user));
    this.changeUserLogo(logo);
  }

  activateAccount(id: number) {
    return this.http.get(`${this.endPoint}/Activate/${id}`);
  }

  appproveAccountAndSetContactsActive(id: number) {
    return this.http.get(`${this.endPoint}/activateAccount/${id}`, undefined, services.orderService);
  }


  /**
   * logout
   * 
   * 
   */
  logout() {
    if (localStorage.getItem(AuthConstants.UserKey)) {
      localStorage.removeItem(AuthConstants.TokenKey);
      localStorage.removeItem(AuthConstants.UserKey);
      this.authenticatedService.changeStatus(false);
      this.router.navigate(['auth/login']);
    }
  }

  forgotPassword(body): Observable<any> {
    return this.http.post(body, `${this.endPoint}/ForgotPassword`, null, services.identityService);
  }

  checkResetToken(body): Observable<any> {
    return this.http.post(body, `${this.endPoint}/CheckResetToken`, null, services.identityService);
  }

  resetPassword(body): Observable<any> {
    return this.http.post(body, `${this.endPoint}/ResetPassword`, null, services.identityService);
  }

  changeUserLogo(value: string) {
    this.logo.next(value)
  }

}

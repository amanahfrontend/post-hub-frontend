import { Injectable } from '@angular/core';
import { HttpClientService } from '../../core/http-client/http-client.service';
import { services } from '../../core/settings';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private readonly endPoint: string = 'User';

  constructor(private http: HttpClientService) { }

  /**
    * change password
    *
    * @param body
    */
  changePassword(body) {
      return this.http.post(body, `${this.endPoint}/ChangePassword`, null, services.identityService);
  }
}



import { Component, OnInit } from '@angular/core';
import { RouteConfigLoadEnd, RouteConfigLoadStart, Router, RouterEvent } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { FacadeService } from './services/facade.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading: boolean = true;
  constructor(private router: Router,
    private facadeService: FacadeService,
    private permissionsService: NgxPermissionsService) {
    this.loading = false;
    this.router.events.subscribe((event: RouterEvent): void => {
      if (event instanceof RouteConfigLoadStart) {
        this.loading = true;
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loading = false;
      }
    });
  }

  ngOnInit() {
    if (this.facadeService.accountService.user && this.facadeService.accountService.user.permissions) {
      const userPermissions: string[] = this.facadeService.accountService.user.permissions || [];
      let permissionsToLoad: string[] = userPermissions.map(permission => {
        return permission.split(".").pop();
      });

      if (this.facadeService.accountService.isLoggedIn) {


              if (this.facadeService.accountService.user.userType != 3) {
                  permissionsToLoad.push("admin");
                  this.permissionsService.loadPermissions(permissionsToLoad);
              }
          else {
              this.permissionsService.loadPermissions(permissionsToLoad);
          }
      }

    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkOrderPickUpRequestsComponent } from './bulk-order-pick-up-requests.component';

describe('BulkOrderPickUpRequestsComponent', () => {
  let component: BulkOrderPickUpRequestsComponent;
  let fixture: ComponentFixture<BulkOrderPickUpRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkOrderPickUpRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkOrderPickUpRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

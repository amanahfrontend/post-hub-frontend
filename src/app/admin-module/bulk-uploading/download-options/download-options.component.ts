import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { DownloadOption } from '../../../shared/models/admin/download-option';

const EXCEL_EXT: string = 'xls';

@Component({
  selector: 'download-options',
  templateUrl: './download-options.component.html',
  styleUrls: ['./download-options.component.scss']
})
export class DownloadOptionsComponent implements OnInit {

  selectedTemplate: number;
  templates: DownloadOption[] = [
    {
      id: 1,
      name: this.translateService.instant('Banks template'),
    },
    {
      id: 2,
      name: this.translateService.instant('Cooperation template'),
    },
    {
      id: 3,
      name: this.translateService.instant('Courts template'),
    },
  ];

  constructor(
    public dialogRef: MatDialogRef<DownloadOptionsComponent>,
    private translateService: TranslateService) {
    this.templates = this.getOptionMenu();
  }

  ngOnInit(): void {
    this.translateService.onLangChange.subscribe(res => {
      this.templates = this.getOptionMenu();
    });
  }

  download() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');

    switch (this.selectedTemplate) {
      case 1:
        link.setAttribute('href', 'assets/files/Banks.xls');
        link.setAttribute('download', `${this.translateService.instant('Banks template')}.${EXCEL_EXT}`);
        break;

      case 2:
        link.setAttribute('href', 'assets/files/Cooperation.xls');
        link.setAttribute('download', `${this.translateService.instant('Cooperation template')}.${EXCEL_EXT}`);
        break;

      case 3:
        link.setAttribute('href', 'assets/files/Courts.xls');
        link.setAttribute('download', `${this.translateService.instant('Courts template')}.${EXCEL_EXT}`);
        break;

      default:
        break;
    }

    document.body.appendChild(link);
    link.click();
    link.remove();
    this.dialogRef.close();
  }

  getOptionMenu(): DownloadOption[] {
    return [
      {
        id: 1,
        name: this.translateService.instant('Banks template'),
      },
      {
        id: 2,
        name: this.translateService.instant('Cooperation template'),
      },
      {
        id: 3,
        name: this.translateService.instant('Courts template'),
      },
    ];
  }
}

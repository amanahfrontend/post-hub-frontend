import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BulkUploadingRoutingModule } from './bulk-uploading-routing.module';

// components
import { BulkUploadingComponent } from './bulk-uploading.component';
import { DownloadOptionsComponent } from './download-options/download-options.component';

// third-party
import { TranslateModule } from '@ngx-translate/core';
import { DataTablesModule } from "angular-datatables";

// shared
import { MaterialModule } from '../../shared/modules/material/material.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';

@NgModule({
  declarations: [
    BulkUploadingComponent,
    DownloadOptionsComponent
  ],
  imports: [
    CommonModule,
    BulkUploadingRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,

    SetDirModule,
    MaterialModule,
    HeaderAsCardModule,
    DataTablesModule,
    AddressModule,
    ConfirmDeletionModule,
    TimePickerModule
  ],
  entryComponents: [
    DownloadOptionsComponent
  ]
})
export class BulkUploadingModule { }

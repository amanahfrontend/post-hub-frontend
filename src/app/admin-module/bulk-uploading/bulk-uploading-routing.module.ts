import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BulkUploadingComponent } from './bulk-uploading.component';

const routes: Routes = [
  {
    path: '',
    component: BulkUploadingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BulkUploadingRoutingModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalMessageDistributionReportComponent } from './local-message-distribution-report.component';

describe('LocalMessageDistributionReportComponent', () => {
  let component: LocalMessageDistributionReportComponent;
  let fixture: ComponentFixture<LocalMessageDistributionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocalMessageDistributionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalMessageDistributionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

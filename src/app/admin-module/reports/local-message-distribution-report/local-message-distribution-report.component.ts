import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Router } from '@angular/router';
import { saveFile } from '@app/shared/helpers/download-link';
import { LocalMessageDistributionReport } from '@app/shared/models/admin/local-message-distribution-report';
import { FacadeService } from 'app/services/facade.service';
import { BusinessInfo } from 'app/shared/models/admin/business-customer';

@Component({
  selector: 'local-message-distribution-report',
  templateUrl: './local-message-distribution-report.component.html',
  styleUrls: ['./local-message-distribution-report.component.scss']
})
export class LocalMessageDistributionReportComponent implements OnInit {

  form: FormGroup;
  businessCustomers: BusinessInfo[] = [];
  selectedBusinessCustomerName: BusinessInfo;
  localMessageDistributionReports: LocalMessageDistributionReport[] = [];
  displayedColumns: string[] = [
    'id',
    'deliveryDate',
    'itemsNo',
    'returnItemsNo',
    'deliveredItemsNo',
    'remainingItemsNo',
    'workOrderCode',
    'driverName',
    'fromSerial',//رقم الكشف
    'wrokOrderCode',// امر التوزيع
    'pickupDate',
    'caseNo',
  ];
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  constructor(
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private router: Router,
  ) {

    this.form = this.fb.group({

      fromDate: new FormControl(''),
      toDate: new FormControl(''),
      businessCustomerId: [null, [Validators.required]],
      businessCustomerName:[null, [Validators.required]]

    });

  }

  ngOnInit(): void {
    this.getBusinessCustomers();
  }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.list().subscribe(res => {
      this.businessCustomers = res;
    });
  }
  setBusinessName(businessCustomerName: BusinessInfo) {
    this.selectedBusinessCustomerName = businessCustomerName;
    this.form.get('businessCustomerId').setValue(businessCustomerName.id);

  }
  getFilteredData(): void {

    let dt = this.form.value;

    const body: {

      pageNumber: number,
      pageSize: number, businessCustomerId: number, fromDate: Date, toDate: Date
    } = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
      fromDate: this.form.value.fromDate ? new Date(this.form.value.fromDate) : null,
      toDate: this.form.value.toDate ? new Date(this.form.value.toDate) : null
    };



    this.facadeService.adminFacadeService.reportsService.getLocalMessageDistributionReport(body).subscribe(
      (res: any) => {
      this.localMessageDistributionReports = res.result;
      this.total=res.totalCount;
    });
  }
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getFilteredData();
    this.replaceRoutePage(this.page);
  }
  private replaceRoutePage(page: number) {
    this.router.navigate(['local-message-distribution-report'], { queryParams: { page: page } });
  }
  export() {
    this.facadeService.adminFacadeService.reportsService.exportLocalMessageToExcel().subscribe(data => {
      saveFile(`LocalMessage.csv ${new Date().toLocaleDateString()}`, "data:attachment/text", data);
    });
  }
  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const id = event.option.value;
    switch (type) {
      case 'autoCustomerCode':
        // @TODO code runs here 

        break;

      case 'autoBusinessIndustry':
        // @TODO code runs here 
        break;

      case 'autoBusinessName':

        break;
      case 'autoBranch':
        // @TODO code runs here 
        break;

      case 'autoDepartment':
        // @TODO code runs here 
        break;

      case 'autoContractCode':
        // @TODO code runs here 
        break;

      case 'autoOrderedBy':
        // @TODO code runs here 
        break;

      case 'autoDriverCode':
        // @TODO code runs here 
        break;
      default:
        break;
    }
  }
  onSearch(keywords: string, type: string): void {
    switch (type) {
      case 'autoCustomerCode':
        // @TODO service here ...
        break;

      case 'autoBusinessIndustry':
        // @TODO service here ...
        break;

      case 'autoBusinessName':
        // @TODO service here ...
        break;
      case 'autoBranch':
        // @TODO service here ...
        break;

      case 'autoDepartment':
        // @TODO service here ...
        break;

      case 'autoContractCode':
        // @TODO service here ...
        break;

      case 'autoOrderedBy':
        // @TODO service here ...
        break;

      case 'autoDriverCode':
        // @TODO service here ...
        break;
      default:
        break;
    }
  }

}

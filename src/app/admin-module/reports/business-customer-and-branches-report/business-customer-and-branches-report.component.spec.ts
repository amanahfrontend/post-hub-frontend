import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCustomerAndBranchesReportComponent } from './business-customer-and-branches-report.component';

describe('BusinessCustomerAndBranchesReportComponent', () => {
  let component: BusinessCustomerAndBranchesReportComponent;
  let fixture: ComponentFixture<BusinessCustomerAndBranchesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessCustomerAndBranchesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCustomerAndBranchesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

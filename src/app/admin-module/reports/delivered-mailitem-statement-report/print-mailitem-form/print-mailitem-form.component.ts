import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RFQ } from 'app/shared/models/admin/PriceList/RFQ';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { DOCUMENT } from '@angular/common';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '@app/services/facade.service';
import { LanguageService } from '@app/services/shared/language.service';


const COUNTRIES = 'COUNTRIES';

@Component({
  selector: 'print-mailitem-form',
  templateUrl: './print-mailitem-form.component.html',
  styleUrls: ['./print-mailitem-form.component.scss']
})
export class ManagePrintMailItemFormComponent implements OnInit {
  form: FormGroup;
  todayDate: Date;
  mailItem: any;  
  subscriptions = new Subscription();

  locale: string = 'en';
 
  loading: boolean = false;
 
  mailitemId: any;
  type: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    
    this.mailitemId = this.data.id;  
    this.type=this.data.type ;   
  }

  ngOnInit(): void {

    if (this.mailitemId) {
      this.DeliveredMailItemDetails(this.mailitemId);
    }
    this.todayDate=new Date();
    this.locale = this.languageService.currentLanguage;
   
  }
  DeliveredMailItemDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.mailItemService.getDeliveredMailItemById(id).subscribe(res => {
      this.mailItem=res;    
    }));
  }
 
  printForm() {
    this.loading = true;

    const htmlWidth = $("#print-section").width();
    const htmlHeight = $("#print-section").height();

    const topLeftMargin = 15;

    let pdfWidth = htmlWidth + (topLeftMargin * 2);
    let pdfHeight = (pdfWidth * 1.5) + (topLeftMargin * 2);

    const canvasImageWidth = htmlWidth;
    const canvasImageHeight = htmlHeight;

    const totalPDFPages = Math.ceil(htmlHeight / pdfHeight) - 1;

    const data = this.document.getElementById('print-section');
    html2canvas(data, { allowTaint: true }).then(canvas => {

      canvas.getContext('2d');
      const imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new jsPDF('p', 'pt', [pdfWidth, pdfHeight]);
      pdf.addImage(imgData, 'png', topLeftMargin, topLeftMargin, canvasImageWidth, canvasImageHeight);

      for (let i = 1; i <= totalPDFPages; i++) {
        pdf.addPage([pdfWidth, pdfHeight], 'p');
        pdf.addImage(imgData, 'png', topLeftMargin, - (pdfHeight * i) + (topLeftMargin * 4), canvasImageWidth, canvasImageHeight);
      }

      this.loading = false;
      pdf.save(`PriceQuotation(s) Document ${new Date().toLocaleString()}.pdf`);
    });
  }

}

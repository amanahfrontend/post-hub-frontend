import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Router } from '@angular/router';
import { saveFile } from '@app/shared/helpers/download-link';
import { BusinessCustomerDistributionReport } from '@app/shared/models/admin/business-customer-distribution-report';
import { BusinessCustomerAndBranches } from '@app/shared/models/business-customer-and-branches';
import { FacadeService } from 'app/services/facade.service';
import { BusinessCustomerBranch, BusinessInfo } from 'app/shared/models/admin/business-customer';
import * as moment from 'moment';
//import { DatePipe } from '@angular/common'

@Component({
  selector: 'business-customer-distribution-report',
  templateUrl: './business-customer-distribution-report.component.html',
  styleUrls: ['./business-customer-distribution-report.component.scss']
})
export class BusinessCustomerDistributionReportComponent implements OnInit {

  form: FormGroup;
  businessCustomers: BusinessInfo[] = [];
  branches: BusinessCustomerBranch[] = [];
  months=[1,2,3,4,5,6,7,8,9,10,11,12];
  years=[2020,2021,2022];
  columnsToDisplay=[];

  selectedBusinessCustomerName: BusinessInfo;
  businessCustomerDistributions: BusinessCustomerDistributionReport[] = [];
  displayedColumns: string[] = [

    // //'id',
    // 'businessBranchName',
    // 'deliveryDate',
    // 'itemsNo',
    // //'totalRowsNo',
    // 'totalItemsDeliveredForBranch',
  
  ];
 
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  constructor(
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private router: Router,
    //public datepipe: DatePipe
  ) {

    this.form = this.fb.group({

      businessCustomerId: [null, [Validators.required]],
      businessCustomerName:[null, [Validators.nullValidator]],
      // fromDate: ['', [Validators.required]],
       //toDate: ['', [Validators.required]],
      month: ['', [Validators.required]],
      year: ['', [Validators.required]],

    });

  }

  ngOnInit(): void {
    this.getBusinessCustomers();
  }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.list().subscribe(res => {
      this.businessCustomers = res;
    });
  }
  getBusinessCustomerBranch(customerId: number) {
    this.facadeService.adminFacadeService.businessCustomerBranchService.list(customerId)
      .subscribe(res => {
        this.branches = res;
       this.columnsToDisplay=res?res.map(obj=>({
            column:obj.name
        })):[]
        this.columnsToDisplay.push({column:"deliveredDate"})
        if(this.columnsToDisplay){
          this.displayedColumns=this.columnsToDisplay.map(col => col.column)
        }
      });
  }
  getFilteredData(): void {

    let dt = this.form.value;

    const body: {

      pageNumber: number,
      pageSize: number, businessCustomerId: number,
      month:number,
      year:number,

      // fromDate: Date, toDate: Date
    } = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
     //  fromDate: this.form.value.fromDate ? new Date(this.form.value.fromDate) : null,
     //  toDate: this.form.value.toDate ? new Date(this.form.value.toDate) : null
      month:this.form.value.month?+this.form.value.month:0,
      year:this.form.value.year?+this.form.value.year:0
    };



    this.facadeService.adminFacadeService.reportsService.getBusinessCustomerDistributionReport(body).subscribe(
      (res: any) => {
        var newRes=res.map(obj=>({
        //  deliveredDate: this.datepipe.transform(new Date(body.year, body.month, obj.deliveredDay, 0, 0, 0, 0), 'yyyy-MM-dd'),
          deliveredDate: moment(new Date(body.year, body.month, obj.deliveredDay), "MM-DD-YYYY"),
          businessName:obj.businessName,
          branchName:obj.branchName,
          itemsNo:obj.itemsNo
        }))
        
      this.businessCustomerDistributions = newRes;
      this.total=res.length;
    });
  }
  export() {
    const body: {
       businessCustomerId: number,
      month:number,
      year:number,

    } = {
     
      businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
      month:this.form.value.month?+this.form.value.month:0,
      year:this.form.value.year?+this.form.value.year:0
    };

    this.facadeService.adminFacadeService.reportsService.getBusinessCustomerDistributionForExportReport(body).subscribe(data => {
        saveFile(`BusinessCustomerDistribution${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getFilteredData();
    this.replaceRoutePage(this.page);
  }
  private replaceRoutePage(page: number) {
    this.router.navigate(['business-customer-and-branches-report'], { queryParams: { page: page } });
  }
 
  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const id =Number(event)
    switch (type) {
      case 'autoCustomerCode':
        // @TODO code runs here 

        break;

      case 'autoBusinessIndustry':
        // @TODO code runs here 
        break;

      case 'autoBusinessName':
         this.getBusinessCustomerBranch(id);
        break;
      case 'autoBranch':
        // @TODO code runs here 
        break;

      case 'autoDepartment':
        // @TODO code runs here 
        break;

      case 'autoContractCode':
        // @TODO code runs here 
        break;

      case 'autoOrderedBy':
        // @TODO code runs here 
        break;

      case 'autoDriverCode':
        // @TODO code runs here 
        break;
      default:
        break;
    }
  }
  onSearch(keywords: string, type: string): void {
    switch (type) {
      case 'autoCustomerCode':
        // @TODO service here ...
        break;

      case 'autoBusinessIndustry':
        // @TODO service here ...
        break;

      case 'autoBusinessName':
        // @TODO service here ...
        break;
      case 'autoBranch':
        // @TODO service here ...
        break;

      case 'autoDepartment':
        // @TODO service here ...
        break;

      case 'autoContractCode':
        // @TODO service here ...
        break;

      case 'autoOrderedBy':
        // @TODO service here ...
        break;

      case 'autoDriverCode':
        // @TODO service here ...
        break;
      default:
        break;
    }
  }

}




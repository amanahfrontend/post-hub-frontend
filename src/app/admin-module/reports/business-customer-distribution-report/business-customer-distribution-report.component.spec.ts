import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCustomerDistributionReportComponent } from './business-customer-distribution-report.component';

describe('BusinessCustomerDistributionReportComponent', () => {
  let component: BusinessCustomerDistributionReportComponent;
  let fixture: ComponentFixture<BusinessCustomerDistributionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessCustomerDistributionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCustomerDistributionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

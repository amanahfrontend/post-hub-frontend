import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { saveFile } from '@app/shared/helpers/download-link';
import { MailItem } from '@app/shared/models/admin/MailITem';
import { FacadeService } from 'app/services/facade.service';
import { BusinessInfo } from 'app/shared/models/admin/business-customer';
import { ManagePrintMailItemFormComponent } from '../delivered-mailitem-statement-report/print-mailitem-form/print-mailitem-form.component';
//import { ManagePrintMailItemFormComponent } from './print-mailitem-form/print-mailitem-form.component';

@Component({
  selector: 'returned-mailitem-statement-report',
  templateUrl: './returned-mailitem-statement-report.component.html',
  styleUrls: ['./returned-mailitem-statement-report.component.scss']
})
export class ReturnedMailItemsReportComponent implements OnInit {
  minDate: Date;
  maxDate: Date;
  form: FormGroup;
  businessCustomers: BusinessInfo[] = [];
  selectedBusinessCustomerName: BusinessInfo;
  deliveredMailItems: MailItem[] = [];
  displayedColumns: string[] = [
    'code',
    'returnDate',
    'actions'   
  ];
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  constructor(
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private router: Router,
  ) {

    this.form = this.fb.group({
      fromDate:new FormControl(new Date()), //new FormControl(''),
      toDate: new FormControl(new Date()),
      businessCustomerId: [0],
      businessCustomerName:[''],
      code:['']
    });

  }

  ngOnInit(): void {
    this.getBusinessCustomers();
    this.getFilteredData();
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
  }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.list().subscribe(res => {
      this.businessCustomers = res;
    });
  }
  setBusinessName(businessCustomerName: BusinessInfo) {
    this.selectedBusinessCustomerName = businessCustomerName;
    this.form.get('businessCustomerId').setValue(businessCustomerName.id);

  }
  getFilteredData(): void {

    let dt = this.form.value;

    const body: {

      pageNumber: number,
      pageSize: number, businessCustomerId: number, fromDate: Date, toDate: Date ,barCode:string
    } = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
      fromDate: this.form.value.fromDate ? new Date(this.form.value.fromDate) : null,
      toDate: this.form.value.toDate ? new Date(this.form.value.toDate) : null,
      barCode:this.form.value.code ? this.form.value.code : ''
    };



    this.facadeService.adminFacadeService.mailItemService.getReturnedMailItemsPagginated(body).subscribe(
      (res: any) => {
      this.deliveredMailItems = res.result;
      this.total=res.totalCount;
      console.log("ddel",this.deliveredMailItems)
    });
  }
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getFilteredData();
    this.replaceRoutePage(this.page);
  }
  private replaceRoutePage(page: number) {
    this.router.navigate(['returned-mailitem-report'], { queryParams: { page: page } });
  }

  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const id = event.option.value;
    switch (type) {
      case 'autoCustomerCode':
        // @TODO code runs here 

        break;

      case 'autoBusinessIndustry':
        // @TODO code runs here 
        break;

      case 'autoBusinessName':

        break;
      case 'autoBranch':
        // @TODO code runs here 
        break;

      case 'autoDepartment':
        // @TODO code runs here 
        break;

      case 'autoContractCode':
        // @TODO code runs here 
        break;

      case 'autoOrderedBy':
        // @TODO code runs here 
        break;

      case 'autoDriverCode':
        // @TODO code runs here 
        break;
      default:
        break;
    }
  }
  onSearch(keywords: string, type: string): void {
    switch (type) {
      case 'autoCustomerCode':
        // @TODO service here ...
        break;

      case 'autoBusinessIndustry':
        // @TODO service here ...
        break;

      case 'autoBusinessName':
        // @TODO service here ...
        break;
      case 'autoBranch':
        // @TODO service here ...
        break;

      case 'autoDepartment':
        // @TODO service here ...
        break;

      case 'autoContractCode':
        // @TODO service here ...
        break;

      case 'autoOrderedBy':
        // @TODO service here ...
        break;

      case 'autoDriverCode':
        // @TODO service here ...
        break;
      default:
        break;
    }
  }
  print(element: any ,itemType:any): void { 
   
    this.opentPrintDialog(element.id ,itemType);   
  }
  opentPrintDialog(itemId:any ,type :any){
    const dialog = this.dialog.open(ManagePrintMailItemFormComponent, {
      width: '60%',
      data: {
      id:itemId,
      type:type
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {    
      //this.list(this.page);
    });
  }

}

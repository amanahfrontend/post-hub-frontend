import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusinessCustomerAndBranchesReportComponent } from './business-customer-and-branches-report/business-customer-and-branches-report.component';
import { BusinessCustomerDistributionReportComponent } from './business-customer-distribution-report/business-customer-distribution-report.component';
import { BusinessReportsComponent } from './business-reports/business-reports.component';
import { DeliveredMailItemsReportComponent } from './delivered-mailitem-statement-report/delivered-mailitem-statement-report.component';
import { LocalMessageDistributionReportComponent } from './local-message-distribution-report/local-message-distribution-report.component';
import { ReturnedMailItemsReportComponent } from './returned-mailitem-statement-report/returned-mailitem-statement-report.component';


const routes: Routes = [
  {
    path: 'local-message-distribution-report',
    component: LocalMessageDistributionReportComponent
  },
  {
    path: 'business-report',
    component: BusinessReportsComponent
  },
  {
    path:'business-customer-and-branches-report',
    component:BusinessCustomerAndBranchesReportComponent
  },
  {
    path:'business-customer-distribution-report',
    component:BusinessCustomerDistributionReportComponent
  },
 {
    path:'delivered-mailitems-report',
    component:DeliveredMailItemsReportComponent
  }
  ,
  {
    path:'returned-mailitem-report',
    component:ReturnedMailItemsReportComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }

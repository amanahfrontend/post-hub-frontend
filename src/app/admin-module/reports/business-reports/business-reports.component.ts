import { ChangeDetectorRef, Component, AfterContentChecked, OnInit, OnDestroy } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { OrderForDispatch } from '../../../shared/models/admin/Order';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../../services/facade.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'business-reports',
  templateUrl: './business-reports.component.html',
  styleUrls: ['./business-reports.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BusinessReportsComponent implements OnInit, OnDestroy, AfterContentChecked {


  columnsToDisplay = ['showHide', 'select', 'branchName', 'date', 'itemsNo', 'actions'];
  itemsColumnsToDisplay = ['select', 'itemInfo', 'itemStatus', 'deliveryBefore', 'weight', 'actions'];

  itemsDataSource = new MatTableDataSource<any>([]);
  itemsSelection = new SelectionModel<any>(true, []);
  branchesRowsSelection = new SelectionModel<any>(true, []);

  selectedBranchesNum: number = 0;
  selectedItemsNum: number = 0;

  expandedElement: OrderForDispatch | null;

  allRowsExpanded: boolean = false;

  showFilters: boolean = false;
  groupType: number = 0;

  list: OrderForDispatch[] = [];

  totalItemsCount: number = 0;
  totalBranchesCount: number = 0;
  itemsLoaded: boolean = false;

  page: number = 1;
  pageSize: number = 10;
  length: number = 0;

  areasIds: number[] = [];
  branchesIds: number[] = [];
  blocksIds: number[] = [];
  itemsIds: number[] = [];

  subscriptions = new Subscription();
  /**
   * 
   * @param router 
   * @param toastrService 
   * @param facadeService 
   * @param cdr 
   * @param dialog 
   */
  constructor(private router: Router,
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.onSelectBranchBy();
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  /**
   * grouped by orders
   * 
   * 
   */
  groupedByBranches(page: number) {
    this.list = [];

    this.facadeService.adminFacadeService.orderService.orderForDispatch({ pageNumber: page, pageSize: this.pageSize }).subscribe((res: any) => {
      this.length = res.totalCount;
      this.list = res.value;
      this.totalBranchesCount = this.list.length;
    });
  }

  /**
   * grouped by order
   * 
   * 
   * @param orderId 
   */
  getItemsByBranchId(orderId: number, page: number) {
    const orderIndex = this.list.findIndex(order => order.id == orderId);

    this.facadeService.adminFacadeService.mailItemService.getByItemsOrderId(orderId).subscribe((items: any) => {
      this.list[orderIndex].items = items;
      this.calculateItemsCount();
    });
  }


  /**
   * 
   * @param expandedElement 
   * @param areaElement 
   */
  setExpandedElement(expandedElement: OrderForDispatch) {
    if (expandedElement) {
      this.expandedElement = expandedElement;
      this.getItemsByBranchId(expandedElement.id, this.page);
    }
  }

  /**
   * 
   * @returns 
   */
  isAllSelectedBranchesRows() {
    const numSelected = this.branchesRowsSelection.selected.length;
    const numRows = this.list.length;

    this.selectedBranchesNum = numRows;
    return numSelected === numRows;
  }

  masterToggleGroupedRows() {
    if (this.isAllSelectedBranchesRows()) {
      this.branchesRowsSelection.clear();
      this.branchesIds = this.branchesRowsSelection.selected.map(group => group.id);
      this.selectedBranchesNum = 0;

      this.list.forEach((group: any) => {
        if (group.items) {
          this.itemsSelection.deselect(...group.items);
          this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        }
      });

      return;
    }

    this.branchesRowsSelection.select(...this.list);
    this.branchesIds = this.branchesRowsSelection.selected.map(group => group.id);
    this.selectedBranchesNum = this.branchesRowsSelection.selected.length;

    this.list.forEach((group: any) => {
      if (group.items) {
        this.itemsSelection.select(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
      }
    });
  }

  /**
   * 
   * @param row 
   * @returns 
   */
  checkboxLabelGroupedBranches(row?: any): string {
    this.selectedBranchesNum = this.branchesRowsSelection.selected.length;
    this.branchesIds = this.branchesRowsSelection.selected.map(group => group.id);
    if (!row) {
      return `${this.isAllSelectedBranchesRows() ? 'deselect' : 'select'} all`;
    }
    return `${this.branchesRowsSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /**
   * 
   * @param element 
   * @returns 
   */
  isAllSelectedItems(element: OrderForDispatch) {
    const index: number = this.list.findIndex((order: OrderForDispatch) => order.id == element.id);
    const numSelected = this.itemsSelection.selected.length;
    const numRows = this.list[index].items.length;

    return numSelected === numRows;
  }

  /**
   * 
   * @param element 
   * @returns 
   */
  masterToggleItems(element: OrderForDispatch) {
    const index: number = this.list.findIndex((order: OrderForDispatch) => order.id == element.id);

    if (this.isAllSelectedItems(element)) {
      this.itemsSelection.clear();
      this.itemsIds = this.itemsSelection.selected.map(item => item.id);
      return;
    }

    this.itemsSelection.select(...this.list[index].items);
    this.itemsIds = this.itemsSelection.selected.map(item => item.id);
  }

  /**
   * 
   * @param element 
   * @param row 
   * @returns 
   */
  checkboxLabelItems(element: OrderForDispatch, row?: any): string {
    this.selectedItemsNum = this.itemsSelection.selected.length;
    this.itemsIds = this.itemsSelection.selected.map(item => item.id);

    if (!row) {
      return `${this.isAllSelectedItems(element) ? 'deselect' : 'select'} all`;
    }

    return `${this.itemsSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  expandCollapseAll() {
    this.allRowsExpanded = !this.allRowsExpanded;
    this.expandedElement = null;

    if ((!this.itemsLoaded) && this.allRowsExpanded) {
      this.itemsLoaded = true;
      this.fetchAllItems();
    }
  }

  fetchAllItems() {
    this.list.forEach((order: { id: number }) => {
      this.getItemsByBranchId(order.id, this.page);
    });

    this.calculateItemsCount();
  }

  toggleFilter() {
    this.showFilters = !this.showFilters;
  }

  /**
   * assign to workorder (value or more)
   * 
   * 
   * @param rows 
   * @param fromGroup 
   */
  issuOrAddWorkorder(workorderType: string): void {
    if (this.itemsSelection.selected.length == 0) {
      this.toastrService.error('Please Select Item(s)');
    } else {
      this.router.navigate(['manage-workorders/add'], { state: { rows: this.itemsSelection.selected, type: workorderType } });
    }
  }

  onSelectBranchBy(event?: Event) {
    this.totalItemsCount = 0;
    this.selectedItemsNum = 0;
    this.itemsLoaded = false;

    if (this.groupType == 0) {
      this.groupedByBranches(this.page);
    }
  }

  calculateItemsCount(): void {
    this.totalItemsCount = 0;

    this.list.forEach((group: OrderForDispatch) => {
      if (group.items) {
        this.totalItemsCount = this.totalItemsCount + group.items.length;
      }
    });
  }

  /**
   * select group
   * 
   * 
   * 
   * @param event 
   * @param group 
   * @returns 
   */
  onSelectBranch(event: MatCheckboxChange, group: OrderForDispatch) {
    if (group && group.items) {
      if (event.checked) {
        this.itemsSelection.select(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        return this.branchesRowsSelection.toggle(group);
      } else {
        this.itemsSelection.deselect(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        return this.branchesRowsSelection.toggle(group);
      }
    }
  }

  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangeByOrdersPage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.groupedByBranches(this.page);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

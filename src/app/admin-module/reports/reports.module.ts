import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalMessageDistributionReportComponent } from './local-message-distribution-report/local-message-distribution-report.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { HeaderAsCardModule } from '@app/shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '@app/shared/modules/material/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '@app/shared/components/confirm-deletion/confirm-deletion.module';
import { BusinessReportsComponent } from './business-reports/business-reports.component';
import { BusinessCustomerAndBranchesReportComponent } from './business-customer-and-branches-report/business-customer-and-branches-report.component';
import { BusinessCustomerDistributionReportComponent } from './business-customer-distribution-report/business-customer-distribution-report.component';
import { DeliveredMailItemsReportComponent } from './delivered-mailitem-statement-report/delivered-mailitem-statement-report.component';
import { ManagePrintMailItemFormComponent } from './delivered-mailitem-statement-report/print-mailitem-form/print-mailitem-form.component';
import { ReturnedMailItemsReportComponent } from './returned-mailitem-statement-report/returned-mailitem-statement-report.component';
//import { DatePipe } from '@angular/common'

@NgModule({
  declarations: [
    LocalMessageDistributionReportComponent,
    BusinessReportsComponent,
    BusinessCustomerAndBranchesReportComponent,
    BusinessCustomerDistributionReportComponent,
    DeliveredMailItemsReportComponent,
    ReturnedMailItemsReportComponent,
    ManagePrintMailItemFormComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    HeaderAsCardModule,
    MaterialModule,
    TranslateModule,
    ReactiveFormsModule,
    ConfirmDeletionModule,
    FormsModule
  ],
  entryComponents: [
    ManagePrintMailItemFormComponent
  ],
})
export class ReportsModule { }

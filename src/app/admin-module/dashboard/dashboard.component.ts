import { Component, OnInit, ChangeDetectorRef, } from '@angular/core';
import { FacadeService } from 'app/services/facade.service';
import { Router } from '@angular/router';
import * as Chartist from 'chartist';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  driversCount: number = 0;
  businessCustomersCount: number = 0;
  invoicesCount: number = 0;
  businessOrdersCount: number = 0;
  dailyOrders: any[] = [];
  closedOrders: any[] = [];
  mailItemCountByItemStatus: any[] = [];

  dailyOrdersSeries: number[] = [];
  closedOrdersSeries: number[] = [];
  mailItemCountByItemStatusSeries: number[] = [];

  dailyOrdersLabels: string[] = [];
  closedOrdersLabels: string[] = [];
  mailItemCountByItemStatusLabels: string[] = [];

  dailyOrdersMax: number;
  closedOrdersMax: number;
  mailItemCountByItemStatusMax: number;
  dataDailySalesChart: any;
  optionsDailySalesChart: any;
  dailySalesChart;

  constructor(
    private facadeService: FacadeService,
    private router: Router,
    private cdr: ChangeDetectorRef
  ) {

  }

  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on('draw', (data) => {
      if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if (data.type === 'point') {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq = 0;
  };

  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function (data) {
      if (data.type === 'bar') {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq2 = 0;
  }

  ngOnChanges() {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.dataDailySalesChart = {
      labels: [
        '1', '2', '3', '4', '5', '16', '7', '8', '9',
        '10', '11', '12', '13', '14', '15', '16', '17',
        '18', '19', '20', '21', '22', '23', '24', '25',
        '26', '27', '28', '29', '30', '31'
      ],
    };

    this.facadeService.adminFacadeService.businessOrderService.getDailyOrders().subscribe((res: any) => {
      this.dailyOrders = res;
      this.dailyOrdersSeries = res.map(obj => {
        return obj.count;
      });

      this.dailyOrdersMax = this.dailyOrdersSeries.reduce((a, b) => Math.max(a, b));
      this.dataDailySalesChart.series = [this.dailyOrdersSeries];
    });

    this.optionsDailySalesChart = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: 0,
      high: 30,
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
    }

    setTimeout(() => {
      const dailySalesChart = new Chartist.Line('#dailySalesChart', this.dataDailySalesChart, this.optionsDailySalesChart);
      this.startAnimationForLineChart(dailySalesChart);
    }, 1000);

    const dataCompletedTasksChart: any = {
      labels: ['3p', '6p', '9p', '12p', '3a', '6a', '9a'],
      series: [
        [230, 750, 450, 300, 280, 240, 200, 190]
      ]
    };

    const optionsCompletedTasksChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: 0,
      high: 1000,
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0 }
    }

    const completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
    this.startAnimationForLineChart(completedTasksChart);

    const datawebsiteViewsChart = {
      labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
      series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
    };

    const optionswebsiteViewsChart = {
      axisX: {
        showGrid: false
      },
      low: 0,
      high: 1000,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
    };

    const responsiveOptions: any[] = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];

    const websiteViewsChart = new Chartist.Bar('#websiteViewsChart', datawebsiteViewsChart, optionswebsiteViewsChart, responsiveOptions);
    this.startAnimationForBarChart(websiteViewsChart);

    this.getDriverCount();
    this.getBusinessCustomerCount();
    this.getBusinessOrderCount();
    this.getInvoicesCount();
  }

  getDriverCount() {
    this.facadeService.adminFacadeService.manageDriverService.getDriversCount().subscribe((res: any) => {
      this.driversCount = res;
    });
  }

  getBusinessCustomerCount() {
    this.facadeService.adminFacadeService.businessCustomerService.getBussinessCutomerCount().subscribe((res: any) => {
      this.businessCustomersCount = res;
    });
  }

  getBusinessOrderCount() {
    this.facadeService.adminFacadeService.businessOrderService.getBusinessOrderCount().subscribe((res: any) => {
      this.businessOrdersCount = res;
    });
  }

  getDailyOrders() {
    this.facadeService.adminFacadeService.businessOrderService.getDailyOrders().subscribe(
      (res: any) => {
        this.dailyOrders = res
        this.dailyOrdersSeries = res.map(obj => {
          return obj.count;
        })
        this.dailyOrdersMax = this.dailyOrdersSeries.reduce((a, b) => Math.max(a, b));
      });
  }

  getClosedOrders() {
    this.facadeService.adminFacadeService.businessOrderService.getClosedOrders().subscribe(
      (res: any) => {
        this.closedOrders = res
        this.closedOrdersSeries = res.map(obj => {
          return obj.count;
        })
        this.closedOrdersMax = this.closedOrdersSeries.reduce((a, b) => Math.max(a, b));

      });
  }

  getMailItemCountByItemStatus() {
    this.facadeService.adminFacadeService.mailItemService.getMailItemCountByItemStatus().subscribe((res: any) => {
      this.mailItemCountByItemStatus = res;
      // this.mailItemCountByItemStatusSeries = res.map(obj => {
      //   return obj.count;
      // })
      // this.mailItemCountByItemStatusMax = this.mailItemCountByItemStatusSeries.reduce((a, b) => Math.max(a, b));
    });
  }

  getInvoicesCount() {
    this.facadeService.adminFacadeService.ManageInvoicesService.getInvoicesCount().subscribe(
      (res: any) => {
        this.invoicesCount = res
      });
  }

  addDrivers() {
    this.router.navigate(['manage-drivers/add']);
  }

  addBusinessCustomer() {
    this.router.navigate(['business-accounts/add']);
  }

  addInvoices() {
    this.router.navigate(['manage-invoice/add']);
  }

  addBusinessOrders() {
    this.router.navigate(['create-business-order']);
  }
}

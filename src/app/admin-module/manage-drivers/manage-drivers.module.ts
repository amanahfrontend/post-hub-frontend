import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageDriversRoutingModule } from './manage-drivers-routing.module';
import { ManageDriversComponent } from './manage-drivers.component';
import { ManageDriverFormComponent } from './manage-driver-form/manage-driver-form.component';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// shared
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    ManageDriversComponent,
    ManageDriverFormComponent
  ],
  imports: [
    CommonModule,
    ManageDriversRoutingModule,

    ConfirmDeletionModule,
    AddressModule,
    TranslateModule,
    MaterialModule,
    HeaderAsCardModule,
    TelInputModule,
    ReactiveFormsModule,
    SetDirModule,
    FormsModule,
    PermissionsModule
  ]
})
export class ManageDriversModule { }

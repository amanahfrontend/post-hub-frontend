import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '../../services/facade.service';
import { Driver } from '../../shared/models/admin/driver';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from 'app/shared/helpers/download-link';

@Component({
  selector: 'manage-drivers',
  templateUrl: './manage-drivers.component.html',
  styleUrls: ['./manage-drivers.component.scss']
})
export class ManageDriversComponent implements OnInit {
  url: any = "./assets/img/faces/avtar.jpeg"

  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'nationality',
    'mobileNumber',
    'roleNames',
    'teamName',
    'departmentName',
    'supervisorName',
    'serviceSectorName',
    'branchName',
    'companyEmail',
    'licensePlate',
    'actions'
  ];

  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  drivers: Driver[] = [];
  subscriptions = new Subscription();
  searchBy: string = '';

  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
  constructor(
    private translateService: TranslateService,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));
  }

  ngOnInit(): void {
    this.list(this.page);
  }

  /**
   * list managers
   * 
   * 
   * @param page 
   */
  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.manageDriverService.listByPagination(body).subscribe((res: any) => {
      this.drivers = res.result;
      this.total = res.totalCount;
    });
  }

  /**
   * select image
   * 
   * 
   * @param event 
   */
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.url = event.target.result;
      }
    }
  }

  /**
   * add / edit row
   * 
   * 
   * @param opertaion 
   * @param editMode 
   * @param manager 
   */
  manageDriverSeletedRow(driver?: Driver): void {
    if (driver) {
      this.router.navigateByUrl(`/manage-drivers/edit/${driver.id}`, { state: driver });
    } else {
      this.router.navigateByUrl('/manage-drivers/add');
    }
  }

  /**
   * on change page or per page
   *x
   *
   * @param event
   */
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.list(this.page);
    this.replaceRoutePage(this.page);
  }

  /**
   * 
   * @param page 
   */
  private replaceRoutePage(page: number) {
    this.router.navigate(['manage-drivers'], { queryParams: { page: page } });
  }

  /**
   * Delete staff
   * 
   * 
   * @param event 
   * @param manager 
   */
  onConfirm(event: boolean, manager: Driver): void {
    if (event) {
      this.facadeService.adminFacadeService.manageDriverService.delete(manager.id).subscribe(res => {
        const index = this.drivers.indexOf(manager);
        if (index >= 0) {
          this.drivers.splice(index, 1);

          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`Driver has been deleted successfully`));
        }
      });
    }
  }

  export() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      SearchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageDriverService.exportToExcel(body).subscribe(data => {
        saveFile(`Drivers${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }

  search(): void {
    const body: { pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.manageDriverService.listByPagination(body).subscribe((res: any) => {
      this.drivers = res.result;
      this.total = res.totalCount;
    });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ManageDriverFormComponent } from './manage-driver-form/manage-driver-form.component';
import { ManageDriversComponent } from './manage-drivers.component';

const routes: Routes = [
  {
    path: '',
    component: ManageDriversComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadAgent'
      }
    },
  },
  {
    path: 'add',
    component: ManageDriverFormComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'CreateAgent'
      }
    },
  },
  {
    path: 'edit/:id',
    component: ManageDriverFormComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'UpdateAgent'
      }
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageDriversRoutingModule { }

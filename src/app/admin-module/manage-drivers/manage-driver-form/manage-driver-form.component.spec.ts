import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDriverFormComponent } from './manage-driver-form.component';

describe('ManageDriverFormComponent', () => {
  let component: ManageDriverFormComponent;
  let fixture: ComponentFixture<ManageDriverFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageDriverFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDriverFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

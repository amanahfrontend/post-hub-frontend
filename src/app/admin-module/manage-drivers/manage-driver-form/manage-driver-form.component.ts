import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Address, Country } from '../../../shared/models/components';
import { FacadeService } from '../../../services/facade.service';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from './../../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../services/shared/language.service';
import { TranslateService } from '@ngx-translate/core';
import { Driver } from '../../../shared/models/admin/driver';
import { TransportType } from '../../../shared/models/admin/transport-type';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { App } from '../../../core/app';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

const COUNTRIES = 'COUNTRIES';

@Component({
  selector: 'manage-driver-form',
  templateUrl: './manage-driver-form.component.html',
  styleUrls: ['./manage-driver-form.component.scss']
})
export class ManageDriverFormComponent implements OnInit, OnDestroy {
  form: FormGroup;
  convertDate:String;
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg"
  operation: string;
  selectedMobileNumber: number;
  selectedCountry: Country;
  selectedCountryCode: Country;

  countries: Country[] = [];
  filteredCountries: Observable<Country[]>;
  departments: Option[] = [];
  sectors: Option[] = [];
  status: Option[] = [];
  genders: Option[] = [];
  teams: Option[] = [];
  branchs: Option[] = [];
  managers: Option[] = [];

  minDate: Date;
  maxDate: Date;
  validPhoneNumber: boolean = false;
  accept = 'image/*';
  selectedFile: File;
  locale: string = 'en';
  type: string = 'password';
  isEditMode: boolean = false;
  row: Driver;
  transportTypes: any[] = [];
  selectedTransportionType: string;
  fences: any[];
  address: Address;
  subscriptions = new Subscription();
  submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  )
  {
    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      genderId: [''],
     // dateOfBirth: [''],
      dateOfBirth:['', [Validators.nullValidator]], //new FormControl(''),
      countryCtrl: ['', [Validators.required]],
      nationalId: [''],
      phoneNumber: [''],
      email: [''],
      serviceSectorId: ['', [Validators.required]],
      departmentId: ['', [Validators.required]],
      branchId: ['', [Validators.required]],
      teamId: ['', [Validators.required]],
      companyEmail: [''],
      licensePlate: [''],
      color: [''],
      transportDescription: [''],
      supervisorId: ['', [Validators.required]],
      accountStatusId: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      driverDeliveryGeoFences: [''],
      driverPickUpGeoFences: ['']
    });

    this.subscriptions.add(this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.isEditMode = true;
        this.validPhoneNumber = true;
        this.driverDetails(params.id);
        this.operation = this.translateService.instant('Edit');
      } else {
        this.isEditMode = false;
        this.operation = this.translateService.instant('Add');
      }
    }));

    this.countries = JSON.parse(localStorage.getItem(COUNTRIES));
    this.selectedTransportionType = 'walk';
  }

  ngOnInit(): void {
    this.locale = this.languageService.currentLanguage;
    this.filteredCountries = this.form.get('countryCtrl').valueChanges
      .pipe(
        startWith(''),
        map(country => country ? this.filter(country) : this.countries.slice())
      );

    this.loadPreData();

   // const currentYear = new Date().getFullYear();
   // this.minDate = new Date(currentYear - 59, 11, 31);
   // this.maxDate = new Date(currentYear + 17, 11, 31);
  }

  private filter(value: string): Country[] {
    const filterValue = value.toLowerCase();
    return this.countries.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.path = event.target.result;
      }
    }
  }
  date(e) {
    this.convertDate = new Date(e.target.value.setDate(e.target.value.getDate() + 1)).toISOString().substring(0, 10);
    this.form.get('dateOfBirth').patchValue(this.convertDate, {
     onlyself: true
   })
 }
  submit() {
   
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    let body = { ... this.row, ... this.form.value };
    body.driverDeliveryGeoFences=[body.driverDeliveryGeoFences];
     
    body.driverPickUpGeoFences=[body.driverPickUpGeoFences];
    body.roleName = 'Delegate';

    if (this.selectedMobileNumber) {
      body.mobileNumber = this.selectedMobileNumber;
    }

    if (this.selectedFile) {
      body.formFile = this.selectedFile;
    }

    if (this.selectedCountryCode) {
      body.countryId = this.selectedCountryCode.id;
    }

    if (this.selectedCountry) {
      body.nationalityId = this.selectedCountry.id;
    }
     
    let deliveryList =body.driverDeliveryGeoFences //this.form.get('driverDeliveryGeoFences').value;
    if (deliveryList.findIndex(value => value == "All") > -1) {
      body['allDeliveryGeoFences'] = true;
      body['driverDeliveryGeoFences'] = [];
    } else if (deliveryList.length > 1) {
      const allIndex = deliveryList.findIndex(value => value == "All");
      if (allIndex >= 0) {
        deliveryList.splice(allIndex, 1);
      }
      body['driverDeliveryGeoFences'] = deliveryList;
    }

    let pickupList =   body.driverPickUpGeoFences //this.form.get('driverPickUpGeoFences').value;
    if (pickupList.findIndex(value => value == "All") > -1) {
      body['allPickupGeoFences'] = true;
      body['driverPickUpGeoFences'] = [];
    } else if (pickupList.length > 1) {
      const allIndex = pickupList.findIndex(a => a == "All");
      if (allIndex >= 0) {
        pickupList.splice(allIndex, 1);
      }
      body['driverPickUpGeoFences'] = pickupList;
    }

    const transport = this.transportTypes.find(type => type.name.toLocaleLowerCase() == this.selectedTransportionType.toLocaleLowerCase());
    if (transport) {
      body['transportTypeId'] = transport.id;
    }

    if (this.isEditMode) {
      body.id = this.row.id;
      body.userId = this.row.userId;
    }

    body.address = { ... this.address };
    let { countryCtrl, ...objectToPost } = body;
    this.submitted = true;

    if (this.isEditMode) {
      this.subscriptions.add(this.facadeService.adminFacadeService.manageDriverService.update(objectToPost).subscribe(res => {
        this.toastrService.success(this.translateService.instant('Driver Updated Successfully'));
        this.location.back();
      }, error => {
        this.submitted = false;
      }));
    } else {
      this.subscriptions.add(this.facadeService.adminFacadeService.manageDriverService.create(objectToPost).subscribe(res => {
        this.toastrService.success(this.translateService.instant('New Driver Added Successfully'));
        this.location.back();
      }, error => {
        this.submitted = false;
      }));
    }
  }

  /**
   * on type number 
   * 
   * 
   * @param number 
   */
  number(number: number): void {
    this.selectedMobileNumber = number;
  }

  /**
   * on select country flag from phone number
   * 
   * 
   * @param country 
   */
  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

  setCountry(country: Country) {
    this.selectedCountry = country;
  }

  loadPreData(): void {
    // departments
    this.subscriptions.add(this.facadeService.departmentService.list().subscribe((res: Option[]) => {
      this.departments = res;
    }));

    // sectors
    this.subscriptions.add(this.facadeService.sharedFacadeService.sectorService.list().subscribe((res: Option[]) => {
      this.sectors = res;
    }));

    // status
    this.subscriptions.add(this.facadeService.sharedFacadeService.accountStatusService.list().subscribe((res: Option[]) => {
      this.status = res;
    }));

    // genders
    this.subscriptions.add(this.facadeService.sharedFacadeService.genderService.list().subscribe((res: Option[]) => {
      this.genders = res;
    }));

    // teams
    this.subscriptions.add(this.facadeService.adminFacadeService.teamsService.teams().subscribe((res: Option[]) => {
      this.teams = res;
    }));

    // branches
    this.subscriptions.add(this.facadeService.adminFacadeService.branchService.list().subscribe((res: Option[]) => {
      this.branchs = res;
    }));

    // managers
    this.subscriptions.add(this.facadeService.adminFacadeService.manageStaffService.list().subscribe((res: Option[]) => {
      this.managers = res;
    }));

    // transport types
    this.subscriptions.add(this.facadeService.adminFacadeService.transportTypesService.list().subscribe((result: TransportType[]) => {
      this.transportTypes = result;
    }));

    this.listGeoFences();
  }

  isValidPhoneNumber(valid: boolean): void {
    this.validPhoneNumber = valid;
  }

  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  listGeoFences() {
    this.subscriptions.add(this.facadeService.adminFacadeService.geoFencesService.list().subscribe(result => {
       
        if (result) {
        this.fences = result.map(zone => {
          return {
            geoFenceId: zone['id'],
            geoFenceName: zone['name']
          }
        });

        if (this.isEditMode) {
          let selectedPZones = [];
          this.row.driverPickUpGeoFences.forEach(z => {
            const zone = this.fences.find(zone => zone.geoFenceId == z.geoFenceId);
            if (zone) {
              selectedPZones.push(zone);
            }
          });

          if (this.row.allPickupGeoFences) {
            this.form.get("driverPickUpGeoFences").setValue(['All']);
          } else {
            this.form.get('driverPickUpGeoFences').setValue(selectedPZones[0]);
          }

          let selectedDZones = [];
          this.row.driverDeliveryGeoFences.forEach(z => {
            const zone = this.fences.find(zone => zone.geoFenceId == z.geoFenceId);
            if (zone) {
              selectedDZones.push(zone);
            }
          });

          if (this.row.allDeliveryGeoFences) {
            this.form.get("driverDeliveryGeoFences").setValue(['All']);
          } else {
            this.form.get('driverDeliveryGeoFences').setValue(selectedDZones[0]);
          }
        }
      }
    }));
  }

  /**
   * select transform type
   * 
   * 
   * @param event 
   */
  onChangeType(event: MatButtonToggleChange): void {
    this.selectedTransportionType = event.value;
  }

  onAddressChanges(address: Address): void {
    this.address = address;
  }

  driverDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.manageDriverService.get(id).subscribe(res => {
      this.row = res;

      this.form.patchValue(this.row);
      this.form.get('dateOfBirth').setValue(this.row.dateOfBirth);
      this.form.get('countryCtrl').setValue(this.row.nationality);
      this.form.get('username').clearValidators();
      this.form.removeControl('password');

      this.selectedTransportionType = this.row.transportTypeName.toLocaleLowerCase();
      this.path = `${App.backEndUrl}/${this.row.imageUrl}`;

      if (this.row.allDeliveryGeoFences) {
        this.form.patchValue({ driverPickUpGeoFences: ['All'] });
      }

      if (this.row.allDeliveryGeoFences) {
        this.form.patchValue({ driverDeliveryGeoFences: ['All'] });
      }
    }));
  }

  back() {
    this.location.back();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

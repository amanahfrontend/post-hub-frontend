import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ManageRoleComponent } from './manage-role/manage-role.component';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AccessControl } from '../../shared/models/admin/access-control';
import { FacadeService } from '../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-access-control',
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.scss']
})

export class AccessControlComponent implements OnInit {
  permissions: AccessControl;
  roles: any[] = [];
  currentRole: string = 'manager';

  displayedColumns: string[] = ['id', 'roleName', 'creationDate', 'actions'];
  dialogContent: Object;
  header: string = '';
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<AccessControl>([]);

  /**
   *
   * @param dialog
   * @param facadeService
   * @param snackBar
   */
  constructor(
    public dialog: MatDialog,
    private facadeService: FacadeService,
    private translateService: TranslateService,
    private toastrService: ToastrService
  ) {
  }

  ngOnInit() {
    this.listRoles(this.currentRole);
  }

  /**
   * list roles on role type (driver / manager)
   *
   *
   * @param type
   */
  getAllRoles(type: string) {
    // driver type
    if (type == 'driver') {
      this.facadeService.adminFacadeService.driverAccessControlService.list().subscribe((roles: any) => {
        this.dataSource = new MatTableDataSource(roles);
        this.dataSource.paginator = this.paginator;
      });

      // manager type
    } else if (type == 'manager') {
      this.facadeService.adminFacadeService.managerRolesService.list().subscribe((roles: any) => {
        this.dataSource = new MatTableDataSource(roles);
        this.dataSource.paginator = this.paginator;
      });
    }
  }

  openRoleDialog(role?: any): void {
    const AccessControlType = { 'type': this.currentRole };
    const dialogRef = this.dialog.open(ManageRoleComponent, {
      width: '50%',
      data: { AccessControlType, role },
      height: '95%'
    });

    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.type) {
        this.getAllRoles(result.type);
      }
    });
  }

  /**
   * confirm deletion
   *
   *
   * @param event
   */
  onConfirm(event: boolean, element: AccessControl): void {
     
      if (event === true) {
      this.delete(element.roleName);
    }
  }

  /**
   * delete role
   *
   *
   * @param roleName
   */
  delete(roleName) {
    // driver (agent)
    if (this.currentRole === 'driver') {
      this.facadeService.adminFacadeService.driverAccessControlService.delete(roleName).subscribe(result => {

        const filterResult = this.roles.find((element, index, array) => {
          return element['roleName'] === roleName;
        });

        const index: number = this.roles.indexOf(filterResult);
        this.roles.splice(index, 1);
        this.toastrService.success(`${roleName} ${this.translateService.instant('Role has been deleted successfully')}`);
        this.getAllRoles(this.currentRole);
      });

      // manager
    } else if (this.currentRole === 'manager') {
      this.facadeService.adminFacadeService.managerRolesService.delete(roleName).subscribe(result => {
        const filterResult = this.roles.filter((element, index, array) => {
          return element['roleName'] === roleName;
        });

        const index: number = this.roles.indexOf(filterResult[0]);
        this.roles.splice(index, 1);
        this.toastrService.success(`${roleName} ${this.translateService.instant('Role has been deleted successfully')}`);
        this.getAllRoles(this.currentRole);
      });
    }
  }

  listRoles(role: string) {
    switch (role) {
      case 'driver':
        this.header = this.translateService.instant('Driver Access Control List');
        this.getAllRoles(this.currentRole);
        break;
      case 'manager':
        this.header = this.translateService.instant('Manager Access Control List');
        this.getAllRoles(this.currentRole);
        break;
    }
  }

  selectAccessControlType(event: MatRadioChange) {
    this.currentRole = event.value;
    this.listRoles(this.currentRole);
  }
}

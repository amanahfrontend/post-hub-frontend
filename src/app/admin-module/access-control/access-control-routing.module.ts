import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AccessControlComponent } from './access-control.component';

const routes: Routes = [
  {
    path: '',
    component: AccessControlComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadAllRoles'
      }
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessControlRoutingModule { }

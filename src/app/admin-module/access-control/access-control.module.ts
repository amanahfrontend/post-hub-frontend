import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessControlRoutingModule } from './access-control-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// components
import { AccessControlComponent } from './access-control.component';
import { ManageRoleComponent } from './manage-role/manage-role.component';

import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { TranslateModule } from '@ngx-translate/core';
import { DateToTimezoneModule } from '../../shared/pipes/date-to-timezone/date-to-timezone.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

SetDirModule
@NgModule({
  declarations: [
    AccessControlComponent,
    ManageRoleComponent
  ],
  imports: [
    CommonModule,
    AccessControlRoutingModule,

    // forms
    FormsModule,
    ReactiveFormsModule,

    //material 
    TranslateModule,
    HeaderAsCardModule,

    // shared
    ConfirmDeletionModule,
    DateToTimezoneModule,
    MaterialModule,

    // plugins
    PermissionsModule,
    SetDirModule
  ],
  entryComponents: [
    ManageRoleComponent
  ]
})
export class AccessControlModule { }

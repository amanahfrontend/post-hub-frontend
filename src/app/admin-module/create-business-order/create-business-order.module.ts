import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CreateBusinessOrderRoutingModule } from './create-business-order-routing.module';

// shared
import { MaterialModule } from '../../shared/modules/material/material.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { BarcodeModule } from '@app/shared/components/barcode/barcode.module';

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FilterModule } from 'app/shared/pipes/filter/filter.module';
//import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';



// components
import { CreateBusinessOrderComponent } from './create-business-order.component';
import { BusinessOrderInformationComponent } from './business-order-information/business-order-information.component';
import { InputCounterModule } from '../../shared/components/input-counter/input-counter.module';
import { LoadPreviousOrderComponent } from './load-previous-order/load-previous-order.component';
import { ItemsDetailsComponent } from './items-details/items-details.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ManageItemComponent } from './manage-item/manage-item.component';
import { AddressModule } from '../../shared/components/address/address.module';
import { PrintReceiptForMailitemsComponent } from './print-receipt-for-mailitems/print-receipt-for-mailitems.component';

@NgModule({
  declarations: [
    CreateBusinessOrderComponent,
    BusinessOrderInformationComponent,
    LoadPreviousOrderComponent,
    ItemsDetailsComponent,
    ManageItemComponent,
    PrintReceiptForMailitemsComponent,
  ],
  imports: [
    CommonModule,
    CreateBusinessOrderRoutingModule,
    MaterialModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    InputCounterModule,
    HeaderAsCardModule,
    ConfirmDeletionModule,
    NgxBarcodeModule,
    BarcodeModule,
    SetDirModule,
    AddressModule,
    NgxMatSelectSearchModule,
    FilterModule

    
  ],
  entryComponents: [
    LoadPreviousOrderComponent,
    ManageItemComponent
  ]
})
export class CreateBusinessOrderModule { }

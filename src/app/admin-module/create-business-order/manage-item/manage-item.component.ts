import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '@app/services/facade.service';
import { RFQItems } from '@app/shared/models/admin/RFQItems';
import { WeightUOM } from '@app/shared/models/admin/weight-uom';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Address } from '../../../shared/models/components';
import { Option } from './../../../shared/models/admin/option';

@Component({
  selector: 'manage-item',
  templateUrl: './manage-item.component.html',
  styleUrls: ['./manage-item.component.scss']
})
export class ManageItemComponent implements OnInit {

  form: FormGroup;
  operation: string = '';
  address: Address;
  isValidAddress: boolean = false;
  submitted: boolean = false;
  weightUOMs: WeightUOM[] = [];
  countries: Option[] = [];
  rfqItems: RFQItems[] = [];
  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<ManageItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private facadeService: FacadeService,
  ) {
    this.form = this.fb.group({
      id: [0, [Validators.nullValidator]],
      mailItemBarCode: ['', [Validators.required]],
      consigneeInformation: [''],
      weightUOMId: [null],
      quantity: [null],
      totalPrice: [null],
      consigneeMobile: [''],
      weight: [''],
      filterCountryCtrl: [''],
      countryId: [null],
    });

    this.operation = this.translateService.instant('Add');

    if (this.data.isEdit) {
      this.form.patchValue(this.data.row);
      this.isValidAddress = true;
      this.operation = this.translateService.instant('Edit');
    }
  }

  getAllContries() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllCountiesAsync().subscribe((res: Option[]) => {
      this.countries = res;
    });
  }

  ngOnInit(): void {

    this.getAllContries();
    this.listWieghtUOM();
    if (this.data.internationalId == 3 && this.data.businessCustomerId > 0)
      this.getRFQItemsByBusinessCustomerId(this.data.businessCustomerId)

  }
  getRFQItemsByBusinessCustomerId(businessCustomerId: number) {
    this.facadeService.adminFacadeService.managePriceListService.getRFQItemsByBusinessCustomerId(businessCustomerId).
      subscribe(res => {
        this.rfqItems = res;


      });
  }
  onAddressChanges(address: Address): void {
    this.address = address;
  }

  validAddress(isValidAddress: boolean): void {
    this.isValidAddress = isValidAddress;
  }

  listWieghtUOM() {
    this.facadeService.adminFacadeService.weightUOMService.listActive().subscribe(
      res => {
        this.weightUOMs = res
      })
  }
  optionSelected(event: any, type: string): void {
    const id = Number(event.value)

    switch (type) {
      case 'countryId':
        let weight = this.form.value.weight;
        let quantity = this.form.value.quantity;

        let rfqItem = this.rfqItems.find(c => Number(c.weight) == Number(weight) && c.countryId == id)
        if (rfqItem) {
          let totalPrice = Number(rfqItem.toGoShippingPrice) * Number(quantity);
          this.form.get('totalPrice').setValue(totalPrice)
        }
        else {
          this.toastrService.error('there is not price available for this country please contact admin ');

        }

        break;

      default:
        break;
    }
  }
  submit() {
    this.submitted = true;

    if (!this.isValidAddress && this.submitted) {
      this.toastrService.error('Address not valid!');
      return;
    }
    let body
    if (this.address) {
      body = Object.assign(this.form.value, this.address);

    } else {
      body = Object.assign(this.form.value, this.data.row);

    }
    if (this.data.internationalId == 3) {
      if (this.form.value.totalPrice) {
        this.dialogRef.close({ operation: this.operation, item: body });

      }
      else {
        this.toastrService.error('please Select Country that have Price for Shipping with same weight you entered');

      }
    }
    else {
      this.dialogRef.close({ operation: this.operation, item: body });

    }

  }
}

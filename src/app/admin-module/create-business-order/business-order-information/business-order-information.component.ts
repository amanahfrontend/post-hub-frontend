import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Branch } from 'app/shared/models/admin/branch';
import { FacadeService } from 'app/services/facade.service';
import { BusinessOrder } from 'app/shared/models/admin/businessOrder';
import { BusinessCustomerBranch, BusinessInfo } from 'app/shared/models/admin/business-customer';
import { Driver } from 'app/shared/models/admin/driver';
import { Contract } from 'app/shared/models/admin/contract';
import { ServiceSector } from '@app/shared/models/admin/service-sector';
import { Courier } from '@app/shared/models/admin/courier';
import { Address } from '../../../shared/models/components';

declare var $: any;

interface Option {
  id: number;
  name: string;
}

@Component({
  selector: 'business-order-information',
  templateUrl: './business-order-information.component.html',
  styleUrls: ['./business-order-information.component.scss']
})
export class BusinessOrderInformationComponent implements OnInit, AfterViewInit, OnChanges {
  minDate: Date;
  maxDate: Date;
  address: Address;
  isValidAddress: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  formInternational: FormGroup;
  businessTypes: { id: number, name_ar: string, name_en: string }[] = [];
  isEditMode: boolean = false;
  pickupLocations = [
    { id: 1, name_ar: "عنوان العميل", name_en: "Customer Address" },
    { id: 2, name_ar: " مكتب الشركة", name_en: "Company office" }
  ]

  pickupRequestNotifications = [
    { id: 1, name_ar: "المرسل ", name_en: "Dispatcher" },
    { id: 2, name_ar: " سائق", name_en: "Driver" },
    { id: 3, name_ar: " المرسل والسائق", name_en: "Dispatcher & Driver" }

  ]
  // branches: Option[] = [];
  branchId: number = 0;
  addressDate: any;
  pickUpAddress: any;

  SelectedBranch: Branch;

  show: boolean = false;
  showForCreate: boolean = false;
  isDisabled: boolean = false;
  isDisabledCom: boolean = true;
  row: BusinessOrder;
  orderPlacementDateTime: Date;
  readyForPickupAtDateTime: Date;
  deliveryBeforeDateTime: Date;
  locale: string = '';
  branches: BusinessCustomerBranch[] = [];
  attachments: File[] = [];
  filesToDisplay: (string | ArrayBuffer)[] = [];
  accept: string = '.xlsx, .xls, .csv, image/*';
  businessCustomers: BusinessInfo[] = [];
  businessCustomer: BusinessInfo;
  drivers: Driver[] = [];
  serviceSectors: ServiceSector[] = []
  couriers: Courier[] = [];
  departments: any[] = [];
  contracts: Contract[] = [];
  contract: Contract;
  businessOrder: BusinessOrder;
  businessCustomerContacts: any[] = []
  id: number = 0;
  /////////selected
  selectedBusinessCustomerCode: BusinessInfo;
  selectedBusinessCustomerName: BusinessInfo;
  selectedBusinessTypes: any;
  selectedBranch: BusinessCustomerBranch;
  selecteddepartment: any;
  selectedContract: Contract;
  selectedbusinessCustomerContacts: any;
  selectedFile: File;
  path: (string | ArrayBuffer) = "" //"./assets/img/faces/avtar.jpeg"
  convertDate: String;
  myAddressChecked: boolean = true;
  newAddressChecked: boolean = false;
  //
  @Output() onFormOrder: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() onFormInternational: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() onChangeFile = new EventEmitter<any>();
  @Output() onChangeAddress = new EventEmitter<any>();
  @Output() onChangeInternationId = new EventEmitter<any>();
  @Output() onChangeBusinessCustomerId = new EventEmitter<any>();




  @Input() validateForm: boolean = false;
  @Input() orderId: number = 0;
  @Output() businessCustomersForPervious: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
    private facadeService: FacadeService,
  ) {
    this.form = this.fb.group({
      // id:this.id,
      readyAt: new FormControl(''),
      startedAt: new FormControl(''),
      deliveryBefore: new FormControl(''),

      businessCustomerBranchId: [null, [Validators.required]],
      hQBranchName: ['', [Validators.nullValidator]],

      businessCustomerName: ['', [Validators.nullValidator]],
      businessTypeId: [null, [Validators.required]],
      businessTypeName: ['', [Validators.nullValidator]],
      orderById: [null, [Validators.nullValidator]],
      orderByName: ['', [Validators.nullValidator]],
      pickupRequestNotificationId: [null, [Validators.nullValidator]],
      departmentId: [null, [Validators.nullValidator]],
      departmentName: ['', [Validators.nullValidator]],
      businessOrderId: [null, [Validators.nullValidator]],
      businessOrderName: ['', [Validators.nullValidator]],
      businessCustomerId: [null, [Validators.required]],
      serviceSectorId: [null, [Validators.nullValidator]],
      sectorTypeId: ['', [Validators.nullValidator]],
      isMyAddress: [true, [Validators.nullValidator]],
    });
    this.formInternational = this.fb.group({

      airWayBillNumber: ['', [Validators.required]],
      courierId: [null, [Validators.required]],
      courierTrackingNumber: ['', [Validators.required]],

    })
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    });

  }

  ngOnInit(): void {
    this.getBusinessTypes();
    // this.getAllDrivers();
    // this.getBusinessCustomers();
    this.getAllDepartment();


    // this.getContracts();
    this.id = this.orderId;
    if (this.id > 0) {
      this.getAllCouriers();
      this.isDisabled = true;
      this.getOrderById(this.id);
    }

    this.getAllServiceSectors();

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
  }
  getOrderById(id: number) {
    this.facadeService.adminFacadeService.businessOrderService.get(id).subscribe(
      res => {
        this.businessOrder = res
        ///////////////////////
        if (res.businessCustomerId) {
          this.getBusinessCustomerBranch(res.businessCustomerId);
          this.getBusinessCustomerContacts(res.businessCustomerId);
          this.onChangeBusinessCustomerId.emit(res.businessCustomerId);

        }
        if (res.businessTypeId)
          this.getAllByBussinesTypeId(res.businessTypeId)

        this.form.patchValue(this.businessOrder);
        this.onChangeInternationId.emit(res.serviceSectorId);


        this.formInternational.get('airWayBillNumber').setValue(res.airWayBillNumber ? res.airWayBillNumber : '');
        this.formInternational.get('courierId').setValue(res.courierId ? res.courierId : '');
        this.formInternational.get('courierTrackingNumber').setValue(res.courierTrackingNumber ? res.courierTrackingNumber : '');
        
        this.branchId=res.businessCustomerBranchId;

        if (res.isMyAddress == false) {
          this.myAddressChecked = false;
          this.newAddressChecked = true;

          this.addressDate = res.pickUpAddress
          this.pickUpAddress=res.pickUpAddress
          this.onChangeAddress.emit(this.addressDate);

        }


      });

  }

  setBusinessCustomerCode(businessCustomerCode: BusinessInfo) {
    // return value ? this.businessCustomers.find(_ => _.id === value).businessCode : undefined;
    this.selectedBusinessCustomerCode = businessCustomerCode;
    this.form.get('businessCustomerId').setValue(businessCustomerCode.id);
    this.form.get('businessCode').setValue(businessCustomerCode.businessCode);
    this.getBusinessCustomerBranch(businessCustomerCode.id);
    this.getBusinessTypesByCustomerId(businessCustomerCode.id);
    //this.getOrderByCustomerId(businessCustomerCode.id);
    this.getBusinessCustomerById(businessCustomerCode.id);
    this.getBusinessCustomerContacts(businessCustomerCode.id);
  }

  setBusinessName(businessCustomerName: BusinessInfo) {
    this.selectedBusinessCustomerName = businessCustomerName;
    this.form.get('businessCustomerId').setValue(businessCustomerName.id);
    this.getBusinessCustomerBranch(businessCustomerName.id);
    //  this.getBusinessTypesByCustomerId(businessCustomerName.id);
    //  this.getBusinessCustomerById(businessCustomerName.id);
    this.getBusinessCustomerContacts(businessCustomerName.id);
  }

  setBusinessIndustry(businessTypes: any) {
    this.form.get('businessTypeId').setValue(businessTypes.id);

    this.selectedBusinessTypes = businessTypes;
  }

  setBranch(branch: BusinessCustomerBranch) {
    this.form.get('businessCustomerBranchId').setValue(branch.id);
    this.selectedBranch = branch;
  }

  setDepartment(department: any) {
    this.form.get('departmentId').setValue(department.id);
    this.selecteddepartment = department;
  }

  setContractCode(contract: Contract) {
    this.form.get('contractId').setValue(contract.id);
    this.selectedContract = contract;
  }
  setOrderedBy(businessCustomerContacts: any) {

    this.form.get('orderById').setValue(businessCustomerContacts.id);
    this.selectedbusinessCustomerContacts = businessCustomerContacts;
  }

  getContracts() {
    this.facadeService.adminFacadeService.contractService.list().subscribe(res => {
      this.contracts = res;
    });
  }

  getContractStatus(contractId: number) {
    this.form.get('contractStatus').setValue('');
    this.facadeService.adminFacadeService.contractService.get(contractId).subscribe(res => {
      this.contract = res;
      this.form.get('contractStatus').setValue(res.contractStatusName);

    });
  }

  getBusinessTypes() {
    this.facadeService.sharedFacadeService.businessTypesService.list().subscribe((businessTypes: { id: number, name_ar: string, name_en: string }[]) => {
      this.businessTypes = businessTypes;
    });
  }

  getBusinessTypesByCustomerId(businessCustomerId: number) {
    this.facadeService.sharedFacadeService.businessTypesService.GetAllByBusinessCustomerId(businessCustomerId).subscribe((businessTypes: { id: number, name_ar: string, name_en: string }[]) => {
      this.businessTypes = businessTypes;
    });
  }
  getOrderByCustomerId(businessCustomerId: number) {
    this.facadeService.adminFacadeService.businessOrderService.GetOrderByCustomerId(businessCustomerId).subscribe(
      res => {
        // this.businessCustomer = res;
        if (res) {
          this.form.get('businessTypeName').setValue(res.businessTypeName);
          this.form.get('orderByName').setValue(res.orderByName);
          this.form.get('departmentName').setValue(res.departmentName);
          this.form.get('businessCustomerCode').setValue(res.businessCustomerCode);
          this.form.get('businessCustomerName').setValue(res.businessCustomerName);
          this.form.get('hQBranchName').setValue(res.hqBranchName);
          this.form.get('contractCode').setValue(res.contractCode);
          this.form.get('businessCID').setValue(res.businessCID);
          this.form.get('contractId').setValue(res.contractId);
          this.form.get('businessCustomerId').setValue(res.businessCustomerId);
          this.form.get('businessCustomerBranchId').setValue(res.businessCustomerBranchId);
          this.form.get('businessTypeId').setValue(res.businessTypeId);
          this.form.get('orderById').setValue(res.orderById);

        }
      });
  }
  getBusinessCustomerContacts(businessCustomerId: number) {
    this.facadeService.adminFacadeService.businessCustomerContactService.list(businessCustomerId).subscribe(res => {
      this.businessCustomerContacts = res;
    });
  }

  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.list().subscribe(res => {
      this.businessCustomers = res;
      this.businessCustomersForPervious.emit(res);

    });
  }

  getAllDrivers() {
    this.facadeService.adminFacadeService.manageDriverService.list().subscribe(res => {
      this.drivers = res;
    });
  }

  getAllServiceSectors() {
    this.facadeService.adminFacadeService.serviceSectorService.getAllActive().subscribe(
      res => {
        this.serviceSectors = res;
      });
  }

  getAllCouriers() {
    this.facadeService.adminFacadeService.courierService.listActive().subscribe(res => {
      this.couriers = res;
    });
  }

  getAllDepartment() {
    this.facadeService.departmentService.list().subscribe((res: Option[]) => {
      this.departments = res;
    });
  }
  getAllByBussinesTypeId(id: number) {
    this.facadeService.adminFacadeService.businessCustomerService.getAllByBussinesTypeId(id).subscribe(
      res => {
        this.businessCustomers = res;
      });
  }
  getBusinessCustomerById(id: number) {

    this.form.get('businessTypeName').setValue('');
    this.form.get('orderByName').setValue('');
    this.form.get('hQBranchName').setValue('');
    this.form.get('contractCode').setValue('');
    this.form.get('businessCID').setValue('');
    this.form.get('contractId').setValue('');
    this.form.get('businessCustomerId').setValue('');
    this.form.get('businessCustomerBranchId').setValue('');
    this.form.get('businessTypeId').setValue('');
    this.form.get('orderById').setValue('');

    this.facadeService.adminFacadeService.businessCustomerService.getById(id).subscribe(
      res => {
        this.businessCustomer = res;

        if (res) {
          this.form.get('businessCustomerName').setValue(res.businessCustomerName);
          this.form.get('businessTypeName').setValue(res.businessTypeName);
          this.form.get('orderByName').setValue(res.orderByName);
          this.form.get('businessCode').setValue(res.businessCode);
          this.form.get('hQBranchName').setValue(res.hqBranchName);
          this.form.get('contractCode').setValue(res.contractCode);
          this.form.get('businessCID').setValue(res.businessCID);
          this.form.get('contractId').setValue(res.contractId);
          this.form.get('businessCustomerId').setValue(res.businessCustomerId);
          this.form.get('hqBranchId').setValue(res.hqBranchId);
          this.form.get('businessTypeId').setValue(res.businessTypeId);
          this.form.get('orderById').setValue(res.orderById);
        }
      });
  }

  getBusinessCustomerBranch(customerId: number) {
    this.facadeService.adminFacadeService.businessCustomerBranchService.list(customerId)
      .subscribe(res => {
        this.branches = res;
        if (this.form.value.isMyAddress) {
          let location = res.find(c => c.id == this.form.value.businessCustomerBranchId).location;
          this.addressDate = location
          this.onChangeAddress.emit(this.addressDate);
        }
      });
  }

  ngAfterViewInit() {
    this.initDatePicker();

    this.form.valueChanges.subscribe(res => {
      this.onFormOrder.emit(this.form);
    });
    this.formInternational.valueChanges.subscribe(res => {
      this.onFormInternational.emit(this.formInternational)
    });
  }

  initDatePicker() {
    $(document).ready(() => {
      $('.orderPlacementDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          // format date here
          this.orderPlacementDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });

      $('.readyForPickupAtDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          // format date here
          this.readyForPickupAtDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });

      $('.deliveryBeforeDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          // format date here
          this.deliveryBeforeDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });
    });
  }

  /**
   * count of items
   * 
   * 
   * @param count 
   */
  onCount(count: number) {
    // code runs here
  }

  datechange(e, manageDate) {
    this.convertDate = new Date(e.target.value).toString();//.toISOString().substring(0, 10);

    switch (manageDate) {
      case 'readyAt':
        this.form.get('readyAt').patchValue(this.convertDate, {
          onlyself: true
        })
        break;

      case 'startedAt':
        this.form.get('startedAt').patchValue(this.convertDate, {
          onlyself: true
        })
        break;

      case 'deliveryBefore':
        this.form.get('deliveryBefore').patchValue(this.convertDate, {
          onlyself: true
        })
        break;


      default:
        break;
    }
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];
      this.onChangeFile.emit(this.selectedFile);

      this.filesToDisplay.push(this.selectedFile.name);
      this.attachments.push(this.selectedFile);

      // const reader = new FileReader();
      // reader.readAsDataURL(file);
      // reader.onload = (event) => {
      //   this.filesToDisplay.push(event.target.result);
      // }
    }
    // if (event.target.files && event.target.files[0]) {
    //   this.selectedFile = event.target.files[0];
    //   this.onChangeFile.emit(this.selectedFile);
    //   var reader = new FileReader();
    //   reader.readAsDataURL(event.target.files[0]);
    //   reader.onload = (event) => {
    //     this.path = event.target.result;
    //   }
    // }
  }

  /**
   * remove file using it's name
   * 
   * 
   * @param fileName 
   */
  remove(fileName: string): void {
    const index = this.filesToDisplay.indexOf(fileName);
    this.filesToDisplay.splice(index, 1);
    this.attachments.splice(index, 1);
  }

  radioChange($event: any) {
    console.log($event.source.name, $event.value);

    if ($event.value == '2') {
      this.myAddressChecked = false;
      this.form.get('isMyAddress').setValue(false);
      if(this.id>0){
        this.addressDate = this.pickUpAddress
      
        this.onChangeAddress.emit(this.addressDate);
      }
      else{
        this.addressDate = {};

      }
    }
    if ($event.value == '1') {
      this.myAddressChecked = true;
      this.addressDate = {};
      let location = this.branches.find(c => c.id == this.branchId).location;
      this.addressDate = location
      this.onChangeAddress.emit(this.addressDate);
      this.form.get('isMyAddress').setValue(true);

    }
  }


  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const id = Number(event)
    //) event.option.value;




    switch (type) {
      case 'autoCustomerCode':
        // @TODO code runs here 
        // this.getBusinessCustomerBranch(id);
        // this.getBusinessTypesByCustomerId(id);
        // this.getBusinessCustomerById(id);
        // this.getBusinessCustomerContacts(id);
        break;

      case 'autoBusinessIndustry':
        // @TODO code runs here 
        this.getAllByBussinesTypeId(id)
        if (this.id > 0) {
          this.form.get('businessTypeId').setValue(id);

        }
        break;

      case 'autoBusinessName':
        // @TODO code runs here 
        this.getBusinessCustomerBranch(id);
        this.getBusinessCustomerContacts(id);
        if (this.id > 0) {
          this.form.get('businessCustomerId').setValue(id);

        }
        this.onChangeBusinessCustomerId.emit(id);
        break;
      case 'autoBranch':
        // @TODO code runs here 
        if (this.id > 0) {
          this.form.get('businessCustomerBranchId').setValue(id);
        }
        this.branchId = id;
        let location = this.branches.find(c => c.id == id).location;
        this.addressDate = location
        this.onChangeAddress.emit(this.addressDate);
        break;

      case 'autoDepartment':
        // @TODO code runs here 
        if (this.id > 0) {
          this.form.get('departmentId').setValue(id);
        }
        break;

      case 'autoContractCode':
        // @TODO code runs here 
        this.getContractStatus(id);
        break;

      case 'autoOrderedBy':
        // @TODO code runs here 
        if (this.id > 0) {
          this.form.get('orderById').setValue(id);
        }
        break;

      case 'autoDriverCode':
        // @TODO code runs here 
        break;
      case 'autoServiceSector':
        if (id == 3 && this.id > 0) {
          this.show = true
        }
        else {
          this.show = false

        }
        if (id == 3) {
          this.showForCreate = true
          this.onChangeInternationId.emit(id);
        }
        // else {
        //   this.showForCreate = false

        // }

        break;

      default:
        break;
    }
  }

  optionSelectedBranch(event: MatAutocompleteSelectedEvent, type: string): void {
    const value = event.option.value;
    // switch (type) {
    //   case 'autoCustomerCode':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoBusinessIndustry':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoBusinessName':
    //     // @TODO code runs here 
    //     break;
    //   case 'autoBranch':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoDepartment':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoContractCode':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoOrderedBy':
    //     // @TODO code runs here 
    //     break;

    //   case 'autoDriverCode':
    //     // @TODO code runs here 
    //     break;
    //   default:
    //     break;
    // }
  }

  onAddressChanges(address: Address): void {
    this.address = address;
    this.onChangeAddress.emit(this.address)
  }

  validAddress(isValidAddress: boolean): void {
    this.isValidAddress = isValidAddress;
  }

  /**
   * 
   * @param simpleChanges 
   */
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.validateForm) {
      if (simpleChanges.validateForm.currentValue) {
        this.form.markAllAsTouched();
      }
    }

    if (this.orderId > 0) {
      this.getOrderById(this.orderId);
    }
  }
}

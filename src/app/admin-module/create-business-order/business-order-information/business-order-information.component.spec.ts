import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessOrderInformationComponent } from './business-order-information.component';

describe('BusinessOrderInformationComponent', () => {
  let component: BusinessOrderInformationComponent;
  let fixture: ComponentFixture<BusinessOrderInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessOrderInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessOrderInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

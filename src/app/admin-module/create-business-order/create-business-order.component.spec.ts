import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBusinessOrderComponent } from './create-business-order.component';

describe('CreateBusinessOrderComponent', () => {
  let component: CreateBusinessOrderComponent;
  let fixture: ComponentFixture<CreateBusinessOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateBusinessOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBusinessOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

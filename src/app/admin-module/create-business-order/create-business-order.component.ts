import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { BusinessOrder } from 'app/shared/models/Orders/business-order';
import { LoadPreviousOrderComponent } from './load-previous-order/load-previous-order.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinessOrder } from 'app/shared/models/admin/businessOrder';
import { PrintReceiptForMailitemsComponent } from './print-receipt-for-mailitems/print-receipt-for-mailitems.component';
import { Address } from '../../shared/models/components';


@Component({
  selector: 'create-business-order',
  templateUrl: './create-business-order.component.html',
  styleUrls: ['./create-business-order.component.scss']
})
export class CreateBusinessOrderComponent implements OnInit {
 
  address: Address;
  validateForm: boolean = false;
  businessOrder: BusinessOrder[] = [];
  form: FormGroup;
  selectedFile: File;
  formOrders: FormGroup;
  formInternational: FormGroup;
  isEditMode: boolean = false;
  row: BusinessOrder;
  itemsList: any;
  businessCustomers: any;
  totalItemCount: 0;
  itemTypeId: 0;
  filledItemNo: 0;
  remainingItemNo: 0;
  orderId: number = 0;
  internationalId: number = 0;
  businessCustomerlId: number = 0;

  subscriptions = new Subscription();
  isLiner: boolean = true;
  constructor(public dialog: MatDialog,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) {

    this.subscriptions.add(this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.isEditMode = true;
        this.orderId = Number(params.id);
      } else {
        this.isEditMode = false;
      }
    }));
    this.form = this.fb.group({});
    this.formInternational = this.fb.group({});

  }

  ngOnInit(): void {

    this.formOrders = this.fb.group({
      businessTypeId: [null, [Validators.required]],
    });
  }
  onFormOrder(form: FormGroup) {
    this.formOrders = form;
  }
  onFormInternational(form: FormGroup) {
    this.formInternational = form;
  }
  onForm(form: FormGroup) {
    // @TODO code runs here
    this.form = form;
    console.log('form', form);
  }
  onChangeInternationId(id:any){
     this.internationalId=Number(id);
  }
  onChangeBusinessCustomerId(id:number){
    this.businessCustomerlId=id;
  }
  onChangeAddress(address: Address) {
    this.address = address;

  }
  onChangeFile(file: File) {
    this.selectedFile = file;
  }
  itemList(items: any) {
    this.itemsList = items
    console.log(items)

  }
  businessCustomersForPervious(businessCustomerList: any) {
    this.businessCustomers = businessCustomerList;
    console.log(this.businessCustomers)
  }


  addOrderInformation(isDraft: boolean) {
    if (this.form.invalid || this.formOrders.invalid || this.formInternational.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }


    if (isDraft == false) {
      this.totalItemCount = this.form.value ? this.form.controls['totalItemCount'].value : 0;
      this.itemTypeId = this.form.value ? this.form.controls['itemTypeId'].value : 0;
      this.filledItemNo = this.form.value ? this.form.controls['filledItemNo'].value : 0;
      this.remainingItemNo = this.form.value ? this.form.controls['remainingItemNo'].value : 0;
    }
 
    if (this.totalItemCount < this.filledItemNo) {
      return this.toastrService.error(this.translateService.instant('Mailitems count must equal ' + this.totalItemCount + ' items'));
    }

    if (this.totalItemCount > this.filledItemNo) {
      let remaining = this.totalItemCount - this.filledItemNo;
      return this.toastrService.error(this.translateService.instant('Please Fill All Items There are ' + remaining + ' items remaining'));
    }

    let newItemList = this.itemsList ? this.itemsList.map(obj => ({
      consigneeInfo: {
        firstName: obj.consigneeInformation ? obj.consigneeInformation : "",
        mobile: obj.consigneeMobile ? obj.consigneeMobile.toString() : ""
      },
      pickUpAddress: this.address,
      dropOffAddress: {
        area: obj.area ? obj.area.toString() : "",
        governorate: obj.governorate ? obj.governorate.toString() : "",
        street: obj.street ? obj.street.toString() : "",
        block: obj.block ? obj.block.toString() : "",
        flat: obj.flat ? obj.flat.toString() : "",
        floor: obj.floor ? obj.floor.toString() : "",
        building: obj.building ? obj.building.toString() : "",
      },
      mailItemBarCode: obj.mailItemBarCode ? obj.mailItemBarCode.toString() : "",
      itemApproxWeight: obj.itemApproxWeight ? obj.itemApproxWeight : "",
      weight: Number(obj.weight) ? Number(obj.weight) : null,
      fromSerial: obj.fromSerial ? obj.fromSerial.toString() : "",
      pickupDate: obj.pickupDate ? new Date(obj.pickupDate) : null,
      caseNo: obj.case ? Number(obj.caseNo) : null,
      weightUOMId: obj.weightUOMId ? Number(obj.weightUOMId) : null,
      id: obj.id ? Number(obj.id) : 0,

      quantity: obj.quantity,
      totalPrice: obj.totalPrice,
      countryId: obj.countryId,
      //
    })) : [];


    let readyAt = this.formOrders.value.readyAt ? new Date(this.formOrders.value.readyAt).toLocaleDateString()
    : null;
    let startedAt = this.formOrders.value.startedAt ? new Date(this.formOrders.value.startedAt).toLocaleDateString() : null;
    let deliveryBefore = this.formOrders.value.deliveryBefore ? new Date(this.formOrders.value.deliveryBefore).toLocaleDateString() : null;


    let body = {
      ... this.formOrders.value, mailItems: newItemList,
      pickupLocationId: +this.formOrders.value.pickupLocationId ? +this.formOrders.value.pickupLocationId : null,
      pickupRequestNotificationId: +this.formOrders.value.pickupRequestNotificationId ? +this.formOrders.value.pickupRequestNotificationId : null,
      businessCustomerId: +this.formOrders.value.businessCustomerId ? +this.formOrders.value.businessCustomerId : null,
      businessTypeId: +this.formOrders.value.businessTypeId ? +this.formOrders.value.businessTypeId : null,
     // hqBranchId: +this.formOrders.value.hqBranchId ? +this.formOrders.value.hqBranchId : null,
      businessCustomerBranchId: +this.formOrders.value.businessCustomerBranchId ? +this.formOrders.value.businessCustomerBranchId : null,
      departmentId: +this.formOrders.value.departmentId ? +this.formOrders.value.departmentId : null,
      contractId: +this.formOrders.value.contractId ? +this.formOrders.value.contractId : null,
      orderById: +this.formOrders.value.orderById ? +this.formOrders.value.orderById : null,
    };
    body.airWayBillNumber = this.formInternational.value.airWayBillNumber ? +this.formInternational.value.airWayBillNumber : null
    body.courierId = this.formInternational.value.courierId ? +this.formInternational.value.courierId : null
    body.courierTrackingNumber = this.formInternational.value.courierTrackingNumber ? +this.formInternational.value.courierTrackingNumber : null

    body.totalItemCount = Number(this.totalItemCount);
    body.itemTypeId = Number(this.itemTypeId);
    body.pickUpAddress = this.address;

    body.readyAt = readyAt;
    body.startedAt = startedAt;
    body.deliveryBefore = deliveryBefore;

    if (this.selectedFile) {
      body.orderFile = this.selectedFile;
    }
    if (this.isEditMode) {
      body.id = this.orderId;
    }


    if (this.orderId > 0) {
      this.facadeService.adminFacadeService.businessOrderService.update(body.id, body).subscribe(
        res => {
          this.toastrService.success(this.translateService.instant('Order Information Updated Successfully'));
          this.router.navigate(['manageOrder']);

        });
    } else {
      if (isDraft) {
        this.facadeService.adminFacadeService.businessOrderService.createBusinessOrderDraft(body).subscribe(
          res => {

            this.toastrService.success(this.translateService.instant('New Order Information Added Successfully'));

            this.router.navigate(['manageOrder']);

          });
      }
      else {
        if (newItemList.length > 0) {


          this.facadeService.adminFacadeService.businessOrderService.create(body).subscribe(
            res => {

              this.toastrService.success(this.translateService.instant('New Order Information Added Successfully'));
              this.router.navigate(['manageOrder']);

            });
        }
        else {
          this.toastrService.error(this.translateService.instant('please add item/select file items'));

        }
      }

    }
  }
  loadPreviousOrders(): void {
    const dialogRef = this.dialog.open(LoadPreviousOrderComponent, {
      width: '30%',
      panelClass: 'custom-dialog-container',
      height: 'calc(55vh - 50px)',
      data: { vals: this.businessCustomers }
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.orderId = Number(result.data.draftOrderId);

      }
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPreviousOrderComponent } from './load-previous-order.component';

describe('LoadPreviousOrderComponent', () => {
  let component: LoadPreviousOrderComponent;
  let fixture: ComponentFixture<LoadPreviousOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadPreviousOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPreviousOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from 'app/services/facade.service';
import { BusinessOrderLoad } from 'app/shared/models/admin/business-order-load';


@Component({
  selector: 'load-previous-order',
  templateUrl: './load-previous-order.component.html',
  styleUrls: ['./load-previous-order.component.scss']
})
export class LoadPreviousOrderComponent implements OnInit {

  form: FormGroup;
  orders:BusinessOrderLoad[]=[];
  orderList:any;
  action:string;
  businessCustomers:any[]=[];
  selectedBusinessOrder: BusinessOrderLoad;
  selectedCode: BusinessOrderLoad;
  constructor(
    public dialogRef: MatDialogRef<LoadPreviousOrderComponent>,
    private fb: FormBuilder,
    private facadeService: FacadeService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.fb.group({

      businessCustomerId:[null],
      draftOrderId:[null,[Validators.required]]

    });



  }

  ngOnInit(): void {
    this.businessCustomers =this.data.vals;
   // this.action = this.local_data.action;
   

    this. getAllDraftBusinessOrder();
  }
  setCode(code:BusinessOrderLoad){
   this.selectedCode=code;

  }
  setBusinessCustomer=(businessCustomer:any)=>{
    this.selectedBusinessOrder=businessCustomer;
    let result = this.orders.filter(x => x.businessCustomerId ==businessCustomer.id);
    this.orders= result;
   }
 
 
  submit() {
    this.dialogRef.close({event:this.action,data:{businessCustomerId:this.selectedBusinessOrder?this.selectedBusinessOrder.id:null
      ,draftOrderId:this.selectedCode?this.selectedCode.id:null}});
  }
 
  getAllDraftBusinessOrder=()=>{
    this.facadeService.adminFacadeService.businessOrderService.getAllDraftBusinessOrder().subscribe(res => {
      this.orders = res;
     
    });
  }
  /**
   * auto complete
   * 
   * 
   * @param keywords 
   * @param type (which atuo complete selected) 
   */
  onSearch(keywords: string, type: string): void {
    console.log('keywords', keywords);

    switch (type) {
      case 'autoCustomerCode':
      
        break;

      case 'autoContractCode':
        // @TODO service here ...
        break;
      default:
        break;
    }
  }

  /**
  * after selecting option 
  * 
  * 
  * @param keywords 
  * @param type (which atuo complete selected) 
  */
  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const value = event.option.value;
    console.log('value', value);

    switch (type) {
      case 'autoCustomerCode':
        // @TODO code runs here 
        break;

      case 'autoContractCode':
        // @TODO code runs here 
        break;

      default:
        break;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintReceiptForMailitemsComponent } from './print-receipt-for-mailitems.component';

describe('PrintReceiptForMailitemsComponent', () => {
  let component: PrintReceiptForMailitemsComponent;
  let fixture: ComponentFixture<PrintReceiptForMailitemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintReceiptForMailitemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintReceiptForMailitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BusinessOrder } from '@app/shared/models/admin/businessOrder';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'print-receipt-for-mailitems',
  templateUrl: './print-receipt-for-mailitems.component.html',
  styleUrls: ['./print-receipt-for-mailitems.component.scss']
})
export class PrintReceiptForMailitemsComponent implements OnInit {

  loading: boolean = false;
  mailItems: BusinessOrder[];

  constructor(@Inject(DOCUMENT) private document: Document,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
    console.log('data', data);

    this.mailItems = this.data.mailItems;
  }

  ngOnInit(): void {
  }

  printScreen() {
    this.loading = true;

    const htmlWidth = $("#print-section").width();
    const htmlHeight = $("#print-section").height();

    const topLeftMargin = 15;

    let pdfWidth = htmlWidth + (topLeftMargin * 2);
    let pdfHeight = (pdfWidth * 1.5) + (topLeftMargin * 2);

    const canvasImageWidth = htmlWidth;
    const canvasImageHeight = htmlHeight;

    const totalPDFPages = Math.ceil(htmlHeight / pdfHeight) - 1;

    const data = this.document.getElementById('print-section');
    html2canvas(data, { allowTaint: true }).then(canvas => {

      canvas.getContext('2d');
      const imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new jsPDF('p', 'pt', [pdfWidth, pdfHeight]);
      pdf.addImage(imgData, 'png', topLeftMargin, topLeftMargin, canvasImageWidth, canvasImageHeight);

      for (let i = 1; i <= totalPDFPages; i++) {
        pdf.addPage([pdfWidth, pdfHeight], 'p');
        pdf.addImage(imgData, 'png', topLeftMargin, - (pdfHeight * i) + (topLeftMargin * 4), canvasImageWidth, canvasImageHeight);
      }

      this.loading = false;
      pdf.save(`Barcode(s) Document ${new Date().toLocaleString()}.pdf`);
    });
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateBusinessOrderComponent } from './create-business-order.component';
const routes: Routes = [
  {
    path: '',
    component: CreateBusinessOrderComponent
        
  },
  {
    path: 'create-business-order/edit/:id',
    component: CreateBusinessOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateBusinessOrderRoutingModule { }

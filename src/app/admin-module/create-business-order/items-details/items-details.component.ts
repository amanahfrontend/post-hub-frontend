import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ManageItemComponent } from './../manage-item/manage-item.component';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from '../../../shared/helpers/download-link';
import { FacadeService } from 'app/services/facade.service';
import { MailItemType } from 'app/shared/models/admin/MailItemType';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as XLSX from 'xlsx';
import { BarcodeComponent } from '@app/shared/components/barcode/barcode.component';
import { Barcode } from '@app/shared/models/admin/Barcode';
import { PrintReceiptForMailitemsComponent } from '../print-receipt-for-mailitems/print-receipt-for-mailitems.component';
import { BusinessOrder } from '@app/shared/models/admin/businessOrder';
declare var $: any;

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

@Component({
  selector: 'items-details',
  templateUrl: './items-details.component.html',
  styleUrls: ['./items-details.component.scss']
})
export class ItemsDetailsComponent implements OnInit, AfterViewInit {
  form: FormGroup;
  orderPlacementDateTime: Date;
  readyForPickupAtDateTime: Date;
  deliveryBeforeDateTime: Date;
  attachments: File[] = [];
  id: number = 0;
  mailItems: any;
  interId:number=0;
  filesToDisplay: (string | ArrayBuffer)[] = [];
  accept: string = '.xlsx, .xls, .csv*';
  businessCustlId:number=0;
  @Output() onForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() itemList: EventEmitter<any> = new EventEmitter<any>();

  @Input() validateForm: boolean = false;
  @Input() orderId: number = 0;
  @Input() internationalId: number = 0;
  @Input() businessCustomerlId: number = 0;


  
  displayedColumns: string[] = ['select', 'mailItemBarCode', 'consigneeInformation', 'address', 'weight', 'actions'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();

  faileditemsList: any[] = [];

  importprogessMessage = '';
  selection = new SelectionModel<any>(true, []);

  file: File
  arrayBuffer: any;
  filelist: any;
  index: number;
  mailItemTypes: MailItemType[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  uploading: boolean = false;
  pageSize: number = 10;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService) {
    this.form = this.fb.group({
      itemTypeId: [null, [Validators.required]],
      totalItemCount: [null, [Validators.required]],
      filledItemNo: [null, [Validators.nullValidator]],
      remainingItemNo: [null, [Validators.nullValidator]],
    });
  }

  ngOnInit(): void {
    this.id = this.orderId;
    this.interId=this.internationalId;
    this.businessCustlId=this.businessCustomerlId;
      this.getItemTypes()
    if (this.id > 0) {
      this.getMailItems(this.id);
    }
  }

  ngAfterViewInit() {
    this.initDatePicker();
    console.log(this.dataSource);

    console.log(this.dataSource);
    console.log(this.dataSource.paginator);
    console.log(this.paginator);

    this.dataSource.paginator = this.paginator;

    this.form.valueChanges.subscribe(res => {
      this.onForm.emit(this.form);
    });


  }

  getItemTypes() {
    this.facadeService.adminFacadeService.mailItemTypeService.listActive().subscribe(res => {
      this.mailItemTypes = res;
    });
  }

  getMailItems(orderId: number) {
    this.facadeService.adminFacadeService.mailItemService.getByOrderId(orderId).subscribe(res => {

      this.dataSource.data = res;
      this.dataSource.paginator = this.paginator;

      this.itemList.emit(res);

      this.form.get('itemTypeId').setValue(res[0].itemTypeId);
      this.form.get('totalItemCount').setValue(res.length);

      this.form.get('filledItemNo').setValue(res.length);

      let remaining = Number(res[0].totalItemCount) - Number(res.length);

      this.form.get('remainingItemNo').setValue(0);
    });
  }

  onTotalItemCountChange(val: any) {

    let filledItemNo = this.form.value.filledItemNo;

    let remaining = Number(val) - Number(filledItemNo);

    this.form.get('remainingItemNo').setValue(remaining);
  }

  initDatePicker() {
    $(document).ready(() => {
      $('.orderPlacementDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          this.orderPlacementDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });

      $('.readyForPickupAtDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          this.readyForPickupAtDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });

      $('.deliveryBeforeDateTime').datetimepicker({
        onChangeDateTime: (dp: Date) => {
          this.deliveryBeforeDateTime = new Date(dp);
        },
        validateOnBlur: false,
      });
    });
  }

  /**
   * select only one file 
   * 
   * 
   * 
   * @param event 
   */
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.filesToDisplay.push(file.name);
      this.attachments.push(file);
    }
  }

  /**
   * remove file using it's name
   * 
   * 
   * @param fileName 
   */
  remove(fileName: string): void {
    const index = this.filesToDisplay.indexOf(fileName);
    this.filesToDisplay.splice(index, 1);
    this.attachments.splice(index, 1);
  }

  /**
   * 
   * @param simpleChanges 
   */
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.validateForm) {
      if (simpleChanges.validateForm.currentValue) {
        this.form.markAllAsTouched();
      }
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

  addfile(event) {
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];

      var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      this.filelist = arraylist;
    }
  }

  exportImportData() {
    if (this.dataSource.data?.length > 1) {
      this.exportAsExcelFile(this.dataSource.data, 'ImportedItems.xlsx');
    } else {
      this.toastrService.error(this.translateService.instant('No data imported'));
    }
  }

  importData() {
    this.uploading = true;

    if (this.file) {
      this.importprogessMessage = 'In Progrss ...';
      var errorMessage = '';

      let filldItemsList = this.filelist.map(x => {
        if (!this.isValidateImportedMailITem(x)) {
          return;
        }

        let exportedMailItem = {
          mailItemBarCode: x['bar code'],
          consigneeInformation: x['consignee name'],
          governorate: x['Governorate'],
          area: x['Area'],
          block: x['block'],
          street: x['street'],
          building: x['building'],
          fromSerial: x['form Serial'],
          pickupDate: x['date'],
          caseNo: x['case no.']
        }

        return exportedMailItem;
      }).filter(x => x != undefined);

      this.dataSource.data.push(...filldItemsList);
      this.itemList.emit(filldItemsList);

      this.dataSource._updateChangeSubscription();

      const totalItemCount = this.form.value ? this.form.controls['totalItemCount'].value : 0;
      this.form.get('filledItemNo').setValue(this.dataSource.data.length);
      const remaining = Number(totalItemCount) - Number(this.form.value ? this.form.controls['filledItemNo'].value : 0);
      this.form.get('remainingItemNo').setValue(remaining);
      this.importprogessMessage = 'Successfull..';
      if (this.faileditemsList?.length > 0) {
        this.importprogessMessage = 'Done with faild items.';
        this.exportAsExcelFile(this.faileditemsList, 'faildImprtedItems.xlsx');
      }
    } else {
      this.importprogessMessage = 'Faild';
      this.toastrService.error(this.translateService.instant('No File Selected'));
    }

    this.uploading = false;
    console.log(this.dataSource.paginator);
    console.log(this.paginator);

    this.dataSource.paginator = this.paginator;

  }

  isValidateImportedMailITem(importedMailItem: any) {
    let errorMessage = "";


    var listDuplicateCodes = this.filelist.filter(filterd => filterd['bar code'] == importedMailItem['bar code']);


    if (listDuplicateCodes.length > 1) {
      errorMessage = 'Code ' + importedMailItem['bar code'] + 'appear more than once'
      importedMailItem.error = errorMessage;
      this.faileditemsList.push(importedMailItem);
      return false;
    }
    if (!importedMailItem['consignee name']) {
      errorMessage = 'consignee name is required'
      importedMailItem.error = errorMessage;
      this.faileditemsList.push(importedMailItem);
      return false;
    }
    else if (!importedMailItem['Area']) {
      errorMessage = 'Area is required'
      importedMailItem.error = errorMessage;
      this.faileditemsList.push(importedMailItem);
      return false;
    }
    else if (!importedMailItem['block']) {
      errorMessage = 'block is required'
      importedMailItem.error = errorMessage;
      this.faileditemsList.push(importedMailItem);
      return false;
    }
    else if (!importedMailItem['street']) {
      errorMessage = 'street is required'
      importedMailItem.error = errorMessage;
      this.faileditemsList.push(importedMailItem);
      return false;
    }
    else {
      return true;
    }

  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const myworksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const myworkbook: XLSX.WorkBook = { Sheets: { 'data': myworksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(myworkbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    saveFile(fileName, EXCEL_TYPE, data);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  /**
   * 
   * @param opertaion 
   * @param isEdit 
   * @param element 
   */
  manageItem(opertaion: string, isEdit: boolean, element?: any): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(ManageItemComponent, {
      width: '75%',
      panelClass: 'custom-dialog-container',
      height: 'calc(80vh - 50px)',
      data: { type, row: element, isEdit ,internationalId:this.internationalId,
        businessCustomerId:this.businessCustomerlId
      },
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {

     
      if (result && element) {

        const dataSource = this.dataSource.data;
        var listDuplicateCodes = this.dataSource.data.filter(filterd => filterd.mailItemBarCode == result.item.mailItemBarCode
           &&filterd.id!=result.item.id
          );


        if (listDuplicateCodes.length > 0) {
          this.toastrService.error(this.translateService.instant('Item code already exist'));

          return false;
        }


        dataSource[this.index] = result.item;
        this.dataSource.data = dataSource;
        this.itemList.emit(this.dataSource.data);
        this.form.get('filledItemNo').setValue(this.dataSource.data.length);

        let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);

        this.form.get('remainingItemNo').setValue(remaining);

      } else if (result) {
        const item = result.item;


        var listDuplicateCodes = this.dataSource.data.filter(filterd => filterd.mailItemBarCode == item.mailItemBarCode);


        if (listDuplicateCodes.length >= 1) {
          this.toastrService.error(this.translateService.instant('Item code already exist'));

          return false;
        }

        const dataSource = this.dataSource.data;
        dataSource.push(item);

        this.dataSource.data = dataSource;
        this.itemList.emit(this.dataSource.data);

        this.form.get('filledItemNo').setValue(this.dataSource.data.length);
        let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);
        this.form.get('remainingItemNo').setValue(remaining);
      }
    });
  }

  setIndex(index: number) {
    this.index = index;
  }

  /**
   * 
   * @param event 
   * @param index 
   */
  onDelete(event: boolean, index: number) {
    if (event) {
      const dataSource = this.dataSource.data;
      dataSource.splice(index, 1);
      this.dataSource.data = dataSource;
      // this.form.get('filledItemNo').setValue(this.dataSource.data.length);
      // this.form.get('totalItemCount').setValue(this.dataSource.data.length);

      // this.form.get('remainingItemNo').setValue(0);

    }
  }

  printLabels(items: any[]) {
    if (items.length == 0) {
      this.toastrService.error(this.translateService.instant('Please Select Item(s)'));
      return;
    }

    const itemsToPrint: Barcode[] = items.map(item => {
      return {
        code: item.mailItemBarCode,
        createdDate: item.pickupDate,
        driverName: '',
        recieverName: item.consigneeInformation,
        address: `${item.governorate}, ${item.area}, ${item.block}, ${item.street}, ${item.building}`
      }
    });

    this.barcodeDialog(itemsToPrint);
  }




  onChangePage(event) {
    this.pageSize = event.pageSize;
  }



  barcodeDialog(barcodes: Barcode[]): void {
    const dialog = this.dialog.open(BarcodeComponent, {
      width: '95%',
      data: { barcodes },
      panelClass: 'custom-dialog-container',
      height: 'auto',
      minHeight: 'calc(75vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
    });
  }

  printMailitems(): void {
    console.log(this.selection.selected);

    if (this.selection.selected.length == 0) {
      this.toastrService.error(this.translateService.instant('Please Select Item(s)'));
      return;
    }

    const dialogRef = this.dialog.open(PrintReceiptForMailitemsComponent, {
      width: '75%',
      panelClass: 'custom-dialog-container',
      height: 'auto',
      data: {
        mailItems: this.selection.selected,
      }
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  printMailitem(mailItem: BusinessOrder): void {
    const dialogRef = this.dialog.open(PrintReceiptForMailitemsComponent, {
      width: '75%',
      panelClass: 'custom-dialog-container',
      height: 'auto',
      data: {
        mailItems: [mailItem],
      }
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}
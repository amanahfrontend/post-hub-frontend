import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContractsRoutingModule } from './contracts-routing.module';
//import { FilterModule } from 'app/shared/pipes/filter/filter.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/shared/modules/material/material.module';
import { HeaderAsCardModule } from '@app/shared/components/header-as-card/header-as-card.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
// components
import { ContractsComponent } from './contracts.component';
import { ManageContractComponent } from './manage-contract/manage-contract.component';
import { ChangeContractStatusComponent } from './change-contract-status/change-contract-status.component';
import { ConfirmDeletionModule } from '@app/shared/components/confirm-deletion/confirm-deletion.module';
import { ManagePrintContractComponent } from './manage-print-contract/manage-print-contract.component';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';


@NgModule({
  declarations: [
    ContractsComponent,
    ManageContractComponent,
    ChangeContractStatusComponent,
    ManagePrintContractComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    FormsModule,
    NgxMaskModule.forChild(),
    ReactiveFormsModule,
    HeaderAsCardModule,
    ContractsRoutingModule,
      ConfirmDeletionModule,
      PermissionsModule

   // FilterModule,
  ],
  providers: [
    ChangeContractStatusComponent
  ]
})
export class ContractsModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractsComponent } from './contracts.component';
import { ManageContractComponent } from './manage-contract/manage-contract.component';

const routes: Routes = [
  {
    path: '',
        component: ContractsComponent,
       
  },
  {
    path: 'add',
    component: ManageContractComponent
  },
  {
    path: 'edit',
    component: ManageContractComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractsRoutingModule { }

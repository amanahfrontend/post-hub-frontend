import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '@app/services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ContractStatus } from '@app/shared/models/admin/contract-status';


@Component({
  selector: 'change-contract-status',
  templateUrl: './change-contract-status.component.html',
  styleUrls: ['./change-contract-status.component.scss']
})
export class ChangeContractStatusComponent implements OnInit {
  form: FormGroup;
  contractStatuss:ContractStatus[]=[]

  constructor(
    private dialogRef: MatDialogRef<ChangeContractStatusComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private toastrService: ToastrService,
    private translateService: TranslateService,
  ) {
    this.form = this.fb.group({
      contractStatusId: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this. getContractStatuss()
  }
  getContractStatuss(){
    this.facadeService.adminFacadeService.contractService.getContractStatuss().subscribe(res => {
      this.contractStatuss = res;
     
    });
  }

  changeContractStatus() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please choose  contract '));
    }
    let body = { ...this.form.value }
    body.contractId=this.data.id;

    this.facadeService.adminFacadeService.contractService.changeContractStatus(body).subscribe((res: any) => {
      this.toastrService.success(this.translateService.instant(' contract status Updated Successfully'));
        this.dialogRef.close();
    });
  }
  close() {
    this.dialogRef.close();
  }
}

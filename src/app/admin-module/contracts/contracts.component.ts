import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ChangeContractStatusComponent } from './change-contract-status/change-contract-status.component';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../services/facade.service';
import { saveFile } from '@app/shared/helpers/download-link';
import { ContractType } from '@app/shared/models/admin/contract-type';
import { ContractStatus } from '@app/shared/models/admin/contract-status';
import { ServiceSector } from '@app/shared/models/admin/service-sector';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ManagePrintContractComponent } from './manage-print-contract/manage-print-contract.component';



@Component({
  selector: 'contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {

  searchBy: string = '';
  contracts: any[] = [];
  displayedColumns: string[] = ['select', 'date', 'period', 'type'
 // , 'contractValue'
  , 'contractStatus', 'business', 'customer', 'actions'];
  //dataSource = new MatTableDataSource<any>(DATA);
  selection = new SelectionModel<any>(true, []);
  page: number = 1;
  pageSize: number = 10;
  showFilters: boolean = false;
  form: FormGroup;
  contractTypes:ContractType[]=[]
  contractStatuss:ContractStatus[]=[]
  serviceSectors:ServiceSector[]=[];

  total: number = 0;
    constructor(
        private router: Router,
        private dialog: MatDialog,
        private translateService: TranslateService,
        private facadeService: FacadeService,
        private fb: FormBuilder,
        private toastrService: ToastrService) {

          this.form = this.fb.group({
            fromDate: new FormControl(''),
            toDate: new FormControl(''),
            contractTypeId: [null,[Validators.nullValidator]],
            contractStatusId: [null,[Validators.nullValidator]],
            serviceSectorId: [null,[Validators.nullValidator]],

          })
    }

  ngOnInit(): void {

    this.getContractTypes();
    this.getContractStatuss();
    this.getServiceSectors();

    this.list(this.page);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.contracts.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.contracts);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  search(): void {
    const body: { 
      pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.contractService.listByPagination(body).subscribe((res: any) => {
      this.contracts = res.result;
      this.total = res.totalCount;
  });
  }
  printContract(contractId:number){
    const dialog = this.dialog.open(ManagePrintContractComponent, {
      width: '60%',
      data: {
        id: contractId
      },
      panelClass: 'custom-dialog-container',
      height: '100%'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
    });
  }

  sendContractToCustomer(contractId:number){
    // const dialog = this.dialog.open(ManagePrintContractComponent, {
    //   width: '60%',
    //   data: {
    //     id: contractId
    //   },
    //   panelClass: 'custom-dialog-container',
    //   height: '100%'
    // });

    // dialog.disableClose = true;

    // dialog.afterClosed().subscribe(res => {
    // });


  }

  export() {
    const body = {
    }

    this.facadeService.adminFacadeService.contractService.exportToExcel(body).subscribe(data => {
      saveFile(`Contracts${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }

    PrintContract(contractId) {
        const body = {
        }

        this.facadeService.adminFacadeService.contractService.PrintContract(contractId).subscribe(data => {
            saveFile(`Contracts${new Date().toLocaleDateString()}.pdf`, "application/pdf", data);
        });
    }

    SendContractToCustomer(contractId) {
        const body = {
        }

        this.facadeService.adminFacadeService.contractService.SendToCustomer(contractId).subscribe(data => {
            saveFile(`Contracts${new Date().toLocaleDateString()}.pdf`, "application/pdf", data);
        });
    }


    list(page: number): void {
        const body: { pageNumber: number, pageSize: number } = {
            pageNumber: page,
            pageSize: this.pageSize,
        };

        this.facadeService.adminFacadeService.contractService.listByPagination(body).subscribe((res: any) => {
            this.contracts = res.result;
            this.total = res.totalCount;
        });
    }
    onConfirm(event: boolean, manager: any): void {
      if (event) {
        this.facadeService.adminFacadeService.contractService.delete(manager).subscribe(res => {
          const index = this.contracts.indexOf(manager);
          if (index >= 0) {
            this.contracts.splice(index, 1);
  
            this.list(this.page);
            this.toastrService.success(this.translateService.instant(`Contract has been deleted successfully`));
          }
        });
      }
    }
    getContractTypes(){
      this.facadeService.adminFacadeService.contractService.getContractTypes().subscribe(res => {
        this.contractTypes = res;
       
    });
    }

    getContractStatuss(){
      this.facadeService.adminFacadeService.contractService.getContractStatuss().subscribe(res => {
        this.contractStatuss = res;
       
      });
    }

    getServiceSectors(){
      this.facadeService.adminFacadeService.serviceSectorService.getAll().subscribe(res => {
        this.serviceSectors  = res;
       
      });
    }

    onChangePage(event) {
      this.page = event.pageIndex + 1;
      this.pageSize = event.pageSize;
  
      this.list(this.page);
      this.replaceRoutePage(this.page);
    }
  
    private replaceRoutePage(page: number) {
      this.router.navigate(['contracts'], { queryParams: { page: page } });
    }

    addContract() {
      this.router.navigate(['contracts/add']);
    }

 
  editContract(id: number) {
    this.router.navigate(['contracts/edit'], {queryParams: {id: id,mode:'edit'}});      
  }
  toggleFilter() {
    this.showFilters = !this.showFilters;
  }

  changeStatus(id:number) {
    const dialog = this.dialog.open(ChangeContractStatusComponent, {
      width: '35%',
      data: {
         id:id
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(45vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      console.log(res);
      this.list(this.page);
    });
  }

}

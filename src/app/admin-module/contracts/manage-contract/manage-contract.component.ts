import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from '@app/shared/helpers/download-link';
import { ContractType } from '@app/shared/models/admin/contract-type';
import { ContractStatus } from '@app/shared/models/admin/contract-status';
import { ServiceSector } from '@app/shared/models/admin/service-sector';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FacadeService } from '@app/services/facade.service';
import { ContractForEdit } from '@app/shared/models/admin/contract-for-edit';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { RFQList } from '@app/shared/models/admin/rfq-list';
import { RFQItemsByRFQ } from '@app/shared/models/admin/rfq-items-by-rfq';
import { Option } from './../../../shared/models/admin/option';
import { ManagePrintContractComponent } from '../manage-print-contract/manage-print-contract.component';
import { BusinessInfo } from '@app/shared/models/admin/business-customer';

@Component({
  selector: 'manage-contract',
  templateUrl: './manage-contract.component.html',
  styleUrls: ['./manage-contract.component.scss']
})
export class ManageContractComponent implements OnInit {
  form: FormGroup;
  convertDate: String;
  customers: Option[] = [];
  filterCustomerCtrl: string;

  periodTypes = [
    { id: 1, name: 'Hours' },
    { id: 2, name: 'Days' },
    { id: 3, name: 'Months' },
    { id: 4, name: 'Years' }

  ];

  paymentTypes = [
    { id: 1, name: 'Cash' },
    { id: 2, name: 'Per Mission' },
    { id: 3, name: 'Installments' },
    { id: 4, name: 'Credit' }

  ];

  label: string = 'Create';
  contractTypes: ContractType[] = []
  contractStatuss: ContractStatus[] = []
  serviceSectors: ServiceSector[] = [];
  currencies: any[] = [];
  rFQList: RFQList[] = []
  rFQItemsByRFQs: RFQItemsByRFQ[] = [];
  businessCustomers: BusinessInfo[] = [];

  id: number = 0
  contractId: number = 0;
  mode: any;
  contractForEdit: ContractForEdit;
  constructor(
    private translateService: TranslateService
    , private fb: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute,

  ) {
    this.label = this.translateService.instant('Add');
    this.form = this.fb.group({

      rfqId: [null, [Validators.nullValidator]],
      //  filterCustomerCtrl: ['', [Validators.nullValidator]],
      serviceSectorId: [null, [Validators.nullValidator]],
      contractTypeId: [null, [Validators.nullValidator]],

      contractDate: ['', [Validators.nullValidator]],
      contractStartDate: ['', [Validators.nullValidator]],
      contractEndDate: ['', [Validators.nullValidator]],

      businessCustomerId: [null, [Validators.nullValidator]],
      noOfMessages: [null, [Validators.required]],
      isPrintReciptListAndCollectEnabled: [false, [Validators.nullValidator]],
      isPrintAndFixEnabledEnabled: [false, [Validators.nullValidator]],
     // serviceDistriputionPeriod: [null, [Validators.nullValidator]],
      itemSupplyPeriod: [null, [Validators.nullValidator]],
      distriputionAllowance: [null, [Validators.nullValidator]],
      ///Payment Terms 
      paymentType: [null, [Validators.nullValidator]],
      //  paymentNotLetterThan: [null, [Validators.nullValidator]],
      paymentNotLetterThanPeriodType: [null, [Validators.nullValidator]],
      /// Installments 
      installmentsAtContracting: [null, [Validators.nullValidator]],
      installmentsAfterEachMessage: [null, [Validators.nullValidator]],
      //  installmentsPaymentNotLaterThan: [null, [Validators.nullValidator]],
     // installmentsPaymentNotLaterThanPeriodType: [null, [Validators.nullValidator]],
      contractStatusId: [null, [Validators.nullValidator]],
      pricePerUnit: [null, [Validators.nullValidator]],
      numberOfUnits: [null, [Validators.required]],
      currencyId: [null, [Validators.required]],
      totalValue: [null, [Validators.nullValidator]],
      //
      paymentNotLetterThanPeriodValue: [null, [Validators.nullValidator]],
      itemsSupplyAheadOf: [null, [Validators.nullValidator]],
      itemsSupplyAheadOfValue: [null, [Validators.nullValidator]],
      serviceDistributionPeriod: [null, [Validators.nullValidator]],
      serviceDistributionPeriodValue: [null, [Validators.nullValidator]],
      paymentTermsMessageNo: [null, [Validators.nullValidator]],
      //
      code: ['', [Validators.required]],

      currency: ['', [Validators.nullValidator]],

      terms: this.fb.array([]),
    });
  }

  terms(): FormArray {
    return this.form.get('terms') as FormArray;
  }

  newTerm(): FormGroup {
    return this.fb.group({
      id: [0, [Validators.nullValidator]],
      term: ['', [Validators.nullValidator]],
    });
  }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.list().subscribe(res => {
      this.businessCustomers = res;

    });
  }
  getContractTypes() {
    this.facadeService.adminFacadeService.contractService.getContractTypes().subscribe(res => {
      this.contractTypes = res;

    });
  }

  getAllCustomers() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllCustomerNamesAsync().subscribe(
      (res: Option[]) => {
        this.customers = res;
      });
  }

  getContractStatuss() {
    this.facadeService.adminFacadeService.contractService.getContractStatuss().subscribe(res => {
      this.contractStatuss = res;

    });
  }

  getServiceSectors() {
    this.facadeService.adminFacadeService.serviceSectorService.getAll().subscribe(res => {
      this.serviceSectors = res;

    });
  }
  getActiveCurrencies() {
    this.facadeService.adminFacadeService.managePriceListService.getActiveCurrencies().subscribe(
      (res: any) => {
        this.currencies = res;

      });
  }
  addTerm(): void {
    this.terms().push(this.newTerm());
  }

  editTerm(index: number, term: { name: string }): void {
    this.terms().at(index).patchValue(term);
  }

  removeTerm(termIndex: number): void {
    this.terms().removeAt(termIndex);
  }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe(
        params => {
          this.id = Number(params.id);
          this.mode = params.mode;
          if (Number(this.id) > 0) {
            this.getContractById(Number(this.id));
          }
        });
    this.getBusinessCustomers();
    this.getContractTypes();
    this.getContractStatuss();
    this.getServiceSectors();
    this.getActiveCurrencies();
    this.getAllCustomers();
    this.rfqList();
  }
  getContractById(id: number) {
    this.facadeService.adminFacadeService.contractService.getContractById(id).subscribe((res: any) => {
      this.contractForEdit = res;
      this.contractForEdit.terms.forEach(pq => {
        this.addTerm();
      });
      this.form.patchValue(this.contractForEdit);
      if (res.pricePerUnit && res.numberOfUnits) {
        let total = Number(res.pricePerUnit) * Number(res.numberOfUnits);
        this.form.get('totalValue').setValue(total);
      }
      if (res.currency) {
        this.form.get('currency').setValue(res.currency);
        this.form.get('currencyId').setValue(res.currencyId);

      }
    });
  }
  optionSelected(event: MatAutocompleteSelectedEvent, type: string): void {
    const id = Number(event)
    //) event.option.value;
    switch (type) {
      case 'autoPriceCurrency':
        //// const curr=this.currencies.find(c=>c.id==id).Code;
        //this.form.get('currency').setValue(curr);

        break;
      case 'autoPriceQuotation':
        this.getRFQItemsByRFQId(id);
        //  const curr=this.rFQItemsByRFQs.find(c=>c.id==id).currencyCode;




        break;


      default:
        break;
    }
  }
  rfqList() {

    this.facadeService.adminFacadeService.managePriceListService.rfqList().subscribe(
      (res) => {
        this.rFQList = res;
      });
  }
  printContract() {
    const dialog = this.dialog.open(ManagePrintContractComponent, {
      width: '60%',
      data: {
        id: this.contractId
      },
      panelClass: 'custom-dialog-container',
      height: '100%'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
    });
  }
  getRFQItemsByRFQId(id: number) {

    this.form.get('currency').setValue('');
    this.form.get('currencyId').setValue('');
    this.form.get('totalValue').setValue('');
    this.form.get('pricePerUnit').setValue('');
    this.form.get('numberOfUnits').setValue('');

    this.facadeService.adminFacadeService.managePriceListService.getRFQItemsByRFQId(id).subscribe(
      (res) => {

        this.rFQItemsByRFQs = res;
        if (res) {
          this.form.get('currency').setValue(res[0].currencyCode);
          this.form.get('currencyId').setValue(Number(res[0].currencyId));
          let totalInterItemsPrice = 0
          for (let data of res) {

            totalInterItemsPrice += data.roundTripShippingPrice;
          }
          let total = 0
          if (res[0].hasAmount == true && totalInterItemsPrice == 0) {
            total = Number(res[0].quantity) * Number(res[0].itemPrice);
            this.form.get('totalValue').setValue(total);
            this.form.get('pricePerUnit').setValue(res[0].itemPrice);


          }
          else if ((res[0].hasAmount == false || res[0].hasAmount == null) && totalInterItemsPrice == 0) {
            total = Number(res[0].itemPrice);
            this.form.get('totalValue').setValue(total);
            this.form.get('pricePerUnit').setValue(total);


          }
          else if (totalInterItemsPrice > 0) {
            this.form.get('pricePerUnit').setValue((Number(totalInterItemsPrice) / Number(res.length)));

            this.form.get('numberOfUnits').setValue(res.length);
            this.form.get('totalValue').setValue(totalInterItemsPrice);
          }

        }

      });
  }

  date(e, manageContract) {
    this.convertDate = new Date(e.target.value.setDate(e.target.value.getDate() + 1)).toISOString().substring(0, 10);

    switch (manageContract) {
      case 'contractDate':
        this.form.get('contractDate').patchValue(this.convertDate, {
          onlyself: true
        })
        break;

      case 'contractStartDate':
        this.form.get('contractStartDate').patchValue(this.convertDate, {
          onlyself: true
        })
        break;

      case 'contractEndDate':
        this.form.get('contractEndDate').patchValue(this.convertDate, {
          onlyself: true
        })
        break;


      default:
        break;
    }
  }
  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please fill mandatory data'));
    }
    let body = { ...this.form.value };

    body.currencyId = body.currencyId ? Number(body.currencyId) : null;
    body.businessCustomerId = body.businessCustomerId ? Number(body.businessCustomerId) : null;
    body.contractStatusId = body.contractStatusId ? Number(body.contractStatusId) : null;
    body.contractTypeId = body.contractTypeId ? Number(body.contractTypeId) : null;
    body.paymentNotLetterThanPeriodType = body.paymentNotLetterThanPeriodType ? Number(body.paymentNotLetterThanPeriodType) : null;
    body.paymentType = body.paymentType ? Number(body.paymentType) : null;
    body.rfqId = body.rfqId ? Number(body.rfqId) : null;
    body.serviceSectorId = body.serviceSectorId ? Number(body.serviceSectorId) : null;
    body.paymentNotLetterThanPeriodValue=body.paymentNotLetterThanPeriodValue?Number(body.paymentNotLetterThanPeriodValue):null
    body.itemsSupplyAheadOf= body.itemsSupplyAheadOf?Number( body.itemsSupplyAheadOf):null
    body.itemsSupplyAheadOfValue=body.itemsSupplyAheadOfValue?Number(body.itemsSupplyAheadOfValue):null
    body.serviceDistributionPeriod= body.serviceDistributionPeriod?Number( body.serviceDistributionPeriod):null
    body.serviceDistributionPeriodValue=body.serviceDistributionPeriodValue?Number(body.serviceDistributionPeriodValue):null
    body.paymentTermsMessageNo= body.paymentTermsMessageNo?Number( body.paymentTermsMessageNo):null

    if (Number(this.id)) {

      body.id = this.id;

      this.facadeService.adminFacadeService.contractService.update(body).subscribe((res: any) => {
        this.toastrService.success(this.translateService.instant(' contract Updated Successfully'));
        this.contractId = this.id
        this.router.navigate(['contracts']);

      });

    }
    else {
      this.facadeService.adminFacadeService.contractService.create(body).subscribe((res: any) => {
        this.contractId = res
        this.toastrService.success(this.translateService.instant('New contrct Added Successfully'));
        this.router.navigate(['contracts']);

      });
    }
  }
  txtChange() {
    if (this.form.value.pricePerUnit && this.form.value.numberOfUnits) {
      let total = Number(this.form.value.pricePerUnit) * Number(this.form.value.numberOfUnits);
      this.form.get('totalValue').setValue(total);
    }
  }
  cancel() {
    this.router.navigate(['contracts']);

  }
}

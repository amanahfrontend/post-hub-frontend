import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePrintContractComponent } from './manage-print-contract.component';

describe('ManagePrintContractComponent', () => {
  let component: ManagePrintContractComponent;
  let fixture: ComponentFixture<ManagePrintContractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagePrintContractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePrintContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

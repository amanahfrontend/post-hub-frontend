import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Country } from '../../../shared/models/components';
import { FacadeService } from '../../../services/facade.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from './../../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../services/shared/language.service';
import { TranslateService } from '@ngx-translate/core';
import { Manager } from '../../../shared/models/admin/manager';
import { App } from '../../../core/app';

const COUNTRIES = 'COUNTRIES';

@Component({
  selector: 'manage-staff-form',
  templateUrl: './manage-staff-form.component.html',
  styleUrls: ['./manage-staff-form.component.scss']
})
export class ManageStaffFormComponent implements OnInit {
  form: FormGroup;

  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";
  operation: string;
  selectedMobileNumber: number;
  selectedCountry: Country;
  selectedCountryCode: Country;

  countries: Country[] = [];
  filteredCountries: Observable<Country[]>;

  roles: any[];
  departments: Option[] = [];
  sectors: Option[] = [];
  status: Option[] = [];
  genders: Option[] = [];
  teams: Option[] = [];
  branchs: Option[] = [];
  managers: Option[] = [];

  minDate: Date;
  maxDate: Date;
  validPhoneNumber: boolean = false;
  accept = 'image/*';
  selectedFile: File;
  locale: string = 'en';
  type: string = 'password';
  isEditMode: boolean = false;
  row: Manager;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<ManageStaffFormComponent>,
  ) {
    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      genderId: [''],
      dateOfBirth: [''],
      countryCtrl: ['', [Validators.required]],
      nationalId: [''],
      phoneNumber: [''],
      email: [''],
      serviceSectorId: ['', [Validators.required]],
      departmentId: ['', [Validators.required]],
      branchId: ['', [Validators.required]],
      team: ['', [Validators.required]],
      role: ['', [Validators.required]],
      companyEmail: [''],
      //supervisorId: ['', [Validators.required]],
      accountStatusId: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

    this.operation = this.data.operation;
    this.isEditMode = this.data.isEditMode;
    this.countries = JSON.parse(localStorage.getItem(COUNTRIES));
  }

  ngOnInit(): void {
    this.locale = this.languageService.currentLanguage;
    this.filteredCountries = this.form.get('countryCtrl').valueChanges
      .pipe(
        startWith(''),
        map(country => country ? this.filter(country) : this.countries.slice())
      );

    this.loadPreData();

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 60, 11, 31);
    this.maxDate = new Date(currentYear - 18, 11, 31);

    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      if (this.row.roleNames.length > 0) {
        this.form.get('role').setValue(this.row.roleNames[0]);
      }

      if (this.row.teamManagers.length > 0) {
        this.form.get('team').setValue(this.row.teamManagers[0].teamId);
      }

      this.form.get('dateOfBirth').setValue(new Date(this.row.dateOfBirth));
      this.form.get('countryCtrl').setValue(this.row.nationality);
      this.form.removeControl('password');
      this.path = `${App.backEndUrl}/${this.row.profilePicURL}`;

    }
  }

  private filter(value: string): Country[] {
    const filterValue = value.toLowerCase();
    return this.countries.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.path = event.target.result;
      }
    }
  }

  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    if (!this.validPhoneNumber) {
      return this.toastrService.error(this.translateService.instant('Enter valid phone number'));
    }

    let body = { ... this.row, ... this.form.value };
    body.mobileNumber = this.selectedMobileNumber;
    body.roleNames = [this.form.get('role').value];
    body.profilePicFile = this.selectedFile;
    body.countryId = this.selectedCountryCode.id;
    body.dateOfBirth = new Date(body.dateOfBirth).toUTCString();
     
    if (this.selectedCountry) {
      body.nationalityId = this.selectedCountry.id;
    }

    body.teamManagers = [{
      teamId: this.form.get('team').value
    }];

    let { role, countryCtrl, team, ...objectToPost } = body;
    if (this.isEditMode) {
      this.facadeService.adminFacadeService.manageStaffService.update(objectToPost).subscribe(res => {
        this.toastrService.success(this.translateService.instant('Staff Updated Successfully'));
        this.dialogRef.close({ operation: this.operation });
      });
    } else {
      this.facadeService.adminFacadeService.manageStaffService.create(objectToPost).subscribe(res => {
        this.toastrService.success(this.translateService.instant('New Staff Added Successfully'));
        this.dialogRef.close({ operation: this.operation });
      });
    }
  }

  /**
   * on type number 
   * 
   * 
   * @param number 
   */
  number(number: number): void {
    this.selectedMobileNumber = number;
  }

  /**
   * on select country flag from phone number
   * 
   * 
   * @param country 
   */
  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

 setCountry(country: Country) {
      
    this.selectedCountry = country;
  }

  loadPreData(): void {
    // departments
    this.facadeService.departmentService.list().subscribe((res: Option[]) => {
      this.departments = res;
    });

    // sectors
    this.facadeService.sharedFacadeService.sectorService.list().subscribe((res: Option[]) => {
      this.sectors = res;
    });

    // status
    this.facadeService.sharedFacadeService.accountStatusService.list().subscribe((res: Option[]) => {
      this.status = res;
    });

    // genders
    this.facadeService.sharedFacadeService.genderService.list().subscribe((res: Option[]) => {
      this.genders = res;
    });

    // roles
    this.facadeService.adminFacadeService.managerRolesService.roles().subscribe((res: Option[]) => {
      this.roles = res;
    });

    // teams
    this.facadeService.adminFacadeService.teamsService.teams().subscribe((res: Option[]) => {
      this.teams = res;
    });

    // branches
    this.facadeService.adminFacadeService.branchService.list().subscribe((res: Option[]) => {
      this.branchs = res;
    });

    // managers
    this.facadeService.adminFacadeService.manageStaffService.list().subscribe((res: Option[]) => {
      this.managers = res;
    });
  }

  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
}

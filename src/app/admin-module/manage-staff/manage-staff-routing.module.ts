import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ManageStaffComponent } from './manage-staff.component';
const routes: Routes = [
  {
    path: '',
    component: ManageStaffComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadAllManagers'
      }
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageStaffRoutingModule { }

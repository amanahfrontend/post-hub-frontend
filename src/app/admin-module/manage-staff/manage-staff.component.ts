import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ManageStaffFormComponent } from './manage-staff-form/manage-staff-form.component';
import { FacadeService } from '../../services/facade.service';
import { Manager } from '../../shared/models/admin/manager';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from '../../shared/helpers/download-link';

@Component({
  selector: 'manage-staff',
  templateUrl: './manage-staff.component.html',
  styleUrls: ['./manage-staff.component.scss']
})
export class ManageStaffComponent implements OnInit {
  url: any = "./assets/img/faces/avtar.jpeg"

  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'nationality',
    'mobileNumber',
    'roleNames',
    'teamManagers',
    'departmentName',
    'supervisorName',
    'serviceSectorName',
    'branchName',
    'companyEmail',
    'actions'
  ];

  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  managers: Manager[] = [];
  subscriptions = new Subscription();
  searchBy: string = '';

  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
  constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));
  }

  ngOnInit(): void {
    this.list(this.page);
  }

  /**
   * list managers
   * 
   * 
   * @param page 
   */
  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.manageStaffService.listByPagination(body).subscribe((res: any) => {
      this.managers = res.result;
      this.total = res.totalCount;
    });
  }

  /**
   * select image
   * 
   * 
   * @param event 
   */
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.url = event.target.result;
      }
    }
  }

  /**
   * add / edit row
   * 
   * 
   * @param opertaion 
   * @param editMode 
   * @param manager 
   */
  manageStaffSeletedRow(opertaion: string, editMode: boolean, manager?: Manager): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialog = this.dialog.open(ManageStaffFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: manager ? manager : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      if (res.operation == 'Add') {
        this.page = 1;
      }

      this.list(this.page);
    });
  }

  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.list(this.page);
    this.replaceRoutePage(this.page);
  }

  /**
   * 
   * @param page 
   */
  private replaceRoutePage(page: number) {
    this.router.navigate(['manage-staff'], { queryParams: { page: page } });
  }

  /**
   * Delete staff
   * 
   * 
   * @param event 
   * @param manager 
   */
  onDelete(event: boolean, manager: Manager): void {
    if (event) {
      this.facadeService.adminFacadeService.manageStaffService.delete(manager.id).subscribe(res => {
        const index = this.managers.indexOf(manager);
        if (index >= 0) {
          this.managers.splice(index, 1);

          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`Staff has been deleted successfully`));
        }
      });
    }
  }

  export() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageStaffService.exportToExcel(body).subscribe(data => {
        saveFile(`Staff${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }

  search(): void {
    const body: { pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.manageStaffService.listByPagination(body).subscribe((res: any) => {
      this.managers = res.result;
      this.total = res.totalCount;
    });
  }
}

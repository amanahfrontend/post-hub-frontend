import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageStaffRoutingModule } from './manage-staff-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// components
import { ManageStaffComponent } from './manage-staff.component';
import { ManageStaffFormComponent } from './manage-staff-form/manage-staff-form.component';

// shared
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    ManageStaffComponent,
    ManageStaffFormComponent
  ],
  imports: [
    CommonModule,
    ManageStaffRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,

    // shared
    MaterialModule,
    AddressModule,
    HeaderAsCardModule,
    TelInputModule,
    ConfirmDeletionModule,
    SetDirModule,
    PermissionsModule
  ],
  entryComponents: [
    ManageStaffFormComponent
  ],
})
export class ManageStaffModule { }

import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { AdminFacadeService } from '../../services/admin/admin-facade.service';
import { OrderListModel } from '../../shared/models/admin/Orders/order-list';
import { Subscription } from 'rxjs-compat';
import { ToastrService } from 'ngx-toastr';
import { OrderTypes } from '../../shared/models/admin/Orders/order-types-enum';
import { FacadeService } from '@app/services/facade.service';
import { saveFile } from '@app/shared/helpers/download-link';

const ELEMENT_DATA = [
  {
    barcode: 232232, AWB: 9889231821, receiverName: 'Test1 User', senderName: 'Test6 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 2  ', status: 'Approved',
  },
  {
    barcode: 23232, AWB: 9889231821, receiverName: 'Test2 User', senderName: 'Test5 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 1', status: 'Approved'
  },
  {
    barcode: 243232, AWB: 9889231821, receiverName: 'Test3 User', senderName: 'Test4 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
  },
  {
    barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'test', status: 'Returned'
  },
  {
    barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', status: 'Returned'
  },
  {
    barcode: 3232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test2 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
  },
  {
    barcode: 72323232, AWB: 9889231821, receiverName: 'Test5 User', senderName: 'Test1 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: '', status: 'Delivered',
  },
  {
    barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
    shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'returned', status: 'Returned',
  },

]

@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.scss']
})
export class ManageOrderComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  checkAll = false
  locationDetails = []
  locationForm;
  order;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  companyForm;
  masterCheck = false
  tempArray = [
    {
      barcode: 232232, AWB: 9889231821, receiverName: 'Test1 User', senderName: 'Test6 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 2  ', status: 'Approved',
    },
    {
      barcode: 23232, AWB: 9889231821, receiverName: 'Test2 User', senderName: 'Test5 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 1', status: 'Approved'
    },
    {
      barcode: 243232, AWB: 9889231821, receiverName: 'Test3 User', senderName: 'Test4 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'test', status: 'Returned'
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', status: 'Returned'
    },
    {
      barcode: 3232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test2 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 72323232, AWB: 9889231821, receiverName: 'Test5 User', senderName: 'Test1 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: '', status: 'Delivered',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'returned', status: 'Returned',
    },

  ]

  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;
  checkList: any = {};
  sundayCheck;
  saturdayCheck;
  mondayCheck;
  tuesdayCheck;
  wednesdayCheck;
  thursdayCheck;
  fridayCheck;
  dropdownListStatus = [

    { item_id: 2, item_text: 'Branch 1' },
    { item_id: 3, item_text: 'Branch 2' },
    { item_id: 4, item_text: 'Branch 3' },


  ];
  dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  orderDetails = ELEMENT_DATA;
  displayedColumns: string[] = ['select', 'dates', 'orderInfo', 'itemsInfo', 'status', 'customer', 'sender', 'receiver', 'actions'];
  dataSource = new MatTableDataSource<any>(ELEMENT_DATA);
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  searchBy: string = '';

  orderList: OrderListModel[];
  subscriptions = new Subscription();


  constructor(private router: Router,
    private translateService: TranslateService,
    private adminFacadeService: AdminFacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private facadeService: FacadeService,

  ) {
    this.orderList = [];

    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));

  }

  ngOnInit(): void {

    this.list(this.page);
    this.getLocation()
    this.order = new FormGroup({
      locationName: new FormControl('', Validators.required),
      mobile_no: new FormControl(''),
      paci: new FormControl(''),
      awb: new FormControl(''),
      senderName: new FormControl(),
      receiverName: new FormControl(),
      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      orderDate: new FormControl(''),
      barcode: new FormControl(''),
      payment: new FormControl(''),
      status: new FormControl(''),
      driver: new FormControl(''),
    });

    this.companyForm = new FormGroup({
      companyName: new FormControl(''),
      companyEmail: new FormControl(''),
      companyOtherEmail: new FormControl(''),


      companyPhone: new FormControl(),
      companyWebsite: new FormControl(''),


    });




  }
  getLocation() {
    // this.rerender()

    this.locationDetails = []


    this.locationDetails = [
      {
        locationName: 'test location 1 ', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 1
      },
      {
        locationName: 'test location 2', mobile_no: 9889241821, paci: '721781277122', Governorate: '2overnorate2', Area: 'test area2',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 2
      },
      {
        locationName: 'test location 3', mobile_no: 9889251821, paci: '721781277122', Governorate: 'Governorate3', Area: 'test area3',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 3
      },
      {
        locationName: 'test location 4', mobile_no: 9889261821, paci: '721781277122', Governorate: 'Governorate4', Area: 'test area4',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 4
      },
      {
        locationName: 'test location 5', mobile_no: 9889271821, paci: '721781277122', Governorate: 'Governorate5', Area: 'test area5',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 5
      },
      {
        locationName: 'test location 1 test location 1 test location 1 location 1 test location 1 test location 1', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 6
      },

    ]
    //  this.rerender()



  }
  addNewLocation() {
    this.areaList
    this.addOrEdit = this.translateService.instant('Add')
    this.locationForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = this.translateService.instant('Edit')
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.locationDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.locationForm = new FormGroup({
        locationName: new FormControl(selectedLocation[0]['locationName']),
        mobile_no: new FormControl(selectedLocation[0]['mobile_no']),
        paci: new FormControl(selectedLocation[0]['paci']),


        Street: new FormControl(selectedLocation[0]['Street']),
        Governorate: new FormControl(selectedLocation[0]['Governorate']),
        Area: new FormControl(selectedLocation[0]['Area']),
        Block: new FormControl(selectedLocation[0]['Block']),
        BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
        floorNo: new FormControl(selectedLocation[0]['floorNo']),

      });
    }

  }
  onClick() {

    if (this.isEdit) {
      this.locationForm.value['id'] = this.selectedLocationId;
      this.locationDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.locationDetails[index] = this.locationForm.value
          console.log('data: ', this.locationForm.value);
        }
      })
    }
    if (!this.isEdit) {
      console.log('this.locationForm.value: ', this.locationForm.value);
      this.locationDetails.push(this.locationForm.value)
      console.log(this.locationForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.companyForm.value
    console.log('   this.companyForm.value: ', this.companyForm.value);
    this.companyForm = new FormGroup({
      companyName: new FormControl(this.companyForm.value['companyName']),
      companyEmail: new FormControl(this.companyForm.value['companyEmail']),
      companyOtherEmail: new FormControl(this.companyForm.value['companyOtherEmail']),


      companyPhone: new FormControl(this.companyForm.value['companyPhone']),
      companyWebsite: new FormControl(this.companyForm.value['companyWebsite']),


    });
  }
  changeGovernorate(value) {
    console.log('value: ', value);
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }
  deleteLocation(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.locationDetails.splice(this.idForDelete, 1)
  }
  changeRoutes() {
    if (this.dataSource.data.length > 0) {
      this.router.navigate(['assignDriver', 1]);
    } else {
      alert("please check minimum one order")
    }
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  filer(value) {
    this.orderDetails = this.tempArray
    let temp = this.orderDetails
    if (value == 'all') {
      this.orderDetails = this.tempArray
    } else {
      this.orderDetails = temp.filter(arr => {
        return arr.status == value
      });
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  createBusinessOrder() {
    this.router.navigate(['create-business-order']);
  }

  ngOnDestroy(): void {
  }

  ngAfterViewInit(): void {
  }

  /**
* list managers
* 
* 
* @param page 
*/
  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.adminFacadeService.manageOrderService.listByPagination(body).subscribe((res: any) => {
      this.orderList = res.result;
      this.total = res.totalCount;
    });
  }

  /**
* on change page or per page
*
*
* @param event
*/
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.list(this.page);
    this.replaceRoutePage(this.page);
  }

  /**
* 
* @param page 
*/
  private replaceRoutePage(page: number) {
    this.router.navigate(['manageOrder'], { queryParams: { page: page } });
  }


  onDelete(event, order) {
    if (event) {
      this.adminFacadeService.manageOrderService.delete(order.id).subscribe(res => {
        const index = this.orderList.indexOf(order);
        if (index >= 0) {
          this.orderList.splice(index, 1);
          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`order has been deleted successfully`));
        }
      });
    }
  }

  /**
 * add / edit row
 * 
 * 
 * @param opertaion 
 * @param editMode 
 * @param manager 
 */
  edit(order): void {
    
    if (order.orderTypeId == OrderTypes.BUSINESS) {

      this.router.navigateByUrl(`/create-business-order/edit/${order.id}`);
    } 
    // else {
    //   this.router.navigateByUrl('/create-business-order/add');
    // }
  }

  export() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageOrderService.exportToExcel(body).subscribe(data => {
        saveFile(`Order${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }


}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
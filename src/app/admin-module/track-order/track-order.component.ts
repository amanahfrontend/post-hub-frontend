import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {
  trackId;
  openError = false
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      if (params && params.id != 0) {
        this.trackId = params.id;
        this.search(this.trackId)
      }
    })
  }
  search(value) {
    this.openError = false
    this.trackId = value;
    if (value && value.length > 4) {
      this.trackId = value
    }
    else {
      this.openError = true
    }

  }
}

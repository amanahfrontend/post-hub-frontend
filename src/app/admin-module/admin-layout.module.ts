import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from '../admin-module/user-profile/user-profile.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { BulkOrderPickUpRequestsComponent } from '../admin-module/bulk-order-pick-up-requests/bulk-order-pick-up-requests.component';
import { DataTablesModule } from "angular-datatables";
import { AgmCoreModule } from '@agm/core';
import { DistributorComponent } from '../admin-module/distributor/distributor.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { CustomerComponent } from '../admin-module/customer/customer.component';
import { AssignDriverComponent } from '../admin-module/assign-driver/assign-driver.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ColorPickerModule } from 'ngx-color-picker';
import { ManageOrderComponent } from '../admin-module/manage-order/manage-order.component';
import { ServicesComponent } from './services/services.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { ManageRFQComponent } from './manage-rfq/manage-rfq.component';
import { MaterialModule } from '../shared/modules/material/material.module';
import { FooterModule } from '../shared/components/footer/footer.module';
import { SidebarModule } from '../shared/components/sidebar/sidebar.module';
import { NavbarModule } from '../shared/components/navbar/navbar.module';
import { TranslateModule } from '@ngx-translate/core';
import { AddressModule } from '../shared/components/address/address.module';
import { TelInputModule } from '../shared/components/tel-input/tel-input.module';
import { HeaderAsCardModule } from '../shared/components/header-as-card/header-as-card.module';
import { ConfirmDeletionModule } from '../shared/components/confirm-deletion/confirm-deletion.module';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { PermissionsModule } from '../shared/modules/permissions/permissions.module';
import { DateToTimezoneModule } from '../shared/pipes/date-to-timezone/date-to-timezone.module';
import { CustomerRegistrationComponent } from '../auth/customer-registration/customer-registration.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    NgxBarcodeModule,
    ColorPickerModule,
    
    NgMultiSelectDropDownModule.forRoot(),
    // AgmCoreModule.forRoot({
    //   apiKey: "AIzaSyCDVnGbGw5JXOPoNqNxQTkjZzUx6zfDMFQ"
    // }),
    MaterialModule,
    FooterModule,
    SidebarModule,
    NavbarModule,
    TranslateModule,
    AddressModule,
    TelInputModule,
    HeaderAsCardModule,
    DateToTimezoneModule,
    ConfirmDeletionModule,
    TimePickerModule,
    PermissionsModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    MapsComponent,
    NotificationsComponent,
    BulkOrderPickUpRequestsComponent,
    DistributorComponent,
    CustomerComponent,
    AssignDriverComponent,
    ManageOrderComponent,
    ServicesComponent,
    TrackOrderComponent,
    ManageRFQComponent,
    CustomerRegistrationComponent
  ]
})

export class AdminLayoutModule { }
declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}
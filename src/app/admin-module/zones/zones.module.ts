import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZonesRoutingModule } from './zones-routing.module';
import { ZonesComponent } from './zones.component';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { ManageZoneComponent } from './manage-zone/manage-zone.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { DataTablesModule } from "angular-datatables";

@NgModule({
  declarations: [ZonesComponent, ManageZoneComponent],
  imports: [
    CommonModule,
    ZonesRoutingModule,
    HttpClientModule,
    MaterialModule,
    MatDialogModule,
    TranslateModule,
    ReactiveFormsModule,
    ConfirmDeletionModule,
    HeaderAsCardModule,
    DataTablesModule
  ],
  entryComponents: [
    ManageZoneComponent
  ]
})
export class ZonesModule { }

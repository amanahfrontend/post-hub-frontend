import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'manage-zone',
  templateUrl: './manage-zone.component.html',
  styleUrls: ['./manage-zone.component.scss']
})
export class ManageZoneComponent implements OnInit {

  form: FormGroup;
  countryList;
  zoneList = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
  operation: string;

  constructor(private fb: FormBuilder, private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this.fb.group({
      countryName: [''],
      zones: [''],
    });

    this.operation = this.data.operation;
  }

  ngOnInit(): void {
    this.http.get("./assets/json/country.json").subscribe(data => {
      this.countryList = data
    });
  }

}

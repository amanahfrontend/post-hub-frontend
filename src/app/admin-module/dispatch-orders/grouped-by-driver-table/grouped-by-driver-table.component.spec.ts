import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedByDriverTableComponent } from './grouped-by-driver-table.component';

describe('GroupedByDriverTableComponent', () => {
  let component: GroupedByDriverTableComponent;
  let fixture: ComponentFixture<GroupedByDriverTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupedByDriverTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedByDriverTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

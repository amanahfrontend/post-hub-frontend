import { ChangeDetectorRef, Component, AfterContentChecked, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { DriverForDispatch } from '@app/shared/models/admin/Order';
import { Subscription } from 'rxjs';
import { FacadeService } from '@app/services/facade.service';

@Component({
  selector: 'grouped-by-driver-table',
  templateUrl: './grouped-by-driver-table.component.html',
  styleUrls: ['./grouped-by-driver-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GroupedByDriverTableComponent implements OnInit, AfterContentChecked, OnDestroy {


  columnsToDisplay = ['showHide', 'select', 'driverUserName', 'itemsNo', 'actions'];
  selection = new SelectionModel<any>(true, []);

  itemsColumnsToDisplay = ['itemInfo', 'itemStatus', 'deliveryBefore', 'weight', 'actions'];
  itemsDataSource = new MatTableDataSource<any>([]);

  expandedElement: DriverForDispatch | null;
  allRowsExpanded: boolean = false;

  showFilters: boolean = false;
  groupType: number = 0;

  list: DriverForDispatch[] = [];

  totalItemsCount: number = 0;
  totalGroupsCount: number = 0;

  totalAreasCount: number = 0;
  totalBlocksCount: number = 0;

  itemsLoaded: boolean = false;

  page: number = 1;
  pageSize: number = 10;
  length: number = 0;

  subscriptions = new Subscription();
  @Output() onDrivers: EventEmitter<number[]> = new EventEmitter<number[]>();

  /**
   * 
   * @param router 
   * @param toastrService 
   * @param facadeService 
   * @param cdr 
   * @param dialog 
   */
  constructor(
    private facadeService: FacadeService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.groupedByDrivers(this.page);
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.list.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.list);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  groupedByDrivers(page: number) {
    this.list = [];

    this.facadeService.adminFacadeService.mailItemService.getDrivesrForMailItemDispatch({ pageNumber: page, pageSize: this.pageSize }).subscribe((res: any) => {
      this.length = res.totalCount;
      this.list = res.value;
      this.totalGroupsCount = this.list.length;

      const ids = this.list.map(driver => driver.id);
      this.onDrivers.emit(ids);
    });
  }

  /**
   * grouped by order
   * 
   * 
   * @param orderId 
   */
  getItemsByDriverId(driverId: number) {
    const driverIndex = this.list.findIndex(order => order.id == driverId);

    this.facadeService.adminFacadeService.mailItemService.getMailItemsByAssignedDriverForDispatch({ driverId }).subscribe((items: any) => {
      this.list[driverIndex].items = items;
      this.list[driverIndex].page = 1;
      this.calculateItemsCount();
    });
  }


  /**
   * 
   * @param expandedElement 
   * @param areaElement 
   */
  setExpandedElement(expandedElement: DriverForDispatch) {
    if (expandedElement) {
      this.expandedElement = expandedElement;
      this.getItemsByDriverId(expandedElement.id);
    }
  }

  expandCollapseAll() {
    this.allRowsExpanded = !this.allRowsExpanded;
    this.expandedElement = null;

    if ((!this.itemsLoaded) && this.allRowsExpanded) {
      this.itemsLoaded = true;
      this.fetchAllItems();
    }
  }

  fetchAllItems() {
    this.list.forEach((driver: { id: number }) => {
      this.getItemsByDriverId(driver.id);
    });

    this.calculateItemsCount();
  }

  toggleFilter() {
    this.showFilters = !this.showFilters;
  }


  calculateItemsCount(): void {
    this.totalItemsCount = 0;

    this.list.forEach((group: DriverForDispatch) => {
      if (group.items) {
        this.totalItemsCount = this.totalItemsCount + group.items.length;
      }
    });
  }

  /**
 * on change page or per page
 *
 *
 * @param event
 */
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.groupedByDrivers(this.page);
  }

  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangeByOrdersPage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.groupedByDrivers(this.page);
  }

  onChangeItemsPage(page: number, groupElement: DriverForDispatch) {
    const driverIndex = this.list.findIndex(driver => driver.id == groupElement.id);
    this.list[driverIndex].page = page;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}

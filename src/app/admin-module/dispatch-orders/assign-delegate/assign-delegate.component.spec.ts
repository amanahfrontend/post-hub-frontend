import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDelegateComponent } from './assign-delegate.component';

describe('AssignDelegateComponent', () => {
  let component: AssignDelegateComponent;
  let fixture: ComponentFixture<AssignDelegateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignDelegateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDelegateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

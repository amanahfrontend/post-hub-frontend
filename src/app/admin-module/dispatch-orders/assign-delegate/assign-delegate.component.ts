import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Driver } from '../../../shared/models/admin/driver';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'assign-delegate',
  templateUrl: './assign-delegate.component.html',
  styleUrls: ['./assign-delegate.component.scss']
})
export class AssignDelegateComponent implements OnInit {

  form: FormGroup;
  drivers: Driver[] = [];
  groupedByArea: boolean = false;

  /**
   * 
   * @param fb 
   * @param facadeService 
   */
  constructor(private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private dialogRef: MatDialogRef<AssignDelegateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.form = this.fb.group({
      driverId: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.facadeService.adminFacadeService.manageDriverService.list().subscribe(drivers => {
      this.drivers = drivers;
    });

    if (this.data.type == 'by_area' || this.data.type == 'by_block' || this.data.type == 'by_item_grouped_by_area') {
      this.groupedByArea = true;
    }
  }

  submit() {
    if (this.form.invalid) {
      return this.toastrService.error('Please Select Driver');
    }

    const form = this.form.value;
    const body = { ...form, ...this.data.body };

    if (this.groupedByArea) {
      this.facadeService.adminFacadeService.manageWorkOrderService.workorderFormAreaBlock(body).subscribe(res => {
        this.dialogRef.close(true);
      });
    } else {
      this.facadeService.adminFacadeService.manageWorkOrderService.workorderFormOrders(body).subscribe(res => {
        this.dialogRef.close(true);
      });
    }
  }
}

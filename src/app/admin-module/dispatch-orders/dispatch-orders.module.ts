import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispatchOrdersRoutingModule } from './dispatch-orders-routing.module';
import { DispatchOrdersComponent } from './dispatch-orders.component';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssignDelegateComponent } from './assign-delegate/assign-delegate.component';
import { AddToWorkorderComponent } from './add-to-workorder/add-to-workorder.component';
import { GroupedByAreasTableComponent } from './grouped-by-areas-table/grouped-by-areas-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { GroupedByDriverTableComponent } from './grouped-by-driver-table/grouped-by-driver-table.component';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    DispatchOrdersComponent,
    AssignDelegateComponent,
    AddToWorkorderComponent,
    GroupedByAreasTableComponent,
    GroupedByDriverTableComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HeaderAsCardModule,
    TranslateModule,
    AddressModule,
    ConfirmDeletionModule,
    DispatchOrdersRoutingModule,
    SetDirModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    PermissionsModule

    ],
  entryComponents: [
    AssignDelegateComponent,
    AddToWorkorderComponent
  ]
})
export class DispatchOrdersModule { }

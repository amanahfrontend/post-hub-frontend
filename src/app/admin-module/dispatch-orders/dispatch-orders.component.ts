import { ChangeDetectorRef, Component, AfterContentChecked, OnInit, OnDestroy, ViewChild, QueryList } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AreaForDispatch, BlockForDispatch, MailItemForDispatch, OrderForDispatch } from '../../shared/models/admin/Order';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../services/facade.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { AssignDelegateComponent } from './assign-delegate/assign-delegate.component';
import { AddToWorkorderComponent } from './add-to-workorder/add-to-workorder.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dispatch-orders',
  templateUrl: './dispatch-orders.component.html',
  styleUrls: ['./dispatch-orders.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DispatchOrdersComponent implements OnInit, AfterContentChecked, OnDestroy {

  columnsToDisplay = ['showHide', 'select', 'customer', 'code', 'itemsNo', 'itemStatus', 'actions'];
  areasColumnsToDisplay = ['showHide', 'select', 'area', 'itemsNo', 'raf', 'actions'];
  blocksColumnsToDisplay = ['showHide', 'select', 'name', 'actions'];
  itemsColumnsToDisplay = ['select', 'itemInfo', 'itemStatus', 'deliveryBefore', 'weight', 'actions'];

  itemsDataSource = new MatTableDataSource<any>([]);

  itemsSelection = new SelectionModel<any>(true, []);
  groupedRowsSelection = new SelectionModel<any>(true, []);

  selectedGroupsNum: number = 0;
  selectedAreasNum: number = 0;
  selectedBlocksNum: number = 0;
  selectedItemsNum: number = 0;

  expandedElement: OrderForDispatch | BlockForDispatch | null;

  allRowsExpanded: boolean = false;
  allAreasRowsExpanded: boolean = false;

  showFilters: boolean = false;
  groupType: number = 0;

  list: OrderForDispatch[] = [];
  areas: AreaForDispatch[] = [];

  totalItemsCount: number = 0;
  totalGroupsCount: number = 0;

  totalAreasCount: number = 0;
  totalBlocksCount: number = 0;

  itemsLoaded: boolean = false;

  page: number = 1;
  pageSize: number = 10;
  length: number = 0;

  areasIds: number[] = [];
  ordersIds: number[] = [];
  blocksIds: number[] = [];
  itemsIds: number[] = [];
  driversIds: number[] = [];

  subscriptions = new Subscription();
  /**
   * 
   * @param router 
   * @param toastrService 
   * @param facadeService 
   * @param cdr 
   * @param dialog 
   */
  constructor(private router: Router,
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.onSelectGroupBy();
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  /**
   * grouped by orders
   * 
   * 
   */
  groupedByOrders(page: number) {
    this.list = [];

    this.facadeService.adminFacadeService.orderService.orderForDispatch({ pageNumber: page, pageSize: this.pageSize }).subscribe((res: any) => {
      this.length = res.totalCount;
      this.list = res.value;
      this.totalGroupsCount = this.list.length;
    });
  }

  /**
   * grouped by area
   * 
   * 
  */
  getOrdersGroupedByArea(page: number) {
    this.list = [];

    this.facadeService.adminFacadeService.areaService.areasForDispatch({ pageSize: this.pageSize, pageNumber: page }).subscribe((res: AreaForDispatch[]) => {
      this.areas = res;
    });
  }

  /**
   * grouped by order
   * 
   * 
   * @param orderId 
   */
  getItemsByOrderId(orderId: number, page: number) {
    const orderIndex = this.list.findIndex(order => order.id == orderId);

    this.facadeService.adminFacadeService.mailItemService.getByItemsOrderId(orderId).subscribe((items: any) => {
      this.list[orderIndex].items = items;
      this.list[orderIndex].page = 1;
      this.calculateItemsCount();
    });
  }


  /**
   * 
   * @param expandedElement 
   * @param areaElement 
   */
  setExpandedElement(expandedElement: OrderForDispatch) {
    if (expandedElement) {
      this.expandedElement = expandedElement;
      this.getItemsByOrderId(expandedElement.id, this.page);
    }
  }

  /**
   * 
   * @returns 
   */
  isAllSelectedGroupedRows() {
    const numSelected = this.groupedRowsSelection.selected.length;
    const numRows = this.list.length;

    this.selectedGroupsNum = numRows;
    return numSelected === numRows;
  }

  masterToggleGroupedRows() {
    if (this.isAllSelectedGroupedRows()) {
      this.groupedRowsSelection.clear();
      this.ordersIds = this.groupedRowsSelection.selected.map(group => group.id);
      this.selectedGroupsNum = 0;

      this.list.forEach((group: any) => {
        if (group.items) {
          this.itemsSelection.deselect(...group.items);
          this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        }
      });

      return;
    }

    this.groupedRowsSelection.select(...this.list);
    this.ordersIds = this.groupedRowsSelection.selected.map(group => group.id);
    this.selectedGroupsNum = this.groupedRowsSelection.selected.length;

    this.list.forEach((group: any) => {
      if (group.items) {
        this.itemsSelection.select(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
      }
    });
  }

  /**
   * 
   * @param row 
   * @returns 
   */
  checkboxLabelGroupedOrders(row?: any): string {
    this.selectedGroupsNum = this.groupedRowsSelection.selected.length;
    this.ordersIds = this.groupedRowsSelection.selected.map(group => group.id);
    if (!row) {
      return `${this.isAllSelectedGroupedRows() ? 'deselect' : 'select'} all`;
    }
    return `${this.groupedRowsSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /**
   * 
   * @param element 
   * @returns 
   */
  isAllSelectedItems(element: OrderForDispatch) {
    const index: number = this.list.findIndex((order: OrderForDispatch | BlockForDispatch) => order.id == element.id);
    const numSelected = this.itemsSelection.selected.length;
    const numRows = this.list[index].items.length;

    return numSelected === numRows;
  }

  /**
   * 
   * @param element 
   * @returns 
   */
  masterToggleItems(element: OrderForDispatch) {
    const index: number = this.list.findIndex((order: OrderForDispatch | BlockForDispatch) => order.id == element.id);

    if (this.isAllSelectedItems(element)) {
      this.itemsSelection.clear();
      this.itemsIds = this.itemsSelection.selected.map(item => item.id);
      return;
    }

    this.itemsSelection.select(...this.list[index].items);
    this.itemsIds = this.itemsSelection.selected.map(item => item.id);
  }

  /**
   * 
   * @param element 
   * @param row 
   * @returns 
   */
  checkboxLabelItems(element: OrderForDispatch, row?: any): string {
    this.selectedItemsNum = this.itemsSelection.selected.length;
    this.itemsIds = this.itemsSelection.selected.map(item => item.id);

    if (!row) {
      return `${this.isAllSelectedItems(element) ? 'deselect' : 'select'} all`;
    }

    return `${this.itemsSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  expandCollapseAll() {
    this.allRowsExpanded = !this.allRowsExpanded;
    this.expandedElement = null;

    if ((!this.itemsLoaded) && this.allRowsExpanded) {
      this.itemsLoaded = true;
      this.fetchAllItems();
    }
  }

  fetchAllItems() {
    this.list.forEach((order: { id: number }) => {
      this.getItemsByOrderId(order.id, this.page);
    });

    this.calculateItemsCount();
  }

  toggleFilter() {
    this.showFilters = !this.showFilters;
  }

  /**
   * assign to workorder (value or more)
   * 
   * 
   * @param rows 
   * @param fromGroup 
   */
  issuOrAddWorkorder(workorderType: string): void {
    if (this.itemsSelection.selected.length == 0) {
      this.toastrService.error('Please Select Item(s)');
    } else {
      this.router.navigate(['manage-workorders/add'], { state: { rows: this.itemsSelection.selected, type: workorderType } });
    }
  }

  onSelectGroupBy(event?: Event) {
    this.totalItemsCount = 0;
    this.selectedItemsNum = 0;
    this.itemsLoaded = false;

    if (this.groupType == 0) {
      this.groupedByOrders(this.page);
    } else {
      this.getOrdersGroupedByArea(this.page);
    }
  }

  calculateItemsCount(): void {
    this.totalItemsCount = 0;

    this.list.forEach((group: OrderForDispatch | BlockForDispatch) => {
      if (group.items) {
        this.totalItemsCount = this.totalItemsCount + group.items.length;
      }
    });
  }

  /**
   * select group
   * 
   * 
   * 
   * @param event 
   * @param group 
   * @returns 
   */
  onSelectGroup(event: MatCheckboxChange, group: OrderForDispatch) {
    if (group && group.items) {
      if (event.checked) {
        this.itemsSelection.select(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        return this.groupedRowsSelection.toggle(group);
      } else {
        this.itemsSelection.deselect(...group.items);
        this.itemsIds = this.itemsSelection.selected.map(item => item.id);
        return this.groupedRowsSelection.toggle(group);
      }
    }
  }

  /**
 * on change page or per page
 *
 *
 * @param event
 */
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    if (this.groupType == 0) {
      this.groupedByOrders(this.page);
    } else {
      this.getOrdersGroupedByArea(this.page);
    }
  }


  /**
   * 
   * @param rowType 
   * @param element 
   */
  assignDriver(rowType: string, element: OrderForDispatch | MailItemForDispatch): void {
    let body = {};
    switch (rowType) {
      case 'by_order':
        body = {
          ordersId: [element.id],
        }
        break;

      case 'by_item_grouped_by_order':
        body = {
          mailItemsId: [element.id],
        }
        break;
      default:
        break;
    }

    const dialog = this.dialog.open(AssignDelegateComponent, {
      width: '35%',
      panelClass: 'custom-dialog-container',
      height: 'calc(45vh - 50px)',
      data: {
        type: 'by_order',
        body: body,
      }
    });

    dialog.disableClose = true;
    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.toastrService.success('Items assigned successfully!');
        dialog.close();
      }
    });
  }

  /**
   * 
   * @param rowType 
   * @param element 
   */
  assignDriverFromBulk(): void {
    if (this.groupType == 0 && this.itemsIds.length == 0) {
      this.toastrService.error('Check Item(s)');
      return;
    }

    if (this.groupType == 1 && this.itemsIds.length == 0) {
      this.toastrService.error('Check Item(s)');
      return;
    }

    let body = {};
    if (this.groupType == 0) {
      body = {
        ordersId: this.ordersIds,
        mailItemsId: this.itemsIds,
      }
    } else {
      body = {
        areasId: this.areasIds,
        blocksId: this.blocksIds,
        mailItemsId: this.itemsIds,
      }
    }

    const dialog = this.dialog.open(AssignDelegateComponent, {
      width: '35%',
      panelClass: 'custom-dialog-container',
      height: 'calc(45vh - 50px)',
      data: {
        type: this.groupType == 0 ? 'by_order' : 'by_area',
        body: body,
      }
    });

    dialog.disableClose = true;
    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.toastrService.success('Items assigned successfully!');
        dialog.close();
        this.onSelectGroupBy();
      }
    });
  }

  addToWorkorder() {
    const dialog = this.dialog.open(AddToWorkorderComponent, {
      width: '75%',
      panelClass: 'custom-dialog-container',
      minHeight: 'calc(95vh - 50px)',
      height: 'auto',
      data: {
        items: this.itemsSelection.selected
      }
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {

    });
  }


  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangeByOrdersPage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.groupedByOrders(this.page);
  }

  onAreasCount(count: number): void {
    this.totalAreasCount = count;
  }

  onBlocksCount(count: number): void {
    this.totalBlocksCount = count;
  }

  onItemsCount(count: number): void {
    this.totalItemsCount = count;
  }

  onAreasSelected(areas: AreaForDispatch[]): void {
    this.selectedAreasNum = areas.length;
    this.areasIds = areas.map(area => area.id);
  }

  onBlocksSelected(blocks: BlockForDispatch[]): void {
    this.selectedBlocksNum = blocks.length;
    this.blocksIds = blocks.map(block => block.id);
  }

  onSelectedItems(items: MailItemForDispatch[]) {
    this.selectedItemsNum = items.length;
    this.itemsIds = items.map(item => item.id);
  }

  onChangeItemsPage(page: number, groupElement: OrderForDispatch) {
    const orderIndex = this.list.findIndex(order => order.id == groupElement.id);
    this.list[orderIndex].page = page;
  }

  onDrivers(ids: number[]) {
    this.driversIds = ids;
  }

    autoAssign() {

    const body = {
        defaultDriverIds: this.driversIds
    };

    this.subscriptions.add(this.facadeService.adminFacadeService.manageWorkOrderService.autoAssignWorkOrders(body).subscribe(res => {
      this.toastrService.success('Successfully assigned');
    }));
    // need to refresh the table data
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../../services/facade.service';
import { MailItemForDispatch } from '../../../shared/models/admin/Order';

interface Row {
  id: number;
  code: string;
  deliveryBefore: number;
}

const DATA = [{
  "id": 1,
  "code": "IDR",
  "deliveryBefore": "5/6/2021"
}, {
  "id": 2,
  "code": "EUR",
  "deliveryBefore": "5/15/2021"
}, {
  "id": 3,
  "code": "IDR",
  "deliveryBefore": "7/3/2021"
}, {
  "id": 4,
  "code": "PHP",
  "deliveryBefore": "6/7/2021"
}, {
  "id": 5,
  "code": "MZN",
  "deliveryBefore": "3/6/2021"
}, {
  "id": 6,
  "code": "LKR",
  "deliveryBefore": "12/6/2020"
}, {
  "id": 7,
  "code": "CNY",
  "deliveryBefore": "12/6/2020"
}, {
  "id": 8,
  "code": "PLN",
  "deliveryBefore": "11/28/2020"
}, {
  "id": 9,
  "code": "ILS",
  "deliveryBefore": "10/27/2020"
}, {
  "id": 10,
  "code": "CNY",
  "deliveryBefore": "9/23/2020"
}];

@Component({
  selector: 'add-to-workorder',
  templateUrl: './add-to-workorder.component.html',
  styleUrls: ['./add-to-workorder.component.scss']
})
export class AddToWorkorderComponent implements OnInit {

  displayedColumns = ['selected', 'code', 'deliveryBefore'];
  dataSource = DATA;
  selectedElement: Row;
  items: MailItemForDispatch[] = [];
  workorders: any[] = [];

  page: number = 1;
  pageSize: number = 10;
  total: number = 0;

  constructor(private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private dialogRef: MatDialogRef<AddToWorkorderComponent>,
    private toastrService: ToastrService,
    private tanslateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.items = this.data.items;
    this.workordersByPagination(this.page);
  }

  workordersByPagination(page: number) {
    const body = {
      pageNumber: page,
      pageSize: this.pageSize
    };

    this.facadeService.adminFacadeService.manageOrderService.listWorkordersByPagination(body).subscribe((res: any) => {
      this.total = res.totalCount;
      this.workorders = res.value;
    });
  }

  addToWorkorder() {
    if (!this.selectedElement) {
      return this.toastrService.error(this.tanslateService.instant('Please select workorder'));
    }

    this.dialogRef.close();
      this.router.navigate(['manage-workorders/edit', this.selectedElement.id],
          {
      state: {
        rows: this.items,
        type: 'add_workorder',
      }
    });
  }

  /**
   * 
   * @param event 
   */
  onChangePage(event: { pageIndex: number, pageSize: number }) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.workordersByPagination(this.page);
  }
}

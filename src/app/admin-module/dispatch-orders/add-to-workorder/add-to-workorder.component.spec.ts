import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToWorkorderComponent } from './add-to-workorder.component';

describe('AddToWorkorderComponent', () => {
  let component: AddToWorkorderComponent;
  let fixture: ComponentFixture<AddToWorkorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToWorkorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToWorkorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatchOrdersComponent } from './dispatch-orders.component';

const routes: Routes = [
  {
    path: '',
    component: DispatchOrdersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DispatchOrdersRoutingModule { }

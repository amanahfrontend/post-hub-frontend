import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedByAreasTableComponent } from './grouped-by-areas-table.component';

describe('GroupedByAreasTableComponent', () => {
  let component: GroupedByAreasTableComponent;
  let fixture: ComponentFixture<GroupedByAreasTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupedByAreasTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedByAreasTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

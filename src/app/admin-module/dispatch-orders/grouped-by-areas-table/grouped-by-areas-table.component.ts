import { ChangeDetectorRef, Component, AfterContentChecked, OnInit, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { AreaForDispatch, BlockForDispatch, MailItemForDispatch, OrderForDispatch } from '../../../shared/models/admin/Order';
import { FacadeService } from '../../../services/facade.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Subscription } from 'rxjs';
import { NumberSymbol } from '@angular/common';
import { AssignDelegateComponent } from '../assign-delegate/assign-delegate.component';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'grouped-by-areas-table',
  templateUrl: './grouped-by-areas-table.component.html',
  styleUrls: ['./grouped-by-areas-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class GroupedByAreasTableComponent implements OnInit, AfterContentChecked, OnDestroy {
  columnsToDisplay = ['showHide', 'select', 'customer', 'code', 'itemsNo', 'itemStatus', 'actions'];
  areasColumnsToDisplay = ['showHide', 'select', 'area', 'itemsNo', 'raf', 'actions'];
  blocksColumnsToDisplay = ['showHide', 'select', 'name', 'actions'];
  itemsColumnsToDisplay = ['select', 'itemInfo', 'itemStatus', 'deliveryBefore', 'weight', 'actions'];

  byAreaSelection = new SelectionModel<any>(true, []);
  byBlockSelection = new SelectionModel<any>(true, []);
  byItemSelection = new SelectionModel<any>(true, []);

  expandedAreaElement: AreaForDispatch | null;
  expandedBlockElement: BlockForDispatch | null;

  allAreasExpanded: boolean = false;
  allBlocksExpanded: boolean = false;

  areas: AreaForDispatch[] = [];
  blocks: BlockForDispatch[] = [];

  totalItemsCount: number = 0;
  totalBlocksCount: number = 0;

  itemsLoaded: boolean = false;

  page: number = 1;
  pageSize: number = 10;
  length: number = 0;

  subscriptions = new Subscription();

  @Output() onAreasCount: EventEmitter<number> = new EventEmitter<number>();
  @Output() onAreasSelected: EventEmitter<AreaForDispatch[]> = new EventEmitter<AreaForDispatch[]>();

  @Output() onBlocksCount: EventEmitter<number> = new EventEmitter<number>();
  @Output() onBlocksSelected: EventEmitter<BlockForDispatch[]> = new EventEmitter<BlockForDispatch[]>();

  @Output() onItemsCount: EventEmitter<number> = new EventEmitter<number>();
  @Output() onSelectedItems: EventEmitter<MailItemForDispatch[]> = new EventEmitter<MailItemForDispatch[]>();

  /**
   * 
   * @param router 
   * @param toastrService 
   * @param facadeService 
   * @param cdr 
   * @param dialog 
   */
  constructor(
    private facadeService: FacadeService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.getAllGroupedByArea(this.page);
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  getAllGroupedByArea(page: number) {
    this.areas = [];
    this.subscriptions.add(this.facadeService.adminFacadeService.areaService.areasForDispatch({ pageSize: this.pageSize, pageNumber: page }).subscribe((res: any) => {
      this.areas = res.value;
      this.length = res.totalCount;
      this.onAreasCount.emit(this.areas.length);
    }));
  }

  getBlocksByAreaId(areaId: number, expandedElement?: AreaForDispatch) {
    const areaIndex = this.areas.findIndex((area: AreaForDispatch) => area.id == areaId);

    this.subscriptions.add(this.facadeService.adminFacadeService.areaService.blocksByAreaId(areaId).subscribe(blocks => {
      if (blocks && blocks.length > 0) {
        this.areas[areaIndex].blocks = blocks;
        this.areas[areaIndex].page = 1;

        this.totalBlocksCount = this.totalBlocksCount + this.areas[areaIndex].blocks.length;
        this.onBlocksCount.emit(this.totalBlocksCount);
      }

      if (expandedElement) {
        this.expandedAreaElement = expandedElement;
      }
    }));
  }

  getItemsByBlockId(blockId: number, areaId?: number) {
    const body = { areaId, blockId };
    const areaIndex = this.areas.findIndex((area: AreaForDispatch) => area.id == areaId);
    const blockIndex = this.areas[areaIndex].blocks.findIndex((block: BlockForDispatch) => block.id == blockId);

    this.subscriptions.add(this.facadeService.adminFacadeService.mailItemService.itemsByBlockId(body).subscribe((items: MailItemForDispatch[]) => {
      if (items && items.length > 0) {
        this.areas[areaIndex].blocks[blockIndex].items = items;
        this.areas[areaIndex].blocks[blockIndex].page = 1;
        this.calculateItemsCount();
      }
    }));
  }

  /**
   * 
   * @param expandedElement 
   */
  setExpandedBlockElement(expandedElement: BlockForDispatch, areaElement: AreaForDispatch) {
    if (expandedElement) {
      this.expandedBlockElement = expandedElement;
      this.getItemsByBlockId(expandedElement.id, areaElement.id);
    } else {
      this.expandedBlockElement = null;
    }
  }

  /**
   * 
   * @param expandedAreaElement 
   */
  setExpandedAreaElement(expandedAreaElement: AreaForDispatch) {
    if (expandedAreaElement) {
      this.expandedAreaElement = expandedAreaElement;
      this.getBlocksByAreaId(expandedAreaElement.id);
    } else {
      this.expandedAreaElement = null;
    }
  }

  isAllSelectedByArea() {
    const numSelected = this.byAreaSelection.selected.length;
    const numRows = this.areas.length;
    return numSelected === numRows;
  }

  isAllSelectedByBlock(areaElement: AreaForDispatch) {
    const numSelected = this.byBlockSelection.selected.length;
    const index = this.areaIndex(areaElement.id);

    if (this.areas[index] && this.areas[index].blocks.length) {
      const numRows = this.areas[index].blocks.length;
      return numSelected === numRows;
    }
  }

  isAllSelectedByItem(areaElement: AreaForDispatch, blockElement: BlockForDispatch) {
    const numSelected = this.byBlockSelection.selected.length;
    const areaIndex = this.areaIndex(areaElement.id);
    const blocks = this.areas[areaIndex].blocks;

    const blockIndex = blocks.findIndex((block: BlockForDispatch) => block.id == blockElement.id);
    const items: MailItemForDispatch[] = this.areas[areaIndex].blocks[blockIndex].items;

    if (items.length) {
      const numRows = items.length;
      return numSelected === numRows;
    }
  }

  masterToggleByArea() {
    if (this.isAllSelectedByArea()) {
      this.byAreaSelection.clear();
      this.onAreasSelected.emit(this.byAreaSelection.selected);

      this.areas.forEach((area: AreaForDispatch) => {
        if (area.blocks) {
          this.byBlockSelection.deselect(...area.blocks);

          area.blocks.forEach((block: BlockForDispatch) => {
            if (block.items) {
              this.byItemSelection.deselect(...block.items);
            }
          });
        }
      });

      return;
    }

    this.byAreaSelection.select(...this.areas);
    this.onAreasSelected.emit(this.byAreaSelection.selected);

    this.areas.forEach((area: AreaForDispatch) => {
      if (area.blocks) {
        this.byBlockSelection.select(...area.blocks);

        area.blocks.forEach((block: BlockForDispatch) => {
          if (block.items) {
            this.byItemSelection.select(...block.items);
          }
        })
      }
    });
  }

  /**
   * select / deselect items under block
   * 
   * 
   * @param areaElement 
   * @returns 
   */
  masterToggleByBlock(areaElement: AreaForDispatch) {
    const areaIndex = this.areaIndex(areaElement.id);
    const blocks: BlockForDispatch[] = this.areas[areaIndex].blocks;

    if (this.isAllSelectedByBlock(areaElement)) {
      this.byBlockSelection.clear();
      this.onBlocksSelected.emit(this.byBlockSelection.selected);

      blocks.forEach((block: BlockForDispatch) => {
        if (block.items) {
          this.byItemSelection.deselect(...block.items);
        }
      });

      return;
    }

    this.byBlockSelection.select(...blocks);
    blocks.forEach((block: BlockForDispatch) => {

      if (block.items) {
        this.byItemSelection.select(...block.items);
        this.onBlocksSelected.emit(this.byBlockSelection.selected);
      }
    })
  }

  /**
   * select / deselect all items
   * 
   * 
   * @param areaElement 
   * @param blockElement 
   * @returns 
   */
  masterToggleByItem(areaElement: AreaForDispatch, blockElement: BlockForDispatch) {
    const areaIndex = this.areaIndex(areaElement.id);
    const blocks: BlockForDispatch[] = this.areas[areaIndex].blocks;
    const blockIndex = blocks.findIndex((block: BlockForDispatch) => block.id == blockElement.id);
    const items: MailItemForDispatch[] = this.areas[areaIndex].blocks[blockIndex].items;

    if (this.isAllSelectedByItem(areaElement, blockElement)) {
      blocks.forEach((block: BlockForDispatch) => {
        if (block.items) {
          this.byItemSelection.deselect(...block.items);
        }
      });

      this.onSelectedItems.emit(this.byItemSelection.selected);
      return;
    }

    this.byItemSelection.select(...items);
    this.onSelectedItems.emit(this.byItemSelection.selected);
  }

  /**
   * 
   * @param row 
   * @returns 
   */
  checkboxLabelByArea(row?: any): string {
    this.onAreasSelected.emit(this.byAreaSelection.selected);

    if (!row) {
      return `${this.isAllSelectedByArea() ? 'deselect' : 'select'} all`;
    }

    return `${this.byAreaSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  checkboxLabelByItem(areaElement: AreaForDispatch, blockElement: BlockForDispatch, row?: any): string {
    this.onSelectedItems.emit(this.byItemSelection.selected);

    if (!row) {
      return `${this.isAllSelectedByItem(areaElement, blockElement) ? 'deselect' : 'select'} all`;
    }

    return `${this.byItemSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /**
 * 
 * @param row 
 * @returns 
 */
  checkboxLabelByBlock(areaElement: AreaForDispatch, row?: any): string {
    this.onBlocksSelected.emit(this.byBlockSelection.selected);

    if (!row) {
      return `${this.isAllSelectedByBlock(areaElement) ? 'deselect' : 'select'} all`;
    }

    return `${this.byBlockSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  expandCollapseAllByArea() {
    this.allAreasExpanded = !this.allAreasExpanded;
    this.expandedAreaElement = null;

    this.totalBlocksCount = 0;
    this.totalItemsCount = 0;

    this.onBlocksCount.emit(0);
    this.onItemsCount.emit(0);

    this.onBlocksSelected.emit([]);
    this.onSelectedItems.emit([]);

    if ((!this.itemsLoaded) && this.allAreasExpanded) {
      this.itemsLoaded = true;
      this.fetchAllBlocksByEachArea();
    }
  }

  fetchAllBlocksByEachArea() {
    this.allAreasExpanded = true;

    this.areas.forEach((area: AreaForDispatch, index: number) => {
      this.subscriptions.add(this.facadeService.adminFacadeService.areaService.blocksByAreaId(area.id).subscribe(blocks => {
        if (blocks && blocks.length > 0) {
          this.areas[index].blocks = blocks;
          if (area.blocks) {
            area.blocks.forEach((block: BlockForDispatch) => {
              this.getItemsByBlockId(block.id, area.id);
              this.allBlocksExpanded = true;
            });

            this.totalBlocksCount = this.totalBlocksCount + area.blocks.length;
            this.onBlocksCount.emit(this.totalBlocksCount);
          }
        }
      }));
    });

    this.calculateItemsCount();
  }

  calculateItemsCount(): void {
    this.totalItemsCount = 0;

    this.areas.forEach((area: AreaForDispatch) => {
      if (area.blocks) {
        area.blocks.forEach((block: BlockForDispatch) => {
          if (block.items) {
            this.totalItemsCount = this.totalItemsCount + block.items.length;
            this.onItemsCount.emit(this.totalItemsCount);
          }
        });
      }
    });
  }

  /**
   * select all items under selected area
   * 
   * 
   * @param event 
   * @param byAreaGroup 
   * @returns 
   */
  onSelectByArea(event: MatCheckboxChange, areaForDispatch: AreaForDispatch) {
    if (areaForDispatch && areaForDispatch.blocks) {
      if (event.checked) {
        this.byBlockSelection.select(...areaForDispatch.blocks);

        areaForDispatch.blocks.forEach((block: BlockForDispatch) => {
          if (block.items) {
            this.byItemSelection.select(...block.items);

            this.onSelectedItems.emit(this.byItemSelection.selected);
          }
        });

        return this.byBlockSelection.toggle(areaForDispatch);
      } else {
        areaForDispatch.blocks.forEach((block: BlockForDispatch) => {
          if (block.items) {
            this.byItemSelection.deselect(...block.items);

            this.onSelectedItems.emit(this.byItemSelection.selected);
          }
        });

        this.byBlockSelection.deselect(...areaForDispatch.blocks);
        return this.byBlockSelection.toggle(areaForDispatch);
      }
    }
  }

  /**
   * select all items under selected block
   * 
   * 
   * @param event 
   * @param byAreaGroup 
   * @returns 
   */
  onSelectByBlock(event: MatCheckboxChange, blockForDispatch: BlockForDispatch): void {
    if (blockForDispatch && blockForDispatch.items) {
      if (event.checked) {
        this.byItemSelection.select(...blockForDispatch.items);
        return this.byBlockSelection.toggle(blockForDispatch);
      } else {
        this.byItemSelection.deselect(...blockForDispatch.items);
        return this.byBlockSelection.toggle(blockForDispatch);
      }
    }
  }

  /**
   * 
   * @param event 
   */
  onChangePage(event: { pageIndex: NumberSymbol, pageSize: number }): void {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.getAllGroupedByArea(this.page);
  }

  private areaIndex(areaId: number): number {
    return this.areas.findIndex((area: AreaForDispatch) => area.id == areaId);
  }

  /**
   * 
   * @param page 
   * @param areaElement 
   */
  onChangeBlocksPage(page: number, areaElement: AreaForDispatch): void {
    const areaIndex = this.areas.findIndex((area: AreaForDispatch) => area.id == areaElement.id);
    this.areas[areaIndex].page = page;
  }

  /**
   * 
   * @param page 
   * @param areaElement 
   * @param blockElement 
   */
  onChangeItemsPage(page: number, blockElement: BlockForDispatch, areaElement: AreaForDispatch): void {
    const areaIndex = this.areas.findIndex((area: AreaForDispatch) => area.id == areaElement.id);

    if (this.areas[areaIndex].blocks) {
      const blockIndex = this.areas[areaIndex].blocks.findIndex((block: BlockForDispatch) => block.id == blockElement.id);
      this.areas[areaIndex].blocks[blockIndex].page = page;
    }
  }

  /**
   * 
   * @param rowType 
   * @param element 
   */
  assignDriver(rowType: string, element: AreaForDispatch | BlockForDispatch | MailItemForDispatch): void {
    let body = {};
    switch (rowType) {
      case 'area':
        body = {
          areasIds: [element.id],
        }
        break;

      case 'block':
        body = {
          blocksId: [element.id],
        }
        break;

      case 'by_item_grouped_by_area':
        body = {
          mailItemsId: [element.id],
        }
        break;
      default:
        break;
    }

    const dialog = this.dialog.open(AssignDelegateComponent, {
      width: '35%',
      panelClass: 'custom-dialog-container',
      height: 'calc(45vh - 50px)',
      data: {
        type: 'by_area',
        body: body,
      }
    });

    dialog.disableClose = true;
    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.toastrService.success('Items assigned successfully!');
        dialog.close();
      }
    });
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

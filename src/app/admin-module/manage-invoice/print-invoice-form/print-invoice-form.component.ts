import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
//import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { Observable, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../services/shared/language.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RFQ } from 'app/shared/models/admin/PriceList/RFQ';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { DOCUMENT } from '@angular/common';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { invoice } from '@app/shared/models/admin/invoice';

@Component({
  selector: 'print-invoice-form',
  templateUrl: './print-invoice-form.component.html',
  styleUrls: ['./print-invoice-form.component.scss']
})
export class ManagePrintInvoiceFormComponent implements OnInit {
  form: FormGroup;
  todayDate: Date;  
  invoices:invoice[]=[]; 
  subscriptions = new Subscription();
  locale: string = 'en'; 
  loading: boolean = false;
  displayedColumns: string[] = [
    'serialNo',
    'invoiceTopic',
    'noOfItems',
    'unitPrice',
    'totalUnitsPrice'  
  
  ];  
  InvoiceId: any;
  minDate: Date;
  maxDate: Date;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {   
    this.InvoiceId = this.data.id; 
   
  }

  ngOnInit(): void {

    if (this.InvoiceId) {
      this.getInvoiceDetails(this.InvoiceId);
    }
    this.todayDate=new Date();
    this.locale = this.languageService.currentLanguage; 
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);  
  } 
  getInvoiceDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.ManageInvoicesService.get(id).subscribe(res => {
  
      this.invoices.push(res);   
    }));
  }
  getInnerHtml(word:any){
      return word;
  }
  printForm() {
    this.loading = true;

    const htmlWidth = $("#print-section").width();
    const htmlHeight = $("#print-section").height();

    const topLeftMargin = 15;

    let pdfWidth = htmlWidth + (topLeftMargin * 2);
    let pdfHeight = (pdfWidth * 1.5) + (topLeftMargin * 2);

    const canvasImageWidth = htmlWidth;
    const canvasImageHeight = htmlHeight;

    const totalPDFPages = Math.ceil(htmlHeight / pdfHeight) - 1;

    const data = this.document.getElementById('print-section');
    html2canvas(data, { allowTaint: true }).then(canvas => {

      canvas.getContext('2d');
      const imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new jsPDF('p', 'pt', [pdfWidth, pdfHeight]);
      pdf.addImage(imgData, 'png', topLeftMargin, topLeftMargin, canvasImageWidth, canvasImageHeight);

      for (let i = 1; i <= totalPDFPages; i++) {
        pdf.addPage([pdfWidth, pdfHeight], 'p');
        pdf.addImage(imgData, 'png', topLeftMargin, - (pdfHeight * i) + (topLeftMargin * 4), canvasImageWidth, canvasImageHeight);
      }

      this.loading = false;
      pdf.save(`PriceQuotation(s) Document ${new Date().toLocaleString()}.pdf`);
    });
  }

}

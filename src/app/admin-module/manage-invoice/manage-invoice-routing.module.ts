import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageInvoiceFormComponent } from './manage-invoice-form/manage-invoice-form.component';
import { ManageInvoiceComponent } from './manage-invoice.component';



const routes: Routes = [
  {
    path: '',
    component: ManageInvoiceComponent
  }
  ,
    {
      path: 'add',
      component: ManageInvoiceFormComponent
    },
    {
      path: 'edit',
      component: ManageInvoiceFormComponent
    }
    ,
    {
      path: 'view',
      component: ManageInvoiceFormComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageInvoiceRoutingModule { }

import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FacadeService } from '@app/services/facade.service';
import { invoice } from '@app/shared/models/admin/invoice';
import { PL } from '@app/shared/models/admin/PriceList/PL';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs-compat';
import { ManagePrintInvoiceFormComponent } from '../print-invoice-form/print-invoice-form.component';

import { Option } from './../../../shared/models/admin/option';
@Component({
  selector: 'manage-invoice-form.component',
  templateUrl: './manage-invoice-form.component.html',
  styleUrls: ['./manage-invoice-form.component.scss']
})
export class ManageInvoiceFormComponent implements OnInit {
  form: FormGroup;
  invoice: invoice;
  businessCustomers: Option[] = [];
  subscriptions = new Subscription();
  statuses: Option[] = [];
  sectors: Option[] = [];
  @Output() onForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() itemList: EventEmitter<any> = new EventEmitter<any>();
  index: number;
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = ['select',
    'fromArea', 'toArea',
    'fromWeight', 'toWeight',
    'price', 'serviceTimeUnit','serviceTime','actions'];
  row: PL;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  mode: any; 
  contracts: any;
  contResult: any;
  itemsSum: number;
  unitPrice: any;
  contractId: any;
  InvoiceId: any;
  businessCustomerId: any;
  savedInvoiceId: any;
  minDate: Date;
  maxDate: Date;
  constructor(private fb: FormBuilder, private facadeService: FacadeService,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private dialog: MatDialog,
    private router: Router,
  ) {
    this.form = this.fb.group({
      sectorTypeId: [],    
      validToDate: new FormControl(new Date()),
      validFromDate:new FormControl(new Date()),
      filterCustCtrl:[''],
      businessCustomerId: [''],
      filterContCtrl:[''],
      contractCode:[''],
      statusId: [],
      invoiceTopic: [''], 
      noOfItems:[''],
      unitPrice:[''],
      totalUnitsPrice:['']  ,
      invoiceNo:[''],
      invoiceStatusName:['']  
    
    });
  }

  ngOnInit(): void {
   
    this.activatedRoute.queryParams
      .subscribe(
        params => {
          this.InvoiceId = params.id;
          this.mode = params.mode;
          this.businessCustomerId=params.customerId
          if (this.InvoiceId) {
            this.getInvoiceDetails(this.InvoiceId);
          }
        });
    this.getAllPLStatuses();
    this.getAllServiceSectors();
    this.getBusinessCustomers();
    if(this.mode =='edit'||this.mode=='view'){
      this.getContractsByCustId(this.businessCustomerId);
     
    }
    if (this.mode == 'view') {     
      this.form.disable()
    }
    
    if (this.mode == 'edit') {
      this.form.get('businessCustomerId').disable();
      this.form.get('contractCode').disable();     
      this.form.get('invoiceTopic').disable();     
      this.form.get('noOfItems').disable();     
      this.form.get('unitPrice').disable();     
      this.form.get('totalUnitsPrice').disable();   
     
    }
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31); 
  }
  setDefaultValue(){
    var id=this.contracts.filter((x) => x.code == this.form.controls['contractCode'].value)[0].id ;
    this.form.patchValue({
      contractCode : id
    })
    console.log("ggggg",this.contracts)
  }
  optionSelected(e:any){
   
     this.getContractsByCustId(e);
     this.getContractItemsCountAndPrices(e);
  }
  contractSelected(e:any){
    this.contractId=e;
    if(this.mode !='edit' && this.mode!='view'){
    var code=this.contracts.filter((x) => x.id == e)[0].code
    this.GetUnitPriceByBusinessCustomerIdAndContractCode(code);
    }
  }
  // compareFunction(o1: any, o2: any) {
  //   return (o1.name == o2.name && o1.id == o2.id);
  //  }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.listNew().subscribe((res: Option[]) => {
      this.businessCustomers = res;
    });
  }
  getContractsByCustId(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.ManageInvoicesService.getContractsByCustId(id).subscribe(res => {
      this.contracts = res;  
      if(this.mode =='edit'||this.mode=='view'){    
      this.setDefaultValue();
      }      
    }));
  }

  getInvoiceDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.ManageInvoicesService.get(id).subscribe(res => {
      this.invoice = res;      
      this.form.patchValue(this.invoice);     
    }));
  }
  getAllPLStatuses() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllPLStatuesAsync().subscribe((res: Option[]) => {
      this.statuses = res;
    });
  }
  getAllServiceSectors() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllServiceSectorsAsync().subscribe((res: Option[]) => {
      this.sectors = res;
    });
  }
  getContractItemsCountAndPrices(id:number) {
    const body: {
      businessCustomerId: number, fromDate: Date, toDate: Date
    } = {    
      businessCustomerId:id,
      fromDate: this.form.value.validFromDate ? new Date(this.form.value.validFromDate) : null,
      toDate: this.form.value.validToDate ? new Date(this.form.value.validToDate) : null
     
    };  
    
    this.facadeService.adminFacadeService.ManageInvoicesService.GetBusinessCustomerDistributionCountReport(body).subscribe(
      (res: any) => {        
      this.contResult = res;  
      this.itemsSum =0; 
      this.contResult.forEach(cont => {      
        this.itemsSum +=cont.itemsNo ;
        console.log(this.itemsSum)
      });  
      this.form.get('noOfItems').setValue(this.itemsSum);
    }); 

    
  }
  GetUnitPriceByBusinessCustomerIdAndContractCode(code:string) {
    const body: {
      businessCustomerId: number, contractCode: string
    } = {    
      businessCustomerId:this.form.value.businessCustomerId,
      contractCode:code 
    };  
    
    this.facadeService.adminFacadeService.ManageInvoicesService.GetUnitPriceByBusinessCustomerIdAndContractCode(body).subscribe(
      (res: any) => { 
      this.unitPrice=res;
      this.form.get('unitPrice').setValue(res);
    }); 

    
  }
  calcTotalPrice(){   
   
    var tot = 0;    
    tot=this.form.value.noOfItems * this.unitPrice ;
    this.form.get('totalUnitsPrice').setValue(tot);
  }
  
  /**
   * 
   * @param opertaion 
   * @param isEdit 
   * @param element 
   */
  // manageItem(opertaion: string, isEdit: boolean, element?: any): void {
  //  
  //   const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
  //   const dialogRef = this.dialog.open(ManageItemComponent, {
  //     width: '75%',
  //     panelClass: 'custom-dialog-container',
  //     height: 'calc(80vh - 50px)',
  //     data: { type, row: element, isEdit },
  //   });

  //   dialogRef.disableClose = true;

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result && element) {
  //       const dataSource = this.dataSource.data;
  //       dataSource[this.index] = result.item;
  //       this.dataSource.data = dataSource;

  //       this.itemList.emit(this.dataSource.data);

  //       this.form.get('filledItemNo').setValue(this.dataSource.data.length);

  //       let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);

  //       this.form.get('remainingItemNo').setValue(remaining);

  //     } else if (result) {
  //       const item = result.item;
  //       const dataSource = this.dataSource.data;
  //       dataSource.push(item);

  //       this.dataSource.data = dataSource;
  //       this.itemList.emit(this.dataSource.data);

  //       this.form.get('filledItemNo').setValue(this.dataSource.data.length);

  //       let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);

  //       this.form.get('remainingItemNo').setValue(remaining);

  //     }
  //   });


  // }
  setIndex(index: number) {
    this.index = index;
  }

  /**
   * 
   * @param event 
   * @param index 
   */
  onDelete(event: boolean, index: number) {
    if (event) {
      const dataSource = this.dataSource.data;
      dataSource.splice(index, 1);
      this.dataSource.data = dataSource;
    }
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);

  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  submit(val: any) {
   
    this.form.value.plItems = this.dataSource.data;
    let body = { ... this.row, ... this.form.value };

    let objectToPost = body;
    objectToPost.contractId=this.contractId;

    if (this.mode === 'edit') {
      objectToPost.id = this.InvoiceId;
      this.facadeService.adminFacadeService.ManageInvoicesService.update(objectToPost).subscribe(res => {
        this.savedInvoiceId=res;
        this.toastrService.success(this.translateService.instant('Invoice Updated Successfully'));
        if (val != 'print')
          this.router.navigate(['manage-invoice']);
        if (val === 'print')
          this.printInvoice(this.savedInvoiceId);
      });
    } else {
      this.facadeService.adminFacadeService.ManageInvoicesService.create(objectToPost).subscribe(res => {
        this.savedInvoiceId=res;
        this.toastrService.success(this.translateService.instant('Invoice Generated Successfully'));
        if (val != 'print')
          this.router.navigate(['manage-invoice']);
        if (val === 'print')
          this.printInvoice(this.savedInvoiceId);
      });
    }
  }
  printInvoice(id): void {
     this.openPrintDialog(id);
  }
  openPrintDialog(invId:any){
    const dialog = this.dialog.open(ManagePrintInvoiceFormComponent, {
      width: '60%',
      data: {
      id:invId
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {    
      //this.list(this.page);
    });
  }
  reset() {  
    this.form.reset();
    this.router.navigate(['manage-invoice/add']);
  }
  close(){
    this.router.navigate(['manage-invoice']);
  }
}

import { AfterViewInit, Optional } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { MouseEvent } from '@agm/core';
import { OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '@app/services/facade.service';

import { Router } from '@angular/router';
import { saveFile } from '../../shared/helpers/download-link';
import { Option } from './../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { invoice } from '@app/shared/models/admin/invoice';
import { MatDialog } from '@angular/material/dialog';
import { ManagePrintInvoiceFormComponent } from './print-invoice-form/print-invoice-form.component';

interface InvoiceStatus {
  id:number;
  name: string; 
}
@Component({
  selector: 'manage-invoice.component',
  templateUrl: './manage-invoice.component.html',
  styleUrls: ['./manage-invoice.component.scss']
})

export class ManageInvoiceComponent implements OnInit, AfterViewInit, OnDestroy {
  form: FormGroup; 
  displayedColumns: string[] = [
    'contractCode',
    'invoicePeriodFrom',
    'invoiceStatus',
    'businessCustomerName',
    'invoiceNo',
    'invoicePeriodTo',    
    'totalValue',   
    'actions'
  ];
  
  subscriptions = new Subscription();
 // statuses: Option[] = [];
  sectors: Option[] = [];
  businessCustomers: Option[] = [];
  minDate: Date;
  maxDate: Date;
  searchBy: string = '';
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
   invoices: invoice[] = [];
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  dtTrigger: Subject<any> = new Subject();
  checkAll = false
  locationDetails = []
  locationForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  companyForm;
  masterCheck = false
  countryList;
  public statuses: Array<{id: number, name: string}> = [
    {id: 1, name: 'New'},
    {id: 2, name: 'Active'}
   
  ];
 
  constructor(private translateService: TranslateService , 
    private dialog: MatDialog,
    private router: Router,  
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    ) {
      
      this.form = this.fb.group({
        
        filterCustCtrl:[''],
        filterSectorCtrl:[''] ,
        filterStatusCtrl:[''],
        sectorTypeId:[''],
        statusId:[''],
        fromDate:new FormControl(''), //new FormControl(''),
        toDate: new FormControl(''),
        businessCustomerId: ['']
      });
    
    
  }

  ngOnInit(): void {
   
    this.list(this.page);
    this.getAllServiceSectors();
    //this.getAllPLStatuses();   
    this.getBusinessCustomers(); 
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
  }
 
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.listNew().subscribe((res: Option[]) => {
      this.businessCustomers = res;
    });
  }
  // getAllPLStatuses() {
  //   this.facadeService.adminFacadeService.managePriceListService.GetAllPLStatuesAsync().subscribe((res: Option[]) => {
  //     this.statuses = res;
  //   });
  // }
  getAllServiceSectors() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllServiceSectorsAsync().subscribe((res: Option[]) => {
      this.sectors = res;
    });
  }

  /**
   * list invoices
   * 
   * 
   * @param page 
   */
   getFilteredData(){
      this.list(this.page);
   }
   list(page?: number): void {
    
     let body = { ... this.form.value };

    let objectToPost = body;
    // const body: {

    //   pageNumber: number,
    //   pageSize: number, businessCustomerId: number, fromDate: Date, toDate: Date ,code:string
    // } = {
    //   pageNumber: this.page,
    //   pageSize: this.pageSize,
    //   businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
    //   fromDate: this.form.value.fromDate ? new Date(this.form.value.fromDate) : null,
    //   toDate: this.form.value.toDate ? new Date(this.form.value.toDate) : null,
    //   code:this.form.value.code ? this.form.value.code : ''
    // };
    

    this.facadeService.adminFacadeService.ManageInvoicesService.listByPagination(objectToPost).subscribe((res: any) => {
      this.invoices = res.result;
      this.total = res.totalCount;
    });
  }
   /**
   * add / edit view row
   * 
   *   
   */
    generateInvoice(): void {
      this.router.navigate(['manage-invoice/add']);
    }
    editInvoice(element:any) {
    
      this.router.navigate(['/manage-invoice/edit'], {queryParams: {id: element.id,mode:'edit',customerId:element.businessCustomerId}});      
    }
    viewInvoice(element:any){    
      this.router.navigate(['/manage-invoice/edit'], {queryParams: {id: element.id,mode:'view' ,customerId:element.businessCustomerId}});     
    
    }  
    printInvoice(element: any): void {
    
      this.openPrintDialog(element.id);
    }
    openPrintDialog(invId:any){
      const dialog = this.dialog.open(ManagePrintInvoiceFormComponent, {
        width: '60%',
        data: {
        id:invId
        },
        panelClass: 'custom-dialog-container',
        height: 'calc(95vh - 50px)'
      });
  
      dialog.disableClose = true;
  
      dialog.afterClosed().subscribe(res => {    
        this.list(this.page);
      });
    }
    
    export() {
      const body = {
        pageNumber: this.page,
        pageSize: this.pageSize,
        searchBy: this.searchBy,
      }
  
      this.facadeService.adminFacadeService.managePriceListService.exportToExcel(body).subscribe(data => {
        saveFile(`PriceList.xlsx ${new Date().toLocaleDateString()}`, "data:attachment/text", data);
      });
    }
    deletePriceList(pl: any) {
      this.facadeService.adminFacadeService.managePriceListService.deletePL(pl.id).subscribe((res: any) => {
  
        const index = this.invoices.indexOf(pl);
        if (index >= 0) {
          this.invoices.splice(index, 1);
          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`Price List has been Deleted Successfully`));
        }
  
      });
    }
    private replaceRoutePage(page: number) {
      this.router.navigate(['manage-invoice'], { queryParams: { page: page } });
    }
    search(): void {
      
      const body: { pageNumber: number, pageSize: number, searchBy: string } = {
        pageNumber: 1,
        pageSize: 10,
        searchBy: this.searchBy,
      };
  
      this.facadeService.adminFacadeService.ManageInvoicesService.GetSearchedInvoicesPagginated(body).subscribe((res: any) => {
        this.invoices = res.result;
        this.total = res.totalCount;
      });
    }
  
 
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  
 

 
}
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { App } from '../../core/app';
import { AdminFacadeService } from '../../services/admin/admin-facade.service';
import { FacadeService } from '../../services/facade.service';
import { LanguageService } from '../../services/shared/language.service';
import { ChangeLanguage } from '../../shared/models/admin/profile/ChangeLanguage';
import { Profile } from '../../shared/models/admin/profile/Profile';
import { Address, Country } from '../../shared/models/components';

const COUNTRIES = 'COUNTRIES';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
    data: number;
    point;
    country_list = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
    url: any = "./assets/img/faces/avtar.jpeg"
    panelOpenState = false;
    step = 0;
    currentUser: Profile;
    userLanguages: ChangeLanguage;
    isTenant: boolean = false;
    form: FormGroup;
    selectedMobileNumber: number;
    selectedCountry: Country;
    selectedCountryCode: Country;

    countries: Country[] = [];
    filteredCountries: Observable<Country[]>;
    address: Address;

    minDate: Date;
    maxDate: Date;
    validPhoneNumber: boolean = false;
    accept = 'image/*';
    selectedFile: File;
    locale: string = 'en';
    numberMessage: string = '';


    constructor(
        private route: ActivatedRoute,
        private adminFacadeService: AdminFacadeService,
        fb: FormBuilder,
        private facadeService: FacadeService,
        private toastrService: ToastrService,
        private languageService: LanguageService,
        private translateService: TranslateService,
        private cdr: ChangeDetectorRef
    ) {


        this.form = fb.group({
            firstName: ['', [Validators.required]],
            middleName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            countryCtrl: ['', [Validators.required]],
            nationalId: [''],
            phoneNumber: [''],
            dateOfBirth: [''],
            email: ['', [Validators.required, Validators.email]],
            id: [{ value: '', disabled: true }],
            displayImage: []
        });


        $(document).ready(function () {
            $(".upload-button").on("click", function () {
                $(".file-upload").click();
            });
        });
    }

    ngOnInit() {
        this.countries = JSON.parse(localStorage.getItem(COUNTRIES));
        this.locale = this.languageService.currentLanguage;
        this.currentUser = this.route.snapshot.data.userInfo;
        this.userLanguages = { DashBoardLanguage: this.currentUser.dashBoardLanguage }

        this.filteredCountries = this.form.get('countryCtrl').valueChanges
            .pipe(
                startWith(''),
                map(country => country ? this.filter(country) : this.countries.slice())
            );

        const currentYear = new Date().getFullYear();
        this.minDate = new Date(currentYear - 59, 11, 31);
        this.maxDate = new Date(currentYear + 17, 11, 31);

        this.point = { x: '5' }
        this.form.patchValue(this.currentUser);
        this.form.get('countryCtrl').setValue(this.currentUser.nationality);
        this.form.get('email').setValue(this.currentUser.email);
        this.url = `${App.backEndUrl}/${this.currentUser.profilePhotoURL}`;

        ///Disable Items 
        this.form.get('email').disable();
        this.form.get('nationalId').disable();
        this.form.get('countryCtrl').disable();
    }

    onSelectFile(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            this.selectedFile = event.target.files[0];

            reader.readAsDataURL(event.target.files[0]);

            reader.onload = (event) => {
                this.url = event.target.result;
                this.facadeService.accountService.changeUserLogo(this.url);
            }
        }
    }

    private filter(value: string): Country[] {
        const filterValue = value.toLowerCase();
        return this.countries.filter(option => option.name.toLowerCase().includes(filterValue));
    }

    /**
 * on type number 
 * 
 * 
 * @param number 
 */
    number(number: number): void {
        this.selectedMobileNumber = number;
    }

    /**
     * on select country flag from phone number
     * 
     * 
     * @param country 
     */
    onCountry(country: Country): void {
        this.selectedCountryCode = country;
    }

    setCountry(country: Country) {
        this.selectedCountry = country;
    }

    isValidPhoneNumber(valid: boolean) {
        this.validPhoneNumber = valid;
    }

    onAddressChanges(address: Address) {
        this.address = address;
    }

    /**
     *  Event handler for Button Sumbit
     * 
     * 
     */
    onSubmit() {
        if (this.form.invalid) {
            return;
        }

        let body = { ... this.currentUser, ... this.form.value };
        body.address = this.address;

        if (this.selectedCountry) {
            body.nationalityId = this.selectedCountry.id;
        }

        body.mobileNumber = this.selectedMobileNumber;
        if (this.selectedCountryCode) {
            body.countryId = this.selectedCountryCode.id;
        }

        body.profilePhotoFile = this.selectedFile;
        this.adminFacadeService.profileService.UpdateUserInfo(body).subscribe(res => {
            if (res.succeeded == true) {
                this.toastrService.success(this.translateService.instant('Profile Updated Successfully'));

                const userLogo: string = `${App.backEndUrl}/${body.profilePhotoURL}`;
                this.facadeService.accountService.updateSomeUserData(body.firstName, userLogo);
            }
            else {
                this.toastrService.error(this.translateService.instant('Unexpexted Error'));
            }
        });
    }

    /**
     * Return the Error Message to display
     * 
     * 
     * @param input 
     */
    getError(input: string) {
        switch (input) {
            case 'firstName':
                if (this.form.get('firstName').hasError('required')) {
                    return this.translateService.instant('FullName required');
                }

                if (this.form.get('firstName').hasError('minlength')) {
                    return this.translateService.instant('FullName Must be at least 4 Characters');
                }
                break;

            case 'email':
                if (this.form.get('email').hasError('required')) {
                    return this.translateService.instant('Email required');
                } else if (this.form.get('email').hasError('email')) {
                    return 'This is not a valid Email';
                }

                break;

            case 'phone':
                if (this.form.get('phone').hasError('required')) {
                    return this.translateService.instant('Phone required');
                }
                else if (this.form.get('phone').hasError('minlength')) {
                    return this.translateService.instant('Phone Must be at least 4 Numbers');
                }
                break;

            default:
                return '';
        }
    }

}

interface Point {
    x: number, y: number,
    test(): void
}

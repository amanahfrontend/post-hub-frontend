import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '@app/services/facade.service';
import { WeightUOM } from '@app/shared/models/admin/weight-uom';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs-compat';
import { Option } from './../../../shared/models/admin/option';
import { Address } from '../../../shared/models/components';

@Component({
  selector: 'manage-item',
  templateUrl: './manage-item.component.html',
  styleUrls: ['./manage-item.component.scss']
})
export class ManageItemComponent implements OnInit {
  @Input() col: string = 'col-md-4';
  areas: Option[] = [];
  subscriptions = new Subscription();
  form: FormGroup;
  operation: string = '';
  address: Address;
  isValidAddress: boolean = false;
  submitted: boolean = false;
  weightUOMs: WeightUOM[] = [];
  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<ManageItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private facadeService: FacadeService,
  ) {
    this.form = this.fb.group({
      filterAreaCtrl:[''],
      price:['', [Validators.required]],      
  fromArea:['', [Validators.required]],
  toArea:['', [Validators.required]],
  fromWeight:['', [Validators.required]],
  toWeight:['', [Validators.required]],
  serviceTimeUnit:['', [Validators.required]],
  serviceTime:['', [Validators.required]]
      // mailItemBarCode: ['', [Validators.required]],
      // consigneeInformation: [''],
      // weightUOMId: [null],
      // consigneeMobile: [''],
      // weight: [''],
    });

    this.operation = this.translateService.instant('Add');

    if (this.data.isEdit) {
      this.form.patchValue(this.data.row);
      this.isValidAddress = true;
      this.operation = this.translateService.instant('Edit');
    }
  }

  ngOnInit(): void {
   
    this.listAreas();
    this.listWieghtUOM();
  }

   /**
   * areas
   * 
   * 
   */
    listAreas() {     
      this.subscriptions.add(this.facadeService.addressService.areas.subscribe((areas: Option[]) => {
        this.areas = areas;
      }));
    }

  listWieghtUOM() {
    this.facadeService.adminFacadeService.weightUOMService.listActive().subscribe(
      res => {
        this.weightUOMs = res
      })
  }

  submit() {
   
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }
    if(this.data.oldDataSource){
      let error=0;
      this.data.oldDataSource.forEach(element => {
          if(element.fromArea===this.form.value.fromArea &&element.toArea===this.form.value.toArea
            &&element.fromWeight===this.form.value.fromWeight && element.toWeight===this.form.value.toWeight )
            error= error +1;
          });  
          if(error>0)
          return this.toastrService.error(this.translateService.instant('Please donot enter dublicated data'));
   }
    this.submitted = true;
    const body = Object.assign(this.form.value);    
    this.dialogRef.close({ operation: this.operation, item: body });
  }
}

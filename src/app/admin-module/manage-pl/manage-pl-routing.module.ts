import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagePLFormComponent } from './manage-pl-form/manage-pl-form.component';
import { ManagePLComponent } from './manage-pl.component';


const routes: Routes = [
  {
    path: '',
    component: ManagePLComponent
  }
  ,
    {
      path: 'add',
      component: ManagePLFormComponent
    },
    {
      path: 'edit',
      component: ManagePLFormComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePLRoutingModule { }

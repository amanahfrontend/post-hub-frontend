import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FacadeService } from '@app/services/facade.service';
import { PL } from '@app/shared/models/admin/PriceList/PL';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs-compat';
import { ManageItemComponent } from '../manage-item/manage-item.component';
import { Option } from './../../../shared/models/admin/option';
@Component({
  selector: 'manage-pl-form.component',
  templateUrl: './manage-pl-form.component.html',
  styleUrls: ['./manage-pl-form.component.scss']
})
export class ManagePLFormComponent implements OnInit {
  minDate: Date;
  maxDate: Date;
  form: FormGroup;
  Pl: PL;
  subscriptions = new Subscription();
  statuses: Option[] = [];
  sectors: Option[] = [];
  @Output() onForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() itemList: EventEmitter<any> = new EventEmitter<any>();
  index: number;
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = ['select',
    'fromArea', 'toArea',
    'fromWeight', 'toWeight',
    'price', 'serviceTimeUnit','serviceTime','actions'];
  row: PL;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  mode: any;
  PriceListId: any;
  constructor(private fb: FormBuilder, private facadeService: FacadeService,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private dialog: MatDialog,
    private router: Router,
  ) {
    this.form = this.fb.group({
      sectorTypeId: [],
      pqDate: new FormControl(new Date()),
      validToDate: new FormControl(new Date()),
      validFromDate:new FormControl(new Date()),
      statusId: [],
      code: [''],     
      plItems: this.fb.array([])
     
    });
    
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .subscribe(
        params => {
          this.PriceListId = params.id;
          this.mode = params.mode;
          if (this.PriceListId) {
            this.priceListDetails(this.PriceListId);
          }
        });
    this.getAllPLStatuses();
    this.getAllServiceSectors();
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
   
  }
  priceListDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.managePriceListService.getPLDetails(id).subscribe(res => {
      this.Pl = res;
      const dataSource = this.dataSource.data;
      const items = this.Pl.plItems;
      const arrayToPush = [...dataSource, ...items];

      this.dataSource.data = arrayToPush;
      this.form.patchValue(this.Pl);
      // this.form.get('rfqItems').setValue(this.pQ.rfqItems);
    }));
  }
  getAllPLStatuses() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllPLStatuesAsync().subscribe((res: Option[]) => {
      this.statuses = res;
    });
  }
  getAllServiceSectors() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllServiceSectorsAsync().subscribe((res: Option[]) => {
      this.sectors = res;
    });
  }
  /**
   * 
   * @param opertaion 
   * @param isEdit 
   * @param element 
   */
  manageItem(opertaion: string, isEdit: boolean, element?: any): void {
    
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(ManageItemComponent, {
      width: '75%',
      panelClass: 'custom-dialog-container',
      height: 'calc(80vh - 50px)',
      data: { type, row: element, isEdit,oldDataSource:this.dataSource.data },
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {      
      if (result && element) {
        
        const dataSource = this.dataSource.data;
        dataSource[this.index] = result.item;
        this.dataSource.data = dataSource;

        this.itemList.emit(this.dataSource.data);        
       // localStorage.setItem('StoredPLItems', JSON.stringify(this.dataSource.data));
        this.form.get('filledItemNo').setValue(this.dataSource.data.length);

        let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);

        this.form.get('remainingItemNo').setValue(remaining);

      } else if (result) {
        const item = result.item;
        const dataSource = this.dataSource.data;
        dataSource.push(item);

        this.dataSource.data = dataSource;
       
        this.itemList.emit(this.dataSource.data);
       
        this.form.get('filledItemNo').setValue(this.dataSource.data.length);

        let remaining = Number(this.dataSource.data[0].totalItemCount) - Number(this.dataSource.data.length);

        this.form.get('remainingItemNo').setValue(remaining);

      }
    });


  }
  setIndex(index: number) {
    this.index = index;
  }

  /**
   * 
   * @param event 
   * @param index 
   */
  onDelete(event: boolean, index: number) {
    if (event) {
      const dataSource = this.dataSource.data;
      dataSource.splice(index, 1);
      this.dataSource.data = dataSource;
    }
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);

  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  submit(val: any) {
    
    this.form.value.plItems = this.dataSource.data;
    let body = { ... this.row, ... this.form.value };

    let objectToPost = body;


    if (this.mode === 'edit') {
      objectToPost.id = this.PriceListId;
      this.facadeService.adminFacadeService.managePriceListService.updatePL(objectToPost).subscribe(res => {
        //this.newPqId=res;
        this.toastrService.success(this.translateService.instant('PL Updated Successfully'));
        // if(val!='print')
        this.router.navigate(['manage-pl']);
      });
    } else {
      this.facadeService.adminFacadeService.managePriceListService.createPL(objectToPost).subscribe(res => {
        //this.newPqId=res;
        this.toastrService.success(this.translateService.instant('New PL Added Successfully'));
        this.router.navigate(['manage-pl']);

      });
    }
  }
  reset() {  
    this.form.reset();
    this.router.navigate(['manage-pl/add']);
  }
  close(){
    this.router.navigate(['manage-pl']);
  }
}

import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { MouseEvent } from '@agm/core';
import { OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '@app/services/facade.service';
import { PL } from '@app/shared/models/admin/PriceList/PL';
import { Router } from '@angular/router';
import { saveFile } from '../../shared/helpers/download-link';
import { Option } from './../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'manage-pl.component',
  templateUrl: './manage-pl.component.html',
  styleUrls: ['./manage-pl.component.scss']
})
export class ManagePLComponent implements OnInit, AfterViewInit, OnDestroy {
  form: FormGroup;
  minDate: Date;
  maxDate: Date;
  displayedColumns: string[] = [
    'code',
    'validToDate',
    'priceListStatus',
    'fromArea',
    'toArea',
    'fromWeight',    
    'toWeight',
    'serviceTime',  
    'price',
    'actions'
  ];
  subscriptions = new Subscription();
  statuses: Option[] = [];
  sectors: Option[] = [];
  areas: Option[] = [];
  searchBy: string = '';
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
   pls: PL[] = [];
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  dtTrigger: Subject<any> = new Subject();
  checkAll = false
  locationDetails = []
  locationForm;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  companyForm;
  masterCheck = false
  countryList;
 
 
  constructor(private translateService: TranslateService , 
    private router: Router,  
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    ) {
      this.form = this.fb.group({
        area:[],
        filterAreaCtrl:[''],
        filterSectorCtrl:[''] ,
        filterStatusCtrl:[''],
        sectorTypeId:[''],
        statusId:[''],
        fromDate:new FormControl(''), //new FormControl(''),
        toDate: new FormControl(''),
        businessCustomerId: [0],
        businessCustomerName:[''],
        code:['']
      });
  }

  ngOnInit(): void {
   
    this.list(this.page);
    this.getAllServiceSectors();
    this.getAllPLStatuses();   
    this.listAreas(); 
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
  }
   /**
   * areas
   * 
   * 
   */
    listAreas() {     
      this.subscriptions.add(this.facadeService.addressService.areas.subscribe((areas: Option[]) => {
        this.areas = areas;
      }));
    }
  getAllPLStatuses() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllPLStatuesAsync().subscribe((res: Option[]) => {
      this.statuses = res;
    });
  }
  getAllServiceSectors() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllServiceSectorsAsync().subscribe((res: Option[]) => {
      this.sectors = res;
    });
  }

  /**
   * list pls
   * 
   * 
   * @param page 
   */
   getFilteredData(){
      this.list(this.page);
   }
   list(page?: number): void {
        
     let body = { ... this.form.value };

    let objectToPost = body;
    // const body: {

    //   pageNumber: number,
    //   pageSize: number, businessCustomerId: number, fromDate: Date, toDate: Date ,code:string
    // } = {
    //   pageNumber: this.page,
    //   pageSize: this.pageSize,
    //   businessCustomerId: this.form.value.businessCustomerId ? +this.form.value.businessCustomerId : null,
    //   fromDate: this.form.value.fromDate ? new Date(this.form.value.fromDate) : null,
    //   toDate: this.form.value.toDate ? new Date(this.form.value.toDate) : null,
    //   code:this.form.value.code ? this.form.value.code : ''
    // };
    

    this.facadeService.adminFacadeService.managePriceListService.listPLByPagination(objectToPost).subscribe((res: any) => {
      this.pls = res.result;
      this.total = res.totalCount;
    });
  }
   /**
   * add / edit view row
   * 
   *   
   */
    addNewPL(): void {
      this.router.navigate(['manage-pl/add']);
    }
    editPl(PlId: number) {
      this.router.navigate(['/manage-pl/edit'], {queryParams: {id: PlId,mode:'edit'}});      
    }
    viewPQ(pQId: number){    
      this.router.navigate(['/manage-pl/edit'], {queryParams: {id: pQId,mode:'view'}});     
    
    }  
    export() {
      const body = {
        pageNumber: this.page,
        pageSize: this.pageSize,
        searchBy: this.searchBy,
      }
  
      this.facadeService.adminFacadeService.managePriceListService.exportToExcel(body).subscribe(data => {
        saveFile(`PriceList.xlsx ${new Date().toLocaleDateString()}`, "data:attachment/text", data);
      });
    }
    deletePriceList(pl: any) {
      this.facadeService.adminFacadeService.managePriceListService.deletePL(pl.id).subscribe((res: any) => {
  
        const index = this.pls.indexOf(pl);
        if (index >= 0) {
          this.pls.splice(index, 1);
          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`Price List has been Deleted Successfully`));
        }
  
      });
    }
    private replaceRoutePage(page: number) {
      this.router.navigate(['manage-pl'], { queryParams: { page: page } });
    }
    search(): void {
      
      const body: { pageNumber: number, pageSize: number, searchBy: string } = {
        pageNumber: 1,
        pageSize: 10,
        searchBy: this.searchBy,
      };
  
      this.facadeService.adminFacadeService.managePriceListService.GetFilteredPLPagginated(body).subscribe((res: any) => {
        this.pls = res.result;
        this.total = res.totalCount;
      });
    }
  
  getLocation() {
    // this.rerender()

    this.locationDetails = []


    this.locationDetails = [
      { serviceType: 'IE', zones: 'A', weight: 1, id: 1, price: 5000.12 },
      { serviceType: 'IE', zones: 'A', id: 2, weight: 1.3, price: 2.1200 },
      { serviceType: 'IE', zones: 'A', id: 3, weight: 1.5, price: 3.2200 },
      { serviceType: 'IE', zones: 'A', id: 4, weight: .5, price: 5.2220 },
      { serviceType: 'IE', zones: 'A', id: 5, weight: 2, price: 1.220 },
    ]
    //  this.rerender()



  }
  addNewLocation() {
    this.areaList
    this.addOrEdit = this.translateService.instant('Add');
    this.locationForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
 
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );
  }
 

  /**
 * on clicked
 * 
 * 
 * @param event 
 */
  onBtnClicked(event) {
    if (event) {
      this.addNewLocation();
      document.getElementById("open-add-modal").click();
    }
  }
}
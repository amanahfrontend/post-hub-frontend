import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ManagePLRoutingModule } from './manage-pl-routing.module';
import { ManagePLComponent } from './manage-pl.component';
import { ManagePLFormComponent } from './manage-pl-form/manage-pl-form.component';
import { ManageItemComponent } from './manage-item/manage-item.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FilterModule } from '@app/shared/pipes/filter/filter.module';
import { AddressModule } from '@app/shared/components/address/address.module';
import { TelInputModule } from '@app/shared/components/tel-input/tel-input.module';
import { SetDirModule } from '@app/shared/directives/set-dir/set-dir.module';
import { PermissionsModule } from '@app/shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    ManagePLComponent,
    ManagePLFormComponent,
    ManageItemComponent 
  ],
  imports: [
    CommonModule,  
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,    
    // shared  
    NgxMatSelectSearchModule,
 FilterModule,
    AddressModule,
    HeaderAsCardModule,
    TelInputModule,
    ConfirmDeletionModule,
    SetDirModule,
    PermissionsModule,   
    ManagePLRoutingModule,    
    HttpClientModule,
    MaterialModule,
    MatDialogModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDeletionModule,
    HeaderAsCardModule,
    DataTablesModule
  ],
  entryComponents: [   
    ManageItemComponent
  ]
})
export class ManagePLModule { }

import { Component, Inject, OnInit } from '@angular/core';
//import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '../../services/facade.service';
import { Manager } from '../../shared/models/admin/manager';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from '../../shared/helpers/download-link';
import { RFQ } from 'app/shared/models/admin/PriceList/RFQ';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { DOCUMENT } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ManagePrintPqFormComponent } from './manage-printpq-form/manage-printpq-form.component';
import { ManagePrintInternationalPqFormComponent } from './manage-printinternationalpq-form/manage-printinternationalpq-form.component';

@Component({
  selector: 'manage-priceList',
  templateUrl: './manage-priceList.component.html',
  styleUrls: ['./manage-priceList.component.scss']
})
export class ManagePriceListComponent implements OnInit {
  
  displayedColumns: string[] = [
    'customerName',
    'code',
    'sectorTypeName',
    'pqDate',    
    'pqStatusName',
    'pqValidToDate',  
    
    'actions'
  ];
  pQ: RFQ; 
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  rfqs: RFQ[] = [];
  subscriptions = new Subscription();
  searchBy: string = '';
  todayDate: any;
  loading: boolean = false;

  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private dialog: MatDialog,
    private translateService: TranslateService,
    //private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));
  }

  ngOnInit(): void {
    this.list(this.page);
    this.todayDate=new Date();
  }

  /**
   * list rfqs
   * 
   * 
   * @param page 
   */
  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.managePriceListService.listByPagination(body).subscribe((res: any) => {
      this.rfqs = res.result;
      this.total = res.totalCount;
    });
  }

 
  /**
   * add / edit row
   * 
   * 
   * @param opertaion 
   * @param editMode 
   * @param manager 
   */
   addNewPQ(): void {
    this.router.navigate(['manage-pq/add']);
  }
  editPQ(pQId: number) {
    this.router.navigate(['/manage-pq/edit'], {queryParams: {id: pQId,mode:'edit'}});      
  }
  viewPQ(pQId: number){    
    this.router.navigate(['/manage-pq/edit'], {queryParams: {id: pQId,mode:'view'}});     
  
  }  
  printPQ(element: any): void {  
     
    if(element.sectorTypeId==26)
    this.openLocalPostPrintDialog(element.id);
    else
    this.openIntrPostPrintDialog(element.id);
  }
  openLocalPostPrintDialog(pQId:any){
    const dialog = this.dialog.open(ManagePrintPqFormComponent, {
      width: '60%',
      data: {
      id:pQId
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {    
      this.list(this.page);
    });
  }
  openIntrPostPrintDialog(pQId:any){
    const dialog = this.dialog.open(ManagePrintInternationalPqFormComponent, {
      width: '60%',
      data: {
      id:pQId
      },
      panelClass: 'custom-dialog-container',
      height: '100%'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {    
      this.list(this.page);
    });
  }
  
  /**
   * Delete rfq
   * 
   * 
   * @param event 
   * @param rfq 
   */
   onDelete( rfq: RFQ): void {
  
    this.facadeService.adminFacadeService.managePriceListService.delete(rfq.id).subscribe(res => {
      const index = this.rfqs.indexOf(rfq);
      if (index >= 0) {
        this.rfqs.splice(index, 1);

        this.replaceRoutePage(this.page);
        this.list(this.page);
        this.toastrService.success(this.translateService.instant(`Price Offer has been deleted successfully`));
      }
    });
  
}

priceQuotationDetails(id: number): void {
  this.subscriptions.add(this.facadeService.adminFacadeService.managePriceListService.get(id).subscribe(res => {
      this.pQ = res;     
}));
}
 
  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.list(this.page);
    this.replaceRoutePage(this.page);
  }

  /**
   * 
   * @param page 
   */
  private replaceRoutePage(page: number) {
    this.router.navigate(['manage-pq'], { queryParams: { page: page } });
  }

  

  export() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageStaffService.exportToExcel(body).subscribe(data => {
      saveFile(`Staff.csv ${new Date().toLocaleDateString()}`, "data:attachment/text", data);
    });
  }

  search(): void {
    const body: { pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.managePriceListService.listByPagination(body).subscribe((res: any) => {
      this.rfqs = res.result;
      this.total = res.totalCount;
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageStaffFormComponent } from './manage-staff-form.component';

describe('ManageStaffFormComponent', () => {
  let component: ManageStaffFormComponent;
  let fixture: ComponentFixture<ManageStaffFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageStaffFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageStaffFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

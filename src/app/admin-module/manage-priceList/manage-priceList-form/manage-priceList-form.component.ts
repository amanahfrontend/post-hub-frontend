import { Component, OnInit, Inject, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
//import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Country } from '../../../shared/models/components';
import { FacadeService } from '../../../services/facade.service';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from './../../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../services/shared/language.service';
import { TranslateService } from '@ngx-translate/core';
import { Manager } from '../../../shared/models/admin/manager';
import { App } from '../../../core/app';
import { MatRadioChange } from '@angular/material/radio';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Editor } from 'ngx-editor';
import { ActivatedRoute, Router } from '@angular/router';
import { RFQ } from 'app/shared/models/admin/PriceList/RFQ';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { DOCUMENT } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ManagePrintPqFormComponent } from '../manage-printpq-form/manage-printpq-form.component';
import { ManagePrintInternationalPqFormComponent } from '../manage-printinternationalpq-form/manage-printinternationalpq-form.component';


const COUNTRIES = 'COUNTRIES';

@Component({
  selector: 'manage-priceList-form',
  templateUrl: './manage-priceList-form.component.html',
  styleUrls: ['./manage-priceList-form.component.scss']
})
export class ManagePriceListFormComponent implements OnInit, OnChanges {
  pQ: RFQ;
  letterSubjectEditor: Editor;
  letterTexeditor: Editor;
  pqSubHtmleditor: Editor;
  noteseditor: Editor;
  serviceseditor: Editor;
  html: '';
  // letterTexHtml:'';
  // letterSubHtml:'';
  country: any;
  customer: any;
  form: FormGroup;
  postType: string;
  IsLocalPost: boolean;
  IsHasAmount: boolean;
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";
  operation: string;
  selectedMobileNumber: number;
  selectedCountry: Country;
  selectedCountryCode: Country;
  subscriptions = new Subscription();
  filteredCountries: Observable<Country[]>;
  currencies: Option[] = [];
  statuses: Option[] = [];
  roles: any[];
  //pqItems: any[] = [];
  departments: Option[] = [];
  customers: Option[] = [];
  countries: Option[] = [];
  sectors: Option[] = [];
  status: Option[] = [];
  genders: Option[] = [];
  teams: Option[] = [];
  branchs: Option[] = [];
  managers: Option[] = [];

  minDate: Date;
  maxDate: Date;

  locale: string = 'en';
  type: string = 'password';
  isEditMode: boolean = false;
  row: Manager;
  filterCustomerCtrl: string;
  IsMultiCountry: boolean;
  customerId: any;
  postTypeId: any;
  PqId: any;
  mode: any;
  loading: boolean = false;
  newPqId: any;
  constructor(
    private dialog: MatDialog,
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.form = this.fb.group({      
      sectorTypeId:['', [Validators.nullValidator]],
      filterCustomerCtrl: [''],
      IntFilterCustomerCtrl: [''],
      customerId:['', [Validators.required]],
      currencyId: ['', [Validators.required]],
      // countryCtrl: ['', [Validators.required]],
      pqDate: new FormControl(new Date()), // Current Date
      // pqDate:[''],
      validToDate: new FormControl(new Date()),
      letterTexHtml: [''],
      letterSubHtml: [''],
      pqSubHtml: [''],
      services: [''],
      pqCode:['', [Validators.required]],
      totalRounds: [0],
      rfqItems: this.fb.array([]),

      pqItems: this.fb.group({
        itemDescription: [''],
        itemPrice: [null, [Validators.nullValidator]],       
        notes: [''],
        amount: [0],
        hasAmount: []
      })

    });

    // if (this.pQ.rfqItems && this.pQ.rfqItems.length == 0) {
    //   this.addRFQ();
    // }  
    this.countries = JSON.parse(localStorage.getItem(COUNTRIES));

  }
  ngOnChanges() {

    this.form.valueChanges.subscribe(() => {

      if (this.form.controls['letterTexHtml'].value) {
        // 
      }
    });
  }
  ngOnInit(): void {


    this.activatedRoute.queryParams
      .subscribe(
        params => {
          this.PqId = params.id;
          this.mode = params.mode;
          if (this.PqId) {
            this.priceQuotationDetails(this.PqId);
          }
        });
    this.letterSubjectEditor = new Editor();
    this.noteseditor = new Editor();
    this.serviceseditor = new Editor();
    this.pqSubHtmleditor = new Editor();
    this.letterTexeditor = new Editor();

    this.locale = this.languageService.currentLanguage;

    if (this.mode == 'view') {
      //this.form.get('x').disable();
      this.form.disable()
    }
    this.getBusinessCustomers();
    this.getAllCurrencies();
    this.getAllPQStatuses();
    this.getAllContries();
    // this.pqItems.push({ countryId: 0, weight: 0, price: 0 });

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 59, 11, 31);
    this.maxDate = new Date(currentYear + 17, 11, 31);
  }
  // getAllCustomers() {
  //   this.facadeService.adminFacadeService.managePriceListService.GetAllCustomerNamesAsync().subscribe((res: Option[]) => {
  //     this.customers = res;
  //   });
  // }
  getBusinessCustomers() {
    this.facadeService.adminFacadeService.businessCustomerService.listNew().subscribe((res: Option[]) => {
      this.customers = res;
    });
  }
  getAllContries() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllCountiesAsync().subscribe((res: Option[]) => {
      this.countries = res;
    });
  }
  getAllCurrencies() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllCurrenciesAsync().subscribe((res: Option[]) => {
      this.currencies = res;
      console.log(this.customers)
    });
  }
  getAllPQStatuses() {
    this.facadeService.adminFacadeService.managePriceListService.GetAllPQStatuesAsync().subscribe((res: Option[]) => {
      this.statuses = res;
      console.log(this.customers)
    });
  }
  rfqItems(): FormArray {
    return this.form.get('rfqItems') as FormArray;
  }

  newRFQ(): FormGroup {
    return this.fb.group({      
      weight:[''],
      toGoShippingPrice: [''],
      commingShippingPrice: [''],
      roundTripShippingPrice: 0,
      countryId:[''],
      filterCountryCtrl: ['']
    });
  }

  addRFQ(): void {
    this.rfqItems().push(this.newRFQ());
  }

  editRFQ(index: number, term: { name: string }): void {
    this.rfqItems().at(index).patchValue(term);
  }

  removeRFQ(termIndex: number): void {
    this.rfqItems().removeAt(termIndex);
  }
  calcRounTrip(e: any, item: any, index) {
    console.log("e", e)
    item.value.roundTripShippingPrice = item.value.commingShippingPrice + item.value.toGoShippingPrice;
    //let term=item.value
    this.rfqItems().at(index).patchValue(item.value);
    // this.calcTotalRounTrip(item);
  }
  calcTotalRounTrip(item: any) {

    var tot = 0;
    this.form.value.rfqItems;
    this.form.value.rfqItems.forEach(pq => {
      tot = tot + pq.roundTripShippingPrice;
      this.form.get('totalRounds').setValue(tot);
    });
    // item.value.roundTripShippingPrice=item.value.commingShippingPrice + item.value.toGoShippingPrice;
    // //let term=item.value
    // this.rfqItems().at(index).patchValue(item.value);
  }

  submit(val: any) {
   
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    let body = { ... this.row, ... this.form.value };

    let objectToPost = body;
    if (objectToPost.sectorTypeId == 27)//international
      objectToPost.pqItems = null;
    if (objectToPost.sectorTypeId == 26) //local
      objectToPost.rfqItems = [];
    // if(objectToPost.pqItems.hasAmount==false)
    // objectToPost.pqItems.amount=0;

    if (this.mode === 'edit') {
      objectToPost.id = this.PqId;
      this.facadeService.adminFacadeService.managePriceListService.update(objectToPost).subscribe(res => {
        this.newPqId = res;
        this.toastrService.success(this.translateService.instant('PQ Updated Successfully'));
        if (val != 'print')
          this.router.navigate(['manage-pq']);
        if (val === 'print')
          this.printPQ(this.newPqId, objectToPost.sectorTypeId);
      });
    } else {
      this.facadeService.adminFacadeService.managePriceListService.create(objectToPost).subscribe(res => {
        this.newPqId = res;
        this.toastrService.success(this.translateService.instant('New PQ Added Successfully'));
        if (val != 'print')
          this.router.navigate(['manage-pq']);
        if (val === 'print')
          this.printPQ(this.newPqId, objectToPost.sectorTypeId);
      });
    }
  }
  printPQ(id, sectorTypeId): void {
    if (sectorTypeId == 26)
      this.openLocalPostPrintDialog(id);
    else
      this.openIntrPostPrintDialog(id);
  }
  openLocalPostPrintDialog(pQId: any) {
    const dialog = this.dialog.open(ManagePrintPqFormComponent, {
      width: '60%',
      data: {
        id: pQId
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      //this.list(this.page);
    });
  }
  openIntrPostPrintDialog(pQId: any) {
    const dialog = this.dialog.open(ManagePrintInternationalPqFormComponent, {
      width: '60%',
      data: {
        id: pQId
      },
      panelClass: 'custom-dialog-container',
      height: '100%'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      //this.list(this.page);
    });
  }
  close() {
    this.router.navigate(['manage-pq']);
  }
  reset() {
    this.form.reset();
    this.router.navigate(['manage-pq/add']);
  }

  CheckIfHasAmount(event: any) {
    if (event.checked)
      this.IsHasAmount = true;
    else
      this.IsHasAmount = false;
  }



  selectPostType(event: any) {
    //this.postTypeId = event.value;
    if (event.value == "27")
      this.IsLocalPost = false;
    else
      this.IsLocalPost = true;
  }
  priceQuotationDetails(id: number): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.managePriceListService.get(id).subscribe(res => {
      this.pQ = res;
      this.pQ.rfqItems.forEach(pq => {
        this.addRFQ();
      });
      this.form.patchValue(this.pQ);
      // this.form.get('rfqItems').setValue(this.pQ.rfqItems);
    }));
  }

  // make sure to destory the editor
  ngOnDestroy(): void {
    this.letterSubjectEditor.destroy();
    this.noteseditor.destroy();
    this.serviceseditor.destroy();
    this.pqSubHtmleditor.destroy();
    this.letterTexeditor.destroy();
  }



}

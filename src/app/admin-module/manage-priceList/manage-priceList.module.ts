import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { ManageStaffRoutingModule } from './manage-staff-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


// shared
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
 
import { AddressModule } from '../../shared/components/address/address.module';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';
import { ManagePriceListComponent } from './manage-priceList.component';
import { ManagePriceListFormComponent } from './manage-priceList-form/manage-priceList-form.component';
import { ManagePriceListRoutingModule } from './manage-priceList-routing.module';

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FilterModule } from 'app/shared/pipes/filter/filter.module';
import { NgxEditorModule } from 'ngx-editor';
import { ManagePrintPqFormComponent } from './manage-printpq-form/manage-printpq-form.component';
import { ManagePrintInternationalPqFormComponent } from './manage-printinternationalpq-form/manage-printinternationalpq-form.component';
@NgModule({
  declarations: [
    ManagePriceListComponent,
    ManagePriceListFormComponent,
    ManagePrintPqFormComponent ,
    ManagePrintInternationalPqFormComponent
  ],
  imports: [
    CommonModule,
    ManagePriceListRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    NgxEditorModule,

    // shared
    MaterialModule,
    NgxMatSelectSearchModule,
 FilterModule,
    AddressModule,
    HeaderAsCardModule,
    TelInputModule,
   
    SetDirModule,
    PermissionsModule
  ],
  entryComponents: [
    ManagePrintPqFormComponent ,
    ManagePrintInternationalPqFormComponent
  ],
})
export class ManagePriceListModule { }

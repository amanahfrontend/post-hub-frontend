import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ManagePriceListFormComponent } from './manage-priceList-form/manage-priceList-form.component';
import { ManagePriceListComponent } from './manage-priceList.component';
import { ManagePrintInternationalPqFormComponent } from './manage-printinternationalpq-form/manage-printinternationalpq-form.component';
import { ManagePrintPqFormComponent } from './manage-printpq-form/manage-printpq-form.component';

const routes: Routes = [
  
    {
      path: '',
      component: ManagePriceListComponent
    },
    {
      path: 'manage-pq',
      component: ManagePriceListComponent
    },
  
    {
      path: 'add',
      component: ManagePriceListFormComponent
    },
    {
      path: 'edit',
      component: ManagePriceListFormComponent
    }
    ,
    {
      path: 'print',
      component: ManagePrintPqFormComponent
    }
    ,
    {
      path: 'printinternational',
      component: ManagePrintInternationalPqFormComponent
    }
    
    
   
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePriceListRoutingModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageWorkorderFormComponent } from './manage-workorder-form.component';

describe('ManageWorkorderFormComponent', () => {
  let component: ManageWorkorderFormComponent;
  let fixture: ComponentFixture<ManageWorkorderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageWorkorderFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageWorkorderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

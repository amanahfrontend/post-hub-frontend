import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FacadeService } from 'app/services/facade.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { workOrder, Item } from 'app/shared/models/admin/workOrder';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

interface Status {
    id: number | string;
    name: string;
}

const WORKORDER_DETAILS = 'manage-workorders/details';

@Component({
    selector: 'manage-workorder-form',
    templateUrl: './manage-workorder-form.component.html',
    styleUrls: ['./manage-workorder-form.component.scss']
})
export class ManageWorkorderFormComponent implements OnInit, AfterViewInit {

    statusList: Status[] = [];
    priorityList: Status[] = [];
    form: FormGroup;
    itemsColumnsToDisplay = ['select', 'itemInfo', 'itemStatus', 'deliveryBefore', 'orderInfo', 'sender', 'receiver', 'customerInfo', 'actions'];
    itemsDataSource = new MatTableDataSource<any>();
    itemsSelection = new SelectionModel<any>(true, []);
    @ViewChild(MatPaginator) paginator: MatPaginator;

    isEditMode: boolean = false;
    workOrders: workOrder[] = [];
    workOrder: workOrder;
    workOrderId: number;
    subscriptions = new Subscription();
    workOrderStatuss: [] = [];
    fromDispatchOrdersPage: boolean = false;
    ordersFormDispatchingPage: Item[] = [];
    fromDetailsPage: boolean = false;

    constructor(
        private translateService: TranslateService,
        private fb: FormBuilder,
        private router: Router,
        private facadeService: FacadeService,
        private activatedRoute: ActivatedRoute,
        private toastrService: ToastrService,
        private location: Location,
        private dialog: MatDialog
    ) {
        this.form = this.fb.group({
            code: ['', Validators.required],
            workOrderStatusId: [''],
            priorty: [''],
            plannedStart: [new Date()],
            deliveryBefore: [new Date()],
            issueAt: [new Date()],
        });

        if (this.router.url.includes(WORKORDER_DETAILS)) {
            this.form.disable();
            this.fromDetailsPage = true;
            this.itemsColumnsToDisplay = ['itemInfo', 'itemStatus', 'deliveryBefore', 'orderInfo', 'sender', 'receiver', 'customerInfo'];
        }

        this.subscriptions.add(this.activatedRoute.params.subscribe(params => {
            if (params.id) {
                this.isEditMode = true;
                this.workOrderId = params.id;
                this.workOrderDetails(params.id);

            } else {
                this.isEditMode = false;
            }
        }));

        const ordersFormDispatchingPage: any = this.location.getState();
        if (ordersFormDispatchingPage.rows) {
            if (this.isEditMode) {
                this.itemsDataSource.data.push(...ordersFormDispatchingPage.rows);
            } else {
                this.itemsDataSource.data = ordersFormDispatchingPage.rows;
            }

            this.fromDispatchOrdersPage = true;
            this.ordersFormDispatchingPage = ordersFormDispatchingPage.rows;
        }
    }

    ngOnInit(): void {
        this.GetAllWorkOrderStatuss();
        this.priorityList = [
            {
                id: 1,
                name: this.translateService.instant('Later')
            },
            {
                id: 2,
                name: this.translateService.instant('Normal')
            },
            {
                id: 3,
                name: this.translateService.instant('Hight')
            },
            {
                id: 4,
                name: this.translateService.instant('Critical')
            }
        ];

        this.workOrderId = history.state;

    }

    ngAfterViewInit() {
        this.itemsDataSource.paginator = this.paginator;
    }

    GetAllWorkOrderStatuss() {
        this.facadeService.adminFacadeService.manageWorkOrderService.GetAllWorkOrderStatuss().subscribe((res: any) => {
            this.workOrderStatuss = res
        });
    }

    submit() {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
        }

        let body = {
            ...this.form.value, priorty: +this.form.value.priorty,
            workOrderStatusId: +this.form.value.workOrderStatusId
        }

        if (this.isEditMode) {
            body.id = this.workOrder.id;
            body.MailItemIds = this.itemsDataSource.data.map(x => x.id);

            this.facadeService.adminFacadeService.manageWorkOrderService.update(body).subscribe(res => {
                this.toastrService.success(this.translateService.instant('WorkOrder Updated Successfully'));
                this.router.navigate(['manage-workorders']);
            });
        } else {
            body.MailItemIds = this.itemsDataSource.data.map(x => x.id);
            this.facadeService.adminFacadeService.manageWorkOrderService.create(body).subscribe(res => {
                this.toastrService.success(this.translateService.instant('WorkOrder  Added Successfully'));
                this.router.navigate(['manage-workorders']);
            });
        }
    }

    isAllSelectedItems() {
        const numSelected = this.itemsSelection.selected.length;
        const numRows = this.workOrders.length;
        return numSelected === numRows;
    }

    masterToggleItems() {
        if (this.isAllSelectedItems()) {
            this.itemsSelection.clear();
            return;
        }

        //   this.itemsSelection.select(...this.itemsDataSource.data);
    }

    checkboxLabelItems(row?: any): string {
        if (!row) {
            return `${this.isAllSelectedItems() ? 'deselect' : 'select'} all`;
        }
        return `${this.itemsSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    }

    workOrderDetails(id: number): void {
        this.subscriptions.add(this.facadeService.adminFacadeService.manageWorkOrderService.get(id).subscribe(res => {
            this.workOrder = res;
            this.form.patchValue(this.workOrder);
           
            console.log(res.mailItems);
            
            this.itemsDataSource.data.push(...res.mailItems);
            this.itemsDataSource._updateChangeSubscription();
            //this.form.get('dateOfBirth').setValue(this.row.dateOfBirth);
            //this.form.get('countryCtrl').setValue(this.row.nationality);
            //this.form.get('username').clearValidators();
            //this.form.removeControl('password');

            //this.selectedTransportionType = this.row.transportTypeName.toLocaleLowerCase();
            //this.path = `${App.backEndUrl}/${this.row.imageUrl}`;

            //if (this.row.allDeliveryGeoFences) {
            //    this.form.patchValue({ driverPickUpGeoFences: ['All'] });
            //}

            //if (this.row.allDeliveryGeoFences) {
            //    this.form.patchValue({ driverDeliveryGeoFences: ['All'] });
            //}
        }));
    }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageWorkorderFormComponent } from './manage-workorder-form/manage-workorder-form.component';
import { ManageWorkordersComponent } from './manage-workorders.component';

const routes: Routes = [
  {
    path: '',
    component: ManageWorkordersComponent
  },
  {
    path: 'add',
    component: ManageWorkorderFormComponent
  },
  {
    path: 'edit/:id',
    component: ManageWorkorderFormComponent
  },
  {
    path: 'details/:id',
    component: ManageWorkorderFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageWorkordersRoutingModule { }

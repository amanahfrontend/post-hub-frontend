import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Driver } from '../../../shared/models/admin/driver';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '@app/services/facade.service';

@Component({
  selector: 'assign-or-dispatch-driver',
  templateUrl: './assign-or-dispatch-driver.component.html',
  styleUrls: ['./assign-or-dispatch-driver.component.scss']
})
export class AssignOrDispatchDriverComponent implements OnInit {

  form: FormGroup;
  drivers: Driver[] = [];
  type: string;

  /**
   * 
   * @param fb 
   * @param facadeService 
   * @param data 
   * @param toastrService 
   * @param dialogRef 
   */
  constructor(
    private fb: FormBuilder,
    private facadeService: FacadeService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private toastrService: ToastrService,
    private dialogRef: MatDialogRef<AssignOrDispatchDriverComponent>) {
    this.form = this.fb.group({
      driverId: ['', [Validators.required]],
    });

    this.type = this.data.type;
  }

  ngOnInit(): void {
    this.facadeService.adminFacadeService.manageDriverService.list().subscribe(drivers => {
      this.drivers = drivers;
    });
  }

  cancel() {
    this.dialogRef.close();
  }

  submit() {
    if (this.form.invalid) {
      return this.toastrService.error('Please Select Driver');
    }

    const body = this.form.value;
    body.workOrderId = this.data['workorderId'];

    switch (this.type) {
      case 'assign':
        this.facadeService.adminFacadeService.manageWorkOrderService.assignDelgateToWorkOrder(body).subscribe(
          (drivers: any) => {
            if (Number(drivers) > 0) {
              this.toastrService.success("Assigned Successfully");
              this.dialogRef.close();
            }
            else {
              this.toastrService.error("Not Assigned Choose another Delegate");

            }
          });
        break;

      case 'dispatch':
        this.facadeService.adminFacadeService.manageWorkOrderService.dispatchDelgateToWorkOrder(body).subscribe(
          (drivers: any) => {
            if (Number(drivers) > 0) {
              this.toastrService.success("Dispatched Successfully");
              this.dialogRef.close();
            } else {
              this.toastrService.success("Not Dispatched Choose another Delegate");

            }

          });
        break;
      default:
        break;
    }
  }

}

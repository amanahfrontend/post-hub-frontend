import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignOrDispatchDriverComponent } from './assign-or-dispatch-driver.component';

describe('AssignOrDispatchDriverComponent', () => {
  let component: AssignOrDispatchDriverComponent;
  let fixture: ComponentFixture<AssignOrDispatchDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignOrDispatchDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignOrDispatchDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

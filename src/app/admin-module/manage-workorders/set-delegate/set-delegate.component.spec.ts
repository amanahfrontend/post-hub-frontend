import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDelegateComponent } from './set-delegate.component';

describe('SetDelegateComponent', () => {
  let component: SetDelegateComponent;
  let fixture: ComponentFixture<SetDelegateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetDelegateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDelegateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

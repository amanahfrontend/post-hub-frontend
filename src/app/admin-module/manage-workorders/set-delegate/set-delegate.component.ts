import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'set-delegate',
  templateUrl: './set-delegate.component.html',
  styleUrls: ['./set-delegate.component.scss']
})
export class SetDelegateComponent implements OnInit {

  form: FormGroup;
  options: string[] = ['One', 'Two', 'Three'];

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      branch: [''],
      name: [''],
      code: [''],
      dutyStatus: [''],
    });
  }

  ngOnInit() {

  }
}

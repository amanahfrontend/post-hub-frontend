import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageWorkordersRoutingModule } from './manage-workorders-routing.module';
import { ManageWorkordersComponent } from './manage-workorders.component';
import { SetDirModule } from './../../shared/directives/set-dir/set-dir.module';
import { HeaderAsCardModule } from './../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from './../../shared/modules/material/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageWorkorderFormComponent } from './manage-workorder-form/manage-workorder-form.component';
import { SetDelegateComponent } from './set-delegate/set-delegate.component';
import { BarcodeModule } from '../../shared/components/barcode/barcode.module';
import { ChangeWorkOrderStatusComponent } from './change-workorder-status/change-workorder-status.component';
import { AssignOrDispatchDriverComponent } from './assign-or-dispatch-driver/assign-or-dispatch-driver.component';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    ManageWorkordersComponent,
    ManageWorkorderFormComponent,
    SetDelegateComponent,
    ChangeWorkOrderStatusComponent,
    AssignOrDispatchDriverComponent
  ],
  imports: [
    CommonModule,
    SetDirModule,
    HeaderAsCardModule,
    MaterialModule,
    TranslateModule,
    FormsModule,
    BarcodeModule,
    ReactiveFormsModule,
      ManageWorkordersRoutingModule,
      ConfirmDeletionModule,
      PermissionsModule
  ],
  entryComponents: [
    SetDelegateComponent,
    AssignOrDispatchDriverComponent
  ]
})
export class ManageWorkordersModule { }

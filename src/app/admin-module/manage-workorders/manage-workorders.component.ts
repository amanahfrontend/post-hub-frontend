import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FacadeService } from 'app/services/facade.service';
import { Subscription } from 'rxjs-compat';
import { Barcode } from '../../shared/models/admin/Barcode';
import { BarcodeComponent } from '../../shared/components/barcode/barcode.component';
import { SetDelegateComponent } from './set-delegate/set-delegate.component';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { saveFile } from '../../shared/helpers/download-link';
import { workOrder } from 'app/shared/models/admin/workOrder';
import { ChangeWorkOrderStatusComponent } from './change-workorder-status/change-workorder-status.component';
import { AssignOrDispatchDriverComponent } from './assign-or-dispatch-driver/assign-or-dispatch-driver.component';

type AssignOrDispatch = 'assign' | 'dispatch';

// @TODO change it if something changed at status list
const DISPATCHED_STATUS_ID: number = 5;

@Component({
  selector: 'manage-workorders',
  templateUrl: './manage-workorders.component.html',
  styleUrls: ['./manage-workorders.component.scss']
})
export class ManageWorkordersComponent implements OnInit {

  displayedColumns: string[] = ['select', 'workorderInfo', 'dates', 'status', 'rafCode', 'areas', 'assignedTo', 'dispatchedTo', 'actions'];
  selection = new SelectionModel<any>(true, []);
  searchBy: string = '';
  showFilters: boolean = false;
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  workOrders: workOrder[] = [];
  subscriptions = new Subscription();
  dispatchedStatusId: number;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private translateService: TranslateService,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
  ) {

    this.dispatchedStatusId = DISPATCHED_STATUS_ID;
  }

  ngOnInit(): void {
    this.list(this.page);
  }

  deleteWorkorder(deleteConfirmed: boolean, manager: any) {
    if (deleteConfirmed) {
      this.facadeService.adminFacadeService.manageWorkOrderService.delete(manager.id).subscribe((res: any) => {
        const index = this.workOrders.indexOf(manager);

        if (index >= 0) {
          this.workOrders.splice(index, 1);
          this.replaceRoutePage(this.page);
          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`WorkOrder has been Deleted Successfully`));
        }
      });
    }
  }

  addWorkorder(): void {
    this.router.navigate(['manage-workorders/add']);
  }

  editWorkorder(workOrderId: number) {
    this.router.navigate(['manage-workorders/edit', workOrderId]);
  }

  viewWorkorder(workOrderId: number) {
    this.router.navigate(['manage-workorders/details', workOrderId]);
  }

  assignOrDispatch(workorderId: number, assignOrDispatchType: AssignOrDispatch): void {
    const dialog = this.dialog.open(AssignOrDispatchDriverComponent, {
      width: '60%',
      panelClass: 'custom-dialog-container',
      height: 'calc(50vh - 50px)',
      data: {
        workorderId: workorderId,
        type: assignOrDispatchType
      }
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      this.list(this.page);
    });
  }

  changeWorkOrderStatus(workorderid) {
    const dialog = this.dialog.open(ChangeWorkOrderStatusComponent, {
      width: '60%',
      panelClass: 'custom-dialog-container',
      height: 'calc(95vh - 50px)',
      data: {
        workorderId: workorderid
      }
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      this.list(this.page);
    });
  }

  export() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageWorkOrderService.exportToExcel(body).subscribe(data => {
      saveFile(`WorkOrder.csv ${new Date().toLocaleDateString()}`, "data:attachment/text", data);
    });
  }

  setDelegate(): void {
    const dialog = this.dialog.open(SetDelegateComponent, {
      width: '50%',
      data: {
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(55vh - 50px)'
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {

    });
  }

  barcodeDialog(barcodes: Barcode[]): void {
    const dialog = this.dialog.open(BarcodeComponent, {
      width: '95%',
      data: { barcodes },
      panelClass: 'custom-dialog-container',
      height: 'auto',
      minHeight: 'calc(75vh - 50px)'
    });

    //dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      this.list(this.page);
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.workOrders.length;
    return numSelected === numRows;
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.workOrders);
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onDelete(event: boolean, index: number) {
    if (event) {

    }
  }

  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

    this.list(this.page);
    this.replaceRoutePage(this.page);
  }

  private replaceRoutePage(page: number) {
    this.router.navigate(['manage-workorders'], { queryParams: { page: page } });
  }

  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.manageWorkOrderService.listByPagination(body).subscribe((res: any) => {
      this.workOrders = res.result;
      this.total = res.totalCount;
    });
  }

  search() {
    const body = {
      pageNumber: this.page,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    }

    this.facadeService.adminFacadeService.manageWorkOrderService.listByPagination(body).subscribe((res: any) => {
      this.workOrders = res.result;
      this.total = res.totalCount;
    });
  }

  toggleFilter() {
    this.showFilters = !this.showFilters;
  }

  printOneBarcode() {
    // this.barcodeDialog([BARCODE]);
  }

  printGroupOfBarcodes() {
    //  this.barcodeDialog(BARCODES);
  }
}

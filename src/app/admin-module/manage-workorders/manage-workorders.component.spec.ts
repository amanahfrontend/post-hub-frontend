import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageWorkordersComponent } from './manage-workorders.component';

describe('ManageWorkordersComponent', () => {
  let component: ManageWorkordersComponent;
  let fixture: ComponentFixture<ManageWorkordersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageWorkordersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageWorkordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FacadeService } from '../../../services/facade.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'change-workorder-status',
    templateUrl: './change-workorder-status.component.html',
    styleUrls: ['./change-workorder-status.component.scss']
})
export class ChangeWorkOrderStatusComponent implements OnInit {

  form: FormGroup;
  workOrderStatuss: any[] = [];

  /**
   * 
   * @param fb 
   * @param facadeService 
   */
    constructor(
        private fb: FormBuilder,
        private facadeService: FacadeService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private toastrService: ToastrService,
        public dialogRef: MatDialogRef<ChangeWorkOrderStatusComponent>    ) {
     
      this.form = this.fb.group({
          workOrderStatusId: [''],
    })
  }

    ngOnInit(): void {
        this.facadeService.adminFacadeService.manageWorkOrderService.GetAllWorkOrderStatuss().subscribe(result => {
            this.workOrderStatuss = result;
    });
  }
    cancel() {
        this.dialogRef.close();

    }


  submit() {
  
      const body = this.form.value;   
      body.id = this.data['workorderId'];
      this.facadeService.adminFacadeService.manageWorkOrderService.changeWorkOrderStatus(body).subscribe(drivers => {
          this.toastrService.success("Assigned Successfully");

          this.dialogRef.close();
      });

    // @TODO send form to backend
  }
}

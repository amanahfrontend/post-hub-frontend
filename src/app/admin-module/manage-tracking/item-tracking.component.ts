import { AfterViewInit, Optional } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { MouseEvent } from '@agm/core';
import { OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '@app/services/facade.service';
//import { MailItemStatusLog, StatusLogs } from 'app/shared/models/admin/MailItemStatusLog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { invoice } from '@app/shared/models/admin/invoice';
import { MatDialog } from '@angular/material/dialog';
export interface StatusLogs{
  createdDate:Date,
    mailItemStatusName:string
}
@Component({
  selector: 'item-tracking.component',
  templateUrl: './item-tracking.component.html',
  styleUrls: ['./item-tracking.component.scss']
})

export class ItemTrackingComponent implements OnInit, AfterViewInit, OnDestroy {
  isLinear = false;
  formGroup : FormGroup;
  form: FormArray;
  //formG: FormGroup; 
  logs: StatusLogs[]=[];
  displayedColumns: string[] = [
    'contractCode',
    'invoicePeriodFrom',
    'invoiceStatus',
    'businessCustomerName',
    'invoiceNo',
    'invoicePeriodTo',    
    'totalValue',   
    'actions'
  ];
  
  subscriptions = new Subscription(); 
  searchBy: string = '';
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
   invoices: invoice[] = [];
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective; 
  dtTrigger: Subject<any> = new Subject();
 
  dataSource;
  companyForm;
  masterCheck = false
  newlogs: StatusLogs[]=[];
 
  
  constructor(private translateService: TranslateService , 
    private dialog: MatDialog,
    private router: Router,  
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private _formBuilder: FormBuilder
    ) {
     
  }

  ngOnInit(): void {   
   
  } 

  trackItem(): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.mailItemService.geMailItemStatusLogByItemCode(this.searchBy).subscribe(res => {
     
      this.newlogs=res;     
     
    }));
  }
 
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
 
}
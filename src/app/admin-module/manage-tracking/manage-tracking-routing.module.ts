import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemTrackingComponent } from './item-tracking.component';



const routes: Routes = [
  {
    path: '',
    component: ItemTrackingComponent
  } 
  
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageTrackingRoutingModule { }

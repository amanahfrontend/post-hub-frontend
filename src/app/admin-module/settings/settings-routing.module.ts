import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageLanguagesComponent } from './manage-languages/manage-languages.component';
import { SettingsComponent } from './settings.component';
import { MailItemTypeComponent } from './mail-item-type/mail-item-type.component';
import { WeightUOMComponent } from './weight-uom/weight-uom.component';
import { LengthUOMComponent } from './length-uom/length-uom.component';
import { ItemReturnReasonComponent } from './item-return-reason/item-return-reason.component';
import { GovernorateComponent } from './governorate/governorate.component';
import { CountryComponent } from './country/country.component';
import { AreaComponent } from './area/area.component';

const routes: Routes = [
  {
    path:'',
    component:SettingsComponent
  },
  {
    path:'manage-languages',
    component:ManageLanguagesComponent
  },
  {
    path:'mail-item-type',
    component: MailItemTypeComponent
  },
  {
    path:'item-return-reason',
    component: ItemReturnReasonComponent
  },
  {
    path:'weight-uom',
    component: WeightUOMComponent
  },
  {
    path:'length-uom',
    component: LengthUOMComponent
  }
  ,
  {
    path:'governorate',
    component: GovernorateComponent
  }
  ,
  {
    
    path:'country',
    component: CountryComponent
  }
  ,
  {
    
    path:'area',
    component: AreaComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }

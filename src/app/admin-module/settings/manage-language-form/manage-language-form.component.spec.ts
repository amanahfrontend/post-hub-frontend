import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageLanguageFormComponent } from './manage-language-form.component';

describe('ManageLanguageFormComponent', () => {
  let component: ManageLanguageFormComponent;
  let fixture: ComponentFixture<ManageLanguageFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageLanguageFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageLanguageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

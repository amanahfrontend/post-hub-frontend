import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from './../../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { LanguageService } from '../../../services/shared/language.service';
import { TranslateService } from '@ngx-translate/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Language } from 'app/shared/models/admin/language';
@Component({
  selector: 'manage-language-form',
  templateUrl: './manage-language-form.component.html',
  styleUrls: ['./manage-language-form.component.scss']
})
export class ManageLanguageFormComponent implements OnInit {
  form: FormGroup;
  row: Language;
  isEditMode: boolean = false;
  operation: string;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<ManageLanguageFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      isActive: [false, [Validators.nullValidator],

    ],
     // code: ['', [Validators.required]],
    
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('code').setValue(this.row.code);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
  }

  
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
      //body.id=0;
      let { countryCtrl, ...objectToPost } = body;
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.manageLaguageService.update(this.row.id,objectToPost).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('Language Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {
        this.facadeService.adminFacadeService.manageLaguageService.create(objectToPost).subscribe(
          res => {
           
          this.toastrService.success(this.translateService.instant('New Laguage Added Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

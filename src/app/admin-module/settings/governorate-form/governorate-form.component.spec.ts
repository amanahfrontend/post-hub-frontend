import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernorateFormComponent } from './governorate-form.component';

describe('GovernorateFormComponent', () => {
  let component: GovernorateFormComponent;
  let fixture: ComponentFixture<GovernorateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovernorateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernorateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

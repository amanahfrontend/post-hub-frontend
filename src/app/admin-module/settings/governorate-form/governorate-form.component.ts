import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Governorate } from 'app/shared/models/admin/governorate';
import { Option } from './../../../shared/models/admin/option';
import { CreateGovernorate } from 'app/shared/models/admin/createGovernorate';
@Component({
  selector: 'governorate-form',
  templateUrl: './governorate-form.component.html',
  styleUrls: ['./governorate-form.component.scss']
})
export class GovernorateFormComponent implements OnInit {
  form: FormGroup;
  row: CreateGovernorate;
  isEditMode: boolean = false;
  operation: string;
  countries  : Option[] = [];
  maxId:string
  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<GovernorateFormComponent>,
  ) {
    this.form = this.fb.group({
    
      nameEN: ['', [Validators.required]],
      nameAR: ['', [Validators.required]],
      isActive: [false, [Validators.nullValidator]],
      countryId : [null, [Validators.nullValidator]],
     
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
    this.maxId=this.data.maxId;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('nameAR').setValue(this.row.nameAR);
      this.form.get('nameEN').setValue(this.row.nameEN);
      this.form.get('countryId').setValue(this.row.countryId);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
    this.listActive()
  }

  listActive(): void {
  
    this.facadeService.adminFacadeService.countryService.listActive().subscribe((res: any) => {
      this.countries = res;
    });
  }
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
     
    
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.governorateService.update(this.row.id,body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('Governorate Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {
          //  let id =Number(this.maxId)
          //  let newId=id+1;
          //  body.id= newId.toString()
        this.facadeService.adminFacadeService.governorateService.create(body).subscribe(
          res => {
           
          this.toastrService.success(this.translateService.instant('New Governorate Added Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

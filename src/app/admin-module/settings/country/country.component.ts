
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Country } from 'app/shared/models/components';
import { CountryFormComponent } from '../country-form/country-form.component';
@Component({
  selector: 'country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  subscriptions = new Subscription();

 
  countries: Country[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'topLevel',
'courierZoneName',
    'code',
    'isActive',  
    'actions'
   ];
  
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
       
      }
    }));
  }

  ngOnInit(): void {
    
    this.list();
  
  }

  list(): void {
  
    this.facadeService.adminFacadeService.CountryAddService.list().subscribe((res: any) => {
      this.countries = res;
    });
  }

  manageCountry(opertaion: string, editMode: boolean, country?: Country): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(CountryFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: country ? country : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list();
    });

  }

  onConfirm(event: boolean, manager: Country): void {
    if (event) {
      this.facadeService.adminFacadeService.CountryAddService.delete(manager.id).subscribe(res => {
        const index = this.countries.indexOf(manager);
        if (index >= 0) {
          this.countries.splice(index, 1);

          this.list();
          this.toastrService.success(this.translateService.instant(`Country has been deleted successfully`));
        }
      });
    }
  }
}


import {  Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MailItemTypeFormComponent } from '../mail-item-type-form/mail-item-type-form.component';
import { MailItemType } from 'app/shared/models/admin/MailItemType';
@Component({
  selector: 'mail-item-type',
  templateUrl: './mail-item-type.component.html',
  styleUrls: ['./mail-item-type.component.scss']
})
export class MailItemTypeComponent implements OnInit {
  subscriptions = new Subscription();
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  // languages: Language[] = [
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"},
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"},
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"}
  // ];
  mailItemTypes: MailItemType[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'isActive', 
    'actions'
   ];
  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));
  }

  ngOnInit(): void {
    this.list(this.page);
   // console.log(this.mailItemTypes)
  }

  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.mailItemTypeService.list().subscribe((res: any) => {
      
      this.mailItemTypes = res;
      this.total = res.totalCount;
    });
  }
  manageMailItemType(opertaion: string, editMode: boolean, itemType?: MailItemType): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(MailItemTypeFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: itemType ? itemType : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list(this.page);
    });
      
    // dialogRef.afterClosed().subscribe(
    //    result => {
    //     if(result.operation=='Edit'){
    //        let index = this.mailItemTypes.findIndex(x => x.id ==result.data.id );
    //        this.mailItemTypes[index] =[...  result.data];
    //     }else{
    //       this.mailItemTypes.push(... [result.data]);
    //     }
    // });
    
  }
  onConfirmDelete(event: boolean, manager: MailItemType): void {
    if (event) {
      this.facadeService.adminFacadeService.mailItemTypeService.delete(manager.id).subscribe(res => {
        const index = this.mailItemTypes.indexOf(manager);
        if (index >= 0) {
          this.mailItemTypes.splice(index, 1);

          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`MailItemType has been deleted successfully`));
        }
      });
    }
  }
}

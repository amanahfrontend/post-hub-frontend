import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailItemTypeComponent } from './mail-item-type.component';

describe('MailItemTypeComponent', () => {
  let component: MailItemTypeComponent;
  let fixture: ComponentFixture<MailItemTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailItemTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailItemTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

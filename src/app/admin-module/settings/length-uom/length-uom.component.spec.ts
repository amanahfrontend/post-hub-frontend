import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LengthUOMComponent } from './length-uom.component';

describe('LengthUOMComponent', () => {
  let component: LengthUOMComponent;
  let fixture: ComponentFixture<LengthUOMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LengthUOMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LengthUOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

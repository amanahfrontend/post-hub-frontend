
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LengthUOM } from 'app/shared/models/admin/length-uom';
import { LengthUOMFormComponent } from '../length-uomform/length-uomform.component';
@Component({
  selector: 'length-uom',
  templateUrl: './length-uom.component.html',
  styleUrls: ['./length-uom.component.scss']
})
export class LengthUOMComponent implements OnInit {
  subscriptions = new Subscription();

 
  lengthUOMs: LengthUOM[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'isActive',
    'code',  
    'actions'
   ];
  
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
       
      }
    }));
  }

  ngOnInit(): void {
    
    this.list();
  
  }

  list(): void {
  
    this.facadeService.adminFacadeService.lengthUOMService.list().subscribe((res: any) => {
      this.lengthUOMs = res;
    });
  }

  manageLengthUOM(opertaion: string, editMode: boolean, lengthUOM?: LengthUOM): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(LengthUOMFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: lengthUOM ? lengthUOM : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list();
    });

  }

  onConfirm(event: boolean, manager: LengthUOM): void {
    if (event) {
      this.facadeService.adminFacadeService.lengthUOMService.delete(manager.id).subscribe(res => {
        const index = this.lengthUOMs.indexOf(manager);
        if (index >= 0) {
          this.lengthUOMs.splice(index, 1);

          this.list();
          this.toastrService.success(this.translateService.instant(`LengthUOM has been deleted successfully`));
        }
      });
    }
  }
}

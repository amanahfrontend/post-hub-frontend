
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GovernorateFormComponent } from '../governorate-form/governorate-form.component';
import { Governorate } from 'app/shared/models/admin/governorate';
@Component({
  selector: 'governorate',
  templateUrl: './governorate.component.html',
  styleUrls: ['./governorate.component.scss']
})
export class GovernorateComponent implements OnInit {
  subscriptions = new Subscription();
  maxId:string;
  governorates: Governorate[] = [];

  displayedColumns: string[] = [
    'id',
    'nameEN',
    'nameAR',
    'countryName',
    'isActive',  
    'actions'
   ];
  
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
       
      }
    }));
  }

  ngOnInit(): void {
    
    this.list();
  
  }

  list(): void {
  
    this.facadeService.adminFacadeService.governorateService.list().subscribe((res: any) => {
      this.governorates = res;
      this.maxId=Math.max.apply(Math,res.map(obj => obj.id)); 
    });
  }

  manageGovernorate(opertaion: string, editMode: boolean, governorate?: Governorate): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(GovernorateFormComponent, {
      width: '60%',
      data: {
        maxId:this.maxId,
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: governorate ? governorate : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list();
    });

  }

  onConfirm(event: boolean, manager: Governorate): void {
    if (event) {
      this.facadeService.adminFacadeService.governorateService.delete(manager.id).subscribe(res => {
        const index = this.governorates.indexOf(manager);
        if (index >= 0) {
          this.governorates.splice(index, 1);

          this.list();
          this.toastrService.success(this.translateService.instant(`Governorate has been deleted successfully`));
        }
      });
    }
  }
}


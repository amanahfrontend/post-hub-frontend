
import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ItemReturnReason } from 'app/shared/models/admin/itemReturnReason';
@Component({
  selector: 'item-return-reason-form',
  templateUrl: './item-return-reason-form.component.html',
  styleUrls: ['./item-return-reason-form.component.scss']
})
export class ItemReturnReasonFormComponent implements OnInit {
  form: FormGroup;
  row: ItemReturnReason;
  isEditMode: boolean = false;
  operation: string;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<ItemReturnReasonFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      isActive: [false, [Validators.nullValidator],

    ],
    
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
  }

  
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
     
    
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.itemReturnReasonService.update(this.row.id,body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('Item Return Reason Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {
        this.facadeService.adminFacadeService.itemReturnReasonService.create(body).subscribe(
          res => {
           

          this.toastrService.success(this.translateService.instant('New ie Item Return Reason Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

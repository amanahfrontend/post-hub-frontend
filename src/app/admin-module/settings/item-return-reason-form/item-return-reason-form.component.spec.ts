import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnReasonFormComponent } from './item-return-reason-form.component';

describe('ItemReturnReasonFormComponent', () => {
  let component: ItemReturnReasonFormComponent;
  let fixture: ComponentFixture<ItemReturnReasonFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnReasonFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnReasonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

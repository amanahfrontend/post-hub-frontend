import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightUOMFormComponent } from './weight-uomform.component';

describe('WeightUOMFormComponent', () => {
  let component: WeightUOMFormComponent;
  let fixture: ComponentFixture<WeightUOMFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeightUOMFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightUOMFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { WeightUOM } from 'app/shared/models/admin/weight-uom';
@Component({
  selector: 'weight-uomform',
  templateUrl: './weight-uomform.component.html',
  styleUrls: ['./weight-uomform.component.scss']
})
export class WeightUOMFormComponent implements OnInit {
  form: FormGroup;
  row: WeightUOM;
  isEditMode: boolean = false;
  operation: string;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<WeightUOMFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      description: ['', [Validators.nullValidator]],
      isActive: [false, [Validators.nullValidator],

      ],

    });
    this.operation = this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {

    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('code').setValue(this.row.code);
      this.form.get('description').setValue(this.row.description);
      this.form.get('isActive').setValue(this.row.isActive);

    }
  }


  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    let body = { ... this.form.value };

    if (this.isEditMode) {
      body.id = this.row.id;
    }


    if (this.isEditMode) {
      this.facadeService.adminFacadeService.weightUOMService.update(this.row.id, body).subscribe(
        res => {
          this.toastrService.success(this.translateService.instant('Weight-UOM Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res });
        });
    } else {
      this.facadeService.adminFacadeService.weightUOMService.create(body).subscribe(
        res => {

          this.toastrService.success(this.translateService.instant('New Weight-UOM Added Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res });
        });
    }
  }
}

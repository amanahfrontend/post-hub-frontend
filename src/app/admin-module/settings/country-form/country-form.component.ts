

import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Country } from 'app/shared/models/admin/country';
import { CourierZone } from 'app/shared/models/admin/CourierZone';
import { FacadeService } from 'app/services/facade.service';
@Component({
  selector: 'country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss']
})
export class CountryFormComponent implements OnInit {
  form: FormGroup;
  row: Country;
  isEditMode: boolean = false;
  operation: string;
  courierZones:CourierZone[]=[];

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private FacadeServiceCountry:FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<CountryFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      topLevel:['', [Validators.required]],
      courierZoneId:['', [Validators.nullValidator]],
      isActive: [false, [Validators.nullValidator],

    ],
    
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('code').setValue(this.row.code);
      this.form.get('topLevel').setValue(this.row.topLevel);
      this.form.get('courierZoneId').setValue(this.row.courierZoneId);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
    this.listActive();
  }
  listActive(): void {
  
    this.facadeService.adminFacadeService.courierZoneService.listActive().subscribe((res: any) => {
      this.courierZones = res;
    });
  }
  
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
     
    
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.CountryAddService.update(body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('Country Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {
        this.facadeService.adminFacadeService.CountryAddService.create(body).subscribe(
          res => {
           
          this.toastrService.success(this.translateService.instant('New Country Added Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightUOMComponent } from './weight-uom.component';

describe('WeightUOMComponent', () => {
  let component: WeightUOMComponent;
  let fixture: ComponentFixture<WeightUOMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeightUOMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightUOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

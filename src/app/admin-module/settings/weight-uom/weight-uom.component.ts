
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { WeightUOM } from 'app/shared/models/admin/weight-uom';
import { WeightUOMFormComponent } from '../weight-uomform/weight-uomform.component';
@Component({
  selector: 'weight-uom',
  templateUrl: './weight-uom.component.html',
  styleUrls: ['./weight-uom.component.scss']
})
export class WeightUOMComponent implements OnInit {
  subscriptions = new Subscription();


  weightUOMs: WeightUOM[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'isActive',
    'code',
    'description',
    'actions'
  ];

  constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {

      }
    }));
  }

  ngOnInit(): void {

    this.list();

  }

  list(): void {

    this.facadeService.adminFacadeService.weightUOMService.list().subscribe((res: any) => {
      this.weightUOMs = res;
    });
  }

  manageWeightUOM(opertaion: string, editMode: boolean, weightUOM?: WeightUOM): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(WeightUOMFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: weightUOM ? weightUOM : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list();
    });

  }

  onConfirm(event: boolean, manager: WeightUOM): void {
    if (event) {
      this.facadeService.adminFacadeService.weightUOMService.delete(manager.id).subscribe(res => {
        const index = this.weightUOMs.indexOf(manager);
        if (index >= 0) {
          this.weightUOMs.splice(index, 1);

          this.list();
          this.toastrService.success(this.translateService.instant(`Weight-UOM has been deleted successfully`));
        }
      });
    }
  }
}

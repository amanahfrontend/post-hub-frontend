import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LengthUOMFormComponent } from './length-uomform.component';

describe('LengthUOMFormComponent', () => {
  let component: LengthUOMFormComponent;
  let fixture: ComponentFixture<LengthUOMFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LengthUOMFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LengthUOMFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { LengthUOM } from 'app/shared/models/admin/length-uom';
@Component({
  selector: 'length-uomform',
  templateUrl: './length-uomform.component.html',
  styleUrls: ['./length-uomform.component.scss']
})
export class LengthUOMFormComponent implements OnInit {
  form: FormGroup;
  row: LengthUOM;
  isEditMode: boolean = false;
  operation: string;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<LengthUOMFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      code: ['', [Validators.required]],
      isActive: [false, [Validators.nullValidator],

    ],
    
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('code').setValue(this.row.code);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
  }

  
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
     
    
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.lengthUOMService.update(this.row.id,body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('LengthUOM Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {
        this.facadeService.adminFacadeService.lengthUOMService.create(body).subscribe(
          res => {
           
          this.toastrService.success(this.translateService.instant('New LengthUOM Added Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

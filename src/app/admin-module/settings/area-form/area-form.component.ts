
import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Country } from 'app/shared/models/admin/country';
import { CourierZone } from 'app/shared/models/admin/CourierZone';
import { FacadeService } from 'app/services/facade.service';
import { Governorate } from '@app/shared/models/admin/governorate';
import { Area } from '@app/shared/models/admin/area';
import { AreaBlock } from '@app/shared/models/admin/area-block';
@Component({
  selector: 'area-form',
  templateUrl: './area-form.component.html',
  styleUrls: ['./area-form.component.scss']
})
export class AreaFormComponent implements OnInit {
  form: FormGroup;
  row: Area;
  isEditMode: boolean = false;
  operation: string;
  governorates: Governorate[] = [];
  blocks:AreaBlock[];

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private FacadeServiceCountry:FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<AreaFormComponent>,
  ) {
    this.form = this.fb.group({

      nameEN: ['', [Validators.required]],
      nameAR: ['', [Validators.required]],
      fk_Governrate_Id:[null, [Validators.required]],
      isActive: [false, [Validators.nullValidator]],
      isSystem : [false, [Validators.nullValidator]],
      governateName : ['', [Validators.nullValidator]],
      areaBlocks: this.fb.array([]),
     

    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
     // this.form.patchValue(this.row);

      // this.form.get('fk_Governrate_Id').setValue(this.row.fk_Governrate_Id);
      // this.form.get('nameEN').setValue(this.row.nameEN);
      // this.form.get('nameAR').setValue(this.row.nameAR);
      // this.form.get('isSystem').setValue(this.row.isSystem);
      // this.form.get('isActive').setValue(this.row.isActive);
      this.getAllBlocksByAreaId(this.row.id);

       
    }
    this.listGovernorates();
  }

  listGovernorates(): void {
  
    this.facadeService.adminFacadeService.governorateService.list().subscribe((res: any) => {
      this.governorates = res;
      //this.maxId=Math.max.apply(Math,res.map(obj => obj.id)); 
    });
  }

  areaBlocks(): FormArray {
    return this.form.get('areaBlocks') as FormArray;
  }

  newBlock(): FormGroup {
    return this.fb.group({
      id:[0, [Validators.nullValidator]],
      blockNo:['', [Validators.required]],
      isActive : [false, [Validators.nullValidator]],
      isSystem : [false, [Validators.nullValidator]],
    });
  }

  addBlock(): void {
    this.areaBlocks().push(this.newBlock());
  }
  onInputChange(e,index){
   let areaList= this.form.get('areaBlocks').value
   let newareaBlocks=areaList.map(obj=>{
     return obj.blockNo.toString()
   })
   if( newareaBlocks.length > 1 ){
  if( newareaBlocks.indexOf(e.target.value) !== -1 && newareaBlocks.indexOf(e.target.value)!=index){
   /// areaList.push(e.target.value) 
   this.toastrService.error(this.translateService.instant('Block already exist enter another block'));
   let block={
       id:0,
       blockNo:'',
       isActive :false,
       isSystem : false,
     }
     this.areaBlocks().at(index).patchValue(block);
  }
}
   

 }
  editBlock(index: number, term: { name: string }): void {
    this.areaBlocks().at(index).patchValue(term);
  }

  removeBlock(termIndex: number): void {
    this.areaBlocks().removeAt(termIndex);
  }
  getAllBlocksByAreaId(areaId:number){
    this.facadeService.adminFacadeService.areaService.getAllBlocksByAreaId(areaId).subscribe(
      (res:any) => {
        this.blocks=res
        this.blocks.forEach(pq => {
          this.addBlock();
        });
        //this.form.get('areaBlocks').setValue(this.blocks);
         // this.form.get('fk_Governrate_Id').setValue(this.row.fk_Governrate_Id);
      // this.form.get('nameEN').setValue(this.row.nameEN);
      // this.form.get('nameAR').setValue(this.row.nameAR);
      // this.form.get('isSystem').setValue(this.row.isSystem);
      // this.form.get('isActive').setValue(this.row.isActive);
        let newObj={
          nameEN: this.row.nameEN,
          nameAR:this.row.nameAR,
          fk_Governrate_Id:this.row.fK_Governrate_Id,
          isActive: this.row.isSystem,
          isSystem : this.row.isSystem,
          areaBlocks: this.blocks,
        }
        this.form.patchValue(newObj);       

    });
  }
    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
     
    
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.areaService.updateArea(body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('Area Updated Successfully'));
          this.dialogRef.close({ operation: this.operation, data: res});
        });
      } else {

        this.facadeService.adminFacadeService.areaService.createArea(body).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('New Area Added Successfully'));
          this.dialogRef.close({ operation: this.operation,data: res});
        });
      }
    }
}

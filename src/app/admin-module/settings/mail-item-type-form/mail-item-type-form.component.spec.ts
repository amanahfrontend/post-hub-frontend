import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailItemTypeFormComponent } from './mail-item-type-form.component';

describe('MailItemTypeFormComponent', () => {
  let component: MailItemTypeFormComponent;
  let fixture: ComponentFixture<MailItemTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailItemTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailItemTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

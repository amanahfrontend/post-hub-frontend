
import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Option } from './../../../shared/models/admin/option';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MailItemType } from 'app/shared/models/admin/MailItemType';
@Component({
  selector: 'mail-item-type-form',
  templateUrl: './mail-item-type-form.component.html',
  styleUrls: ['./mail-item-type-form.component.scss']
})
export class MailItemTypeFormComponent implements OnInit {
  form: FormGroup;
  row: MailItemType;
  isEditMode: boolean = false;
  operation: string;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<MailItemTypeFormComponent>,
  ) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      isActive: [false, [Validators.nullValidator],

    ],
    
    });
    this.operation=this.data.operation;
    this.isEditMode = this.data.isEditMode;
  }

  ngOnInit(): void {
    
    if (this.isEditMode) {
      this.row = this.data.row;
      this.form.patchValue(this.row);

      this.form.get('name').setValue(this.row.name);
      this.form.get('isActive').setValue(this.row.isActive);
       
    }
  }

    submit() {
      if (this.form.invalid) {
        this.form.markAllAsTouched();
        return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      }
  
      let body = { ... this.form.value };
  
      if (this.isEditMode) {
        body.id = this.row.id;
      }
      //body.id=0;
      let { ...objectToPost } = body;
      if (this.isEditMode) {
        this.facadeService.adminFacadeService.mailItemTypeService.update(this.row.id,objectToPost).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('MailItem Type Updated Successfully'));
          this.dialogRef.close({ operation: this.operation ,data:objectToPost });
        });
      } else {
        this.facadeService.adminFacadeService.mailItemTypeService.create(objectToPost).subscribe(
          res => {
          this.toastrService.success(this.translateService.instant('New MailItem Type  Added Successfully'));
          objectToPost.id=res;
          this.dialogRef.close({ operation: this.operation ,data:objectToPost});
        });
      }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { ManageLanguagesComponent } from './manage-languages/manage-languages.component';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { FormsModule } from '@angular/forms';
import { ManageLanguageFormComponent } from './manage-language-form/manage-language-form.component';
import { MailItemTypeComponent } from './mail-item-type/mail-item-type.component';
import { MailItemTypeFormComponent } from './mail-item-type-form/mail-item-type-form.component';
import { WeightUOMComponent } from './weight-uom/weight-uom.component';
import { WeightUOMFormComponent } from './weight-uomform/weight-uomform.component';
import { LengthUOMFormComponent } from './length-uomform/length-uomform.component';
import { LengthUOMComponent } from './length-uom/length-uom.component';
import { ItemReturnReasonComponent } from './item-return-reason/item-return-reason.component';
import { ItemReturnReasonFormComponent } from './item-return-reason-form/item-return-reason-form.component';

import { CountryComponent } from './country/country.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { GovernorateComponent } from './governorate/governorate.component';
import { GovernorateFormComponent } from './governorate-form/governorate-form.component';
import { AreaComponent } from './area/area.component';
import { AreaFormComponent } from './area-form/area-form.component';
import { MatFormFieldModule } from "@angular/material/form-field";
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    SettingsComponent,
    ManageLanguagesComponent,
    ManageLanguageFormComponent,
    MailItemTypeComponent,
    MailItemTypeFormComponent,
    WeightUOMComponent,
    WeightUOMFormComponent,
    LengthUOMFormComponent,
    LengthUOMComponent,
    CountryComponent,
    CountryFormComponent,
    GovernorateComponent,
    GovernorateFormComponent,
    ItemReturnReasonComponent,
    ItemReturnReasonFormComponent,
    AreaComponent,
    AreaFormComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    HeaderAsCardModule,
    MaterialModule,
    TranslateModule,
    ReactiveFormsModule,
    ConfirmDeletionModule,
    FormsModule,
    MatFormFieldModule,
    PermissionsModule
  ]
})
export class SettingsModule { }

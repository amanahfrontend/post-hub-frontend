import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemReturnReasonFormComponent } from '../item-return-reason-form/item-return-reason-form.component';
import { ItemReturnReason } from 'app/shared/models/admin/itemReturnReason';
import { LengthUOMFormComponent } from '../length-uomform/length-uomform.component';
@Component({
  selector: 'item-return-reason',
  templateUrl: './item-return-reason.component.html',
  styleUrls: ['./item-return-reason.component.scss']
})
export class ItemReturnReasonComponent implements OnInit {
  subscriptions = new Subscription();

 
  itemReturnReasons: ItemReturnReason[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'isActive',
    'actions'
   ];
  
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
       
      }
    }));
  }

  ngOnInit(): void {
    
    this.list();
  
  }

  list(): void {
  
    this.facadeService.adminFacadeService.itemReturnReasonService.list().subscribe((res: any) => {
      this.itemReturnReasons = res;
    });
  }

  manageItemReturnReason(opertaion: string, editMode: boolean, itemReturnReason?: ItemReturnReason): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(ItemReturnReasonFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: itemReturnReason ? itemReturnReason : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list();
    });

  }

  onConfirm(event: boolean, manager: ItemReturnReason): void {
    if (event) {
      this.facadeService.adminFacadeService.itemReturnReasonService.delete(manager.id).subscribe(res => {
        const index = this.itemReturnReasons.indexOf(manager);
        if (index >= 0) {
          this.itemReturnReasons.splice(index, 1);

          this.list();
          this.toastrService.success(this.translateService.instant(`item Return Reason has been deleted successfully`));
        }
      });
    }
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnReasonComponent } from './item-return-reason.component';

describe('ItemReturnReasonComponent', () => {
  let component: ItemReturnReasonComponent;
  let fixture: ComponentFixture<ItemReturnReasonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnReasonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

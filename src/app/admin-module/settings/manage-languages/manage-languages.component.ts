import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageLanguageFormComponent } from '../manage-language-form/manage-language-form.component';
import { Language } from 'app/shared/models/admin/language';
@Component({
  selector: 'manage-languages',
  templateUrl: './manage-languages.component.html',
  styleUrls: ['./manage-languages.component.scss']
})
export class ManageLanguagesComponent implements OnInit {
  subscriptions = new Subscription();
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  // languages: Language[] = [
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"},
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"},
  //   {Id:1,Name:"amer",IsActive:true,Code:"sd"}
  // ];
  languages: Language[] = [];

  displayedColumns: string[] = [
    'id',
    'name',
    'isActive',
    'code', 
    'actions'
   ];
  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
        this.page = params.page;
      }
    }));
  }

  ngOnInit(): void {
    
    this.list(this.page);
    console.log(this.languages)
  }

  list(page: number): void {
    const body: { pageNumber: number, pageSize: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
    };

    this.facadeService.adminFacadeService.manageLaguageService.list().subscribe((res: any) => {
      
      this.languages = res;
      this.total = res.totalCount;
    });
  }

  manageLanguageSeletedRow(opertaion: string, editMode: boolean, language?: Language): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(ManageLanguageFormComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: language ? language : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.list(this.page);
    });

  //  dialogRef.afterClosed().subscribe(
  //      result => {
  //      console.log(result);
    
  //      if(result.operation=='Edit'){
  //         let index = this.languages.findIndex(x => x.id == result.data.id );
  //         this.languages[index] = result.data;
  //      }else{
  //        this.languages.push(... [result.data]);
  //        console.log( this.languages)
  //      }
  //  });
  }

  onConfirm(event: boolean, manager: Language): void {
    if (event) {
      this.facadeService.adminFacadeService.manageLaguageService.delete(manager.id).subscribe(res => {
        const index = this.languages.indexOf(manager);
        if (index >= 0) {
          this.languages.splice(index, 1);

          this.list(this.page);
          this.toastrService.success(this.translateService.instant(`language has been deleted successfully`));
        }
      });
    }
  }
}

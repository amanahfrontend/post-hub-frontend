
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from 'app/services/facade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Governorate } from 'app/shared/models/admin/governorate';
import { Area } from '@app/shared/models/admin/area';
import { AreaFormComponent } from '../area-form/area-form.component';
@Component({
  selector: 'area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {
  subscriptions = new Subscription();
  maxId:string;
  page: number = 1;
  pageSize: number = 10;
  areas:Area[]=[]
  searchBy:string='';
  total:number=0;
  displayedColumns: string[] = [
    'id',
    'nameEN' ,
    'nameAR' ,
    'isActive' ,
   // 'isSystem' ,
    //'fk_Governrate_Id' ,
    //'raf' ,
    'governateName'  ,
    'actions'

   ];
  
   constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.page) {
       
      }
    }));
  }

  ngOnInit(): void {
    
    this.getAllAreasPaging(this.page)
  }

  
  getAllAreasPaging(page:number){
    const body: { pageNumber: number, pageSize: number, searchBy:string} = {
      pageNumber:page ,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
    };
    this.facadeService.adminFacadeService.areaService.listAreas(body).subscribe(
      (res:any) => {
        this.areas=res.result;
        this.total = res.totalCount;
    })
  }
  search(): void {
    const body: { 
      pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.areaService.listAreas(body).subscribe(
      (res:any) => {
        this.areas=res.result;
        this.total = res.totalCount;
    })
  }
  onChangePage(event) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getAllAreasPaging(this.page);
    this.replaceRoutePage(this.page);
  }

 
  private replaceRoutePage(page: number) {
    this.router.navigate(['area'], { queryParams: { page: page } });
  }
  manageArea(opertaion: string, editMode: boolean, area?: Area): void {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialogRef = this.dialog.open(AreaFormComponent, {
      width: '60%',
      data: {
        maxId:this.maxId,
        operation: this.translateService.instant(type),
        isEditMode: editMode,
        row: area ? area : null
      },
      panelClass: 'custom-dialog-container',
      height: 'calc(100vh - 50px)'
    }).afterClosed().subscribe(result => {
      this.getAllAreasPaging(this.page);
    });

  }

  onConfirm(event: boolean, manager: Area): void {
    if (event) {
      this.facadeService.adminFacadeService.areaService.deleteArea(manager).subscribe(res => {
        const index = this.areas.indexOf(manager);
        if (index >= 0) {
          this.areas.splice(index, 1);

          this.getAllAreasPaging(this.page);
          this.toastrService.success(this.translateService.instant(`Area has been deleted successfully`));
        }
      });
    }
  }
}



import { AfterViewInit, Optional } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Subscription } from 'rxjs';
import { MouseEvent } from '@agm/core';
import { OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '@app/services/facade.service';
//import { MailItemStatusLog, StatusLogs } from 'app/shared/models/admin/MailItemStatusLog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { invoice } from '@app/shared/models/admin/invoice';
import { MatDialog } from '@angular/material/dialog';
export interface StatusLogs{
  createdDate:Date,
    mailItemStatusName:string
}
@Component({
  selector: 'order-tracking',
  templateUrl: './order-tracking.component.html',
  styleUrls: ['./order-tracking.component.scss']
})
export class OrderTrackingComponent implements OnInit {
  isLinear = false;
  formGroup : FormGroup;
  form: FormArray;
  //formG: FormGroup; 
  logs: StatusLogs[]=[]; 
  
  subscriptions = new Subscription(); 
  searchBy: string = '';
  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
   invoices: invoice[] = [];
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective; 
  dtTrigger: Subject<any> = new Subject();
 
  dataSource;
  companyForm;
  masterCheck = false
  newlogs: StatusLogs[]=[];
 
  
  constructor(private translateService: TranslateService , 
    private dialog: MatDialog,
    private router: Router,  
    private toastrService: ToastrService,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private _formBuilder: FormBuilder
    ) {
     
  }

  ngOnInit(): void {   
   debugger
  } 

  trackOrder(): void {
    this.subscriptions.add(this.facadeService.adminFacadeService.orderService.geOrderStatusLogsByOrderCode(this.searchBy).subscribe(res => {
      debugger
      this.newlogs=res;     
     
    }));
  }
 
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
 
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriceListRoutingModule } from './price-list-routing.module';
import { PriceListComponent } from './price-list.component';
import { ManagePriceComponent } from './manage-price/manage-price.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PriceListComponent,
    ManagePriceComponent
  ],
  imports: [
    CommonModule,
    PriceListRoutingModule,
    CommonModule,
    HttpClientModule,
    MaterialModule,
    MatDialogModule,
    TranslateModule,
    ReactiveFormsModule,
    ConfirmDeletionModule,
    HeaderAsCardModule,
    DataTablesModule
  ],
  entryComponents: [
    ManagePriceComponent
  ]
})
export class PriceListModule { }

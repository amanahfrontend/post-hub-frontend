import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Country } from '../../../shared/models/components';
import { Subscription } from 'rxjs';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { App } from '../../../core/app';
import { BusinessCustomerContact, BusinessInfo } from '../../../shared/models/admin/business-customer';

interface Option { id: number, name_ar: string, name_en: string }

@Component({
  selector: 'business-contacts',
  templateUrl: './business-contacts.component.html',
  styleUrls: ['./business-contacts.component.scss']
})
export class BusinessContactsComponent implements OnInit, OnChanges, OnDestroy {

  form: FormGroup;
  accept = 'image/*';
  selectedFile: File;
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";
  type: string = 'password';
  operation: string = '';
  businessAccountfromRoute: number = 0;
  @Input() businessAccountId: number;
  @Input() businessCustomer: BusinessInfo;
  @Input() disableForm: boolean = false;
  @Input() accountType: number = 1;
  @Output() onBusinessContactFile: EventEmitter<File> = new EventEmitter<File>();
  @Output() onContacts: EventEmitter<BusinessCustomerContact[]> = new EventEmitter<BusinessCustomerContact[]>();
  @Output() onEnableActions: EventEmitter<boolean> = new EventEmitter<boolean>();

  showHideForm: boolean = false;
  selectedMobileNumber: number;
  selectedCountryCode: Country;
  validPhoneNumber: boolean = false;

  displayedColumns: string[] = [
    'image',
    'id',
    'nameEmail',
    'mobile',
    'phone',
    'faxNo',
    'status',
    'actions'
  ];

  total: number = 0;
  page: number = 1;
  pageSize: number = 10;
  contacts: any[] = [];

  courtesies: Option[] = [];
  functions: Option[] = [];
  status: Option[] = [];

  locale: string;

  subscriptions = new Subscription();
  allContacts: any[] = [];
  baseUrl = App.backEndUrl;
  isEditMode: boolean = false;
  seletcedRow: BusinessCustomerContact;
  returnedMainContact: any;

  constructor(private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute) {
    this.form = this.fb.group({
      fullName: ['', Validators.required],
      fax: [''],
      phone: [''],
      mobileNumber: [''],
      accountStatusId: [''],
      isMainContact: [false],
      courtesyId: ['', Validators.required],
      email: [''],
      contactFunctionId: ['', Validators.required],
      contactCode: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8),
         Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)]],
      userName: ['', Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
    
    });

    this.subscriptions.add(this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    }));

    this.subscriptions.add(this.activatedRoute.queryParams.subscribe((qp: { businessCustomerId: number }) => {
      if (qp && qp.businessCustomerId) {
        this.businessAccountId = Number(qp.businessCustomerId);
        this.businessAccountfromRoute = Number(qp.businessCustomerId);

        this.page = 1;
        this.list();
        this.listPaginated(this.page);
      }
    }));
  }

  ngOnInit(): void {

    this.list();
    this.listPaginated(this.page);

    this.loadPreData();
    this.operation = this.translateService.instant('Save');
    console.log("businessAccountId", this.businessAccountId, this.form.value);
    console.log("businessCustomer", this.businessCustomer)
  }

  loadPreData() {

    // courtesies
    this.subscriptions.add(this.facadeService.sharedFacadeService.courtesyService.list().subscribe((res: Option[]) => {
      this.courtesies = res;
    }));

    // status
    this.subscriptions.add(this.facadeService.sharedFacadeService.accountStatusService.list().subscribe((res: Option[]) => {
      this.status = res;
    }));

    // functions
    this.subscriptions.add(this.facadeService.sharedFacadeService.contactFunctionsService.list().subscribe((res: Option[]) => {
      this.functions = res;
    }));
  }


  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      if (!this.selectedFile.type.match(this.accept)) {
        this.toastrService.error(this.translateService.instant('Invalid image format'));
        return;
      }

      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.path = event.target.result;
      }
    }
  }

  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  /**
   * update contact
   * 
   * 
   * @param value 
   */
  editContact(value: BusinessCustomerContact) {
       this.isEditMode = true;
    this.showHideForm = true;
    this.onEnableActions.emit(false);
    this.seletcedRow = value;
    this.seletcedRow['countryId'] = value['countryId'];


      this.businessCustomer;
    this.onContacts;
    this.form.patchValue(this.seletcedRow);
    this.form.get('userName').disable();
    this.form.get('userName').clearValidators();
    this.form.get('password').disable();
    this.form.get('password').clearValidators();
    if(this.businessCustomer.mainContactId==null)
    this.getInfoById(this.seletcedRow.id);
    if (this.businessCustomer.mainContactId == this.seletcedRow.id) {
      this.form.get('isMainContact').setValue(true);
    }
    if(this.returnedMainContact== this.seletcedRow.id)
    this.form.get('isMainContact').setValue(true);
    this.operation = this.translateService.instant('Edit');
      this.path = `${App.backEndUrl}/${value.personalPhotoURL}`;
      if (this.businessCustomer?.accountType == 2) {
          this.form.get('isMainContact').setValue(true);
          if (!value['courtesyId']) {
              this.form.get('courtesyId').setValue(this.courtesies[0].id);
          }
      }

  }

  getInfoById(id:any){
    this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerService.getById(this.businessCustomer.id).subscribe(res => {
      this.returnedMainContact=res.mainContactId;
      if(this.returnedMainContact== id)
      this.form.get('isMainContact').setValue(true);
       }));
  }

  manageContact() {
    
    this.showHideForm = true;   

    if (!this.isEditMode && this.form.get('password').invalid) {
      this.toastrService.error(this.translateService.instant('Password must be at least 8 characters long and must contain one lower case, one uppercase, one numeric and one special character'));
    }

    if (!this.validPhoneNumber) {
      this.toastrService.error(this.translateService.instant('Enter valid phone number'));
      return;
    }
    if (!this.isEditMode && this.form.get('userName').invalid) {
      this.toastrService.error(this.translateService.instant('please enter valid email'));
    }
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    let body = this.form.value;
    body.roleName = "businessCustomer";
    body.businessCustomerId = this.businessAccountId ? this.businessAccountId : this.businessAccountfromRoute;
    body.personalPhotoFile = this.selectedFile;

    body.mobileNumber = this.selectedMobileNumber ? this.selectedMobileNumber : body.mobileNumber;
    body.countryId = this.selectedCountryCode ? this.selectedCountryCode.id : body.countryId;
    body.accountType = this.accountType;
    body.email = body.userName;
    
    if (this.selectedCountryCode) {
      body.countryId = this.selectedCountryCode.id;
    }

    if (this.isEditMode) {
      delete body['userName'];
      delete body['password'];
      delete body['businessCustomerId'];

      const formToPost = { ... this.seletcedRow, ...body };
      this.facadeService.adminFacadeService.businessCustomerContactService.update(formToPost).subscribe(
        res => {
          this.page = 1;
          this.list();
          this.listPaginated(this.page);

          this.form.reset();
          this.isEditMode = false;
          this.showHideForm = false;
          this.enableAuthSection();
          this.selectedFile = null;
          this.selectedMobileNumber = null;

          this.onEnableActions.emit(true);
          this.operation = this.translateService.instant('Save');
          this.toastrService.success(this.translateService.instant(`Contact has been updated successfully`));
        });
    } else {
      
      body.accountStatusId = 2;
      this.facadeService.adminFacadeService.businessCustomerContactService.create(body).subscribe(res => {

        this.page = 1;
        this.list();
        this.listPaginated(this.page);

        this.form.reset();
        this.isEditMode = false;
        this.showHideForm = false;
        this.selectedFile = null;
        this.selectedMobileNumber = null;
        this.enableAuthSection();
        this.operation = this.translateService.instant('Save');
        this.onEnableActions.emit(true);
        this.toastrService.success(this.translateService.instant(`New contact has been added successfully`));
      });
    }

  }

  /**
   * on change number value
   * 
   * 
   * @param number 
   */
  number(number: number): void {
    this.selectedMobileNumber = number;
  }

  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  cancel(): void {
    this.form.reset();
    this.isEditMode = false;
    this.showHideForm = false;

    this.enableAuthSection();
    this.operation = this.translateService.instant('Save');
    this.onEnableActions.emit(true);
  }

  showForm(): void {
    this.showHideForm = true;
    this.onEnableActions.emit(false);
  }

  enableAuthSection() {
    this.form.get('userName').enable();
    this.form.get('password').enable();

    this.form.get('userName').setValidators([Validators.required]);
    this.form.get('password').setValidators([Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)]);
  }

  /**
   * 
   * @param simpleChanges 
   */
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.businessAccountId && simpleChanges.businessAccountId.currentValue) {

      this.businessAccountId = simpleChanges.businessAccountId.currentValue;
    }
  }

  /**
   * list contacts
   * 
   * 
   * @param page 
   */
  list(): void {

    if (this.businessAccountId || this.businessAccountfromRoute) {
      let businessCustomerId = this.businessAccountId ? this.businessAccountId : this.businessAccountfromRoute;
      this.facadeService.adminFacadeService.businessCustomerContactService.list(businessCustomerId).subscribe((res: any) => {
        this.allContacts = res;
        this.onContacts.emit(this.allContacts);

        if (this.allContacts.length == 0) {
          this.showHideForm = true;
        }
      });
    }
  }

  /**
   * list using pagination for business customers
   * 
   * 
   * @param page 
   */
  listPaginated(page: number): void {
    const body: { pageNumber: number, pageSize: number, id: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
      id: this.businessAccountId ? this.businessAccountId : this.businessAccountfromRoute
    };

    if (this.businessAccountId || this.businessAccountfromRoute) {
      this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerContactService.listByBusinssCustomerPagination(body).subscribe((res: any) => {
        this.contacts = res.result;
        this.total = res.totalCount;
        if (this.contacts.length == 0) {
          this.showHideForm = true;
          this.onEnableActions.emit(false);
        }
      }));
    }
  }

  /**
   * paginate
   * 
   * 
   * @param event 
   */
  onChangePage(event: { pageIndex: number, pageSize: number }) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.listPaginated(this.page);
  }

  onConfirm(event: boolean, contact: BusinessCustomerContact): void {
    if (event) {
      this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerContactService.delete(contact.id).subscribe(res => {
        const index = this.contacts.indexOf(contact);
        if (index >= 0) {
          this.contacts.splice(index, 1);
          this.listPaginated(this.page);
          this.toastrService.success(this.translateService.instant(`Contact has been deleted successfully`));
        }
      }));
    }
  }

  /**
   * Change main contact 
   * 
   * 
   */
  setMainContact(contactId: number): void {

    const body = { ... this.businessCustomer, mainContactId: contactId };

    this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
      this.toastrService.success(this.translateService.instant(`Main Contact has been updated`));
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

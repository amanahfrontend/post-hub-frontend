import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BusinessCustomerContact, BusinessInfo } from '../../../shared/models/admin/business-customer';
import { FacadeService } from '../../../services/facade.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'business-account-details',
  templateUrl: './business-account-details.component.html',
  styleUrls: ['./business-account-details.component.scss']
})
export class BusinessAccountDetailsComponent implements OnInit {
  basicInfoFile: File;
  basicInfoForm: FormGroup;
  businessAccountId: number;
  validateBasicInfoForm: boolean = false;
  editMode: boolean = false;
  businessCustomer: BusinessInfo;
  contacts: BusinessCustomerContact[] = [];

  /**
   * 
   * @param facadeService 
   * @param router 
   * @param activatedRoute 
   */
  constructor(
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,) {
    this.activatedRoute.queryParams.subscribe((qp: { businessCustomerId: number }) => {
      if (qp && qp.businessCustomerId) {
        this.facadeService.adminFacadeService.businessCustomerService.getBusinessCustomerDetails(qp.businessCustomerId).subscribe(businessCustomer => {
          this.businessCustomer = businessCustomer;
        });
      }
    });
  }

  ngOnInit(): void {
  }
}

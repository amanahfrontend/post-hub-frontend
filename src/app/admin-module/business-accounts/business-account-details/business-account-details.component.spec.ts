import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAccountDetailsComponent } from './business-account-details.component';

describe('BusinessAccountDetailsComponent', () => {
  let component: BusinessAccountDetailsComponent;
  let fixture: ComponentFixture<BusinessAccountDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessAccountDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAccountDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { App } from '../../core/app';
import { FacadeService } from '../../services/facade.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { BusinessInfo } from '.././../shared/models/admin/business-customer';
import { saveFile } from 'app/shared/helpers/download-link';
enum Tabs{
  approved = 0,
  registered = 1
}
@Component({
  selector: 'business-accounts',
  templateUrl: './business-accounts.component.html',
  styleUrls: ['./business-accounts.component.scss']
})
export class BusinessAccountsComponent implements OnInit {
 
  tabIndex : Tabs = Tabs.approved;
  totalapproved: number = 0;
  approvedPage: number = 1;
  approvedPageSize: number = 10;
  totalUnApproved: number = 0;
  unApprovedPage: number = 1;
  unApprovedPageSize: number = 10;
  subscriptions = new Subscription();
  searchBy: string = '';

  displayedColumns: string[] = [
    'nameCode',
    'HQAddress',
    'joinDate',
    'industry',
    'collection',
    'actions'
  ];

    approvedAccounts: any[] = [];
    unApprovedAccounts: any[] = [];

    accounts: any[] = [];

    baseUrl: string = App.backEndUrl;

  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param activatedRoute 
   * @param toastrService 
   * @param router 
   */
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
    private facadeService: FacadeService,
    private toastrService: ToastrService
  ) {
    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
        if (params && params.page) {
            this.approvedPage = params.page;
            this.unApprovedPage = params.page;

        }
    }));
  }

    ngOnInit(): void {
        this.listApprovedAccounts(this.approvedPage);
        this.listUnApprovedAccounts(this.unApprovedPage);
    }

  /**
   * 
   * @param page 
   */
  private replaceRoutePage(page: number): void {
    this.router.navigate(['business-accounts'], { queryParams: { page: page } });
  }

  addBusinessAccount(): void {
    this.router.navigate(['business-accounts/add']);
  }

  /**
   * edit current customer
   * 
   * 
   * @param id 
   */
  editBusinessCustomer(id: number): void {
    this.router.navigate(['customer-registration/edit'], { queryParams: { businessCustomerId: id } });
  }

  /**
   * current business customer details
   * 
   * 
   * @param id 
   */
  viewBusinessCustomer(id: number): void {
    this.router.navigate(['customer-registration/details'], { queryParams: { businessCustomerId: id } });
  }


  listApprovedAccounts(page: number): void {
    const body = {
      "pageNumber": page,
      "pageSize": this.approvedPageSize,
      "searchBy": this.searchBy,
      "isApproved":true
    };

      this.facadeService.adminFacadeService.businessCustomerService.listByPagination(body).subscribe((res: any) => {
          this.approvedAccounts = res.result;
        this.totalapproved = res.totalCount;
    });
  }

  listUnApprovedAccounts(page: number): void {
        const body = {
            "pageNumber": page,
            "pageSize": this.unApprovedPageSize,
            "searchBy": this.searchBy,
            "isApproved": false,
        };

      this.facadeService.adminFacadeService.businessCustomerService.listByPagination(body).subscribe((res: any) => {
          this.unApprovedAccounts = res.result;
          this.totalUnApproved = res.totalCount;
        });
    }


  /**
   * on change page or per page
   *
   *
   * @param event
   */
    onChangeApprovedPage(event: { pageIndex: number, pageSize: number }): void {
        this.approvedPage = event.pageIndex + 1;
        this.approvedPageSize = event.pageSize;
        this.listApprovedAccounts(this.approvedPage);
        this.replaceRoutePage(this.approvedPage);
  }

    onChangeUnApprovedPage(event: { pageIndex: number, pageSize: number }): void {
        this.unApprovedPage = event.pageIndex + 1;
        this.unApprovedPageSize = event.pageSize;
        this.listUnApprovedAccounts(this.approvedPage);
        this.replaceRoutePage(this.unApprovedPageSize);
    }

  /**
 * Delete staff
 * 
 * 
 * @param event 
 * @param manager 
 */
  onConfirm(event: boolean, account: BusinessInfo): void {
    if (event) {
      this.facadeService.adminFacadeService.manageStaffService.delete(account.id).subscribe(res => {
        const index = this.accounts.indexOf(account);
        if (index >= 0) {
          this.accounts.splice(index, 1);

            this.replaceRoutePage(this.approvedPage);
            this.listApprovedAccounts(this.unApprovedPage);
          this.toastrService.success(this.translateService.instant(`Business customer has been deleted successfully`));
        }
      });
    }
  }

  export() {
      const body = {
          pageNumber: this.approvedPage,
          pageSize: this.approvedPage,
          SearchBy: this.searchBy,
          isApproved:true,
    }

    this.facadeService.adminFacadeService.businessCustomerService.exportToExcel(body).subscribe(data => {
      saveFile(`${new Date().toLocaleDateString()}_BusinessCustomers.csv`, "data:attachment/text", data);
    });
  }

  activateAccount(id: number) {
    this.facadeService.accountService.activateAccount(id).subscribe(res => {
      this.toastrService.success(this.translateService.instant(`Individual customer account has been activated successfully`));
        this.listApprovedAccounts(1);
        this.listUnApprovedAccounts(1);

    });
    
  }
  approve(id: number){
    this.facadeService.accountService.appproveAccountAndSetContactsActive(id).subscribe(res => {
      this.toastrService.success(this.translateService.instant(`Customer account has been approved successfully`));
      
      this.tabIndex=0;
      this.setTab(Tabs.approved);
        this.listApprovedAccounts(1);
        this.listUnApprovedAccounts(1);
    });
    }
    setTab(tab : Tabs){
      this.tabIndex = tab; 
    }
}
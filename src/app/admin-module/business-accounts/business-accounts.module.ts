import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessAccountsRoutingModule } from './business-accounts-routing.module';
import { BusinessAccountsComponent } from './business-accounts.component';
import { AddBusinessAccountComponent } from './add-business-account/add-business-account.component';

// shared
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// components
import { BusinessInfoComponent } from './business-info/business-info.component';
import { BusinessContactsComponent } from './business-contacts/business-contacts.component';
import { BusinessBranchesComponent } from './business-branches/business-branches.component';
import { EditBusinessAccountComponent } from './edit-business-account/edit-business-account.component';
import { BusinessAccountDetailsComponent } from './business-account-details/business-account-details.component';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    BusinessAccountsComponent,
    AddBusinessAccountComponent,
    BusinessInfoComponent,
    BusinessContactsComponent,
    BusinessBranchesComponent,
    EditBusinessAccountComponent,
    BusinessAccountDetailsComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,

    // shared
    MaterialModule,
    AddressModule,
    HeaderAsCardModule,
    TelInputModule,
    ConfirmDeletionModule,
    SetDirModule,
    BusinessAccountsRoutingModule,
    PermissionsModule
  ], exports: [
    BusinessInfoComponent,
    BusinessContactsComponent,
    BusinessBranchesComponent
  ]
})
export class BusinessAccountsModule { }

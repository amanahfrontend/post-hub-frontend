import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AddBusinessAccountComponent } from './add-business-account/add-business-account.component';
import { BusinessAccountDetailsComponent } from './business-account-details/business-account-details.component';
import { BusinessAccountsComponent } from './business-accounts.component';
import { EditBusinessAccountComponent } from './edit-business-account/edit-business-account.component';

const routes: Routes = [
  {
    path: '',
    component: BusinessAccountsComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadBusinessCustomer'
      }
    },
  },
  {
    path: 'add',
    component: AddBusinessAccountComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'AddBusinessCustomer'
      }
    },
  },
  {
    path: 'edit',
    component: EditBusinessAccountComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'UpdateBusinessCustomer'
      }
    },
  },
  {
    path: 'details',
    component: BusinessAccountDetailsComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadBusinessCustomer'
      }
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessAccountsRoutingModule { }

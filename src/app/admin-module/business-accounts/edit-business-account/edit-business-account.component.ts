import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BusinessCustomerContact, BusinessInfo } from '../../../shared/models/admin/business-customer';
import { FacadeService } from '../../../services/facade.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'edit-business-account',
  templateUrl: './edit-business-account.component.html',
  styleUrls: ['./edit-business-account.component.scss']
})
export class EditBusinessAccountComponent implements OnInit {

  basicInfoFile: File;
  basicInfoForm: FormGroup;
  businessAccountId: number;
  validateBasicInfoForm: boolean = false;
  editMode: boolean = false;
  businessCustomer: BusinessInfo;
  subscriptions = new Subscription();
  contacts: BusinessCustomerContact[] = [];
  enableContactActions: boolean = true;
  enableBranchActions: boolean = true;


  /**
   * 
   * @param facadeService 
   * @param router 
   * @param activatedRoute 
   * @param toastrService 
   * @param translateService 
   */
  constructor(
    private facadeService: FacadeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private translateService: TranslateService) {
  }

  ngOnInit(): void {    
    this.loadPreviousCustomerBusinessData();
  }

  /**
   * previous customer data
   * 
   * 
   */
  loadPreviousCustomerBusinessData() {
    
    this.activatedRoute.queryParams.subscribe((qp: { businessCustomerId: number }) => {
      if (qp && qp.businessCustomerId) {
        this.facadeService.adminFacadeService.businessCustomerService.getBusinessCustomerDetails(qp.businessCustomerId).subscribe(businessCustomer => {
          this.businessCustomer = businessCustomer;
        });
      }
    });
  }

    onBasicInfoForm(form: FormGroup) {
    
    this.basicInfoForm = form;
  }

  /**
   * business file
   * 
   * 
   * @param file 
   */
  onBasicInfoFile(file: File) {
      this.basicInfoFile = file;

  }

  onEditMode(event: any) {
    this.editMode = event.isEditMode;
    this.businessCustomer = event.businessCustomer;
  }

  /**
   * edit first step (business inforamtion)
   * 
   * 
   */
  editBusinessInfo() {
    if (this.basicInfoForm.valid) {
      let body = { ... this.businessCustomer, ...this.basicInfoForm.value };

      if (this.basicInfoFile) {
        body.businessLogoFile = this.basicInfoFile;
        }
       
        body.isApproved = this.businessCustomer.isApproved;
        this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
        this.toastrService.success(this.translateService.instant(`Business information has been updated`));
      }));
    } else {
      this.validateBasicInfoForm = true;
      this.toastrService.success(this.translateService.instant(`Please fill the mandatory fields`));
    }
  }

  onEnableContactActions(enableActions: boolean) {
      this.enableContactActions = enableActions;
      this.loadPreviousCustomerBusinessData();


  }

  onEnableBranchActions(enableActions: boolean) {
      this.enableBranchActions = enableActions;
      this.loadPreviousCustomerBusinessData();

  }
    saveFromBranchs() {
       
        const body = { ... this.businessCustomer, ...this.basicInfoForm.value  ,...{ isCompleted: true } };

    this.facadeService.adminFacadeService.businessCustomerService.getBusinessCustomerDetails(this.businessCustomer.id).subscribe(businessCustomer => {
      this.businessCustomer = businessCustomer;
      if (this.businessCustomer.mainContactId == null) {
        this.toastrService.error(this.translateService.instant(`Please set main contact`));
      }
      else if (this.businessCustomer.hqBranchId == null) {
        this.toastrService.error(this.translateService.instant(`Please set HQ branch`));
      } else {
        this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
          
          this.toastrService.success(this.translateService.instant(`New Business Customer has been created, Check your email`));
          this.router.navigate(['business-accounts']);
        });
      }
    });
  }
}

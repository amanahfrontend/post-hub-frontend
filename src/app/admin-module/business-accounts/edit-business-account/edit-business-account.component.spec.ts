import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBusinessAccountComponent } from './edit-business-account.component';

describe('EditBusinessAccountComponent', () => {
  let component: EditBusinessAccountComponent;
  let fixture: ComponentFixture<EditBusinessAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditBusinessAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBusinessAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

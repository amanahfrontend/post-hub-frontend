import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessBranchesComponent } from './business-branches.component';

describe('BusinessBranchesComponent', () => {
  let component: BusinessBranchesComponent;
  let fixture: ComponentFixture<BusinessBranchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessBranchesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessBranchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

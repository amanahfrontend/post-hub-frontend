import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BusinessCustomerBranch } from '../../../shared/models/admin/business-customer';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../../services/facade.service';
import { Address, Country } from '../../../shared/models/components';
import { App } from '../../../core/app';
import { Subscription } from 'rxjs';

@Component({
  selector: 'business-branches',
  templateUrl: './business-branches.component.html',
  styleUrls: ['./business-branches.component.scss']
})
export class BusinessBranchesComponent implements OnInit, OnChanges {
  form: FormGroup;
  showHideForm: boolean = false;
  selectedMobileNumber: number;
  selectedCountryCode: Country;
  validPhoneNumber: boolean = false;

  branches: BusinessCustomerBranch[] = [];
  address: Address;

  @Input() businessCustomer: BusinessCustomerBranch;
  @Input() contacts: any[];
  @Input() disableForm: boolean = false;

  @Output() onBranches: EventEmitter<BusinessCustomerBranch[]> = new EventEmitter<BusinessCustomerBranch[]>();
  @Output() onEnableActions: EventEmitter<boolean> = new EventEmitter<boolean>();

  baseUrl = App.backEndUrl;
  isEditMode: boolean = false;
  seletcedRow: BusinessCustomerBranch;
  operation: string = '';
  subscriptions = new Subscription();

  displayedColumns: string[] = [
    'id',
    'name',
    'governorate',
    'actions'
  ];

  page: number = 1;
  pageSize: number = 10;
  total: any;
  returnedMainBranch: number;

  /**
   * 
   * @param fb 
   * @param facadeService 
   * @param toastrService 
   * @param translateService 
   */
  constructor(private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      branchContactId: ['', Validators.required],
      pickupContactId: ['', Validators.required],
      email: ['', Validators.required],
      faxNo: [''],
      phone: [''],
      closeTime: [''],
      ishqBranch: [false],
      preferedPickupTimeFrom: [''],
      preferedPickupTimeTo: [''],
      pickupNotes: [''],
      deliveryNotes: [''],
    });
  }

  ngOnInit(): void {
    this.operation = this.translateService.instant('Add');
    this.listContactsPerBusinessCustomer();
    this.listBranchesPerBusinessCustomer();
  }

  addBranch() {
    this.showHideForm = true;
    this.onEnableActions.emit(false);
  }

  cancel() {
    this.showHideForm = false;
    this.onEnableActions.emit(true);
  }

  /**
   * list contacts
   * 
   * 
   * @param page 
   */
  listContactsPerBusinessCustomer(): void {
    if (this.businessCustomer && this.businessCustomer.id) {
      this.facadeService.adminFacadeService.businessCustomerContactService.list(this.businessCustomer.id).subscribe((res: any) => {
        this.contacts = res;
      });
    }
  }

  /**
   * delete branch
   * 
   * 
   * @param event 
   * @param branch 
   */
  onConfirm(event: boolean, branch: any): void {
    if (event) {
      this.facadeService.adminFacadeService.businessCustomerBranchService.delete(branch.id).subscribe(res => {
        const index = this.branches.indexOf(branch);
        if (index >= 0) {
          this.branches.splice(index, 1);

          this.listBranchesPerBusinessCustomer();
          this.toastrService.success(this.translateService.instant(`Branch has been deleted successfully`));
        }
      });
    }
  }

  listBranchesPerBusinessCustomer(): void {
    if (this.businessCustomer) {
      this.facadeService.adminFacadeService.businessCustomerBranchService.list(this.businessCustomer.id).subscribe((res: any) => {
        this.branches = res;      
        this.total = this.branches.length;
       
        this.onBranches.emit(this.branches);

        if (this.branches.length == 0) {
          this.showHideForm = true;
          this.onEnableActions.emit(false);
        }
      });
    }
  }

  /**
   * 
   * @param simpleChanges 
   */
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.businessCustomer && simpleChanges.businessCustomer.currentValue) {
      this.businessCustomer = simpleChanges.businessCustomer.currentValue;
      this.listBranchesPerBusinessCustomer();
    }
  }

  setMainBranch(branchId: number): void {
    const body: any = { ... this.businessCustomer, hqBranchId: branchId };

    this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
      this.toastrService.success(this.translateService.instant(`Main Branch has been updated`));
    });
  }

  manageBranch() {
    
    this.showHideForm = true;
    this.onEnableActions.emit(false);

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    if (!this.validPhoneNumber) {
      this.toastrService.error(this.translateService.instant('Enter valid phone number'));
      return;
    }

    let body = this.form.value;

     
    body.location = this.address;
    body.mobile = this.selectedMobileNumber ? this.selectedMobileNumber : body.mobile;
    body.countryId = this.selectedCountryCode ? this.selectedCountryCode.id : body.countryId;
    body.businessCustomerId = this.businessCustomer.id;
      if (!body.ishqBranch) {
          body.ishqBranch = false;
      }
    if (this.isEditMode) {
      const bodyToPost = { ... this.seletcedRow, ...body };
        if (!this.address) {
            bodyToPost.location = this.seletcedRow.location;
        }
        this.facadeService.adminFacadeService.businessCustomerBranchService.update(bodyToPost).subscribe(res => {
        this.listBranchesPerBusinessCustomer();

        this.showHideForm = false;
        this.onEnableActions.emit(true);
        this.form.reset();
        this.toastrService.success(this.translateService.instant(`Branch has been updated successfully`));
      });
    } else {
      this.facadeService.adminFacadeService.businessCustomerBranchService.create(body).subscribe(res => {
        this.listBranchesPerBusinessCustomer();

        this.showHideForm = false;
        this.onEnableActions.emit(true);
        this.form.reset();
        this.toastrService.success(this.translateService.instant(`New branch has been added successfully`));
      });
    }
  }

  /**
   * on change number value
   * 
   * 
   * @param number 
   */
  number(number: number): void {
    this.selectedMobileNumber = number;
  }

  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  /**
 * update contact
 * 
 * 
 * @param value 
 */
  editBranch(value: BusinessCustomerBranch) {
    
    this.isEditMode = true;
    this.showHideForm = true;
    this.validPhoneNumber = true;
    this.onEnableActions.emit(false);
    this.seletcedRow = value;
    this.form.patchValue(this.seletcedRow);
    if(this.businessCustomer.hqBranchId==null)
    this.getInfoById(this.seletcedRow.id);
    if(this.businessCustomer.hqBranchId==this.seletcedRow.id){
      this.form.get('ishqBranch').setValue(true);
   }
   if(this.returnedMainBranch== this.seletcedRow.id)
      this.form.get('ishqBranch').setValue(true);

    this.operation = this.translateService.instant('Edit');
  }
  getInfoById(id:any){
    this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerService.getById(this.businessCustomer.id).subscribe(res => {
      this.returnedMainBranch=res.hqBranchId;
      if(this.returnedMainBranch== id)
      this.form.get('ishqBranch').setValue(true);
       }));
  }

  onAddressChanges(address: Address) {
    this.address = address;
  }

  onChangePage(event: { pageIndex: number, pageSize: number }) {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;

  }
 

}



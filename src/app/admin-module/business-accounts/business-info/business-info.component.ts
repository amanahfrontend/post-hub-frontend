import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FacadeService } from '../../../services/facade.service';
import { App } from '../../../core/app';
import { BusinessInfo } from '../../../shared/models/admin/business-customer';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

interface EditMode {
  isEditMode: boolean,
  businessCustomer: BusinessInfo
}

@Component({
  selector: 'business-info',
  templateUrl: './business-info.component.html',
  styleUrls: ['./business-info.component.scss']
})
export class BusinessInfoComponent implements OnInit, OnChanges {

  form: FormGroup;
  accept = 'image/*';
  selectedFile: File;
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";
  locale: string = '';

  collections: { id: number, name_ar: string, name_en: string }[] = [];
  businessTypes: { id: number, name_ar: string, name_en: string }[] = [];

  @Input() validateBasicInfoForm: boolean = false;
  @Input() businessCustomer: BusinessInfo;
  @Input() codeEditable: boolean = false;
  @Input() disableForm: boolean = false;
  @Input() enableLogoEdit: boolean = true;

  @Output() onBasicInfoForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  @Output() onBasicInfoFile: EventEmitter<File> = new EventEmitter<File>();
  @Output() onEditMode: EventEmitter<EditMode> = new EventEmitter<EditMode>();


  constructor(
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService) {
    this.form = this.fb.group({
      businessName: ['', Validators.required],
      businessCID: ['', Validators.required],
      businessCode: [''],
      collectionMethodId: ['', Validators.required],
      businessTypeId: ['', Validators.required],
    });

    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    });
  }

  ngOnInit(): void {
    this.facadeService.sharedFacadeService.collectionMethodsService.list().subscribe((collections: { id: number, name_ar: string, name_en: string }[]) => {
      this.collections = collections;
    });

    this.facadeService.sharedFacadeService.businessTypesService.list().subscribe((businessTypes: { id: number, name_ar: string, name_en: string }[]) => {
      this.businessTypes = businessTypes;
    });

    this.onBasicInfoForm.emit(this.form);

      
      if (this.businessCustomer) {
        this.path = `${App.backEndUrl}/${this.businessCustomer.businessLogoUrl}`
      this.form.patchValue(this.businessCustomer);
      this.onBasicInfoForm.emit(this.form);
      this.onEditMode.emit({ isEditMode: true, businessCustomer: this.businessCustomer });
      this.codeEditable = true;
      this.form.get('businessCode').disable();

       
    }

    this.form.valueChanges.subscribe(res => {
      this.onBasicInfoForm.emit(this.form);
    });

    if (this.disableForm) {
      this.form.disable();
    } else {
      this.form.enable();
    }
    if (this.codeEditable) {
      this.form.get('businessCode').enable();
    } else {
      this.form.get('businessCode').disable();
      }
      
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      if (!this.selectedFile.type.match(this.accept)) {
        this.toastrService.error(this.translateService.instant('Invalid image format'));
        return;
      }


      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.path = event.target.result;
      }

      this.onBasicInfoFile.emit(this.selectedFile);
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.validateBasicInfoForm) {
      if (simpleChanges.validateBasicInfoForm.currentValue) {
        this.form.markAllAsTouched();
      }
    }

    if (simpleChanges && simpleChanges.businessCustomer) {
      if (simpleChanges.businessCustomer.currentValue) {
        this.businessCustomer = simpleChanges.businessCustomer.currentValue;

        if (this.businessCustomer?.accountType == 2) {
              this.form.get('businessTypeId').clearValidators();
              this.form.get('businessTypeId').setValue(13);
        }
        this.form.patchValue(this.businessCustomer);
        this.onBasicInfoForm.emit(this.form);
        this.path = `${App.backEndUrl}/${this.businessCustomer.businessLogoUrl}`;
      }
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessAccountComponent } from './add-business-account.component';

describe('AddBusinessAccountComponent', () => {
  let component: AddBusinessAccountComponent;
  let fixture: ComponentFixture<AddBusinessAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBusinessAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BusinessCustomerBranch, BusinessCustomerContact, BusinessInfo } from '../../../shared/models/admin/business-customer';
import { FacadeService } from '../../../services/facade.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatStepper } from '@angular/material/stepper';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'add-business-account',
  templateUrl: './add-business-account.component.html',
  styleUrls: ['./add-business-account.component.scss'],
})
export class AddBusinessAccountComponent implements OnInit {

  basicInfoFile: File;
  basicInfoForm: FormGroup;
  businessAccountId: number;
  validateBasicInfoForm: boolean = false;
  editMode: boolean = false;
  businessCustomer: BusinessInfo;
  subscriptions = new Subscription();
  contacts: BusinessCustomerContact[] = [];
  businessCustomerBranches: BusinessCustomerBranch[] = [];
  enableContactActions: boolean = true;
  enableBranchActions: boolean = true;

  /**
   * 
   * @param facadeService 
   * @param router 
   * @param activatedRoute 
   */
  constructor(
    private facadeService: FacadeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private translateService: TranslateService,
  ) {
    this.activatedRoute.queryParams.subscribe((qp: { businessCustomerId: number }) => {
      if (qp && qp.businessCustomerId) {
        this.facadeService.adminFacadeService.businessCustomerService.getBusinessCustomerDetails(qp.businessCustomerId).subscribe(businessCustomer => {
          this.businessCustomer = businessCustomer;
          this.editMode = true;
        });
      }
    });
  }

  ngOnInit(): void {
  }

  onBasicInfoForm(form: FormGroup) {
    this.basicInfoForm = form;
  }

  /**
   * business file
   * 
   * 
   * @param file 
   */
  onBasicInfoFile(file: File) {
    this.basicInfoFile = file;
  }

  onEditMode(event: any) {
    this.editMode = event.isEditMode;
    this.businessCustomer = event.businessCustomer;
  }

  createBusinessInfo(stepper: MatStepper) {
    if (this.basicInfoForm.valid) {
      if (this.editMode) {
        let body = { ... this.businessCustomer, ...this.basicInfoForm.value };

        if (this.basicInfoFile || (this.businessCustomer && this.businessCustomer.businessLogoUrl)) {
          body.businessLogoFile = this.basicInfoFile;
        } else {
          return this.toastrService.error(this.translateService.instant(`Select logo`));
        }

        this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
          stepper.next();
        }));
      } else {
        let body = this.basicInfoForm.value;

        if (this.basicInfoFile || (this.businessCustomer && this.businessCustomer.businessLogoUrl)) {
          body.businessLogoFile = this.basicInfoFile;
        } else {
          return this.toastrService.error(this.translateService.instant(`Select logo`));
        }
        body.IsApproved = true;
        this.subscriptions.add(this.facadeService.adminFacadeService.businessCustomerService.create(body).subscribe((res: BusinessInfo) => {
          this.businessAccountId = res.id;
          this.replaceRouteParam(this.businessAccountId);
          stepper.next();
        }));
      }
    } else {
      this.validateBasicInfoForm = true;
      this.toastrService.success(this.translateService.instant(`Please fill the mandatory fields`));
    }
  }

  private replaceRouteParam(id: number) {
    this.router.navigate(['business-accounts/add'], { queryParams: { businessCustomerId: id } });
  }

  reset(stepper: MatStepper) {
    stepper.reset();
    this.basicInfoForm.reset();
    this.router.navigate(['business-accounts/add']);
  }

  save() {
    this.businessCustomer.isApproved=true;
    const body = { ... this.businessCustomer, ...{ isCompleted: true } };

    this.facadeService.adminFacadeService.businessCustomerService.getBusinessCustomerDetails(this.businessCustomer.id).subscribe(businessCustomer => {
      this.businessCustomer = businessCustomer;

      if (this.businessCustomer.mainContactId == null) {
        this.toastrService.error(this.translateService.instant(`Please set main contact`));
      } else if (this.businessCustomer.hqBranchId == null) {
        this.toastrService.error(this.translateService.instant(`Please set HQ branch`));
      } else {
        this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
          this.router.navigate(['business-accounts']);
          this.toastrService.success(this.translateService.instant(`New Business Customer has been created`));
        });
      }
    });
  }

  onBranches(branches: BusinessCustomerBranch[]) {
    this.businessCustomerBranches = branches;
  }

  onEnableContactActions(enableActions: boolean) {
    this.enableContactActions = enableActions;
  }

  onEnableBranchActions(enableActions: boolean) {
    this.enableBranchActions = enableActions;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCompanyLogoComponent } from './manage-company-logo.component';

describe('ManageCompanyLogoComponent', () => {
  let component: ManageCompanyLogoComponent;
  let fixture: ComponentFixture<ManageCompanyLogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCompanyLogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCompanyLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

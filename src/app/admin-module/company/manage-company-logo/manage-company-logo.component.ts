
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { App } from '../../../core/app';
import { Company } from '../../../shared/models/admin/company';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'manage-company-logo',
  templateUrl: './manage-company-logo.component.html',
  styleUrls: ['./manage-company-logo.component.scss']
})
export class ManageCompanyLogoComponent implements OnInit {

  accept = 'image/*';
  selectedFile: File;
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";

  form: FormGroup;
  subscription = new Subscription();
  valuesChanged: boolean = false;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { companyProfile: Company },
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private dialogRef: MatDialogRef<ManageCompanyLogoComponent>,

  ) {
    this.form = this.fb.group({
      missionSlogan: [''],
    });

    this.form.valueChanges.subscribe(changes => {
      if (this.form.touched) {
        this.valuesChanged = true;
      }
    });
  }

  ngOnInit(): void {
    this.form.get('missionSlogan').setValue(this.data.companyProfile.missionSlogan);
    this.path = `${App.backEndUrl}/${this.data.companyProfile.logoUrl}`;
  }

  onSelectFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      if (!this.selectedFile.type.match(this.accept)) {
        this.toastrService.error(this.translateService.instant('Invalid image format'));
        return;
      }

      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.valuesChanged = true;
        this.path = event.target.result;
      }
    }
  }

  submit() {
    const body = this.data.companyProfile;
    body.missionSlogan = this.form.get('missionSlogan').value;
    body.LogoFile = this.selectedFile;

    this.subscription.add(this.facadeService.adminFacadeService.companyService.update(body).subscribe(res => {
      this.dialogRef.close(true);
      this.toastrService.success(this.translateService.instant('Company profile Updated Successfully'));
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

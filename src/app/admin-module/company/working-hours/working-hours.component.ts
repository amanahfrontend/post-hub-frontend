import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '../../../services/facade.service';
import { ToastrService } from 'ngx-toastr';
import { Company, WorkingHour } from '../../../shared/models/admin/company';
import { Subscription } from 'rxjs';
declare var $: any;

const ELEMENT_DATA_WORKING_HOURS = [
  {
    day: 'Monday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Tuesday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Wednesday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Thursday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Friday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Saturday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0, required: false,
  },
  {
    day: 'Sunday', from: null, to: null, secondFrom: null, secondTo: null, disabled: true, companyId: 0,
  },
]

const TIME_FORMAT: string = 'hh:mm';

@Component({
  selector: 'working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.scss'],
})
export class WorkingHoursComponent implements OnInit, AfterViewInit {

  displayedColumnsForWorkingHours: string[] = ['select', 'day', 'from', 'to', 'secondFrom', 'secondTo'];
  dataSourceForWorkingHours: any = new MatTableDataSource();

  selection = new SelectionModel<any>(true, []);
  format: string = TIME_FORMAT;
  interval: number = 60;
  subscription = new Subscription();

  constructor(public dialogRef: MatDialogRef<WorkingHoursComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { companyProfile: Company },
    private translateService: TranslateService,
    private toastrService: ToastrService,
    private facadeService: FacadeService,
  ) { }

  ngOnInit(): void {
    if (this.data.companyProfile.workingHours.length == 0) {
      this.dataSourceForWorkingHours.data = ELEMENT_DATA_WORKING_HOURS;
    } else {
      this.dataSourceForWorkingHours.data = this.data.companyProfile.workingHours;
    }

    this.dataSourceForWorkingHours.data.forEach(element => {
      element.disabled = true;
      element.required = false;
    });
  }

  ngAfterViewInit() {
    this.initDatePicker();
  }

  initDatePicker() {
    $(document).ready(() => {
      $('.datetimepicker').datetimepicker({
        onChangeDateTime: (dp: Date, inputs: any) => {

          const index: string = inputs[0]['id'].split('_')[1];
          const type: string = inputs[0].dataset.type;
          const date = this.timeFromDate(dp);

          switch (type) {
            case 'from':
              if (date >= this.dataSourceForWorkingHours.data[index].to) {
                this.dataSourceForWorkingHours.data[index].from = null;
                this.toastrService.error(this.translateService.instant('First shift (from) value is later than First shift (to) value'));
              } else {
                this.dataSourceForWorkingHours.data[index].from = this.timeFromDate(dp);
              }
              break;

            case 'to':
              if (date <= this.dataSourceForWorkingHours.data[index].from) {
                this.dataSourceForWorkingHours.data[index].to = null;
                this.toastrService.error(this.translateService.instant('First shift (to) value is less than First shift (from) value'));
              } else {
                this.dataSourceForWorkingHours.data[index].to = this.timeFromDate(dp);
              }
              break;

            case 'secondFrom':
              if (date >= this.dataSourceForWorkingHours.data[index].secondTo) {
                this.dataSourceForWorkingHours.data[index].secondFrom = null;
                this.toastrService.error(this.translateService.instant('Second shift (from) value is later than Second shift (to) value'));
              } else {
                this.dataSourceForWorkingHours.data[index].secondFrom = this.timeFromDate(dp);
              }
              break;

            case 'secondTo':
              if (date >= this.dataSourceForWorkingHours.data[index].secondFrom) {
                this.dataSourceForWorkingHours.data[index].secondTo = null;
                this.toastrService.error(this.translateService.instant('Second shift (to) value is less than Second shift (from) value'));
              } else {
                this.dataSourceForWorkingHours.data[index].secondTo = this.timeFromDate(dp);
              }
              break;
            default:
              break;
          }
        },
        validateOnBlur: false,
        datepicker: false,
        format: 'H:i',
      });
    });
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceForWorkingHours.data.length;
    return numSelected === numRows;
  }

  masterToggle(event: MatCheckboxChange): void {
    if (!event.checked) {
      this.selection.clear();
      this.dataSourceForWorkingHours.data.forEach(element => {
        element.disabled = true;
        element.required = false;
      });
    } else {
      this.selection.select(...this.dataSourceForWorkingHours.data);

      this.dataSourceForWorkingHours.data.forEach(element => {
        element.disabled = false;
        element.required = true;
      });
    }
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onSelectRow(row: MatCheckboxChange, index: number): void {
    this.dataSourceForWorkingHours.data.forEach((rowTofind, indexToFind) => {
      if (indexToFind == index) {
        rowTofind.disabled = !row.checked;
        rowTofind.required = row.checked;
      }
    });

    if (row) {
      return this.selection.toggle(row);
    }

    return null;
  }

  sumbit() {
    let workingHours: any[] = [... this.dataSourceForWorkingHours.data];
    workingHours.forEach((value: WorkingHour) => {
      value.companyId = this.data.companyProfile.id;
    });

    let body = { ... this.data.companyProfile };
    body.workingHours = [...workingHours];

    delete body['disabled'];
    delete body['required'];

    this.subscription.add(this.facadeService.adminFacadeService.companyService.update(body).subscribe(res => {
      this.dialogRef.close(true);
      this.toastrService.success(this.translateService.instant('Company Working hours Updated Successfully'));
    }));
  }

  /**
   * time form date
   * 
   * 
   * @param date 
   * @returns 
   */
  timeFromDate(date: Date): string {
    const currentDate = new Date(date);
    return `${currentDate.getHours() > 9 ? currentDate.getHours() : '0' + currentDate.getHours()}:${currentDate.getMinutes() > 9 ? currentDate.getMinutes() : '0' + currentDate.getMinutes()}:00`;
  }
}

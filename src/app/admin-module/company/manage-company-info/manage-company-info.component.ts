import { Component, OnInit, Inject, AfterViewInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Country } from '../../../shared/models/components/country';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Company } from '../../../shared/models/admin/company';
import { FacadeService } from '../../../services/facade.service';
import { Subscription } from 'rxjs';
import { Branch } from '../../../shared/models/admin/branch';

@Component({
  selector: 'manage-company-info',
  templateUrl: './manage-company-info.component.html',
  styleUrls: ['./manage-company-info.component.scss']
})
export class ManageCompanyInfoComponent implements OnInit, AfterViewInit, OnDestroy {

  form: FormGroup;

  validPhoneNumber: boolean = false;
  selectedCountryCode: Country;
  urlRegEx = '[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)';
  companyProfile: Company;
  selectedMobileNumber: number;
  subscription = new Subscription();
  branchs: Branch[] = [];
  valuesChanged: boolean = false;
  submitted: boolean = false;

  /**
   * 
   * @param fb 
   * @param toastrService 
   * @param translateService 
   * @param facadeService 
   * @param dialogRef 
   * @param data 
   */
  constructor(private fb: FormBuilder,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private facadeService: FacadeService,
    private dialogRef: MatDialogRef<ManageCompanyInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { companyProfile: Company }) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      bussinesDiscription: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      phone: ['', Validators.compose([Validators.required, Validators.pattern(/^(\+965[569]\d{7})$/g)])],
      faxNo: [''],
      email: ['', [Validators.required, Validators.email]],
      website: [''],
      address: ['', [Validators.required]],
      mainBranchId: ['', [Validators.required]],
      socialLinks: this.fb.array([]),
    });

    if (this.data.companyProfile.socialLinks && this.data.companyProfile.socialLinks.length == 0) {
      this.addLink();
    }

    this.form.valueChanges.subscribe(changes => {
      if (this.form.touched) {
        this.valuesChanged = true;
      }
    });
  }

  ngOnInit(): void {
    this.companyProfile = this.data.companyProfile;
    this.form.patchValue(this.companyProfile);

    // branches
    this.facadeService.adminFacadeService.branchService.list().subscribe((res: any) => {
      this.branchs = res;
    });
  }

  ngAfterViewInit() {
    this.companyProfile.socialLinks.forEach(link => {
      this.addLink(link.siteLink);
    });
  }

  sumbit() {
    this.submitted = true;

    if (this.form.get('phone').invalid) {
      return this.toastrService.error(this.translateService.instant('This value is too short. It should have 8 numbers or more, and must contain +965XXXXXXXX'));
    }

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    if (!this.validPhoneNumber) {
      return this.toastrService.error(this.translateService.instant('Enter valid phone number'));
    }

    let body: Company = { ... this.data.companyProfile, ...this.form.value };
    body.latitude = 0;
    body.longitude = 0;

    body.countryId = this.selectedCountryCode ? this.selectedCountryCode.id : body.countryId;
    body.mobile = this.selectedMobileNumber ? this.selectedMobileNumber : body.mobile;

    this.subscription.add(this.facadeService.adminFacadeService.companyService.update(body).subscribe(res => {
      this.submitted = false;
      this.dialogRef.close(true);
      this.toastrService.success(this.translateService.instant('Company profile Updated Successfully'));
    }));
  }

  /**
   * valid phone number or not 
   * 
   * 
   * 
   * @param valid 
   */
  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  /**
   * select country 
   * 
   * 
   * 
   * @param country 
   */
  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

  newLink(value?: string): FormGroup {
    return this.fb.group({
      siteLink: [value ? value : '']
    })
  }

  addLink(value?: string) {
    this.socialLinks.push(this.newLink(value));
  }

  removeLink(index: number) {
    this.socialLinks.removeAt(index);
  }

  /**
 * on type number 
 * 
 * 
 * @param number 
 */
  number(number: number): void {
    this.valuesChanged = true;
    this.selectedMobileNumber = number;
  }

  get socialLinks(): FormArray {
    return this.form.get("socialLinks") as FormArray
  }

  handleAddressChange(event) {
    console.log(event);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

import { AfterViewInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { ManageLocationComponent } from './manage-location/manage-location.component';
import { FacadeService } from '../../services/facade.service';
import { ManageCompanyLogoComponent } from './manage-company-logo/manage-company-logo.component';
import { ManageCompanyInfoComponent } from './manage-company-info/manage-company-info.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Company } from '../../shared/models/admin/company';
import { App } from 'app/core/app';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Branch } from '../../shared/models/admin/branch';
import { Subscription } from 'rxjs';
import { WorkingHoursComponent } from './working-hours/working-hours.component';
import { saveFile } from 'app/shared/helpers/download-link';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, AfterViewInit, OnDestroy {
  idForDelete: any;
  checkAll = false
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  masterCheck = false
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;

  displayedColumns: string[] = ['#', 'phoneNo', 'mobileNo', 'name', 'branchManager', 'paci', 'Governorate', 'Area', 'Block', 'Street', 'BuildingNo', 'floorNo','flat', 'map', 'actions'];
  dataSource: any[] = [];

  branches: Branch[] = [];
  form: FormGroup;
  companyProfile: Company;
  logo: string = 'assets/img/express_logo.jpeg';

  page: number = 1;
  pageSize: number = 10;
  total: number = 0;
  subscription = new Subscription();
  searchBy: string = '';

  /**
   * 
   * @param translateService 
   * @param dialog 
   * @param facadeService 
   * @param toastrService 
   * @param fb 
   * @param router 
   */
  constructor(
    private translateService: TranslateService,
    private dialog: MatDialog,
    private facadeService: FacadeService,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    private permissionsService: NgxPermissionsService) {
    this.form = this.fb.group({
      name: [''],
      bussinesDiscription: [''],
      mobile: [''],
      phone: [''],
      faxNo: [''],
      email: [''],
      website: [''],
      address: [''],
      socialMediaLinks: [''],
    });
  }

  ngOnInit(): void {
    this.form.disable();
    this.companyDetails(true);
  }

  /**
   * company profile
   * 
   * 
   */
  companyDetails(withBranches: boolean): void {
    this.subscription.add(this.facadeService.adminFacadeService.companyService.currentCompanyProfile.subscribe(res => {
      this.companyProfile = res;
      this.form.patchValue(this.companyProfile);
      this.logo = `${App.backEndUrl}/${this.companyProfile.logoUrl}`;

      if (withBranches) {
        this.listBranches(this.page);
      }
    }));
  }

  /**
   * add / edit location
   * 
   * 
   * @param opertaion 
   * @param isEditMode 
   * @param branch 
   */
  manageLocation(opertaion: string, isEditMode: boolean, branch?: Branch) {
    const type: string = opertaion == 'Add' ? this.translateService.instant('Add') : this.translateService.instant('Edit');
    const dialog = this.dialog.open(ManageLocationComponent, {
      width: '60%',
      data: {
        operation: this.translateService.instant(type),
        isEditMode: isEditMode,
        branch: branch ? branch : null,
        companyProfile: this.companyProfile,
      },
      panelClass: 'custom-dialog-container',
      height: '95%',
      maxHeight: '100%',
    });

    dialog.afterClosed().subscribe(res => {
      if (res.operation == 'Add') {
        this.page = 1;
      }

      this.listBranches(this.page);
    });
  }

  /**
   * update company logo data
   * 
   * 
   * @param opertaion 
   */
  async manageCompanyLogo() {
    const permissionExist = await this.permissionsService.hasPermission('UpdateLogo');

    if (permissionExist) {
      const dialog = this.dialog.open(ManageCompanyLogoComponent, {
        width: '60%',
        panelClass: 'custom-dialog-container',
        height: 'calc(75vh - 90px)',
        data: {
          companyProfile: this.companyProfile
        }
      });

      dialog.disableClose = true;

      dialog.afterClosed().subscribe(res => {
        if (res) {
          this.companyDetails(false);
        }
      });
    }
  }

  /**
   * edit company info
   * 
   * 
   */
  manageCompanyInfo() {
    const dialog = this.dialog.open(ManageCompanyInfoComponent, {
      width: '60%',
      panelClass: 'custom-dialog-container',
      height: 'calc(90vh - 90px)',
      data: {
        companyProfile: this.companyProfile
      }
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.companyDetails(false);
      }
    });
  }

  /**
 * edit company info
 * 
 * 
 */
  manageWorkingHours() {
    const dialog = this.dialog.open(WorkingHoursComponent, {
      width: '65%',
      panelClass: 'custom-dialog-container',
      height: '90%',
      data: {
        companyProfile: this.companyProfile
      }
    });

    dialog.disableClose = true;

    dialog.afterClosed().subscribe(res => {
      if (res) {
        this.companyDetails(false);
      }
    });
  }

  ngAfterViewInit(): void {
  }

  /**
   * company branches
   * 
   * 
   * @param page 
   */
  listBranches(page: number) {
    const body: { pageNumber: number, pageSize: number, companyId: number } = {
      pageNumber: page,
      pageSize: this.pageSize,
      companyId: this.companyProfile.id,
    };

    this.subscription.add(this.facadeService.adminFacadeService.branchService.listByPagination(body).subscribe((res: any) => {
      if (res && res.result) {
        this.branches = res.result;
        this.total = res.totalCount;
      }
    }));
  }

  /**
   * on change page or per page
   *
   *
   * @param event
   */
  onChangePage(event: { pageIndex: number, pageSize: number }): void {
    this.page = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.listBranches(this.page);
    this.replaceRoutePage(this.page);
  }

  /**
  * change route param with current page number
  * 
  * 
  * @param page 
  */
  private replaceRoutePage(page: number): void {
    this.router.navigate(['company'], { queryParams: { page: page } });
  }

  /**
   * Delete branch
   * 
   * 
   * @param event 
   * @param branch 
   */
  onConfirm(event: boolean, branch: any): void {
    if (event) {
      this.subscription.add(this.facadeService.adminFacadeService.branchService.delete(branch.id).subscribe(res => {
        const index = this.branches.indexOf(branch);
        if (index >= 0) {
          this.branches.splice(index, 1);
          this.replaceRoutePage(this.page);
          this.listBranches(this.page);
          this.toastrService.success(this.translateService.instant(`Branch has been deleted successfully`));
        }
      }));
    }
  }

  export() {
    const body = {
      companyId: this.companyProfile.id
    }

    this.facadeService.adminFacadeService.branchService.exportToExcel(body).subscribe(data => {
      saveFile(`Branches${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }

  search(): void {
    const body: { pageNumber: number, pageSize: number, searchBy: string } = {
      pageNumber: 1,
      pageSize: 10,
      searchBy: this.searchBy,
    };

    this.facadeService.adminFacadeService.branchService.listByPagination(body).subscribe((res: any) => {
      this.branches = res.result;
      this.total = res.totalCount;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

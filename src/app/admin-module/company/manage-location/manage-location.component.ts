import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Country } from '../../../shared/models/components/country';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../../services/facade.service';
import { Manager } from '../../../shared/models/admin/manager';
import { Branch } from '../../../shared/models/admin/branch';
import { Address } from '../../../shared/models/components';
import { Company } from '../../../shared/models/admin/company';

@Component({
  selector: 'manage-location',
  templateUrl: './manage-location.component.html',
  styleUrls: ['./manage-location.component.scss']
})
export class ManageLocationComponent implements OnInit {

  isEditMode: boolean = false;
  operation: string;
  form: FormGroup;
  validPhoneNumber: boolean = false;
  managers: Manager[] = [];
  selectedMobileNumber: number;
  selectedCountryCode: Country;
  selectedManager: Manager;

  branch: Branch;
  address: Address;
  companyProfile: Company;

  constructor(
    public dialogRef: MatDialogRef<ManageLocationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private toastrService: ToastrService) {
    this.operation = this.data.operation;
    this.isEditMode = this.data.isEditMode;

    this.form = this.fb.group({
      branchManagerId: ['', [Validators.required]],
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      code: ['', [Validators.required]],
      email: ['', [Validators.required]],
      fax: ['', [Validators.required]],
    });

    if (this.isEditMode) {
      this.branch = this.data.branch;
      this.form.patchValue(this.branch);
    }

    this.companyProfile = this.data.companyProfile;
  }

  ngOnInit(): void {
    this.facadeService.adminFacadeService.manageStaffService.list().subscribe((res: any) => {
      this.managers = res;
    });
  }

  submit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    if (!this.isValidPhoneNumber) {
      return this.toastrService.error(this.translateService.instant('Enter valid phone number'));
    }

    
    let body = { ... this.form.value };
   // , ...this.branch
   if (this.isEditMode){

    body.address=this.branch.address?this.branch.address:"";
   // body. companyId=this.branch.companyId
    body.countryId=this.branch.countryId?this.branch.countryId:null;
    body.createdDate=this.branch.createdDate;
    body. geoFenceId=this.branch.geoFenceId?this.branch.geoFenceId:null;
    body. id=this.branch.id;
    body.isActive=this.branch.isActive;
    body.isActivePrevious=this.branch.isActivePrevious;
    body.isDeleted=this.branch.isDeleted;
    body. location=this.branch.location;
    body. restaurant=this.branch.restaurant?this.branch.restaurant:null;
    body. restaurantId=this.branch.restaurantId?this.branch.restaurantId:null;
    body. tenant_Id=this.branch.tenant_Id;
    body.updatedDate = this.branch.updatedDate;
    body.branchManagerId = this.branch.branchManagerId;
      }
     

    body.branchManagerId = this.form.value.branchManagerId ? this.form.value.branchManagerId : "";
    body.mobile = this.selectedMobileNumber ? this.selectedMobileNumber : body.mobile;
    body.countryId = this.selectedCountryCode ? this.selectedCountryCode.id : body.countryId;
    body.location = this.address ? this.address : body.location;
    body.companyId = this.companyProfile.id;
    body.address = body.address? body.address: this.address?`${this.address.governorate?this.address.governorate.toString():""}, ${this.address.area?this.address.area.toString():""}, ${this.address.block?this.address.block.toString():""}, ${this.address.street?this.address.street.toString():""}`:"";
    body.latitude = 0;
    body.longitude = 0;
    


    if (this.isEditMode) {
      this.facadeService.adminFacadeService.branchService.update(body).subscribe(res => {
        this.toastrService.success(this.translateService.instant('Branch Updated Successfully'));
        this.dialogRef.close({ operation: this.operation });
      });
    } else {
      this.facadeService.adminFacadeService.branchService.create(body).subscribe(res => {
        this.toastrService.success(this.translateService.instant('New Branch Added Successfully'));
        this.dialogRef.close({ operation: this.operation });
      });
    }
  }

  /**
   * validate phone number
   * 
   * 
   * @param valid 
   */
  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  /**
 * on type number 
 * 
 * 
 * @param number 
 */
  number(number: number): void {
    this.selectedMobileNumber = number;
  }

  /**
   * on select country flag from phone number
   * 
   * 
   * @param country 
   */
  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }
  on
  onAddressChanges(address: Address): void {
    this.address = address;
  }
  onManagerChanges(manager: Manager): void {
    this.selectedManager = manager;
  }
  
}

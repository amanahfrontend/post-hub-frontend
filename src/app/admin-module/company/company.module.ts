import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyRoutingModule } from './company-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';

// shared
import { AddressModule } from '../../shared/components/address/address.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';

// components
import { ManageCompanyLogoComponent } from './manage-company-logo/manage-company-logo.component';
import { ManageCompanyInfoComponent } from './manage-company-info/manage-company-info.component';
import { ManageLocationComponent } from './manage-location/manage-location.component';
import { WorkingHoursComponent } from './working-hours/working-hours.component';
import { TranslateModule } from '@ngx-translate/core';
import { CompanyComponent } from './company.component';

@NgModule({
  declarations: [
    ManageLocationComponent,
    ManageCompanyLogoComponent,
    ManageCompanyInfoComponent,
    WorkingHoursComponent,
    CompanyComponent
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TimePickerModule,

    // shared
    MaterialModule,
    AddressModule,
    HeaderAsCardModule,
    TelInputModule,
    ConfirmDeletionModule,
    TranslateModule,
    PermissionsModule,
  ],
  entryComponents: [
    ManageLocationComponent,
    ManageCompanyLogoComponent,
    WorkingHoursComponent,
    ManageCompanyInfoComponent,
  ]
})
export class CompanyModule { }

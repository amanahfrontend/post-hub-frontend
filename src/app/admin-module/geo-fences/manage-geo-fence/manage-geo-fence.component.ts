import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { FacadeService } from '../../../services/facade.service';
import { GeoFence } from '../../../shared/models/admin/geo-fence';
import { MatSelectChange } from '@angular/material/select';
import { Subscription } from 'rxjs';

const FENCES_ROUTE = 'geo-fences';
@Component({
  selector: 'manage-geo-fence',
  templateUrl: './manage-geo-fence.component.html',
  styleUrls: ['./manage-geo-fence.component.scss']
})
export class ManageGeoFenceComponent implements OnInit, OnDestroy {
  private map: any;
  private layer: any;

  form: FormGroup;
  geoFenceId: number;
  polyLayers = [];
  editMode: boolean = false;
  geoFence: GeoFence;
  previousLocations: any[] = [];
  isSubmitted: boolean = false;
  fences: GeoFence[];
  loading: boolean = false;
  label: string = this.translateService.instant('Add');
  lat: number = 29.3044;
  lng: number = 47.9443;
  isDeleteing: boolean = false;
  mapInitialized: boolean = false;
  blocks: any[] = [];
  areas: any[] = [];
  areaBlocks: any[] = [];
  subscriptions = new Subscription();

  filterArea: string;
  filterBlock: string;
  selectedBlocks: string[] = [];



  constructor(
    private fb: FormBuilder,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
    private toastrService: ToastrService
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      areaId: ['', [Validators.required]],
      blockId: ['', [Validators.required]],
      filterBlock: [''],
      filterArea: [''],
      description: [''],
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.geoFenceId = params.id;
        this.label = this.translateService.instant('Edit');
        this.editMode = true;
        this.getGeoFence(this.geoFenceId);
      }
    });

    this.listAreas();
  }

  fillFormData() {
    this.form.patchValue(this.geoFence);
    let areas: string[] = [];
    this.geoFence.geoFenceBlocks.forEach(areaBlocks => {
      areas.push(areaBlocks.area);

      const areasUnique = areas.filter(this.onlyUnique);
      const areaIndex: number = areasUnique.findIndex(area => area === areaBlocks.area);

      this.listBlocksByArea(areaBlocks.area, areaIndex);
    });

    this.form.get('areaId').setValue(areas);
  }

  onlyUnique(value: any, index: number, self: any) {
    return self.indexOf(value) === index;
  }

  /**
    *Fetch geo Fence data
    *
    */
  getGeoFence(geoFenceId: number): void {
    this.facadeService.adminFacadeService.geoFenceService.get(geoFenceId).subscribe(geoFence => {

     
      this.geoFence = geoFence;
      const result = {};

      //for (const { area, block } of this.geoFence.geoFenceBlocks) {
      //  if (!result[area]) result[area] = [];
      //  result[area].push(block);
      //}

      //for (const key in result) {
      //  this.populateValues(result[key]);
      //}

      //this.form.get('blockId').setValue(this.selectedBlocks);
      this.fillFormData();
    });
  }

  //populateValues(blocks: string[]) {
  //  this.selectedBlocks.push(...blocks);
  //}

  /**
   * return to feo fence list
   *
   */
  cancel() {
    this.router.navigate([FENCES_ROUTE]);
  }

  /**
   * update geo fence name
   *
   *
   * @param event
   */
  onType(event) {
    if (this.layer) {
      this.layer.unbindTooltip();

      this.layer.bindTooltip(event.target.value,
        { permanent: true, direction: "center" }
      ).openTooltip();

      this.map.addLayer(this.layer);
    }
  }

  /**
   * create / edit geo fence
   *
   *
   */
  onSubmit() {
   
    
    this.isSubmitted = true;
    if (this.form.invalid) {
      this.isSubmitted = false;
      this.form.markAllAsTouched();
      this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
      return;
    }

    var areasBlocks: { area: string, block: string }[] = [];
    this.areaBlocks.forEach((area: { id: string, blocks: any[] }) => {
      if (area.blocks && area.blocks.length > 0) {

        console.log("areablocks before submit");
        console.log(area.blocks);

        area.blocks.forEach((block: { id: string, checked: boolean }) => {
          if (block.checked) {
             areasBlocks.push({
              area: area.id,
              block: block.id,
            });
          }
        });
      }
    });

    let body = this.form.value;
    body.geoFenceBlocks = areasBlocks;
    if (!this.editMode) {
      this.facadeService.adminFacadeService.geoFenceService.create(body).subscribe(
        geoFence => {
          this.toastrService.success(this.translateService.instant('Geofence has been added successfully'))

          //   if(geoFence.toString()=="1")
          //        this.toastrService.success(this.translateService.instant('Geofence has been added successfully'))
          //  else if(geoFence.toString()=="0")
          //      this.toastrService.success(this.translateService.instant('there is problem  Geofence has not been added '))

          // else{
          //  let response=`Block is already exist on ${geoFence.toString()} choose  another area `;
          //   this.toastrService.success(this.translateService.instant(response))

          // }

          this.isSubmitted = false;
          this.router.navigate([FENCES_ROUTE]);
        }, error => {
          this.isSubmitted = false;
        });
    } else {
      body.id = this.geoFence.id;
      this.facadeService.adminFacadeService.geoFenceService.update(body).subscribe(geoFence => {
        this.toastrService.success(this.translateService.instant('Geofence has been updated successfully'))
        this.isSubmitted = false;
        this.router.navigate([FENCES_ROUTE]);
      }, error => {
        this.isSubmitted = false;
      });
    }
  }


  /**
   * list all Geo Fences
   *
   *
   */
  list() {
    this.loading = true;
    this.facadeService.adminFacadeService.geoFenceService.list().subscribe(result => {
      if (result && result.length > 0) {
        this.fences = result;
      }

      this.loading = false;
    })
  }

  onSelectArea(option: MatSelectChange): void {
     
      this.areas.forEach((area: any, areaIndex: number) => {
      option.value.forEach((value: { id: string }) => {
        if (value == area.id) {
          this.listBlocksByArea(area.id, areaIndex,);
        }
      });
    });

    let result = this.areaBlocks.filter(function (item) {
       
        if (option.value.indexOf(item.id) !== -1)
        return item;
    });
      this.areaBlocks = result

      console.log( this.form);
  }

    listBlocksByArea(area: string, index: number,) {
        this.subscriptions.add(this.facadeService.addressService.getBlocksByArea(area)
          .subscribe((blocks: any[]) => {

              blocks = blocks.map((block: any) => {
                  block.area = area;
                  if (this.geoFence) {
                      var blockindex = this.geoFence.geoFenceBlocks.findIndex(x => x.area == area && x.block == block.id);
                      if (blockindex >= 0) {
                          block.checked = true;
                          if (this.selectedBlocks.findIndex(x=>x== block.area + block.id) == -1) {
                              this.selectedBlocks.push(block.area + block.id);
                          }
                      }
                  }
                  return block;
                })
              console.log(blocks);
     
      if (!this.areaBlocks.find(o => o.name === area))
             this.areaBlocks.push({ blocks: blocks, id: area, name: area })

              if (this.geoFence) {
                  console.log("Selected Blocks");
                  console.log(this.selectedBlocks);
                  this.form.get('blockId').setValue(this.selectedBlocks);
                  this.form.markAllAsTouched();
              }

    }));
  }

  listAreas() {
    this.subscriptions.add(this.facadeService.addressService.areas.subscribe((areas: any[]) => {
      this.areas = areas;
    }));
  }

  toggleBlock(areaToFind: { id: string }, blockToFind: { id: string }): void {
    this.areaBlocks.forEach((area: { id: string, blocks: any[] }) => {
      if (areaToFind.id == area.id) {

        area.blocks.forEach((block: any) => {
          if (block.id == blockToFind && (!block.checked)) {
              block.checked = true;
              if (this.selectedBlocks.findIndex(x => x == block.area + block.id) == -1)
              {
                  this.selectedBlocks.push(block.area + block.id);
              }
          }
          else if (block.id == blockToFind && block.checked) {
            block.checked = false;
              var blockindex = this.selectedBlocks.findIndex(x => x == block.area + block.id);
              if (blockindex > -1) {
                  this.selectedBlocks.splice(blockindex,1);
            }
          }
        });
      }
    });
  }

  areaIndex(areaToFind: string): number {
    return this.geoFence.geoFenceBlocks.findIndex(area => area.area === areaToFind);
  }

  ngOnDestroy() {
    this.map = null;
  }

  getError(input: string) {
    switch (input) {
      case 'areaId':
        if (this.form.get('areaId').hasError('required')) {
          return this.translateService.instant(`Area required`);
        }
        break;
      case 'blockId':
        if (this.form.get('blockId').hasError('required')) {
          return this.translateService.instant(`Block required`);
        }
        break;
      default:
        return '';
    }
  }
}

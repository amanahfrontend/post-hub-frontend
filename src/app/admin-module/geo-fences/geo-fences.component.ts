import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Leaflet from 'leaflet';
import { TranslateService } from '@ngx-translate/core';
import { saveFile } from '../../shared/helpers/download-link';
import { ToastrService } from 'ngx-toastr';
import { GeoFence } from './../../shared/models/admin/geo-fence';
import { FacadeService } from '../../services/facade.service';

@Component({
  selector: 'geo-fences',
  templateUrl: './geo-fences.component.html',
  styleUrls: ['./geo-fences.component.scss']
})
export class GeoFencesComponent implements OnInit {

  private static readonly ROUTE_ADD = 'geo-fences/add';
  private static readonly ROUTE_EDIT = 'geo-fences/edit';
  private map: any;

  options: any;
  info: any;

  fences: GeoFence[] = [];
  fence: GeoFence;
  polyLayers = [];
  drawnItems = new Leaflet.FeatureGroup();

  lat: number;
  lng: number;
  locale: string;

  constructor(
    private router: Router,
    private facadeService: FacadeService,
    private translateService: TranslateService,
    private toastrService: ToastrService
  ) {
    this.lat = 29.2891;
    this.lng = 48.0164;
  }

  ngOnInit() {
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    });

    this.list();
  }

  list() {
    this.facadeService.adminFacadeService.geoFenceService.list().subscribe(result => {
      if (result && result.length > 0) {
        this.fences = result;
      }
    });
  }

  createGeoFence() {
    this.router.navigate([GeoFencesComponent.ROUTE_ADD]);
  }

  editGeoFence(fence: GeoFence) {
    this.router.navigate([GeoFencesComponent.ROUTE_EDIT, fence.id]);
  }

  /**
   * set fence
   *
   *
   * @param fence
   */
  setTSelectableFence(fence: GeoFence) {
    this.fence = fence;
  }

  // setTSelectableFenceFocus(fence: GeoFence) {
  //   this.fence = fence;

  //   let coordinates = [];
  //   this.fence.locations.forEach(point => {
  //     coordinates.push([+point.latitude, +point.longitude]);
  //   });

  //   const polygon = Leaflet.polygon(coordinates);

  //   //set view Map on ploygon
  //   this.map.fitBounds(polygon.getBounds());
  // }

  /**
   * delete geo fence
   *
   *
   * @param event
   */
  onConfirm(event: boolean, fence: { id: number }) {
    if (event) {
      this.facadeService.adminFacadeService.geoFenceService.delete(fence.id).subscribe(res => {
        this.toastrService.success(this.translateService.instant(`Geo Fence has been deleted successfully`))
        this.drawnItems.clearLayers();
        this.list();
      });
    }
  }

  // private initMap(): void {
  //   this.map = Leaflet.map('geo-map', {
  //     center: [+this.lat, +this.lng],
  //     zoom: 12
  //   });

  //   const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  //   tiles.addTo(this.map);
  //   this.map.addLayer(this.drawnItems);
  // }


  // drawPolyon(shap: any, fence: string) {
  //   const polygon = Leaflet.polygon(shap);
  //   this.drawnItems.addLayer(polygon);

  //   polygon.bindTooltip(fence,
  //     { permanent: true, direction: "center" }
  //   ).openTooltip();
  // }


  /**
   * exportGeoFence to excel
   * */
  exportGeoFence() {
    this.facadeService.adminFacadeService.geoFenceService.exportToExcel().subscribe(data => {
      saveFile(`${this.translateService.instant('Geo Fences')} ${new Date().toLocaleDateString()}.csv`, "data:attachment/text", data);
    });
  }

}

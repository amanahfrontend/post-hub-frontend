import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeoFencesRoutingModule } from './geo-fences-routing.module';
import { GeoFencesComponent } from './geo-fences.component';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { TranslateModule } from '@ngx-translate/core';
import { ManageGeoFenceComponent } from './manage-geo-fence/manage-geo-fence.component';
import { DateToTimezoneModule } from '../../shared/pipes/date-to-timezone/date-to-timezone.module';
import { SetDirModule } from '../../shared/directives/set-dir/set-dir.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { PermissionsModule } from '../../shared/modules/permissions/permissions.module';
import { FilterModule } from '@app/shared/pipes/filter/filter.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [
    GeoFencesComponent,
    ManageGeoFenceComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,
    ConfirmDeletionModule,
    ReactiveFormsModule,
    GeoFencesRoutingModule,
    DateToTimezoneModule,
    SetDirModule,
    HeaderAsCardModule,
    FilterModule,
    PermissionsModule,
    NgxMatSelectSearchModule
  ]
})
export class GeoFencesModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { GeoFencesComponent } from './geo-fences.component';
import { ManageGeoFenceComponent } from './manage-geo-fence/manage-geo-fence.component';

const routes: Routes = [
  {
    path: '',
    component: GeoFencesComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ReadGeofence'
      }
    },
  },
  {
    path: 'add',
    component: ManageGeoFenceComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'AddGeofence'
      }
    },
  },
  {
    path: 'edit/:id',
    component: ManageGeoFenceComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'UpdateGeofence'
      }
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeoFencesRoutingModule { }

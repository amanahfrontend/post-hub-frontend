import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from '../admin-module/user-profile/user-profile.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { BulkOrderPickUpRequestsComponent } from '../admin-module/bulk-order-pick-up-requests/bulk-order-pick-up-requests.component';
import { DistributorComponent } from '../admin-module/distributor/distributor.component';
import { CustomerComponent } from '../admin-module/customer/customer.component';
import { AssignDriverComponent } from '../admin-module/assign-driver/assign-driver.component';
import { ManageOrderComponent } from '../admin-module/manage-order/manage-order.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { ManageRFQComponent } from './manage-rfq/manage-rfq.component';
import { ServicesComponent } from './services/services.component';
import { ProfileResolver } from '../services/admin/Profile/profile.resolver';
import { CreateBusinessOrderComponent } from './create-business-order/create-business-order.component';

export const AdminLayoutRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'company',
                loadChildren: () => import('./company/company.module').then(m => m.CompanyModule),
            },
            {
                path: 'manage-drivers',
                loadChildren: () => import('./manage-drivers/manage-drivers.module').then(m => m.ManageDriversModule),
            },
            {
                path: 'zones',
                loadChildren: () => import('./zones/zones.module').then(m => m.ZonesModule),
            },
            {
                path: 'price-list',
                loadChildren: () => import('./price-list/price-list.module').then(m => m.PriceListModule),
            },
            {
                path: 'bulk-uploading',
                loadChildren: () => import('./bulk-uploading/bulk-uploading.module').then(m => m.BulkUploadingModule),
            },
            {
                path: 'create-order',
                loadChildren: () => import('./create-order/create-order.module').then(m => m.CreateOrderModule),
            },
            {
                path: 'manage-staff',
                loadChildren: () => import('./manage-staff/manage-staff.module').then(m => m.ManageStaffModule),
            },
            {
                path: 'contracts',
                loadChildren: () => import('./contracts/contracts.module').then(m => m.ContractsModule),
            },
            {
                path: 'manage-invoice',
                loadChildren: () => import('./manage-invoice/manage-invoice.module').then(m => m.ManageInvoiceModule),
            },
            {
                path: 'manage-pq',
                loadChildren: () => import('./manage-priceList/manage-priceList.module').then(m => m.ManagePriceListModule),
            },
            {
                path: 'item-tracking',
                loadChildren: () => import('./manage-tracking/manage-tracking.module').then(m => m.ManageTrackingModule),
            }, 
            {
                path: 'order-tracking',
                loadChildren: () => import('./order-tracking/order-tracking.module').then(m => m.OrderTrackingModule),
            }, 

            {
                path: 'customer-registration',
                loadChildren: () => import('./business-accounts/business-accounts.module').then(m => m.BusinessAccountsModule),
            },
            {
                path: 'business-accounts',
                loadChildren: () => import('./business-accounts/business-accounts.module').then(m => m.BusinessAccountsModule),
            },
            {
                path: 'manage-pl',
                loadChildren: () => import('./manage-pl/manage-pl.module').then(m => m.ManagePLModule),
            },
            {
                path: 'access-control',
                loadChildren: () => import('./access-control/access-control.module').then(m => m.AccessControlModule),
            },
            {
                path: 'create-business-order',
                loadChildren: () => import('./create-business-order/create-business-order.module').then(m => m.CreateBusinessOrderModule),
            },
            {
                path: 'settings',
                loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
            },
            {
                path: 'reports',
                loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule),
            },
            {
                path: 'geo-fences',
                loadChildren: () => import('./geo-fences/geo-fences.module').then(m => m.GeoFencesModule),
            },
            {
                path: 'manage-workorders',
                loadChildren: () => import('./manage-workorders/manage-workorders.module').then(m => m.ManageWorkordersModule),
            },
            {
                path: 'dispatch-orders',
                loadChildren: () => import('./dispatch-orders/dispatch-orders.module').then(m => m.DispatchOrdersModule),
            },
        ]
    },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
    {
        path: 'user-profile', component: UserProfileComponent,
        resolve: {
            userInfo: ProfileResolver
        }
    },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'distributor', component: DistributorComponent },
    { path: 'customer', component: CustomerComponent },
    { path: 'assignDriver/:id', component: AssignDriverComponent },
    { path: 'manageOrder', component: ManageOrderComponent },
    { path: 'services', component: ServicesComponent },
    { path: 'trackOrder/:id', component: TrackOrderComponent },
    { path: 'manageRFQ', component: ManageRFQComponent },
    { path: 'create-business-order/edit/:id', component: CreateBusinessOrderComponent }
];

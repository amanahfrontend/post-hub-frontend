import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateOrderRoutingModule } from './create-order-routing.module';
import { CreateOrderComponent } from './create-order.component';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { AddressModule } from '../../shared/components/address/address.module';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TelInputModule } from '../../shared/components/tel-input/tel-input.module';
import { ConfirmDeletionModule } from '../../shared/components/confirm-deletion/confirm-deletion.module';
import { HeaderAsCardModule } from '../../shared/components/header-as-card/header-as-card.module';
import { ManageItemComponent } from './manage-item/manage-item.component';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';

@NgModule({
  declarations: [
    CreateOrderComponent,
    ManageItemComponent
  ],
  imports: [
    CommonModule,
    CreateOrderRoutingModule,

    CommonModule,
    MaterialModule,
    AddressModule,
    MatDialogModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    TelInputModule,
    ConfirmDeletionModule,
    HeaderAsCardModule,
    TimePickerModule
  ],
  providers: [
    ManageItemComponent
  ]
})
export class CreateOrderModule { }

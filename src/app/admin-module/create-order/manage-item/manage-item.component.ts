import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'manage-item',
  templateUrl: './manage-item.component.html',
  styleUrls: ['./manage-item.component.scss']
})
export class ManageItemComponent implements OnInit {

  operation: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.operation = this.data.operation;
  }
  ngOnInit(): void {
  }

}

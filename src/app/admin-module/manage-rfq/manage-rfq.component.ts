import { AfterViewInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MouseEvent } from '@agm/core';

import { Validators } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-manage-rfq',
  templateUrl: './manage-rfq.component.html',
  styleUrls: ['./manage-rfq.component.css']
})
export class ManageRFQComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  newlocationDetails;
  unsubscribedata;
  dtOptions: any = {};
  idForDelete: any;
  areaList = []
  dtTrigger: Subject<any> = new Subject();
  checkAll = false
  locationDetails = []
  locationForm;
  order;
  isEdit: boolean;
  selectedLocationId: any;
  addOrEdit;
  dataSource;
  companyForm;
  masterCheck = false
  orderDetails = [
    {
      barcode: 232232, AWB: 9889231821, receiverName: 'Test1 User', senderName: 'Test6 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 2  ', status: 'Approved',
    },
    {
      barcode: 23232, AWB: 9889231821, receiverName: 'Test2 User', senderName: 'Test5 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 1', status: 'Approved'
    },
    {
      barcode: 243232, AWB: 9889231821, receiverName: 'Test3 User', senderName: 'Test4 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'test', status: 'Returned'
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', status: 'Returned'
    },
    {
      barcode: 3232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test2 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 72323232, AWB: 9889231821, receiverName: 'Test5 User', senderName: 'Test1 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: '', status: 'Delivered',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'returned', status: 'Returned',
    },

  ]
  tempArray = [
    {
      barcode: 232232, AWB: 9889231821, receiverName: 'Test1 User', senderName: 'Test6 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 2  ', status: 'Approved',
    },
    {
      barcode: 23232, AWB: 9889231821, receiverName: 'Test2 User', senderName: 'Test5 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Approved', clientName: 'driver 1', status: 'Approved'
    },
    {
      barcode: 243232, AWB: 9889231821, receiverName: 'Test3 User', senderName: 'Test4 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'test', status: 'Returned'
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', status: 'Returned'
    },
    {
      barcode: 3232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test2 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Out of Pick Up', clientName: 'driver 3 ', status: 'outofpickup',
    },
    {
      barcode: 72323232, AWB: 9889231821, receiverName: 'Test5 User', senderName: 'Test1 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Delivered', clientName: '', status: 'Delivered',
    },
    {
      barcode: 32232, AWB: 9889231821, receiverName: 'Test4 User', senderName: 'Test3 Sender', orderDate: '20/04/2021',
      shipmentDetails: 'Address Block', paymentMethod: 'COD', orderStatus: 'Returned', clientName: 'returned', status: 'Returned',
    },

  ]

  governorateList = [
    'Capital',
    'Hawalli',
    'Mubarak Al-Kaber',
    'Ahmadi',
    'Farwaniya',
    'Jahra'
  ]
  areaListByGovernorate = [{
    gov: 'Capital', area: [
      "Abdulla Al-Salem",
      "Adailiya",
      "Bnaid Al-Qar",
      "Daʿiya",
      "Dasma",
      "Doha",
      "Doha Port",
      "Faiha",
      "Failaka",
      "Ghornata",
      "Jaber Al-Ahmad City",
      "Jibla",
      "Kaifan",
      "Khaldiya",
      "Mansūriya",
      "Mirgab",
      "Nahdha",
      "North West Sulaibikhat",
      "Nuzha",
      "Qadsiya",
      "Qurtuba",
      "Rawda",
      "Shamiya",
      "Sharq",
      "Shuwaikh",
      "Shuwaikh Industrial Area",
      "Shuwaikh Port",
      "Sulaibikhat",
      "Surra",
      "Umm an Namil Island",
      "Yarmouk",

    ]
  }
    ,
  {
    gov: 'Hawalli', area: [
      "Anjafa",
      "Bayān",
      "Bi'da",
      "Hawally",
      "Hittin",
      "Jabriya",
      "Maidan Hawalli",
      "Mishrif",
      "Mubarak Al-Jabir",
      "Nigra",
      'Rumaithiya',
      "Salam",
      "Salmiya",
      "Salwa",
      "Sha'ab",
      "Shuhada",
      "Siddiq",
      "South Surra",
      'Zahra'


    ]
  },
  {
    gov: 'Mubarak Al-Kaber', area: [
      " Abu Al Hasaniya",
      "Abu Futaira",
      "Adān",
      "Al Qurain",
      "Al-Qusour",
      "Fintās",
      "Funaitīs",
      "Misīla",
      "Mubarak Al-Kabeer",
      "Sabah Al-Salem",
      "Sabhān",
      "South Wista",
      "Wista",


    ]
  },
  {
    gov: 'Ahmadi', area: [
      "Abu Halifa",
      "Abdullah Port",
      "Ahmadi",
      "Ali As-Salim",
      "Aqila",
      "Bar Al Ahmadi",
      "Bneidar",
      "Dhaher",
      "Fahaheel",
      "Fahad",
      "Hadiya",
      "Jaber Al-Ali",
      "Jawaher Al Wafra",
      "Jilei'a",
      "Khairan",
      "Mahbula",
      "Mangaf",
      "Miqwa",
      "New Khairan City",
      "New Wafra",
      "Nuwaiseeb",
      'Riqqa',
      "Sabah Al-Ahmad City",
      "Sabah Al-Ahmad Nautical City",
      "Sabahiya",
      "Shu'aiba (North)",
      "Shu'aiba (South)",
      "South Sabahiya",
      'Wafra',
      "Zoor",
      "Zuhar",

    ]
  },
  {
    gov: 'Farwaniya', area: [
      "Abdullah Al-Mubarak",
      'Airport District',
      "Andalous",
      "Ardiya",
      "Ardiya Herafiya",
      "Ardiya Industrial Area",
      "Ashbelya",
      "Dhajeej",
      "Farwaniya",
      "Fordous",
      "Jleeb Al-Shuyoukh",
      "Khaitan",
      'Omariya',
      "Rabiya",
      "Rai",
      "Al-Riggae",
      "Rihab",
      "Sabah Al-Nasser",
      "Sabaq Al Hajan"


    ]
  },
  {
    gov: 'Jahra', area: [
      "Abdali",
      "Al Nahda / East Sulaibikhat",
      "Amghara",
      "Bar Jahra",
      "Jahra",
      "Jahra Industrial Area",
      "Kabad",
      "Naeem",
      "Nasseem",
      "Oyoun",
      "Qasr",
      "Saad Al Abdullah City",
      "Salmi",
      "Sikrab",
      'South Doha / Qairawān',
      "Subiya",
      'Sulaibiya',
      "Sulaibiya Agricultural Area",
      'Taima',
      'Waha'


    ]
  }

  ]
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;
  checkList: any = {};
  sundayCheck;
  saturdayCheck;
  mondayCheck;
  tuesdayCheck;
  wednesdayCheck;
  thursdayCheck;
  fridayCheck;
  dropdownListStatus = [

    { item_id: 2, item_text: 'Branch 1' },
    { item_id: 3, item_text: 'Branch 2' },
    { item_id: 4, item_text: 'Branch 3' },


  ];
  dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  constructor(private router: Router,
    private translateService: TranslateService) {
  }

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 5,
      destroy: true,
      order: [],
      info: false,
      lengthChange: false,
      select: true,
      columnDefs: [{ orderable: false, targets: [] }],
      language: {
        zeroRecords: "No Data to display",
        paginate: {
          next: ">>", // or '→'
          previous: "<<", // or '←',
          first: null,
          last: null,
        },
      },
      dom: "Bfrtip",
      buttons: [],
    };
    this.getLocation()

    this.order = new FormGroup({
      locationName: new FormControl('', Validators.required),
      mobile_no: new FormControl(''),
      paci: new FormControl(''),
      awb: new FormControl(''),
      senderName: new FormControl(),
      receiverName: new FormControl(),
      Street: new FormControl(''),
      Governorate: new FormControl(''),
      Area: new FormControl(''),
      Block: new FormControl(),
      BuildingNo: new FormControl(''),
      floorNo: new FormControl(''),
      governorateListFilterCtrl: new FormControl(''),
      orderDate: new FormControl(''),
      barcode: new FormControl(''),
      payment: new FormControl(''),
      status: new FormControl(''),
      driver: new FormControl(''),
      clientName: new FormControl(''),
      endDate: new FormControl(''),
      RFQ: new FormControl(''),
    });

    this.companyForm = new FormGroup({
      companyName: new FormControl(''),
      companyEmail: new FormControl(''),
      companyOtherEmail: new FormControl(''),


      companyPhone: new FormControl(),
      companyWebsite: new FormControl(''),


    });




  }
  getLocation() {
    // this.rerender()

    this.locationDetails = []


    this.locationDetails = [
      {
        locationName: 'test location 1 ', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 1, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021', status: 'Active'
      },
      {
        locationName: 'test location 2', mobile_no: 9889241821, paci: '721781277122', Governorate: '2overnorate2', Area: 'test area2',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 2, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021', status: 'Active'
      },
      {
        locationName: 'test location 3', mobile_no: 9889251821, paci: '721781277122', Governorate: 'Governorate3', Area: 'test area3',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 3, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021', status: 'Active'
      },
      {
        locationName: 'test location 4', mobile_no: 9889261821, paci: '721781277122', Governorate: 'Governorate4', Area: 'test area4',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 4, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021', status: 'Inactive'
      },
      {
        locationName: 'test location 5', mobile_no: 9889271821, paci: '721781277122', Governorate: 'Governorate5', Area: 'test area5',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 5, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021', status: 'Active'
      },
      {
        locationName: 'test location 1 test location 1 test location 1 location 1 test location 1 test location 1', mobile_no: 9889231821, paci: '721781277122', Governorate: 'Governorate1', Area: 'test area1',
        Block: 'test Block', Street: 'test Street', BuildingNo: 'test building 1', floorNo: '12', id: 6, clientName: "test client", RFQ: "RFhWSKDWD", endDate: '12-MAR-2021'
      },

    ]
    //  this.rerender()



  }
  addNewLocation() {
    this.areaList
    this.addOrEdit = this.translateService.instant('Add');
    this.locationForm.reset()
    this.isEdit = false;
    // this.locationDetails.push(this.locationForm.value)
  }
  selectedItem(id) {
    this.areaList = []
    this.addOrEdit = this.translateService.instant('Edit');
    this.isEdit = true;
    this.selectedLocationId = id
    console.log('id: ', id);
    let selectedLocation = this.locationDetails.filter(arr => {
      return arr.id == id
    })
    console.log('selectedLocation: ', selectedLocation);
    if (selectedLocation && selectedLocation.length > 0) {

      this.locationForm = new FormGroup({
        locationName: new FormControl(selectedLocation[0]['locationName']),
        mobile_no: new FormControl(selectedLocation[0]['mobile_no']),
        paci: new FormControl(selectedLocation[0]['paci']),


        Street: new FormControl(selectedLocation[0]['Street']),
        Governorate: new FormControl(selectedLocation[0]['Governorate']),
        Area: new FormControl(selectedLocation[0]['Area']),
        Block: new FormControl(selectedLocation[0]['Block']),
        BuildingNo: new FormControl(selectedLocation[0]['BuildingNo']),
        floorNo: new FormControl(selectedLocation[0]['floorNo']),

      });
    }

  }
  onClick() {

    if (this.isEdit) {
      this.locationForm.value['id'] = this.selectedLocationId;
      this.locationDetails.forEach((data, index) => {
        if (data.id == this.selectedLocationId) {
          this.locationDetails[index] = this.locationForm.value
          console.log('data: ', this.locationForm.value);
        }
      })
    }
    if (!this.isEdit) {
      console.log('this.locationForm.value: ', this.locationForm.value);
      this.locationDetails.push(this.locationForm.value)
      console.log(this.locationForm.value)
    }


  }
  onSaveCompanyDetails() {
    // this.companyForm.value
    console.log('   this.companyForm.value: ', this.companyForm.value);
    this.companyForm = new FormGroup({
      companyName: new FormControl(this.companyForm.value['companyName']),
      companyEmail: new FormControl(this.companyForm.value['companyEmail']),
      companyOtherEmail: new FormControl(this.companyForm.value['companyOtherEmail']),


      companyPhone: new FormControl(this.companyForm.value['companyPhone']),
      companyWebsite: new FormControl(this.companyForm.value['companyWebsite']),


    });
  }

  changeGovernorate(value) {
    console.log('value: ', value);
    console.log('this.areaListByGovernorate: ', this.areaListByGovernorate);
    let selectGov = this.areaListByGovernorate.filter(arr => {
      return arr.gov == value
    })
    console.log('selectGov: ', selectGov);
    if (selectGov && selectGov.length > 0) {
      this.areaList = selectGov[0]['area']
    }

  }

  deleteLocation(id) {
    this.idForDelete = id;
  }
  confirmDelete() {
    this.locationDetails.splice(this.idForDelete, 1)
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    }
    );
  }
  changeCheckBox(value, day) {
    console.log('value: ', value);
    if (!value) {
      this.masterCheck = value
      console.log(' this.masterCheck: ', this.masterCheck);
    }
    console.log('value, day: ', value, day);
    this.checkList[day] = value
    console.log('value: ', value);

  }
  ischeck() {
    if (this.masterCheck) {
      return true
    }
    return false
  }
  checkedAll(value) {
    console.log('value: ', value);

    this.checkAll = value
    this.checkList = {
      'monday': value,
      'tuesday': value,
      'wednesday': value,
      'thursday': value,
      'friday': value,
      'saturday': value,
      'sunday': value,
    }

  }
  changeRoutes() {
    if (this.govList && this.govList.length > 0) {
      this.router.navigate(['assignDriver', this.govList.length])
    }
    else {
      alert("please check minimum one order")
    }
  }
  govList = []
  checkBoxAdd(index) {
    console.log('value: ', index);
    let checkData = false
    for (let i = 0; i < this.govList.length; i++) {
      if (this.govList[i] == index) {
        this.govList.splice(i, 1);
        checkData = true
        break;
      }
    }

    if (!checkData) {
      this.govList.push(index)

    }





  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }
  filer(value) {
    this.orderDetails = this.tempArray
    let temp = this.orderDetails
    console.log('value: ', value);
    if (value == 'all') {
      this.orderDetails = this.tempArray
    }
    else {
      this.orderDetails = temp.filter(arr => {
        return arr.status == value
      })
    }


  }
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]



}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

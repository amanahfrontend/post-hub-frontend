import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRFQComponent } from './manage-rfq.component';

describe('ManageRFQComponent', () => {
  let component: ManageRFQComponent;
  let fixture: ComponentFixture<ManageRFQComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageRFQComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRFQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

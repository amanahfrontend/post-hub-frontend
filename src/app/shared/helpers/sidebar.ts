import * as $ from "jquery";

export function displaySidebar() {
    $('#sidebar').css('display', 'block');
    $('#main-panel').css('width', 'calc(100% - 260px)');
    $('#navbar').css('width', 'calc(100% - 260px)');
}

export function hideSidebar() {
    $('#sidebar').css('display', 'none');
    $('#main-panel').css('width', '100%');
    $('#navbar').css('width', '100%');
}



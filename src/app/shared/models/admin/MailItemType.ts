export interface MailItemType {
    id:number;
    name:string;
    isActive :Boolean;
}
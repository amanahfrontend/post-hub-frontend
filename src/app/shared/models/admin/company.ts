

export interface Company {
    id: number;
    logoUrl: string;
    LogoFile?: File,
    missionSlogan: string;
    name: string;
    bussinesDiscription: string;
    companyAddress: CompanyAddress;
    countryId: number;
    country: Country;
    phone: number;
    mobile: number;
    faxNo: string;
    email: string;
    website: string;
    tradeName: string;
    mainBranchId: number;
    socialLinks: SocialLink[];
    workingHours: WorkingHour[];
    latitude: number;
    longitude: number;
}

interface CompanyAddress {
    governorate: string;
    area: string;
    block: string;
    street: string;
    building: string;
    floor: string;
    flat: string;
}

interface Country {
    createdBy_Id: string;
    updatedBy_Id: string;
    deletedBy_Id: string;
    isDeleted: boolean;
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
    id: number;
    name: string;
    code: string;
    flag: string;
    topLevel: string;
}

interface SocialLink {
    companyId?: number;
    siteName?: string;
    siteLink?: string;
}

export interface WorkingHour {
    id: number;
    companyId: number;
    day: string;
    from: string;
    to: string;
    secondFrom: string;
    secondTo: string;
    offset: string;
    timeZone: string;
}

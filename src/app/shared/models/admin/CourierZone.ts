export interface CourierZone {
    id:number;
    name:string;
    isActive :Boolean;
    courierId :number;
    courierName:string;
}
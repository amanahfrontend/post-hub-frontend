
export interface MailItemStatusLog{
    id :number  ;   
    statusLogs: StatusLogs[];  
}
export interface StatusLogs{
    createdDate:Date,
      mailItemStatusName:string
}
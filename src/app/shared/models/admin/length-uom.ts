export interface LengthUOM {
    id:number;
    name:string;
    isActive :Boolean;
    code:string;
}
export interface BusinessCustomerDistributionReport {

      id :number;
      businessName :string
      dayOfMonth :Date
      deliveredDay  :number;
      branchName :string
      itemsNo  :number;
}
export interface Language {
      id:number;
      name:string;
      isActive :Boolean;
      code:string;
}
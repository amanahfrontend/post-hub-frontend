import { Address } from '../../../shared/models/components';

export interface BusinessOrder {
    id:number;
    orderCode: string;
    statusId: number;
    orderStatus: string;
    orderTypeId: number;
    orderType: string;
    serviceSectorId: number;
    serviceSector: string;
    serviceTypeId: number;
    serviceType: string;
    issueAt: Date
    readyAt: Date;
    startedAt: Date;
    deliveryBefore: Date;
    fulfilledAt: Date;
    totalWeight: number;
    weightUOMId: number;
    weightUOM: string;
    totalPostage: number;
    paymentMethodId: number;
    paymentMethod: string;
    currencyId: number;
    currency: string;
    directOrderId: number;
    directOrder: string;
    mailItemsCount: number;
    address: string;
    pickupNote: string;
    dropOffNote: string;
    customerId: number;
    customerName: string;
    contactId: number;
    contactName: string;
    contractId: number;
    contractName: string;
    mailItemsTypeId: number;
    mailItemsTypeName: string;
    postagePerPiece: number;
    collectionFormSerialNo: number;
    businessName :string;
    businessTypeId :number;
    businessCID:string; 
    businessCode :string;
    hqBranchId:number;
    contractStatus:string;
    pickupLocationId:number;
    pickupRequestNotificationId:number;
    departmentId:number;
    businessOrderId:number;
    businessCustomerId:number;
    mailItems:any;
    orderById:number;
    businessTypeName :string ;
    orderByName :string ;
    departmentName :string ;
    businessCustomerName :string ;
    businessCustomerCode :string; 
    hqBranchName :string ;
    contractCode :string ;
    airWayBillNumber :number;
    courierId :number;
    courierTrackingNumber :number;
    //contractCode: string;
    businessCustomerBranchId: number;
    isMyAddress:boolean;
    pickUpAddress:Address
}
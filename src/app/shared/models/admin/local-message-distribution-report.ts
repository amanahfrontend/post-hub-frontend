export interface LocalMessageDistributionReport{
      id :number;
      deliveryDate :Date;
      itemsNo :number;
      returnItemsNo :number;
      deliveredItemsNo :number;
      remainingItemsNo :number;
      workOrderCode :string;
      driverName :string;
      fromSerial :string;//رقم الكشف
      wrokOrderCode :string;// امر التوزيع
      pickupDate :Date;
      caseNo :number;
}
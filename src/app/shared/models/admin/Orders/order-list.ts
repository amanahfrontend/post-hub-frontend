import { Address } from "../../components";

export interface OrderListModel {
    id: number;
    issueAt: Date;
    deliveryBefore: Date;
    pickupDate: Date;
    deliveryDate: Date;
    orderTypeId: string;
    orderTypeName: string;
    orderCode: string;
    totalWeight: number;
    price: number;
    mailItemsCount: number;
    envelop: string;
    statusId: number;
    statusName: string;
    businessOrders_Customer: string;
    businessOrders_Business: string;
    businessOrders_Branch?: any;
    businessOrders_BusinessCode: string;
    personalOrders_Name: string;
    personalOrders_Company: string;
    personalOrders_Code: string;
    sender_BusinessOrders_ContactName: string;
    sender_BusinessOrders_ContactMobile: string;
    sender_BusinessOrders_ContactAddress: string;
    sender_BusinessOrders_ContactBranch?: any;
    sender_PersonalOrders_Name: string;
    sender_PersonalOrders_Mobile: string;
    sender_PersonalOrders_Address: Address;
    reciver_PersonalOrders_Name: string;
    reciver_PersonalOrders_Mobile: string;
    reciver_PersonalOrders_Address: Address;
}

export interface DownloadOption {
    id: number;
    name: string;
}
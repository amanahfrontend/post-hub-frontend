import { Address } from './../components/address';

export interface BusinessInfo {
    id?: number;
    businessName: string;
    businessCID: string;
    businessCode: string;
    collectionMethodId: number;
    businessTypeId: number;
    businessLogoUrl?: string;
    isCompleted?: boolean;
    hqBranchId?: number;
    mainContactId?: number;
    businessTypeName: string;
    orderByName: string;
    departmentName: string;
    businessCustomerName: string;
    businessCustomerCode: string;
    hqBranchName: string;
    contractCode: string;
    contractId: number;
    businessCustomerId: number;
    orderById: number;
    isApproved?: boolean;
    accountType: number;
}

export interface BusinessCustomerContact {
    id?: number;
    fullName: string;
    fax: string;
    phoneExt: string;
    phone: string;
    mobileNumber: string;
    accountStatusId: string;
    courtesyId: number;
    email: string;
    countryId: number;
    contactFunctionId: number;
    businessCustomerId: number;
    personalPhotoURL?: string;
    contactCode: string;
    password: string;
    username: string;
}

export interface BusinessCustomerBranch {
    id?: number;
    name: string;
    countryId: number;
    phone: string;
    mobile: string;
    email: string;
    address: string;
    latitude: number;
    longitude: number;
    location: Address;
    hqBranchId?: number;
    branchContactId: number;
    pickupContactId: number;
    closeTime: string;
    preferedPickupTimeFrom: string;
    preferedPickupTimeTo: string;
    pickupNotes: string;
    deliveryNotes: string;
    businessCustomerId: number;
    accountType: number;
}

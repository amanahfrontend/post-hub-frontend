export interface Flag {
    flagImg: string,
    name: string,
    code: string
}
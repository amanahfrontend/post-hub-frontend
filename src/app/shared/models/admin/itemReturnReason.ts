export interface ItemReturnReason {
    id:number;
    name:string;
    isActive :Boolean;
}
export interface PL {
  id: number;
  sectorTypeName: string;
  code: string;
  statusId: number;
  sectorTypeId: number;
  plItems: PLItems[]
}
export interface PLItems {
  fromArea: string;
  toArea: string;
  fromWeight: number;
  toWeight: number;
  serviceTimeUnit: string;
  serviceTime: number;
  price: number;
}

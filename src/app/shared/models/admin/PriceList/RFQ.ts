export interface RFQ{
    id :number  ;
   
    sectorTypeName :  string;
   customerName:string;
   customerId:number;
   code:string;
   letterText:string; 
   letterSubject :string;   
   PqSubject:string;
   PqDate:Date;
   PqValidToDate:Date;
   statusId:number;
   description:string;
   quantity:number;  
   price:number;
     sectorTypeId:number;  
   total:number;
   pqItems:RFQItem;
   rfqItems: RFQItem[];  
}
export interface RFQItem{
    itemDescription:string;
    itemPrice:number;
    currencyId:number;
    notes:string;
    hasAmount:boolean;
    amount:number ;
    weight: number,     
    toGoShippingPrice:number,
    commingShippingPrice:number,
    roundTripShippingPrice:number,
    countryId: number
}


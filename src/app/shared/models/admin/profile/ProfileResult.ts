import { Profile } from "./Profile";

/**
 * AdminResult interface
 *
 * 
 * @interface Admin
 */
export class ProfileResult{
    succeeded?: boolean;
    errors?: {code: string, description: string}[];
    error?: string;
    result?: Profile;
}
import { Address } from "../../components/Address";

/**
 * Admin interface
 *
 * 
 * @interface Admin
 */
export interface Profile {
  companyName: string;
  companyAddress: string;
  displayImage: number;
  id: string;
  email: string;
  phone: string;
  countryId: number;
  nationalityId: number;
  nationality: string;
  firstName: string;
  residentCountryId: number;
  dashBoardLanguage: string;
  trackingPanelLanguage: string;
  branchId: number;
  isManager: boolean;
  address: Address;
  profilePhotoURL: string;
  fullName?: string;
  mobileNumber?: string;
}

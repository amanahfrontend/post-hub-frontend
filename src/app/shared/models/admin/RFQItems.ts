export interface RFQItems {
    id: number;
    rFQId: number;
    hasAmount: boolean
    itemPrice: number;
    quantity: number;
    roundTripShippingPrice: number;
    countryId: number;
    weight: number;
    toGoShippingPrice: number;
    commingShippingPrice: number;
}
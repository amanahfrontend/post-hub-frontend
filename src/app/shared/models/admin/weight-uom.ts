export interface WeightUOM {
    id:number;
    name:string;
    isActive :Boolean;
    code:string;
    description:string
}
export interface Governorate{
     
      id :string;
      countryId :number; 
      countryName :string;
      isActive :boolean;
      nameEN:string;
      nameAR:string;
}
/**
 * GeoFence interface
 *
 * @interface GeoFence
 */

export interface GeoFence {
    id?: number;
    name: string;
    description: string;
    updatedDate: string;
    geoFenceBlocks: { area: string, block: string }[];
}

interface GeoLocation {
    longitude: string;
    latitude: string;
}

export interface polyCoordinates {
    latitude?: string;
    longitude?: string;
    pointIndex?: number;
}
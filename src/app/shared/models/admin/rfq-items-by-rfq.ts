export interface RFQItemsByRFQ {
    id: number;
    rFQId: number;
    currencyId: number;
    currencyCode: string
    hasAmount: boolean
    itemPrice: number;
    quantity: number;
    roundTripShippingPrice: number;
}
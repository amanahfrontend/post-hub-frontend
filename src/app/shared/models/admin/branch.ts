export interface Branch {
    id: number;
    name: string;
    countryId: number;
    phone: string;
    mobile: string;
    restaurantId: number;
    geoFenceId: number;
    address: string;
    latitude: number;
    longitude: number;
    isActive: boolean;
    isActivePrevious: boolean;
    reason?: any;
    customer: Customer;
    restaurant: Restaurant;
    location: Location;
    tenant_Id: string;
    isDeleted: boolean;
    createdDate: Date;
    updatedDate: Date;
    branchManagerId: string;
}

interface Country {
    id: number;
    name: string;
    code: string;
    flag: string;
    flagUrl?: any;
    topLevel: string;
}

interface Customer {
    id: number;
    name: string;
    email: string;
    phone: string;
    address: string;
    latitude: number;
    longitude: number;
    tags?: any;
    countryId: number;
    branchId: number;
    country: Country;
    location?: any;
}

interface Restaurant {
    id: number;
    name: string;
}

interface Location {
    governorate: string;
    area: string;
    block: string;
    street: string;
    building: string;
    floor?: any;
    flat?: any;
}
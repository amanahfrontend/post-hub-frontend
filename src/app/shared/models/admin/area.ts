export interface Area{
      id:number;
      nameEN :string;
      nameAR :string;
      isActive :boolean;
      isSystem :boolean;
      fK_Governrate_Id :number;
      raf :number;
      governateName:string;
      areaBlocks:any;
}
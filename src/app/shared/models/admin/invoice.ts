export interface invoice{
    id:number;
     invoiceNo:string
     invoiceStatus:string;
     contractCode :number;
     invoicePeriodFrom:Date;
     invoicePeriodTo:Date;
}
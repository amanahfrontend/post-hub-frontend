import { ExtraTerm } from "./extra-term";

export interface ContractForEdit {
    id: number;
    serviceSectorId: number;
    contractTypeId: number;
    contractDate: Date
    contractStartDate: Date
    contractEndDate: Date
    businessCustomerId: number;
    noOfMessages: number;
    isPrReciptListAndCollectEnabled: boolean
    isPrAndFixEnabledEnabled: boolean
    serviceDistriputionPeriod: number;
    itemSupplyPeriod: number;
    distriputionAllowance: number;
    paymentType: number;
    paymentNotLetterThan: number;
    paymentNotLetterThanPeriodType: number;
    installmentsAtContracting: number;
    installmentsAfterEachMessage: number;
    installmentsPaymentNotLaterThan: number;
    installmentsPaymentNotLaterThanPeriodType: number;
    contractStatusId: number;
    code: string
    currencyId: number;
    pricePerUnit: number;
    numberOfUnits: number;
    businessCustomerName: string;
    terms: ExtraTerm[];
    contractDays: number;
    mainContactName: string;
    mobile: string;
    address: string;
}
export interface Manager {
    id: number;
    userId: string;
    username: string;
    email: string;
    phoneNumber: string;
    mobileNumber: string;
    firstName: string;
    middleName?: any;
    lastName: string;
    countryId?: number;
    country: Country;
    roleNames: string[];
    teamManagers: TeamManager[];
    departmentId: number;
    departmentName: string;
    serviceSectorName: string;
    dateOfBirth?: Date;
    nationalId: string;
    nationalIdExtractDate?: Date;
    nationalIdExpiryDate?: Date;
    nationalityId: number;
    nationality: string;
    profilePicURL: string;
    supervisorId?: any;
    supervisorName: string;
    branchId: number;
    branchName: string;
    genderId: number;
    genderName: string;
    accountStatusId: number;
    accountStatusName: string;
    companyEmail: string;
    tenant_Id: string;
    createdBy_Id: string;
    updatedBy_Id?: any;
    deletedBy_Id?: any;
    isDeleted: boolean;
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
}

export interface Result {
    result: Manager[];
    totalCount: number;
}


interface Country {
    id: number;
    name: string;
    code: string;
    flag: string;
    flagUrl: string;
    topLevel: string;
}

interface TeamManager {
    id: number;
    teamId: number;
    teamName: string;
    managerId: number;
    isDeleted: boolean;
}


export interface Barcode {
    code: string;
    createdDate: string;
    driverName: string;
    recieverName: string;
    address: string;
}
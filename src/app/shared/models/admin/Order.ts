export interface OrderForDispatch {
    id: number;
    orderType: string;
    orderCode: string;
    businessName: string;
    customerName: string;
    status: string;
    itemsNo: number;
    page?: number | 0;
    items?: MailItemForDispatch[] | []
}

export interface DriverForDispatch {
    id: number;
    itemsNo: number;
    driverUserName: string;
    page?: number | 0;
    items?: MailItemForDispatch[] | []
}

export interface MailItemForDispatch {
    id: number;
    itemCode: string;
    itemType: string;
    itemWeight: string | number;
    itemDeleveryType: number;
    status: string;
    deliveryBeforeDate: string;
    orderCode: string;
    orderId: number | null;
}

export interface AreaForDispatch {
    id: number;
    raf: number | null;
    area: string;
    itemsNo: number;
    page?: number;
    blocks?: BlockForDispatch[] | [],
}

export interface BlockForDispatch {
    id: number;
    name: string;
    page?: number;
    items?: MailItemForDispatch[] | []
}
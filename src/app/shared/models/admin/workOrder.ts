export interface workOrder{
      id :number  ;
      sectorTypeName :  string;
      wOrderNo :  string;
      itemsNo : number ;
      issueAt : Date ;
      deliveryBefore : Date ;
      pickupDate : Date ;
      deliveryDate : Date ;
      assignedDate : Date ;
      statusId :number  ;
      statusName :string  ;
      assignedToName : string ;
      assignedToCode :  string;
      assignedToMobile :string  ;
      assignedToStatus: string;
    mailitems: Item[];
}

export interface Item {
    itemInfo: string;
    itemStatus: string;
    deliveryBefore: string;
    orderInfo: string;
    sender: string;
    receiver: string;
    customerInfo: string;
}

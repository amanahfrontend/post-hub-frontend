export interface AreaBlock{
      id:number;
      blockNo:string;
      isActive:boolean;
      isSystem:boolean
      areaId:number;
}
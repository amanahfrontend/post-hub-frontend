import { Address } from "../components";

    export interface ConsigneeInfo {
        id: number;
        creationTime: Date;
        createdBy: string;
        lastModificationTime: Date;
        lastModifiedBy: string;
        isDeleted: boolean;
        deletionTime: Date;
        deletedBy: string;
        firstName: string;
        midelName: string;
        lastname: string;
        addressId: number;
        address: Address;
    }

    

    export interface ReciverInfo {
        id: number;
        creationTime: Date;
        createdBy: string;
        lastModificationTime: Date;
        lastModifiedBy: string;
        isDeleted: boolean;
        deletionTime: Date;
        deletedBy: string;
        firstName: string;
        midelName: string;
        lastname: string;
        addressId: number;
        address: Address;
    }

    export interface FileType {
        id: number;
        name: string;
        isDeleted: boolean;
        isSystem: boolean;
        isActive: boolean;
    }

    export interface ProofOfDeliveryImage {
        id: number;
        creationTime: Date;
        createdBy: string;
        lastModificationTime: Date;
        lastModifiedBy: string;
        isDeleted: boolean;
        deletionTime: Date;
        deletedBy: string;
        name: string;
        path: string;
        fileType: FileType;
        note: string;
    }

    export interface SignatureImage {
        id: number;
        creationTime: Date;
        createdBy: string;
        lastModificationTime: Date;
        lastModifiedBy: string;
        isDeleted: boolean;
        deletionTime: Date;
        deletedBy: string;
        name: string;
        path: string;
        fileType: FileType;
        note: string;
    }

    export interface Status {
        id: number;
        name: string;
        isDeleted: boolean;
        isSystem: boolean;
        isActive: boolean;
        rank: number;
        description: string;
    }

    export interface MailItem {
        mailItemBarCode: string;
        deliveryBefore: Date;
        pickUpAddress: Address;
        dropOffAddress: Address;
        consigneeInfoId: number;
        consigneeInfo: ConsigneeInfo;
        reciverInfoId: number;
        reciverInfo: ReciverInfo;
        isMatchConsigneeId: boolean;
        proofOfDeliveryImage: ProofOfDeliveryImage;
        signatureImage: SignatureImage;
        weight: number;
        weightUOM: number;
        hight: number;
        width: number;
        reservedSpecial: string;
        notes: string;
        itemTypeId: number;
        statusId: number;
        status: Status;
        userId: string;
    }




export class UserInfo{
    userName: string;
    email: string;
    phoneNumber: string;
    countryId: number;
    roleNames: string[];
    userType: number;
}
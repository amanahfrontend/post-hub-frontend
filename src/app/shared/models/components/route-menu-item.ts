export interface RouteMenuItem {
    path: string;
    title: string;
    icon?: string;
    class?: string;
    permissions?: string[];
    children?: RouteMenuItem[];
}
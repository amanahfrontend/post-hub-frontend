export interface DeleteDialog {
    message: string;
    cancelBtn: string;
    okayBtn: string;
}
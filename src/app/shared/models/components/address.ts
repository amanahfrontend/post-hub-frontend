/**
 * Address interface
 *
 * 
 * @interface Address
 */
export interface Address {
  index?: number;
  opened?: boolean;
  paci?: number;
  country?: string;
  governorate?: string;
  area?: string;
  block?: string;
  street?: string;
  building?: string;
  floor?: string;
  flat?: string;
  fullAddress?: string;
  filterGovernorate?: string;
  filterArea?: string;
  filterBlock?: string;
  filterStreet?: string;
  previousGovernorate?: string;
  previousArea?: string;
  previousBlock?: string;
  previousStreet?: string;
}

/**
 * Address Street interface
 *
 * 
 * @interface AddressStreet
 */
export interface AddressStreet {
  govId?: string;
  areaId?: string;
  blockName?: string;
}

export interface Option {
  id: number | string;
  name: string;
}


export interface AddressResult {
  address?: string;
  latitude?: string,
  longtiude: string;
}
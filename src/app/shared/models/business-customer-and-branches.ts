export interface BusinessCustomerAndBranches{
    
    businessBranchName:string;
    creationDate :Date;
    itemsNo :number;
    totalRowsNo :number;
    totalItemsDeliveredForBranch :number;
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './change-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../modules/material/material.module';
import { SetDirModule } from '../../directives/set-dir/set-dir.module';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderAsCardModule } from '../header-as-card/header-as-card.module';

@NgModule({
  declarations: [
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    ChangePasswordRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    SetDirModule,
    TranslateModule,
    HeaderAsCardModule
  ]
})
export class ChangePasswordModule { }

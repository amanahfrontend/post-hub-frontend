import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors,
} from "@angular/forms";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { FacadeService } from "../../../services/facade.service";

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  type: string[] = ["password", "password", "password"];

  constructor(
    fb: FormBuilder,
    private facadeService: FacadeService,
    private readonly translateService: TranslateService,
    private toastrService: ToastrService,
    private router: Router
  ) {
    this.form = fb.group({
      currentPassword: ["", [Validators.required]],
      newPassword: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)]],
      confirmPassword: ["", Validators.compose([Validators.required, (value) => this.validateConfirmPassword(value)])],
    });
  }

  ngOnInit() {

  }

  /**
   * password to text and inverse
   *
   *
   */
  togglePassword(index: number) {
    if (this.type[index] == "password") {
      this.type[index] = "text";
    } else {
      this.type[index] = "password";
    }
  }

  submit() {
    if (!this.form.valid) {
      this.form.markAllAsTouched();
      return this.toastrService.error(this.translateService.instant('Please Fill mandatory fields'));
    }

    this.facadeService.adminFacadeService.userService.changePassword(this.form.value).subscribe((result: any) => {
      if (result) {
        this.toastrService.success(this.translateService.instant('Password has beeen changed successfully.'));

        setTimeout(() => {
          this.facadeService.accountService.logout();
        }, 2500);
      }
    });
  }

  /**
   * confirm two passwords
   *
   *
   * @param control
   */
  private validateConfirmPassword(control: AbstractControl): ValidationErrors | null {
    return !this.form || control.value === this.form.controls.newPassword.value ? null : { confirm: true };
  }
}

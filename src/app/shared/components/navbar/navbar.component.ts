import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Menu } from '../../../shared/constants/menu-routes';
import { displaySidebar } from '../../../shared/helpers/sidebar';
import { FacadeService } from '../../../services/facade.service';
import { TranslateService } from '@ngx-translate/core';
import { Flag } from '../../../shared/models/admin/flag';
import { MatSelectChange } from '@angular/material/select';
import { App } from 'app/core/app';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    providers: [
        Menu
    ]
})
export class NavbarComponent implements OnInit {
    private listTitles: any[] = [];
    location: Location;
    mobile_menu_visible: any = 0;
    ifSidebarDisplayed: boolean = false;
    language: string = 'ar';
    visible: boolean = false;
    menuVisible: boolean = false;

    selectedFlag: Flag;
    flags: Flag[] = [
        {
            flagImg: 'sa.webp',
            name: 'العربية',
            code: 'ar'
        },
        {
            flagImg: 'us.webp',
            name: 'English',
            code: 'en'
        },
    ];

    path: string = 'assets/img/faces/avtar.jpeg';
    username: string = '';

    constructor(location: Location,
        private menu: Menu,
        private facadeService: FacadeService,
        private translateService: TranslateService) {
        this.location = location;
    }

    ngOnInit() {
        this.facadeService.sidebarService.changeStatus(this.menuVisible);
        this.facadeService.authenticatedService.currentStatus.subscribe(status => {
            if (status) {
                this.username = this.facadeService.accountService.user.fullName;
            }
        });

        if (this.facadeService.accountService.isLoggedIn) {
            this.username = this.facadeService.accountService.user.fullName;

            this.facadeService.accountService.currentUserLogo.subscribe((logo: string) => {
                this.path = `${logo}`;
            });
        }

        this.listTitles = this.menu.getMenuRoutes().filter(listTitle => listTitle);
        this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
            this.language = lng;
        });

        this.facadeService.sidebarService.currentStatus.subscribe(status => {
            this.ifSidebarDisplayed = status;
        });

        this.translateService.onLangChange.subscribe(res => {
            this.listTitles = this.menu.getMenuRoutes().filter(listTitle => listTitle);
        });

        this.flags.forEach(flag => {
            if (this.language == flag.code) {
                this.selectedFlag = flag;
            }
        });
    }

    getTitle() {
        let title = this.location.prepareExternalUrl(this.location.path());
        if (title.charAt(0) === '#') {
            title = title.slice(1);
        }

        for (let item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === title) {
                return this.listTitles[item].title;
            }

            if (title === '/user-profile') {
                return this.translateService.instant('User Profile');
            }

            if (title === '/geo-fences/add') {
                return this.translateService.instant('Add Geofence');
            }

            if (title === '/geo-fences/edit') {
                return this.translateService.instant('Edit Geo Fence');
            }

            if (title === '/manage-drivers/add') {
                return this.translateService.instant('Add Driver');
            }

            if (title === '/manage-drivers/edit') {
                return this.translateService.instant('Edit Driver');
            }
        }

        return this.translateService.instant('Dashboard');
    }

    onDisplaySidebar() {
        this.facadeService.sidebarService.changeStatus(false);
        displaySidebar();
    }

    /**
     * toggle language
     * 
     * 
     * @param lng 
     */
    changeLanguage(lng: string) {
        this.facadeService.sharedFacadeService.languageService.changeLanguage(lng);
        this.language = lng;
    }

    get isMobileMenu(): boolean {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    logout() {
        this.facadeService.accountService.logout();
    }

    selectionChange(event: MatSelectChange) {
        this.selectedFlag = event.value;
        this.facadeService.sharedFacadeService.languageService.changeLanguage(event.value.code);
    }

    toggleSidebar() {
        this.menuVisible = !this.menuVisible;
        this.facadeService.sidebarService.changeStatus(this.menuVisible);
    }
}

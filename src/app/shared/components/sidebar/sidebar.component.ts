import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '../../../services/facade.service';
import { Menu } from '../../../shared/constants/menu-routes';
import { RouteMenuItem } from './../../models/components/index';
import { hideSidebar } from '../../../shared/helpers/sidebar';
import { Router } from '@angular/router';
declare let $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [
    Menu
  ]
})
export class SidebarComponent implements OnInit {
  menuItems: RouteMenuItem[] = [];
  dir: string = 'ltr';
  locale: string = 'ar';

  constructor(private menu: Menu,
    private facadeService: FacadeService,
    private translateService: TranslateService,
    private router: Router) {
    this.menuItems = this.menu.getMenuRoutes();
  }

  ngOnInit() {
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      if (lng == 'ar') {
        this.dir = 'rtl'
      } else {
        this.dir = 'ltr'
      }

      this.locale = lng;
      this.menuItems = this.menu.getMenuRoutes();
    });

    this.translateService.onLangChange.subscribe((res: { lang: string }) => {
      if (res.lang == 'ar') {
        this.dir = 'rtl'
      } else {
        this.dir = 'ltr'
      }
      this.menuItems = this.menu.getMenuRoutes();
    });

    this.isMobileMenu;

    this.selectItemThatMatchRoute();
  }

  selectItemThatMatchRoute() {
    this.menuItems.forEach((item: RouteMenuItem) => {
      if (this.router.url.includes(item.path)) {
        item.class = 'active';
      } else {
        item.class = '';
      }

      if (item.children.length > 0) {
        item.children.forEach((child: RouteMenuItem) => {
          if (this.router.url.includes(child.path)) {
            child.class = 'active';
          } else {
            item.class = '';
          }
        });
      }
    });
  }

  get isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }

    hideSidebar();
    return true;
  };

  toggleSidebar() {
    this.facadeService.sidebarService.changeStatus(true);
  }

  setActive(path: string, collapseId?: string) {
    this.menuItems.forEach((item: RouteMenuItem) => {
      if (item.path == path && (path.trim().length > 0)) {
        item.class = 'active';
      } else {
        item.class = '';
      }

      item.children.forEach((child: RouteMenuItem) => {
        if (child.path == path && (path.trim().length > 0)) {
          this.resetActivatedChildrenItem();
          child.class = 'active';

          if (collapseId) {
            $(`#${collapseId}`).addClass('show');
          }
        }
      });
    });
  }

  resetActivatedChildrenItem() {
    this.menuItems.forEach((item: RouteMenuItem) => {
      if (item.children.length > 0) {
        item.children.forEach((child: RouteMenuItem) => {
          child.class = '';
        });
      }
    });
  }
}

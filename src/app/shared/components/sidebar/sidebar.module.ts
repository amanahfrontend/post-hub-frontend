import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SetDirModule } from '../../../shared/directives/set-dir/set-dir.module';
import { PermissionsModule } from '../../../shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SetDirModule,
    PermissionsModule
  ],
  exports: [SidebarComponent]
})
export class SidebarModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressComponent } from './address.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { FilterModule } from '../../../shared/pipes/filter/filter.module';
import { CountriesModule } from '../countries/countries.module';
import { SetDirModule } from '../../../shared/directives/set-dir/set-dir.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [AddressComponent],
  imports: [
    CommonModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    TranslateModule,
    ReactiveFormsModule,
    MatIconModule,
    MatTooltipModule,
    FilterModule,
    CountriesModule,
    SetDirModule,
    NgxMatSelectSearchModule
  ],
  exports: [
    AddressComponent
  ]
})
export class AddressModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarcodeComponent } from './barcode.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../../../shared/modules/material/material.module';

@NgModule({
  declarations: [
    BarcodeComponent
  ],
  imports: [
    CommonModule,
    NgxBarcodeModule,
    TranslateModule,
    MaterialModule,
  ],
  exports: [
    BarcodeComponent
  ],
  entryComponents: [
    BarcodeComponent
  ]
})
export class BarcodeModule { }

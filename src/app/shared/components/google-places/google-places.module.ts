import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GooglePlacesComponent } from './google-places.component';
import { MaterialModule } from '../../../shared/modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    GooglePlacesComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    GooglePlacesComponent
  ]
})
export class GooglePlacesModule { }

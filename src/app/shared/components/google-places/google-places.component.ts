import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'google-places',
  templateUrl: './google-places.component.html',
  styleUrls: ['./google-places.component.scss']
})
export class GooglePlacesComponent implements OnInit {

  searchControl = new FormControl();
  places: any[];
  filteredPlaces: any;

  constructor() {

  }

  ngOnInit() {
    this.filteredPlaces = this.searchControl.valueChanges.pipe(startWith(''), map(value => this.search(value)));
  }

  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.places.filter(place => place.toLowerCase().includes(filterValue));
  }

  /**
   * google places API
   * 
   *  
   * @param keywords 
   */
  search(keywords: string) {
  }
}

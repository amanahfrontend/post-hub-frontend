import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Country } from '../../models/components/country';
import { PhoneNumberUtil } from 'google-libphonenumber';

const COUNTRIES = 'COUNTRIES';

@Component({
  selector: 'app-tel-input',
  templateUrl: './tel-input.component.html',
  styleUrls: ['./tel-input.component.scss']
})
export class TelInputComponent implements OnInit {

  public readonly imageUrl = 'assets/img/flags/';
  private static phoneNumberUtil = PhoneNumberUtil.getInstance();

  @Output() country: EventEmitter<Country> = new EventEmitter<Country>();
  @Output() number: EventEmitter<number> = new EventEmitter<number>();
  @Output() isValid: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() previousCountryId: number;
  @Input() isRequired: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() previousNumber: number;
  @Input() height: number = 50;

  states: Country[] = [];
  selectedState: Country;
  phone: number;
  msgError: string = '';

  constructor() {
  }

  ngOnInit() {
    this.countriesCodes();
    // push old value
    if (this.previousCountryId && this.previousNumber) {
      this.phone = this.previousNumber;
      this.setSelectedPrevious(this.states, this.previousCountryId);
      this.setValue();
    } else {
      const selectedCountry = this.states.filter(state => {
        return state.code == String(965);
      })[0];

      // set kuwait default country
      if (selectedCountry && selectedCountry.id) {
        this.setSelectedPrevious(this.states, selectedCountry.id);
        this.setValue();
      }
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.previousCountryId) {
      if (simpleChanges.previousCountryId.currentValue) {
        this.setSelectedPrevious(this.states, simpleChanges.previousCountryId.currentValue);
      }
    }

    if (simpleChanges && simpleChanges.previousNumber) {
      if (simpleChanges.previousNumber.currentValue) {
        this.phone = simpleChanges.previousNumber.currentValue;
      }
    }
  }

  /**
   * list countries codes
   *
   *
   */
  countriesCodes() {
    this.states = JSON.parse(localStorage.getItem(COUNTRIES));;
  }

  /**
   * emit country value
   *
   *
   * @param event
   */
  setValue() {
    this.country.emit(this.selectedState);
  }

  /**
   * set selected previous country
   *
   *
   * @param countries
   * @param id
   */
  setSelectedPrevious(countries: Country[], id: number) {
    countries.forEach(country => {
      if (country.id == id) {
        this.selectedState = country;

        if (this.phone) {
          this.onChangeNumber(this.phone);
        }
      }
    })
  }

  /**
   * set selected previous country
   * 
   * 
   * @param countries 
   * @param code 
   */
  setSelectedPreviousAccordingToCode(countries: Country[], code: string) {
    countries.forEach(country => {
      if (country.topLevel == code.toLocaleLowerCase()) {
        this.selectedState = country;
        this.country.emit(this.selectedState);
      }
    });
  }

  /**
   * number changes
   * 
   * 
   * @param number 
   */
  onChangeNumber(number: number): void {
    if (number.toString().length > 1) {
      const phoneNumber = TelInputComponent.phoneNumberUtil.parseAndKeepRawInput(number.toString(), this.selectedState.topLevel);
      const validNumber = TelInputComponent.phoneNumberUtil.isValidNumber(phoneNumber);

      this.isValid.emit(validNumber);
    }

    this.number.emit(number);
  }
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {

  @Input() lat = 29.378586;
  @Input() lng = 47.990341;

  @Input() markerLat = 29.378586;
  @Input() markerLng = 47.990341;

  ngOnInit() {

  }

}

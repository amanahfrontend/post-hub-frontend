import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapViewComponent } from './map-view.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    MapViewComponent
  ],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: ''
    }),
  ],
  exports: [
    MapViewComponent,
    CommonModule,
  ],
})
export class MapViewModule { }

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'header-as-card',
  templateUrl: './header-as-card.component.html',
  styleUrls: ['./header-as-card.component.scss']
})
export class HeaderAsCardComponent implements OnInit {

  @Input() title: string = '';
  @Input() subTitle: string = '';
  @Input() btnLabel: string = '';
  @Input() isCentered: boolean = false;

  @Output() onBtnClicked: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor() { }

  ngOnInit(): void {
  }

  clickBtn() {
    this.onBtnClicked.emit(true);
  }
}

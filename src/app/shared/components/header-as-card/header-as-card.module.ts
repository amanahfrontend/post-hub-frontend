import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderAsCardComponent } from './header-as-card.component';
import { MaterialModule } from '../../../shared/modules/material/material.module';

@NgModule({
  declarations: [
    HeaderAsCardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    HeaderAsCardComponent
  ],
})
export class HeaderAsCardModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderAsCardComponent } from './header-as-card.component';

describe('HeaderAsCardComponent', () => {
  let component: HeaderAsCardComponent;
  let fixture: ComponentFixture<HeaderAsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderAsCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderAsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

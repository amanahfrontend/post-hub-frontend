import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'input-counter',
  templateUrl: './input-counter.component.html',
  styleUrls: ['./input-counter.component.scss']
})
export class InputCounterComponent implements OnInit {

  counter: number = 0;
  @Output() onCount: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  valueChanges(event) {
    this.onCount.emit(event.target.value);
  }

  increase() {
    this.counter++;
    this.onCount.emit(this.counter);
  }

  decrease() {
    if (this.counter != 0) {
      this.counter--;
      this.onCount.emit(this.counter);
    }
  }
}

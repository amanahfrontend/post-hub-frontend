import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputCounterComponent } from './input-counter.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    InputCounterComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule
  ],
  exports: [
    InputCounterComponent
  ]
})
export class InputCounterModule { }

import { Component, Input, Inject, Output, EventEmitter, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FacadeService } from '../../../services/facade.service';

@Component({
  selector: 'app-confirm-deletion',
  templateUrl: './confirm-deletion.component.html',
  styleUrls: ['./confirm-deletion.component.scss']
})
export class ConfirmDeletionComponent {

  @Input() title: string = '';
  @Input() btnText: string = null;
  @Input() showIcon: boolean = true;

  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();
  locale: string = 'en';

  constructor(public dialog: MatDialog, private facadeService: FacadeService) {
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverview, {
      width: '50%',
      minHeight: '150px',
      height: 'auto',
      data: {
        title: this.title
      },
      panelClass: 'custom-dialog-container'
    });

    dialogRef.disableClose = true;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.confirm.emit(result);
      } else {
        this.confirm.emit(false);
      }
    });
  }

}

@Component({
  selector: 'app-dialog-overview',
  templateUrl: './dialog-overview.html',
  styleUrls: ['./confirm-deletion.component.scss']
})

export class DialogOverview implements OnInit {
  title: string = '';
  constructor(
    public dialogRef: MatDialogRef<DialogOverview>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.title = this.data.title;
  }

  /**
   * cancel dialog
   * 
   * 
   */
  cancel(): void {
    this.dialogRef.close();
  }
}

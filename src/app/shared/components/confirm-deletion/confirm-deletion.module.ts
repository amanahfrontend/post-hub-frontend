import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeletionComponent } from './confirm-deletion.component';
import { DialogOverview } from './confirm-deletion.component';

// material
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { SetDirModule } from '../../../shared/directives/set-dir/set-dir.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    ConfirmDeletionComponent,
    DialogOverview
  ],
  imports: [
    CommonModule,

    // material
    MatButtonModule,
    TranslateModule,
    MatDividerModule,
    MatDialogModule,
    SetDirModule,
    MatIconModule
  ],
  exports: [
    ConfirmDeletionComponent,
  ],
  entryComponents: [
    DialogOverview
  ]
})
export class ConfirmDeletionModule { }

import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { CountryService } from '../../../services/shared/country.service';
import { Country } from '../../models/components/country';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {

  public readonly imageUrl = 'assets/images/flags/';

  @Output() country: EventEmitter<Country> = new EventEmitter<Country>();
  @Input() previousCountryId: number;
  @Input() disabled: boolean = false;

  states: Country[] = [];
  selectedState: Country;
  msgError: string = '';

  /**
   *
   * @param countryService
   */
  constructor(private countryService: CountryService) {
  }

  ngOnInit() {
    this.countriesCodes();

    // push old value
    if (this.previousCountryId) {
      this.setSelectedPrevious(this.states, this.previousCountryId);
      this.setValue();
    } else {
      const selectedCountry = this.states.filter(state => {
        return state.code == String(965);
      })[0];

      this.country.emit(selectedCountry);

      // set kuwait default country
      if (selectedCountry && selectedCountry.id) {
        this.setSelectedPrevious(this.states, selectedCountry.id);
        this.setValue();
      }
    }
  }

  /**
   * 
   * @param simpleChanges 
   */
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges && simpleChanges.previousCountryId) {
      // push old value
      if (simpleChanges.previousCountryId.currentValue) {
        this.setSelectedPrevious(this.states, simpleChanges.previousCountryId.currentValue);
      }
    }
  }

  /**
   * list countries codes
   *
   *
   */
  countriesCodes() {
    this.countryService.list().subscribe((countries: Country[]) => {
      this.states = countries;
    });
  }

  /**
   * emit country value
   *
   *
   * @param event
   */
  setValue() {
    this.country.emit(this.selectedState);
  }

  /**
   * set selected previous country
   *
   *
   * @param countries
   * @param id
   */
  setSelectedPrevious(countries: Country[], id: number) {
    countries.forEach(country => {
      if (country.id == id) {
        this.selectedState = country;
      }
    })
  }

  /**
   * set selected previous country
   * 
   * 
   * @param countries 
   * @param code 
   */
  setSelectedPreviousAccordingToCode(countries: Country[], code: string) {
    countries.forEach(country => {
      if (country.topLevel == code.toLocaleLowerCase()) {
        this.selectedState = country;
        this.country.emit(this.selectedState);
      }
    })
  }


}

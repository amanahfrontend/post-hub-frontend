export class AuthConstants {
    static readonly UserKey: string = 'USER';
    static readonly UserInfoKey: string = 'USERINFO';
    static readonly TokenKey: string = 'TOKEN';
}
 import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { RouteMenuItem } from './../models/components/route-menu-item';

@Injectable()
export class Menu {

    constructor(private translateService: TranslateService) {
    }

    getMenuRoutes(): RouteMenuItem[] {
        return [
            {
                path: '/dashboard',
                title: this.translateService.instant('Dashboard'),
                icon: 'dashboard',
                children: [],
                class: 'active',
                permissions: ["admin"],
            },

            {
                path: '',
                title: this.translateService.instant('Operations'),
                permissions: ["CreateBusinessOrders", "Orders_ViewOrders", "Orders_DispatchOrdersPage" ],
                icon: 'library_books',
                children: [
                    {
                        path: '/create-business-order',
                        title: this.translateService.instant('Create Business Order'),
                        icon: 'fact_check',
                        children: [],
                        permissions: ["CreateBusinessOrders"],
                    },
                    {
                        path: '/manageOrder',
                        title: this.translateService.instant('Manage Order'),
                        icon: 'analytics',
                        children: [],
                        permissions: ["Orders_ViewOrders"],
                    },
                    {
                        path: '/dispatch-orders',
                        title: this.translateService.instant('Dispatch Orders'),
                        icon: 'fact_check',
                        children: [],
                        permissions: ["Orders_DispatchOrdersPage"],
                    },
                    {
                        path: '/manage-workorders',
                        title: this.translateService.instant('Manage Workorders'),
                        icon: 'bubble_chart',
                        children: [],
                        permissions: ["WorkOrders_ViewOrders"],
                    },
                    {
                        path: '/item-tracking',
                        title: this.translateService.instant('Mail Item Tracking'),
                        icon: 'search',
                        permissions: ["MailItemTracking"],
                        children: []
                    },
                    {
                        path: '/order-tracking',
                        title: this.translateService.instant('Order Tracking'),
                        icon: 'search',
                        permissions: ["OrderTracking"],
                        children: []
                    },
                ]
            },

            {
                path: '/contracts',
                title: this.translateService.instant('Contracts'),
                icon: 'bookmark_added',
                children: [],
                permissions: ["ReadContract"],
            },
            // {
            //     path: '/bulk-uploading',
            //     title: this.translateService.instant('Bulk Upload'),
            //     icon: 'content_paste',
            //     children: [],
            //     permissions: [],
            // },
            // {
            //     path: '/bulkOrder',
            //     title: this.translateService.instant('Bulk Request'),
            //     icon: 'library_books',
            //     children: [],
            //     permissions: [],
            // },
            // {
            //     path: '/create-order',
            //     title: this.translateService.instant('Create Order'),
            //     icon: 'bubble_chart',
            //     children: [],
            //     permissions: [],
            // },


            // {
            //     path: '/distributor',
            //     title: this.translateService.instant('Distribution Order'),
            //     icon: 'library_books',
            //     children: []
            // },
            // {
            //     path: '/customer',
            //     title: this.translateService.instant('Manage Customer'),
            //     icon: 'location_on',
            //     children: [],
            //     permissions: [],
            // },
            // {
            //     path: '/assignDriver/:0',
            //     title: this.translateService.instant('Assign Driver'),
            //     icon: 'person',
            //     children: [],
            //     permissions: [],
            // },
            // {
            //     path: '/zones',
            //     title: this.translateService.instant('Zones'),
            //     icon: 'crop_landscape',
            //     permissions: [],
            //     children: []
            // },
            // {


            //     path: '/price-list',
            //     title: this.translateService.instant('Price List'),
            //     icon: 'money',
            //     permissions: [],
            //     children: []
            // },
            
            
            {
                path: '/manage-pq',
                title: this.translateService.instant('Manage PQ'),
                icon: 'library_books',
                children: [],
                permissions: ["ReadPriceQoutation"],
            },
            {
                path: '/manage-pl',
                title: this.translateService.instant('Manage Price List'),
                icon: 'library_books',
                children: [],
                permissions: ["ReadPriceList"],
            },
            {
                path: '/manage-invoice',
                title: this.translateService.instant('Invoices'),
                children: [],
                permissions: ["admin","GenerateInvoice","ListInvoices","ViewInvoice","EditInvoices","PrintInvoice","DeleteInvoices"],
                icon: 'notifications'
            },
            {
                path: '',
                title: this.translateService.instant('Reports'),
                permissions: ["LocalMessageDistributionReport",
                                "BusinessCustomerAndBranchesReport",
                                "BusinessCustomerDistributionReport",
                                "DeliveredMailitemsReport",
                                "ReturnedMailitemReport"
                            ],
                icon: 'library_books',
                children: [
                    {
                        path: '/reports/local-message-distribution-report',
                        title: this.translateService.instant('Local Distribution Report'),
                        permissions: ["LocalMessageDistributionReport"],
                        icon: 'library_books'
                    },
                    {
                        path: '/reports/business-customer-and-branches-report',
                        title: this.translateService.instant('Business Customer AndBranches Report'),
                        permissions: ["BusinessCustomerAndBranchesReport"],
                        icon: 'library_books'
                    },
                    {
                        path: '/reports/business-customer-distribution-report',
                        title: this.translateService.instant('business Customer Distribution Report'),
                        permissions: ["BusinessCustomerDistributionReport"],
                        icon: 'library_books'
                    },

                    {
                        path: '/reports/delivered-mailitems-report',
                        title: this.translateService.instant('Delivered MailItem Statement'),
                        permissions: ["DeliveredMailitemsReport"],
                        icon: 'library_books'
                    },
                    {
                        path: '/reports/returned-mailitem-report',
                        title: this.translateService.instant('Returned MailItem Statement'),
                        permissions: ["ReturnedMailitemReport"],
                        icon: 'library_books'
                    }
                    
                    
                ]
            },
            {
                path: '',
                title: this.translateService.instant('Resources'),
                permissions: ["admin"],
                icon: 'library_books',
                children: [
                    {
                        path: '/company',
                        title: this.translateService.instant('Company Profile'),
                        icon: 'person',
                        children: [],
                        permissions: ["UpdateCompanyProfile"],
                    },
                    {
                        path: '/manage-staff',
                        title: this.translateService.instant('Manage Staff'),
                        icon: 'person',
                        children: [],
                        permissions: ['ReadAllManagers'],
                    },
                    {
                        path: '/manage-drivers',
                        title: this.translateService.instant('Manage Drivers'),
                        icon: 'person',
                        children: [],
                        permissions: ['ReadAgent','CreateDriver','UpdateDriver','DeleteDriver','ListDrivers','ExportDriversToExcel'],
                    },
                    {
                        path: '/business-accounts',
                        title: this.translateService.instant('Business Customers'),
                        icon: 'person',
                        permissions: ["ReadBusinessCustomer"],
                        children: []
                    },
                ]
            },
            {
                path: '',
                title: this.translateService.instant('Settings'),
                permissions: ["admin"],
                icon: 'library_books',
                children: [
                    {
                        path: '/geo-fences',
                        title: this.translateService.instant('Geo Fences'),
                        icon: 'person',
                        permissions: ['ReadGeofence'],
                        children: [],
                    },
                    {
                        path: '/access-control',
                        title: this.translateService.instant('Access Control'),
                        icon: 'verified',
                        permissions: ['ReadAllRoles'],
                        children: [],
                    },
                    {
                        path: '/settings/manage-languages',
                        title: this.translateService.instant('Manage Languages'),
                        permissions: ["CreateLanguage","UpdateLanguage","DeleteLanguage","ListLanguages"],
                        icon: 'language'
                    },
                    {
                        path: '/settings/item-return-reason',
                        title: this.translateService.instant('Item Return Reason'),
                        permissions: ["CreateItemReturnReason","UpdateItemReturnReason","DeleteItemReturnReason","ListItemReturnReasons"],
                        icon: 'swap_horizontal_circle'
                    },
                    {
                        path: '/settings/mail-item-type',
                        title: this.translateService.instant('Mail Item Type'),
                        permissions: ["CreateItemType","UpdateItemType","DeleteItemType","ListItemTypes"],
                        icon: 'markunread_mailbox'
                    },
                    {
                        path: '/settings/weight-uom',
                        title: this.translateService.instant('Weight-UOM'),
                        permissions: ["CreateWeightUOM","UpdateWeightUOM","DeleteWeightUOM","ListWeightUOMs"],
                        icon: 'line_weight'
                    },
                    {
                        path: '/settings/length-uom',
                        title: this.translateService.instant('LengthUOM'),
                        permissions: ["CreateLengthUOM","UpdateLengthUOM","DeleteLengthUOM","ListLengthUOMs"],
                        icon: 'swap_horizontal_circle'
                    },
                    {
                        path: '/settings/governorate',
                        title: this.translateService.instant('Governorate'),
                        permissions: ["CreateGovernorate","UpdateGovernorate","DeleteGovernorate","ListGovernorates"],
                        icon: 'language'
                    },
                    {
                        path: '/settings/country',
                        title: this.translateService.instant('Country'),
                        permissions: ["CreateCountry","UpdateCountry","DeleteCountry","ListCountries"],
                        icon: 'language'
                    }
                    ,
                    {
                        path: '/settings/area',
                        title: this.translateService.instant('Area'),
                        permissions: ["CreateArea", "DeleteArea", "UpdateArea", "ListAreas"],
                        icon: 'library_books'
                    },
                    // {
                    //     path: '/RFQ',
                    //     title: this.translateService.instant('RFQ'),
                    //     permissions: [],
                    //     icon: 'notifications'
                    // },
                    // {
                    //     path: '/contracts',
                    //     title: this.translateService.instant('Contracts'),
                    //     permissions: [],
                    //     icon: 'notifications'
                    // },
                    // {
                    //     path: '/invoices',
                    //     title: this.translateService.instant('Invoices'),
                    //     permissions: [],
                    //     icon: 'notifications'
                    // },
                    // {
                    //     path: '/reports',
                    //     title: this.translateService.instant('Reports'),
                    //     permissions: [],
                    //     icon: 'library_books'
                    // },
                    // {
                    //     path: '/master',
                    //     title: this.translateService.instant('Master Table'),
                    //     permissions: [],
                    //     icon: 'dashboard'
                    // },
                    // {
                    //     path: '/manage-order',
                    //     title: this.translateService.instant('Maps'),
                    //     permissions: [],
                    //     icon: 'library_books'
                    // }
                   
                   
                ]
            },
            
            // {
            //     path: '/manage-pq',
            //     title: this.translateService.instant('Manage PQ'),
            //     icon: 'library_books',
            //     children: [],
            //     permissions: ["ReadPriceQoutation"]
            // },
            // {
            //     path: '/manage-pl',
            //     title: this.translateService.instant('Manage Price List'),
            //     icon: 'library_books',
            //     children: [],
            //     permissions: ["ReadPriceList"]    
            // },
            // {
            //     path: '/manage-priceList',
            //     title: this.translateService.instant('Manage PQ'),
            //     icon: 'library_books',
            //     children: [],
            //     permissions: [],
            // },           
            
            // {
            //     path: '/services',
            //     title: this.translateService.instant('Services'),
            //     icon: 'miscellaneous_services',
            //     permissions: [],
            //     children: []
            // },
            // {
            //     path: '/trackOrder/:0',
            //     title: this.translateService.instant('Track Order'),
            //     icon: 'timeline',
            //     permissions: [],
            //     children: []
            // },
            // {
            //     path: '/manageRFQ',
            //     title: this.translateService.instant('Manage RFQ'),
            //     icon: 'library_books',
            //     permissions: [],
            //     children: []
            // },
        ];
    }
}

    // trackOrder
    // manageRFQ
    // manageOrder
    // { path: '/RFQ', title: 'RFQ', icon: 'notifications', class: '' },
    // { path: '/contracts', title: 'Contracts', icon: 'notifications', class: '' },
    // { path: '/invoices', title: 'Invoices', icon: 'notifications', class: '' },
    // { path: '/reports', title: 'Reports', icon: 'library_books', class: '' },
    // { path: '/master', title: 'Master Table', icon: 'dashboard', class: '' },
    // { path: 'login', component: LoginComponent },
    // { path: 'bulkUploading', component: BulkUploadingComponent },
    // { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
    // { path: 'createOrder', component: CreateOrderComponent },

    // children
    // { path: 'login', component: LoginComponent },
    // { path: 'bulkUploading', component: BulkUploadingComponent },
    // { path: 'bulkOrder', component: BulkOrderPickUpRequestsComponent },
    // { path: 'createOrder', component: CreateOrderComponent },
    // { path: 'login', component: LoginComponent },
    // { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    // { path: '/company', title: 'Company Profile', icon: 'person', class: '' },
    // { path: '/ManageDriver', title: 'Manage Drivers ', icon: 'person', class: '' },
    // { path: '/bulkUploading', title: 'Bulk Upload', icon: 'content_paste', class: '' },
    // { path: '/bulkOrder', title: 'Bulk Request', icon: 'library_books', class: '' },
    // { path: '/createOrder', title: 'Create Order', icon: 'bubble_chart', class: '' },
    // { path: '/distributor', title: 'Distribution Order', icon: 'library_books', class: '' },
    // { path: '/manage-order', title: 'Maps', icon: 'library_books', class: '' },
    // { path: '/customer', title: 'Manage Customer', icon: 'location_on', class: '' },
    // { path: '/assignDriver', title: 'Assign Driver', icon: 'person', class: '' },
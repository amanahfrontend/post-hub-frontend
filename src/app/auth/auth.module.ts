import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/modules/material/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { SetDirModule } from '@app/shared/directives/set-dir/set-dir.module';
import { BusinessAccountsModule } from '@app/admin-module/business-accounts/business-accounts.module';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegisterNewBusinessAccountComponent } from './register-new-business-account/register-new-business-account.component';
import { ManageIndividualAccountComponent } from './manage-individual-account/manage-individual-account.component';
import { HeaderAsCardModule } from '@app/shared/components/header-as-card/header-as-card.module';
import { AddressModule } from '@app/shared/components/address/address.module';
import { TelInputModule } from '@app/shared/components/tel-input/tel-input.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FilterModule } from '@app/shared/pipes/filter/filter.module';
import { ConfirmDeletionModule } from '@app/shared/components/confirm-deletion/confirm-deletion.module';
import { PermissionsModule } from '@app/shared/modules/permissions/permissions.module';

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    RegisterNewBusinessAccountComponent,
    ManageIndividualAccountComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    MaterialModule,
    TranslateModule,
    SetDirModule,
    BusinessAccountsModule,
    HeaderAsCardModule,
    AddressModule,
    TelInputModule,

   
    FormsModule,
  

    // shared
  
    NgxMatSelectSearchModule,
 FilterModule,
  
    HeaderAsCardModule,
    TelInputModule,
    ConfirmDeletionModule,
    SetDirModule,
    PermissionsModule
  ]
})
export class AuthModule { }

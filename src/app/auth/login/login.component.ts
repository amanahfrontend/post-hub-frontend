
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Flag } from '../../shared/models/admin/flag';
import { FacadeService } from '../../services/facade.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { AdminFacadeService } from '../../services/admin/admin-facade.service';
import { AuthConstants } from '../../shared/constants/auth';
import { App } from '@app/core/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  errors: string[] = [];
  color: ThemePalette = 'accent';
  type: string = 'password';

  selectedFlag: Flag;
  flags: Flag[] = [
    {
      flagImg: 'sa.webp',
      name: 'العربية',
      code: 'ar'
    },
    {
      flagImg: 'us.webp',
      name: 'English',
      code: 'en'
    },
  ];

  loading: boolean = false;

  constructor(fb: FormBuilder,
    private translateService: TranslateService,
    private facadeService: FacadeService,
    private adminFacadeService: AdminFacadeService,
    private router: Router,
    private permissionsService: NgxPermissionsService) {
    this.form = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      flag: ['']
    });
  }

  ngOnInit() {
    if (this.facadeService.accountService.isLoggedIn) {
      this.router.navigate(['/']);
    }

    this.selectedFlag = this.flags[1];
    this.form.get('flag').patchValue(this.selectedFlag);
  }

  /**
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'email':
        if (this.form.get('email').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;

      case 'password':
        if (this.form.get('password').hasError('required')) {
          return this.translateService.instant(`Password required`);
        }
        break;

      default:
        return '';
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.facadeService.accountService.login(this.form.value).subscribe(res => {
      this.loading = false;

      this.adminFacadeService.profileService.GetCurrentAdmin().subscribe(adminRes => {

        const userLogo: string = `${App.backEndUrl}/${adminRes.profilePhotoURL}`;
        this.facadeService.accountService.changeUserLogo(userLogo);
        this.setUserInfo(adminRes);
      });

      const userPermissions: string[] = this.facadeService.accountService.user.permissions;
      let permissionsToLoad: string[] = userPermissions.map(permission => {
        return permission.split(".").pop();
      });

      if (res.user.userType != 3) {
        permissionsToLoad.push("admin");
        this.permissionsService.loadPermissions(permissionsToLoad);
      } else {
        this.permissionsService.loadPermissions(permissionsToLoad);
      }

      this.facadeService.authenticatedService.changeStatus(true);
      this.router.navigate(['/']);
    }, err => {
      this.loading = false;
    });
  }


  private setUserInfo(res: any): boolean {
    if (res) {
      localStorage.setItem(AuthConstants.UserInfoKey, JSON.stringify(res));
      return true;
    }
    return false;
  }


  /**
   *@todo password to text and inverse
   *
   */
  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  selectionChange(event: MatSelectChange) {
    this.selectedFlag = event.value;
    this.facadeService.sharedFacadeService.languageService.changeLanguage(event.value.code);
  }

  register() {
    this.router.navigate(['auth/register']);
  }
}

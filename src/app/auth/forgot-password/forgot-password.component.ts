import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ThemePalette } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { FacadeService } from '../../services/facade.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  readonly login = 'auth/login';

  form: FormGroup;
  errors: string[] = [];
  progress: boolean = false;
  color: ThemePalette = 'accent';


  /**
   *
   * @param fb
   * @param facadeService
   * @param router
   */
  constructor(fb: FormBuilder,
    private facadeService: FacadeService,
    private router: Router,
    private translateService: TranslateService,
    private toastrService: ToastrService) {

    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit() {
    if (this.facadeService.accountService.isLoggedIn) {
      this.router.navigate(['app']);
    }
  }

  /**
   * validate form inputs
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'email':
        if (this.form.get('email').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }

        if (this.form.get('email').hasError('email')) {
          return this.translateService.instant(`Email not valid`);
        }
        break;

      default:
        return '';
    }
  }

  /**
   * sign in
   *
   *
   * @param value
   */
  onSubmit() {
    if (!this.form.valid) {
      this.form.markAllAsTouched();
      return;
    };

    this.facadeService.accountService.forgotPassword(this.form.value).subscribe(res => {
      if (res.succeeded === true) {
        this.form.reset();
        this.toastrService.success('Please check your email');

        setTimeout(() => {
          this.router.navigate([this.login]);
        }, 2500);
      } else {
        this.errors = res.errors.map(e => e.description);
      }
    });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CustomerRegistrationComponent } from './customer-registration/customer-registration.component';
import { RegisterNewBusinessAccountComponent } from './register-new-business-account/register-new-business-account.component';
import { ManageIndividualAccountComponent } from './manage-individual-account/manage-individual-account.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: CustomerRegistrationComponent
  },
  {
    path: 'register-business-account',
    component: RegisterNewBusinessAccountComponent
  },
  {
    path: 'register-business-account',
    component: RegisterNewBusinessAccountComponent
  },
  {
    path: 'register-individual-account/:businessCustomerId',
    component: ManageIndividualAccountComponent
  },
  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent,
  },
  {
    path: 'reset-password/:token/:email',
    component: ResetPasswordComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }

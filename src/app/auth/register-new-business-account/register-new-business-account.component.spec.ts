import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterNewBusinessAccountComponent } from './register-new-business-account.component';

describe('RegisterNewBusinessAccountComponent', () => {
  let component: RegisterNewBusinessAccountComponent;
  let fixture: ComponentFixture<RegisterNewBusinessAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterNewBusinessAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterNewBusinessAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

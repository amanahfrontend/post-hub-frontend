import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Flag } from '../../shared/models/admin/flag';
import { MatSelectChange } from '@angular/material/select';
import { FacadeService } from '@app/services/facade.service';

@Component({
  selector: 'customer-registration',
  templateUrl: './customer-registration.component.html',
  styleUrls: ['./customer-registration.component.scss']
})
export class CustomerRegistrationComponent implements OnInit {
  language: string = 'ar';
  selectedFlag: Flag;
    flags: Flag[] = [
        {
            flagImg: 'sa.webp',
            name: 'العربية',
            code: 'ar'
        },
        {
            flagImg: 'us.webp',
            name: 'English',
            code: 'en'
        },
    ];
  flag: Flag;
  constructor(private router: Router,
    private facadeService: FacadeService,
    ) {
    //  
    //   const lng: string = this.facadeService.sharedFacadeService.languageService.currentLanguage || 'ar';
    // this.facadeService.sharedFacadeService.languageService.changeLanguage(lng);

     }

  ngOnInit(): void {
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.language = lng;
  });
    this.flags.forEach(flag => {
      if (this.language == flag.code) {
          this.selectedFlag = flag;
      }
  });
    
  }

  navigate(path: string): void {
    this.router.navigate([path]);
  }
  selectionChange(event: MatSelectChange) {
    this.selectedFlag = event.value;
    this.facadeService.sharedFacadeService.languageService.changeLanguage(event.value.code);
}

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageIndividualAccountComponent } from './manage-individual-account.component';

describe('ManageIndividualAccountComponent', () => {
  let component: ManageIndividualAccountComponent;
  let fixture: ComponentFixture<ManageIndividualAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageIndividualAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageIndividualAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

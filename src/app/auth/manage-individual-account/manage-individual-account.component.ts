﻿import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { FacadeService } from '@app/services/facade.service';
import { BusinessCustomerBranch, BusinessCustomerContact, BusinessInfo } from '@app/shared/models/admin/business-customer';

import { Address, Country } from '@app/shared/models/components';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Flag } from '../../shared/models/admin/flag';
import { MatSelectChange } from '@angular/material/select';
const ACCOUNT_TYPE = 2;
const ACCOUNT_STATUS = 2;

@Component({
  selector: 'manage-individual-account',
  templateUrl: './manage-individual-account.component.html',
  styleUrls: ['./manage-individual-account.component.scss']
})
export class ManageIndividualAccountComponent implements OnInit {
  selectedFlag: Flag;
  flags: Flag[] = [
    {
      flagImg: 'sa.webp',
      name: 'العربية',
      code: 'ar'
    },
    {
      flagImg: 'us.webp',
      name: 'English',
      code: 'en'
    },
  ];
  language: string = 'ar';
  flag: Flag;
  type: string[] = ['password', 'password'];
  form: FormGroup;
  submitted: boolean = false;
  selectedFile: File;
  accept = 'image/*';
  ousideRoute: boolean = false;
  selectedMobileNumber: string;
  selectedCountryCode: Country;
  validPhoneNumber: boolean = false;
  address: Address;
  subscriptions = new Subscription();
  path: (string | ArrayBuffer) = "./assets/img/faces/avtar.jpeg";
  businessCustomer: BusinessInfo;
  addressValid: boolean;

  /**
   * 
   * @param fb 
   * @param router 
   * @param toastrService 
   * @param translateService 
   * @param facadeService 
   */
  constructor(fb: FormBuilder,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private facadeService: FacadeService) {
    this.form = fb.group({
      //email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.email]],
      company: ['', [Validators.required]],
      businessCID: ['', [Validators.required]],
      businessName: ['', [Validators.required]],
      customerCode: ['', [Validators.required]],
      password: ['', [Validators.required,
      Validators.required,
      Validators.minLength(6),
      Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)]],
      confirmPassword: ['', [Validators.required, (value) => this.validateConfirmPassword(value)]],
    });

    if (this.router.url.includes('register-individual-account')) {
      this.ousideRoute = true;
      this.form.removeControl('customerCode');
    }
  }

  ngOnInit(): void {
    this.facadeService.sharedFacadeService.languageService.language.subscribe(lng => {
      this.language = lng;
    });
    this.flags.forEach(flag => {
      if (this.language == flag.code) {
        this.selectedFlag = flag;
      }
    });
  }

  /**
   * password to text and inverse
   *
   *
   */
  togglePassword(index: number) {
    if (this.type[index] == "password") {
      this.type[index] = "text";
    } else {
      this.type[index] = "password";
    }
  }

  private validateConfirmPassword(control: AbstractControl): ValidationErrors | null {
    return !this.form || control.value === this.form.controls.password.value ? null : { confirm: true };
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];

      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.path = event.target.result;
      }
    }
  }

  submit(): void {

    this.submitted = true;

    if (this.form.invalid) {
      this.submitted = false;
      this.form.markAllAsTouched();
      this.toastrService.error(this.translateService.instant(`Please fill the mandatory fields`));
      return;
    }

    if (!this.validPhoneNumber) {
      this.toastrService.error(this.translateService.instant('Enter valid phone number'));
      return;
    }

    if (this.form.get('password').invalid) {
      this.toastrService.error(this.translateService.instant('Password must be at least 8 characters long and must contain one lower case, one uppercase, one numeric and one special character'));
      return;
    }

    const businessBody = {
      businessLogoFile: this.selectedFile,
      accountType: ACCOUNT_TYPE,
      businessName: this.form.get('businessName').value,
      businessCID: this.form.get('businessCID').value,
      isCompleted: true,
      IsApproved: false,
    };

    this.facadeService.adminFacadeService.businessCustomerService.create(businessBody).subscribe(businessInfo => {
      if (businessInfo.id) {
        let contactBody = {
          userName: this.form.get('username').value,
          accountStatusId: ACCOUNT_STATUS,
          fullName: this.form.get('businessName').value,
          email: this.form.get('username').value,
          password: this.form.get('password').value,
          mainContactId: true,
          businessCustomerId: businessInfo.id,
          personalPhotoFile: this.selectedFile,
          mobileNumber: this.selectedMobileNumber,
          countryId: this.selectedCountryCode.id,
          roleName: 'individualCustomer',
          accountType: ACCOUNT_TYPE,
          IsMainContact: true
        };

        this.facadeService.adminFacadeService.businessCustomerContactService.create(contactBody).subscribe((branch) => {
          let branchBody = {
            name: this.form.get('businessName').value,
            email: this.form.get('username').value,
            hqBranchId: true,
            businessCustomerId: businessInfo.id,
            mobileNumber: this.selectedMobileNumber,
            countryId: this.selectedCountryCode.id,
            branchContactId: branch.id,
            pickupContactId: branch.id,
            location: this.address,
            IshqBranch: true
          };

          this.facadeService.adminFacadeService.businessCustomerBranchService.create(branchBody).subscribe(res => {
            //this.save();
            this.toastrService.success(this.translateService.instant(`New Individual Customer has been created`));
            this.router.navigate(['auth/login']);
          });
        });
      }
    });
  }

  getError(input: string) {
    switch (input) {
      case 'businessCID':
        if (this.form.get('businessCID').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;
      case 'company':
        if (this.form.get('company').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;
      case 'businessName':
        if (this.form.get('businessName').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;
      case 'username':
        if (this.form.get('username').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;

      case 'password':
        if (this.form.get('password').hasError('required')) {
          return this.translateService.instant(`Password required`);
        }
        break;

      default:
        return '';
    }
  }

  /**
   * on change number value
   * 
   * 
   * @param number 
   */
  number(number: string): void {
    this.selectedMobileNumber = number;
  }

  onCountry(country: Country): void {
    this.selectedCountryCode = country;
  }

  isValidPhoneNumber(valid: boolean) {
    this.validPhoneNumber = valid;
  }

  onAddressChanges(address: Address): void {
    this.address = address;
  }

  save() {
    const body = { ... this.businessCustomer, ...{ isCompleted: true } };

    this.facadeService.adminFacadeService.businessCustomerService.update(body).subscribe(res => {
      this.toastrService.success(this.translateService.instant(`New Individual Customer has been created`));
      this.router.navigate(['auth/login']);
    });
  }

  selectionChange(event: MatSelectChange) {
    this.selectedFlag = event.value;
    this.facadeService.sharedFacadeService.languageService.changeLanguage(event.value.code);
  }
}

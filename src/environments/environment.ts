import { Settings, EnvironmentType } from './../app/core/settings';

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
    //identityServiceUrl: 'https://deliveropedia.com/',
    //identityServiceUrl: 'http://37.34.189.202:8050/Development/Posthub.IdentityService',
    //backendUrl: 'http://37.34.189.202:8050/Development/Posthub.DeliveryService',
    //orderServiceUrl: 'http://37.34.189.202:8050/Development/Posthub.OrderService',
    // identityServiceUrl: 'http://37.34.189.202:8050/test/Posthub.IdentityService',
    // backendUrl: 'http://37.34.189.202:8050/test/Posthub.DeliveryService',
    // orderServiceUrl: 'http://37.34.189.202:8050/test/Posthub.OrderService',
    backendUrl: 'http://localhost:5000',
    identityServiceUrl: 'https://localhost:44365',
    orderServiceUrl: 'https://localhost:44395',
    environmentType: EnvironmentType.Development
};

export const environment = {
    production: false,
    settings: settings,
};

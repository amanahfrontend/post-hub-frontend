import { Settings, EnvironmentType } from './../app/core/settings';

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
  //identityServiceUrl: 'https://deliveropedia.com/',
  identityServiceUrl: 'http://37.34.189.202:8050/Development/Posthub.IdentityService',
  backendUrl: 'http://37.34.189.202:8050/Development/Posthub.DeliveryService',
  orderServiceUrl: 'http://37.34.189.202:8050/Development/Posthub.OrderService',
  // identityServiceUrl: 'http://37.34.189.202:8050/test/Posthub.IdentityService',
  // backendUrl: 'http://37.34.189.202:8050/test/Posthub.DeliveryService',
  // orderServiceUrl: 'http://37.34.189.202:8050/test/Posthub.OrderService',
  environmentType: EnvironmentType.Production
};

export const environment = {
  production: true,
  settings: settings,
};
